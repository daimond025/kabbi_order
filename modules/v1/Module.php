<?php

namespace app\modules\v1;

use app\modules\v1\models\workers\TimeHelper;
use app\modules\v1\models\workers\WorkerRepository;
use app\modules\v1\models\workers\WorkerTariffService;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setupDependencies();
    }

    /**
     * Setup dependencies
     */
    private function setupDependencies()
    {
        \Yii::$container->setSingleton('workerService', WorkerRepository::className());
        \Yii::$container->setSingleton('workerTariffService', WorkerTariffService::className());
        \Yii::$container->setSingleton('timeHelper', TimeHelper::className());
    }
}
