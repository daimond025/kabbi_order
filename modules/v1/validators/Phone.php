<?php

namespace app\modules\v1\validators;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;

class Phone extends \miserenkov\validators\PhoneValidator{
    /**
     * @var bool
     */
    private $_successValidation = false;

    public $format = false;

    public function validateAttribute($model, $attribute)
    {
        $countries = [];
        if ($this->country !== null) {
            $countries = [$this->country];
        } elseif ($this->countryAttribute !== null) {
            $countries = [$model->{$this->countryAttribute}];
        } elseif (is_array($this->countries) && count($this->countries) > 0) {
            $countries = $this->countries;
        }

        if (count($countries) <= 0) {
            $countries = self::getPhoneUtil()->getSupportedRegions();
        }

        foreach ($countries as $country) {
            try {
                $number = self::getPhoneUtil()->parse($model->$attribute, $country);

                if (self::getPhoneUtil()->isValidNumberForRegion($number, $country)) {

                    if ($this->format) {
                        $model->$attribute = self::getPhoneUtil()->format($number, PhoneNumberFormat::E164);
                    }
                    $this->_successValidation = true;

                    break;
                }
            } catch (NumberParseException $e) {
                $this->addError($model, $attribute, $this->numberParseExceptionMessage);
            }
        }




        if (!$this->_successValidation) {
            $this->addError($model, $attribute, $this->notValidPhoneNumberMessage);
        }

        return $this->_successValidation;
    }

    public function correctPhone($telephone){
        $telephone = str_replace("+", '', $telephone);
        $telephone = str_replace("(", '', $telephone);
        $telephone = str_replace(")", '', $telephone);
        $telephone = str_replace(" ", '', $telephone);
        return $telephone;

    }

}
