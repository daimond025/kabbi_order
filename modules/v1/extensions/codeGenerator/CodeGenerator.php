<?php

namespace app\modules\v1\extensions\codeGenerator;

class CodeGenerator
{
    const LIMIT_ITERATION = 100;

    protected $codes = [];
    protected $symbols;

    protected $countSymbolsInCode;
    protected $countSymbolsForCreateCode;
    protected $maxCountUniqueCodes;

    /**
     * CodeGenerator constructor.
     *
     * @param array $symbols
     * @param int   $countSymbolsInCode
     * @param int   $countSymbolsForCreateCode
     */
    public function __construct(
        $symbols,
        $countSymbolsInCode = 6,
        $countSymbolsForCreateCode = 4
    ) {
        $this->symbols = $symbols;

        $this->countSymbolsInCode = $countSymbolsInCode;
        $this->countSymbolsForCreateCode = $countSymbolsForCreateCode;

        $this->testFeasibility();
        $this->maxCountUniqueCodes = pow(count($symbols), $countSymbolsInCode);
    }

    public function setUsedCodes(array $codes)
    {
        $this->codes = array_merge($this->codes, $codes);
    }

    public function getUniqueCode()
    {
        $this->testMaxFeasibleCountCodes();

        return $this->findUniqueCode();
    }

    public function getCode()
    {
        return $this->createCode();
    }

    public function clearHistoryCodes()
    {
        $this->codes = [];
    }


    protected function testMaxFeasibleCountCodes()
    {
        if (count($this->codes) >= $this->maxCountUniqueCodes) {
            throw new \RuntimeException('Error, limit is exceeded max codes');
        }
    }

    protected function testFeasibility()
    {
        if ($this->countSymbolsForCreateCode > count($this->symbols)) {
            throw new \RuntimeException('Error not feasibility create code.');
        }
    }

    protected function getSymbolsForCreateCode()
    {
        shuffle($this->symbols);

        return array_slice($this->symbols, 0, $this->countSymbolsForCreateCode);
    }

    protected function findUniqueCode()
    {
        $iteration = 0;
        do {
            $code = $this->createCode();

            $iteration++;
            if ($iteration > self::LIMIT_ITERATION) {
                throw new \RuntimeException('Error: limit iteration');
            }
        } while ($this->isUsedCode($code));
        $this->codes[] = $code;

        return $code;
    }

    protected function isUsedCode($code)
    {
        return in_array($code, $this->codes);
    }

    protected function createCode()
    {
        $code = '';
        $symbolsForCreateCode = $this->getSymbolsForCreateCode();

        foreach (range(1, $this->countSymbolsInCode) as $item) {
            $code .= $symbolsForCreateCode[array_rand($symbolsForCreateCode)];
        }

        return $code;
    }
}
