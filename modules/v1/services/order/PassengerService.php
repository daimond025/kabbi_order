<?php

namespace app\modules\v1\services\order;

use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\models\OrderRecord;

class PassengerService
{
    protected $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function preparePassenger(OrderRecord $order)
    {
        if ($order->isGoPassenger()) {

            $client = $this->clientService->findClientByPhoneAndCreateIfNotExists(
                $order->getClientPassengerPhone(),
                $order->tenant_id,
                $order->city_id,
                $order->getNamePassenger()
            );

            $order->setClientPassengerId($client->client_id);
        }
    }
}
