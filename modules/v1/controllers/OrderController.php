<?php

namespace app\modules\v1\controllers;

use app\behaviors\StopJsonEncode;
use app\models\OrderTrackService;
use app\modules\v1\components\services\order\orderSave\CreateOrderService;
use app\modules\v1\models\orderDataProviders\ActiveOrdersProvider;
use app\modules\v1\models\Order;
use app\modules\v1\models\OrderChangeRuleManager;
use app\modules\v1\models\OrderDetailCost;
use app\modules\v1\models\orderEvents\OrderEvents;
use app\modules\v1\models\orderEvents\OrderSendToExchangeEvent;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrdersFactory;
use app\modules\v1\models\orderEvents\OrderUpdateEvent;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for OrderRecord model.
 */
class OrderController extends BaseController
{
    protected $createOrderService;

    public function __construct( $id, Module $module,  CreateOrderService $createOrderService,array $config = [] ) {
        parent::__construct($id, $module, $config);

        $this->createOrderService = $createOrderService;
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'stopJsonEncode' => [
                'class'   => StopJsonEncode::className(),
                'actions' => ['track'],
            ],
        ]);
    }

    /**
     * @param int $position_id
     * @param int $tenant_id
     * @param int $city_id
     * @param array|null $allowCityId
     * @param string $group
     * @param null|string $date Format: d.m.Y
     *
     * @return array
     * @throws \yii\base\NotSupportedException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex(
        $position_id,
        $tenant_id,
        $city_id = null,
        $allowCityId = null,
        $group = 'new',
        $date = null
    )
    {
        return OrdersFactory::getInstance([
            'cityId'      => $city_id,
            'positionId'  => $position_id,
            'statusGroup' => $group,
            'tenantId'    => $tenant_id,
            'date'        => $date,
            'allowCityId' => $allowCityId,
        ])->getList();
    }

    /**
     * @param int $order_id
     * @param int $tenant_id
     *
     * @return array
     */
    public function actionActive($order_id, $tenant_id)
    {
        return (new ActiveOrdersProvider(['tenantId' => $tenant_id]))->getOne($order_id);
    }

    /**
     * @param int $tenant_id
     *
     * @return array
     */
    public function actionActiveList($tenant_id)
    {
        return (new ActiveOrdersProvider(['tenantId' => $tenant_id]))->getRawDataList();
    }

    /**
     * Order info by number
     *
     * @param integer $order_number
     * @param integer $tenant_id
     *
     * @return array
     */
    public function actionViewByNumber($order_number, $tenant_id)
    {
        return (new Order())->getDetailData(['order_number' => $order_number, 'tenant_id' => $tenant_id]);
    }

    /**
     * Order info by id
     *
     * @param integer $order_id
     *
     * @return array
     */
    public function actionView($order_id)
    {
        return (new Order())->getDetailData(['order_id' => $order_id]);
    }

    /**
     * @param int $position_id
     * @param int $tenant_id
     * @param int $city_id
     * @param null|string $date Format: d.m.Y
     *
     * @return array
     */
    public function actionCount($position_id, $tenant_id, $city_id, $date = null)
    {
        return (new Order([
            'positionId' => $position_id,
            'tenantId'   => $tenant_id,
            'cityId'     => $city_id,
            'date'       => $date,
        ]))->getCounts();
    }

    /**
     * Getting order tracking coords.
     *
     * @param integer $order_id
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTrack($order_id)
    {
        /* @var $service OrderTrackService */
        $service = \Yii::createObject(OrderTrackService::class);

        $track = [
            'order_route' => $service->getTrack($order_id),
        ];

        return json_encode($track);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $post = \Yii::$app->request->post();
        \Yii::$app->apiLogger->log(json_encode($post, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        $order = new OrderRecord();

        $order->load($post, '');
        return $this->createOrderService->createOrder($order);
    }

    /**
     * @param int $order_id
     *
     * @return array
     */
    public function actionEvents($order_id)
    {
        return (new OrderEvents(['orderId' => $order_id]))->getEvents();
    }

    /**
     * @deprecated  offer to worker
     *
     * @param int $order_number
     * @param int $tenant_id
     *
     * @return array
     */
    public function actionStopOffer($order_number, $tenant_id)
    {
        $res = (int)(new Order(['tenantId' => $tenant_id]))->stopOffer($order_number);

        return ['code' => $res];
    }

    /**
     * @param int $order_id
     *
     * @return array
     */
    public function actionDetailCost($order_id)
    {
        return (new OrderDetailCost(['orderId' => $order_id]))->getData();
    }

    /**
     * Update event
     * @return array
     * @throws \yii\base\NotSupportedException
     */
    public function actionUpdateEvent()
    {
        $data = \Yii::$app->request->post();
        \Yii::$app->apiLogger->log(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        return (new OrderUpdateEvent())->execute($data);
    }

    /**
     * @return array
     * @throws \yii\base\NotSupportedException
     */
    public function actionSendToExchangeEvent()
    {
        $data = \Yii::$app->request->post();
        \Yii::$app->apiLogger->log(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        return (new OrderSendToExchangeEvent())->execute($data);
    }

    /**
     * @param $order_id
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionFields($order_id)
    {
        $order = OrderRecord::findOne($order_id);

        if (empty($order)) {
            throw new NotFoundHttpException();
        }

        /* @var $manager OrderChangeRuleManager */
        $manager = \Yii::createObject(OrderChangeRuleManager::class);

        return $manager->getAvailableAttributes(
            $order->status_id, array_keys($order->getAttributes()));
    }

    /**
     * Getting available attributes
     *
     * @param int $statusId
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetAvailableAttributes($statusId)
    {
        /* @var $order OrderRecord */
        $order = \Yii::createObject(OrderRecord::className());

        /* @var $manager OrderChangeRuleManager */
        $manager = \Yii::createObject(OrderChangeRuleManager::class);

        return $manager->getAvailableAttributes(
            $statusId, array_keys($order->getAttributes()));
    }

    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result                      = [
            "code" => "404",
            "info" => "METHOD_NOT_FOUND",
        ];

        return $result;
    }

}
