<?php

namespace app\modules\v1\models\dto;

class Name
{
    public $name;
    public $lastName;
    public $secondName;
}
