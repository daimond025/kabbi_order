<?php

namespace app\modules\v1\models\dto;

class SettingExistOrderDto
{
    public $typeOfDistributionOrder;
    public $orderOfferSec;
    public $offerOrderType;
    public $offerOrderWorkerBatchCount;
    public $circlesDistributionOrder;
    public $checkWorkerRatingToOfferOrder;
    public $delayCirclesDistributionOrder;
    public $freeStatusBetweenDistributionCycles;
    public $orderRejectTime;
    public $sendPushFreeOrderForAll;
    public $distanceToFilterFreeOrders;
    public $sendPushPreOrderForAll;
    public $workerPreOrderReminder;
    public $preOrderWorking;
    public $circleDistance1;
    public $circleRepeat1;
    public $circleDistance2;
    public $circleRepeat2;
    public $circleDistance3;
    public $circleRepeat3;
    public $circleDistance4;
    public $circleRepeat4;
    public $circleDistance5;
    public $circleRepeat5;
    public $restrictVisibilityOfPreOrder;
    public $timeOfPreOrderVisibility;
    public $exchangeEnableSaleOrders;
    public $exchangeHowWaitWorker;
    public $orderFormAddPointPhone;
    public $orderFormAddPointComment;
    public $requirePointConfirmationCode;
    public $smsNotifyPointRecipientAboutOrderExecuting;

    public function loadSettings(array $params)
    {
        foreach ($params as $setting => $value) {
            $setting = $this->getCamelCaseName($setting);

            if (property_exists(self::class, $setting)) {
                $this->$setting = $value;
            } else {
                \Yii::info('Setting unknown property: ' . get_class($this) . '::' . $setting);
            }
        }
    }

    protected function getCamelCaseName($name)
    {
        $nameParts = explode('_', mb_strtolower($name));

        $nameCamelCase = '';
        foreach ($nameParts as $namePart) {
            $nameCamelCase .= ucfirst($namePart);
        }

        return lcfirst($nameCamelCase);
    }
}
