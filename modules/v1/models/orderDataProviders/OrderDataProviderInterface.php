<?php


namespace app\modules\v1\models\orderDataProviders;


interface OrderDataProviderInterface
{
    /**
     * @return array
     */
    public function getList();

    /**
     * @return int
     */
    public function getCounts();
}