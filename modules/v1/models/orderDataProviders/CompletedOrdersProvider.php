<?php


namespace app\modules\v1\models\orderDataProviders;



use app\modules\v1\models\Order;

class CompletedOrdersProvider extends OrdersProviderBase implements OrderDataProviderInterface
{
    /**
     * @return array
     */
    public function getList()
    {
        return (new Order([
            'tenantId'   => $this->tenantId,
            'cityId'     => $this->cityId,
            'positionId' => $this->positionId,
        ]))->getByStatusGroup($this->statusGroup, $this->date, [], [], []);
    }

    /**
     * @return int
     */
    public function getCounts()
    {
        return (new Order([
            'tenantId'   => $this->tenantId,
            'cityId'     => $this->cityId,
            'positionId' => $this->positionId,
        ]))->getByStatusGroup($this->statusGroup, $this->date, [], [], [], true);
    }
}