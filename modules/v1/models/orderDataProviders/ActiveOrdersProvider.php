<?php


namespace app\modules\v1\models\orderDataProviders;

use app\models\ActiveOrderRepository;
use app\models\interfaces\ActiveOrderRepositoryInterface;
use app\modules\v1\models\Order;
use app\modules\v1\models\OrderStatus;
use Yii;

class ActiveOrdersProvider extends OrdersProviderBase implements OrderDataProviderInterface
{
    private $_repository;

    public function init()
    {
        parent::init();
        $this->setRepository();
    }

    /**
     * Getting active orders
     * @return array
     */
    public function getList()
    {
        $orders   = [];
        $dbOrders = $this->getRawDataList();

        foreach ($dbOrders as $order) {
            $order = unserialize($order);

            if (!$this->isValid($order)) {
                continue;
            }

            $orders[] = $order;
        }

        return $orders;
    }

    /**
     * Getting one active order.
     *
     * @param int $orderId
     *
     * @return array|null
     */
    public function getOne($orderId)
    {
        return $this->getRepository()->getOne($orderId);
    }

    /**
     * @return array
     */
    public function getRawDataList()
    {
        return $this->getRepository()->getList();
    }

    /**
     * Getting count of active orders
     * @return int
     */
    public function getCounts()
    {
        return count($this->getList());
    }

    /**
     * Getting active order by phone.
     *
     * @param string $phone Caller phone.
     *
     * @return array
     */
    public function getCallerOrder($phone)
    {
        $db_orders = $this->getRawDataList();
        $orders    = [];

        foreach ($db_orders as $order) {
            $order = unserialize($order);

            if ($order['phone'] == $phone || $order['worker']['phone'] == $phone) {
                $order_obj = new Order();
                $order_obj->load($order, '');
                $order_obj->order_id         = $order['order_id'];
                $order_obj->address          = unserialize($order_obj->address);
                $order_obj->statusRedis      = $order['status'];
                $order_obj->costData         = $order['costData'];
                $order_obj->phone            = $order['phone'];
                $order_obj->caller           = $order['phone'] == $phone ? 'client' : 'worker';
                $order_obj->isActive         = true;
                $order_obj->clientRedis      = $order['client'];
                $order_obj->workerRedis      = $order['worker'];
                $order_obj->carRedis         = $order['car'];
                $order_obj->cityRedis        = $order['city'];
                $order_obj->userCreatedRedis = $order['userCreated'];

                $orders[] = $order_obj;

                //Водила звонит, у него может быть только один активный заказ.
                if ($order['worker']['phone'] == $phone) {
                    break;
                }
            }
        }

        return $orders;
    }

    /**
     * @param array $order
     *
     * @return bool
     */
    private function isValid($order)
    {
        $conditions = [
            in_array($this->statusGroup, OrderStatus::getStatusGroup($order['status_id']))
            || (!empty($order['call_warning_id']) && $this->statusGroup == OrderStatus::STATUS_GROUP_WARNING),
            $this->tenantId == $order['tenant_id'],
            $this->positionId == $order['position_id'],
        ];

        if (!empty($this->date)) {
            $conditions[] = date('d.m.Y', $order['order_time']) == $this->date;
        }

        if (!empty($this->cityId)) {
            $conditions[] = (is_array($this->cityId) && in_array($order['city_id'],
                        $this->cityId)) || $this->cityId == $order['city_id'];
        }

        if (!empty($this->allowCityId) && is_array($this->allowCityId)) {
            $conditions[] = in_array($order['city_id'], $this->allowCityId);
        }

        return !in_array(false, $conditions, true);
    }

    protected function getRepositoryObject()
    {
        return Yii::createObject(['class' => ActiveOrderRepository::className(), 'tenantId' => $this->tenantId]);
    }

    protected function setRepository()
    {
        $repository = $this->getRepositoryObject();

        $this->_repository = $repository;
    }

    /**
     * @return ActiveOrderRepositoryInterface
     */
    protected function getRepository()
    {
        return $this->_repository;
    }
}