<?php


namespace app\modules\v1\models\orderDataProviders;


use yii\base\Object;

abstract class OrdersProviderBase extends Object
{
    public $cityId;
    public $positionId;
    public $tenantId;
    public $statusGroup;
    /** @var  string Format: d.m.Y */
    public $date;
    public $allowCityId;

    public function init()
    {
        parent::init();

        if (empty($this->cityId) && !empty($this->allowCityId)) {
            $this->cityId = $this->allowCityId;
        }
    }
}