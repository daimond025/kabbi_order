<?php

namespace app\modules\v1\models\subModels;

interface SubModuleInterface
{
    public function load($data, $formName = null);
    public function validate($attributeNames = null, $clearErrors = true);
    public function getErrors();
}
