<?php

namespace app\modules\v1\models\subModels;

use app\components\OrderError;
use app\models\ClientPhone;
use app\modules\v1\models\client\Client;
use app\modules\v1\models\dto\Name;
use yii\base\Model;

class OrderPassenger extends Model implements SubModuleInterface
{
    public $client_passenger_name;
    public $client_passenger_lastname;
    public $client_passenger_secondname;

    public $client_passenger_id;
    public $client_passenger_phone;

    public function rules()
    {
        return [
            [['client_passenger_phone', 'client_passenger_id'], 'integer'],
            [['client_passenger_phone'], 'string', 'max' => 20],
            [
                'client_passenger_id',
                'exist',
                'targetClass'     => Client::class,
                'targetAttribute' => ['client_passenger_id' => 'client_id'],
                'filter'          => [
                    'black_list' => 0,
                    'active'     => 1,
                ],
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                'client_passenger_phone',
                'exist',
                'targetClass'     => ClientPhone::class,
                'targetAttribute' => [
                    'client_passenger_id'    => 'client_id',
                    'client_passenger_phone' => 'value',
                ],
                'when'            => function () {
                    return (bool)$this->client_passenger_id;
                },
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                ['client_passenger_name', 'client_passenger_lastname', 'client_passenger_secondname'],
                'string',
                'max' => 45,
            ],
        ];
    }

    public function isGoPassenger()
    {
        return (bool)$this->client_passenger_phone;
    }

    /**
     * @return Name
     */
    public function getName()
    {
        $name             = new Name();
        $name->name       = $this->client_passenger_name;
        $name->lastName   = $this->client_passenger_lastname;
        $name->secondName = $this->client_passenger_secondname;

        return $name;
    }
}
