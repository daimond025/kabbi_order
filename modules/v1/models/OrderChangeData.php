<?php

namespace app\modules\v1\models;

use app\models\OrderChangeData as OrderChangeDataRecord;

/**
 * Class OrderChangeData
 * @package app\modules\v1\models
 */
class OrderChangeData
{
    const FIELDS_SAVE_TO_LOG = [
        'order_time',
        'status_id',
        'city_id',
        'client_id',
        'phone',
        'position_id',
        'tariff_id',
        'company_id',
        'payment',
        'bonus_payment',
        'predv_distance',
        'predv_time',
        'predv_price',
        'is_fix',
        'car_id',
        'worker_id',
        'address',
        'parking_id',
        'comment',
        'additional_option',
    ];

    /**
     * Getting filtered fields
     *
     * @param array $fields
     *
     * @return array
     */
    private static function getFilteredFields($fields)
    {
        return array_filter($fields, function ($field) {
            return in_array($field, self::FIELDS_SAVE_TO_LOG, false);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Allow to log user order update.
     *
     * @param integer $order_id
     * @param integer $tenant_id
     * @param string  $subject
     * @param array   $arChangeField Array of order change field.
     *
     * @return boolean
     * @throws \yii\db\Exception
     */
    public static function userLog($order_id, $tenant_id, $subject, $arChangeField)
    {

        if (is_array($arChangeField) && !empty($arChangeField)) {
            $insertValue = [];
            $connection  = app()->db;

            $fields = self::getFilteredFields($arChangeField);
            foreach ($fields as $field => $value) {
                $insertValue[] = [
                    $tenant_id,
                    $order_id,
                    $field,
                    $order_id,
                    'order',
                    $subject,
                    $value,
                    time(),
                ];
            }

            if (!empty($insertValue)) {
                $arFields = [
                    'tenant_id',
                    'order_id',
                    'change_field',
                    'change_object_id',
                    'change_object_type',
                    'change_subject',
                    'change_val',
                    'change_time',
                ];



                $connection->createCommand()->batchInsert(OrderChangeDataRecord::tableName(), $arFields,
                    $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
