<?php

namespace app\modules\v1\models;

use app\models\OrderStatusRecord;
use yii\helpers\ArrayHelper;

/**
 * Class OrderChangeRuleManager
 * @package app\modules\v1\models
 */
class OrderChangeRuleManager
{
    const ALL   = '*';
    const ALLOW = 'allow';
    const DENY  = 'deny';

    const DEFAULT_STRATEGY = self::DENY;

    const STATUS_ATTRIBUTE = 'status_id';

    private $attributeMap = [
        self::ALL => [
            self::ALL => [
                self::ALLOW => [
                    'user_create',
                    'user_modified',
                    'create_time',
                    'status_time',
                    'update_time',
                    'deny_refuse_order',
                ],
                self::DENY  => [self::ALL],
            ],
        ],
    ];

    const PRIORITY_ATTR_ALL       = 0;
    const PRIORITY_ATTR           = 1;
    const PRIORITY_NEW_ALL        = 2;
    const PRIORITY_NEW_GROUP      = 4;
    const PRIORITY_NEW_STATUS     = 8;
    const PRIORITY_CURRENT_ALL    = 16;
    const PRIORITY_CURRENT_GROUP  = 32;
    const PRIORITY_CURRENT_STATUS = 64;


    public function __construct()
    {
        // MANUAL GROUP
        $this->attributeMap[(string)OrderStatus::STATUS_MANUAL_MODE] = [
            self::ALL                                                     => [
                self::DENY  => [self::STATUS_ATTRIBUTE, 'phone', 'client_id'],
                self::ALLOW => [self::ALL],
            ],
            (string)OrderStatus::STATUS_GROUP_REJECTED                    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE                       => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NEW                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE                              => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NOPARKING                         => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_NOPARKING                     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];


        // NEW GROUP
        $newGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE           => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE_FOR_HOSPITAL => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE_FOR_COMPANY => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NEW            => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NOPARKING      => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE            => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_NOPARKING  => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_NEW] = $newGroupActions;
        $this->attributeMap[(string)OrderStatus::STATUS_NOPARKING] = $newGroupActions;


        // OFFER ORDER GROUP
        $offerOrderGroupActions = [
            (string)OrderStatus::STATUS_MANUAL_MODE => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_OFFER_ORDER] = $offerOrderGroupActions;
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_REFUSED] = $offerOrderGroupActions;
        $this->attributeMap[(string)OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER] = $offerOrderGroupActions;


        $withoutCityAndClient = [
            self::ALL => [
                self::ALLOW => [
                    'worker_id',
                    'car_id',
                    'order_time',
                    'address',
                    'parking_id',
                    'comment',
                    'position_id',
                    'tariff_id',
                    'additional_option',
                    'payment',
                    'bonus_payment',
                    'company_id',
                    'is_fix',
                    'predv_price',
                    'predv_distance',
                    'predv_time',
                    'currency_id',
                ],
            ],
        ];

        // FREE GROUP
        $freeGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED                    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE                       => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NEW                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NOPARKING                         => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_NOPARKING                     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE                              => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE_FOR_HOSPITAL                 => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE_FOR_COMPANY                  => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_FREE] =
        $this->attributeMap[(string)OrderStatus::STATUS_FREE_FOR_HOSPITAL] =
        $this->attributeMap[(string)OrderStatus::STATUS_FREE_FOR_COMPANY] =
        $this->attributeMap[(string)OrderStatus::STATUS_OVERDUE] =
        $this->attributeMap[(string)OrderStatus::STATUS_REFUSED_ORDER_ASSIGN] =
            ArrayHelper::merge($withoutCityAndClient, $freeGroupActions);


        // NEW PRE GROUP
        $newPreGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED                    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE                       => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NEW                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_NOPARKING                         => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE                               => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_NOPARKING                     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_ORDER_FOR_HOSPITAL                     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY                     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_PRE] =
        $this->attributeMap[(string)OrderStatus::STATUS_PRE_NOPARKING] =
        $this->attributeMap[(string)OrderStatus::STATUS_PRE_REFUSE_WORKER] =
        $this->attributeMap[(string)OrderStatus::STATUS_PRE_ORDER_FOR_HOSPITAL] =
        $this->attributeMap[(string)OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY] =
            ArrayHelper::merge($withoutCityAndClient, $newPreGroupActions);


        $withoutCityAndClientAndWorker = [
            self::ALL => [
                self::ALLOW => [
                    'order_time',
                    'address',
                    'parking_id',
                    'comment',
                    'additional_option',
                    'payment',
                    'bonus_payment',
                    'company_id',
                    'is_fix',
                    'predv_price',
                    'predv_distance',
                    'predv_time',
                    'currency_id',
                ],
            ],
        ];

        // ASSIGNED PRE GROUP
        $assignPreGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED                => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE                   => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_PAID                => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_NOT_PAID            => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_PRE_GET_WORKER] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD] =
        $this->attributeMap[(string)OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION] =
            ArrayHelper::merge($withoutCityAndClientAndWorker, $assignPreGroupActions);

        // ASSIGNED GROUP
        $assignGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED                    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE                       => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_FREE                              => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_PAID                    => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_NOT_PAID                => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT] =
        $this->attributeMap[(string)OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD] =
            ArrayHelper::merge($withoutCityAndClientAndWorker, $assignGroupActions);


        $withoutCityAndClientAndWorkerAndTariffAndOrderTime = [
            self::ALL => [
                self::ALLOW => [
                    'address',
                    'parking_id',
                    'comment',
                    'additional_option',
                    'payment',
                    'bonus_payment',
                    'company_id',
                    'is_fix',
                    'predv_price',
                    'predv_distance',
                    'predv_time',
                    'currency_id',
                ],
            ],
        ];

        // CAR ASSIGNED GROUP
        $carAssignedGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE        => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_PAID     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_NOT_PAID => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_GROUP_CAR_ASSIGNED] =
            ArrayHelper::merge($withoutCityAndClientAndWorkerAndTariffAndOrderTime, $carAssignedGroupActions);


        // CAR AT PLACE GROUP
        $carAtPlaceGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE        => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_PAID     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_NOT_PAID => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_GROUP_CAR_AT_PLACE] =
            ArrayHelper::merge($withoutCityAndClientAndWorkerAndTariffAndOrderTime, $carAtPlaceGroupActions);


        // EXECUTING GROUP
        $executingGroupActions = [
            (string)OrderStatus::STATUS_GROUP_REJECTED     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_MANUAL_MODE        => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_PAID     => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
            (string)OrderStatus::STATUS_COMPLETED_NOT_PAID => [self::ALLOW => [self::STATUS_ATTRIBUTE]],
        ];
        $this->attributeMap[(string)OrderStatus::STATUS_GROUP_EXECUTING] =
            ArrayHelper::merge($withoutCityAndClientAndWorkerAndTariffAndOrderTime, $executingGroupActions);

    }

    /**
     * Check exists rule
     *
     * @param string $rule
     * @param int $current
     * @param int $new
     * @param string $attribute
     *
     * @return bool
     */
    private function existsRule($rule, $current, $new, $attribute)
    {
        $values = isset($this->attributeMap[$current][$new][$rule])
            ? $this->attributeMap[$current][$new][$rule] : [];

        return in_array($attribute, $values, true);
    }

    /**
     * Getting priority of rule
     *
     * @param string $rule
     * @param int $statusId
     * @param int $statusGroup
     * @param int|null $newStatusId
     * @param int|null $newStatusGroup
     * @param string $attribute
     *
     * @return bool
     */
    private function getRulePriority($rule, $statusId, $statusGroup, $newStatusId, $newStatusGroup, $attribute)
    {
        $currentMap = [
            self::PRIORITY_CURRENT_STATUS => $statusId,
            self::PRIORITY_CURRENT_GROUP  => $statusGroup,
            self::PRIORITY_CURRENT_ALL    => self::ALL,
        ];



        $newMap = [
            self::PRIORITY_NEW_STATUS => $newStatusId,
            self::PRIORITY_NEW_GROUP  => $newStatusGroup,
            self::PRIORITY_NEW_ALL    => self::ALL,
        ];


        $attributeMap = [
            self::PRIORITY_ATTR     => $attribute,
            self::PRIORITY_ATTR_ALL => self::ALL,
        ];



        $resultPriority = 0;
        foreach ($currentMap as $currentPriority => $currentValue) {
            foreach ($newMap as $newPriority => $newValue) {

                foreach ($attributeMap as $attrPriority => $attrValue) {
                    if ($this->existsRule($rule, $currentValue, $newValue, $attrValue)) {

                        $rulePriority = $currentPriority + $newPriority + $attrPriority;
                        $resultPriority = $rulePriority > $resultPriority ? $rulePriority : $resultPriority;
                    }
                }
            }
        }

        return $resultPriority;
    }

    /**
     * Check can update by current strategy
     *
     * @param $denyPriority
     * @param $allowPriority
     *
     * @return bool
     */
    private function canUpdate($denyPriority, $allowPriority)
    {
        return ($allowPriority > $denyPriority)
            || ($allowPriority === $denyPriority && self::DEFAULT_STRATEGY === self::ALLOW);
    }

    /**
     * Check can update order attribute
     *
     * @param OrderStatusRecord $status
     * @param OrderStatusRecord | null $newStatus
     * @param string $attribute
     *
     * @return bool
     */
    public function canUpdateAttribute(OrderStatusRecord $status, $newStatus, $attribute)
    {
        $statusId = $status->status_id;
        $statusGroup = $status->status_group;

        if ($newStatus instanceof OrderStatusRecord) {
            $newStatusId = $newStatus->status_id;
            $newStatusGroup = $newStatus->status_group;
        } else {
            $newStatusId = $newStatusGroup = null;
        }


        $denyPriority = $this->getRulePriority(
            self::DENY, $statusId, $statusGroup, $newStatusId, $newStatusGroup, $attribute);

        $allowPriority = $this->getRulePriority(
            self::ALLOW, $statusId, $statusGroup, $newStatusId, $newStatusGroup, $attribute);

        return $this->canUpdate($denyPriority, $allowPriority);
    }

    /**
     * Check can update order status
     *
     * @param OrderStatusRecord $status
     * @param OrderStatusRecord $newStatus
     *
     * @return bool
     */
    public function canUpdateStatus(OrderStatusRecord $status, OrderStatusRecord $newStatus)
    {
        return $this->canUpdateAttribute($status, $newStatus, self::STATUS_ATTRIBUTE);
    }

    /**
     * Cand send order to exchange
     * @param OrderStatusRecord $status
     * @param $eventSubject
     * @return bool
     */
    public function canSendToExchange(OrderStatusRecord $status, $eventSubject)
    {
        $statusId = (int)$status->status_id;
        if ($eventSubject === 'system') {
            return in_array($statusId, [
                OrderStatus::STATUS_FREE,
                OrderStatus::STATUS_OVERDUE,
            ], false);
        }
        return in_array($statusId, [
            OrderStatus::STATUS_FREE,
            OrderStatus::STATUS_OVERDUE,
        ], false);

    }


    /**
     * Getting available attributes
     *
     * @param int $statusId
     * @param string[] $attributes
     *
     * @return array
     */
    public function getAvailableAttributes($statusId, $attributes)
    {
        $status = OrderStatusRecord::findOne($statusId);

        $result = [];

        if ($status && is_array($attributes)) {
            foreach ($attributes as $attribute) {
                $result[$attribute] = $this->canUpdateAttribute($status, null, $attribute);
            }
        }

        return $result;
    }
}