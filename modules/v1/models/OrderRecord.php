<?php

namespace app\modules\v1\models;

use app\components\serviceEngine\ServiceEngine;
use app\components\Card;
use app\components\settings\OrderSetting;
use app\models\ActiveOrderRepository;
use app\models\ActiveWorkerRepository;
use app\models\CardPayment;
use app\models\Client;
use app\models\ClientCompany;
use app\models\ClientPhone;
use app\models\DefaultSettings;
use app\models\Order as OrderRecordBase;
use app\models\Parking;
use app\models\TenantSetting;
use app\models\Worker;
use app\modules\v1\components\services\order\orderSave\OrderAddress;
use app\modules\v1\components\services\order\orderSave\OrderStatusDelegate;
use app\modules\v1\components\services\order\orderSave\OrderTime;
use app\modules\v1\models\dto\Name;
use app\modules\v1\models\exceptions\BlockedCompanyException;
use app\modules\v1\models\exceptions\ForbiddenChangeOrderTimeException;
use app\modules\v1\models\exceptions\ForbiddenChangeWorkerException;
use app\modules\v1\models\exceptions\InvalidAttributeValueException;
use app\modules\v1\models\exceptions\InvalidClientCard;
use app\modules\v1\models\exceptions\InvalidOrderTimeException;
use app\models\OrderStatusRecord as OrderStatusRecord;
use app\modules\v1\models\orderDataProviders\ActiveOrdersProvider;
use app\modules\v1\models\rules\order\OrderRules;
use app\modules\v1\models\subModels\OrderPassenger;
use yii\base\ErrorException;
use app\models\OrderHasOption as OrderHasOptionRecord;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string $phone
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property array $address
 * @property string $comment
 * @property string $predv_price
 * @property string $predv_distance
 * @property string $predv_time
 * @property string $device
 * @property string $order_number
 * @property integer $edit
 * @property string $payment
 * @property integer $show_phone
 * @property string $create_time
 * @property string $updated_time
 * @property string $order_time
 * @property string $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 * @property integer $app_id
 *
 *
 * @property OrderStatusRecord $status
 * @property Worker $worker
 * @property Client $client
 * @property Client $clientPassenger
 */
class OrderRecord extends OrderRecordBase
{

    const DEVICE_0              = 'DISPATCHER';
    const DEVICE_WORKER         = 'WORKER';
    const DEVICE_WEB            = 'WEB';
    const PRE_ODRER_TIME        = 1800;
    const PICK_UP_TIME          = 300;
    const MAP_DATA_TYPE_ADDRESS = 'address';
    const MAP_DATA_TYPE_WORKER  = 'worker';
    const MAP_DATA_TYPE_ROUTE   = 'route';

    const PAYMENT_CASH             = 'CASH';
    const PAYMENT_CORP             = 'CORP_BALANCE';
    const PAYMENT_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const PAYMENT_CARD             = 'CARD';

    const TYPES_PAYMENT = [
        self::PAYMENT_CASH,
        self::PAYMENT_CORP,
        self::PAYMENT_PERSONAL_ACCOUNT,
        self::PAYMENT_CARD,
    ];

    public $additional_option;
    public $pan;
    public $lang;

    public $versionclient;
    public $bonus_promo_code;

    public $forPreProcessingErrors;

    /**
     * Who is calling.
     * @var string
     */
    public $caller;
    public $order_now;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $order_seconds;
    public $status_reject;

    public $call_id;

    public $subject;

    /**
     * Активный заказ или нет.
     * @var bool
     */
    public $isActive = false;
    public $statusRedis;
    public $clientRedis;
    public $cityRedis;
    public $workerRedis;
    public $carRedis;
    public $userCreatedRedis;

    public $new_add_option;
    public $summary_cost;
    public $originally_preorder;
    public $orderAction;

    public $client_name;
    public $client_lastname;
    public $client_secondname;

    public $except_car_models;
    /**
     * Cost data from redis
     * @var array
     */
    public $costData;
    public $arOldAttributes;
    private $pickUp;

    /**
     * @var string
     */
    public $hash;

    /** @var OrderSetting */
    public $settings;
    /** @var OrderTime */
    public $orderTime;
    /** @var OrderAddress */
    public $orderAddress;
    /** @var OrderStatusDelegate */
    public $orderStatus;

    /** @var OrderPassenger */
    protected $passenger;

    /**
     * @return Model[]
     */
    public function subModels()
    {
        return [
            $this->passenger,
        ];
    }

    public function init()
    {
        parent::init();

        $this->orderStatus  = \Yii::createObject(OrderStatusDelegate::class, [$this]);
        $this->settings     = \Yii::createObject(OrderSetting::class, [$this]);
        $this->orderTime    = \Yii::createObject(OrderTime::class, [$this]);
        $this->orderAddress = \Yii::createObject(OrderAddress::class, [$this]);

        $this->passenger = new OrderPassenger();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return parent::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return (new OrderRules())->getRules($this);
    }

    public function load($data, $formName = null)
    {
        $loadResult = true;

        foreach ($this->subModels() as $subModel) {
            $loadResult = $loadResult && $subModel->load($data, $formName);
        }

        return $loadResult && parent::load($data, $formName);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $commonValidResult = true;
        foreach ($this->subModels() as $subModel) {
            if (!$currentValidResult = $subModel->validate($attributeNames, $clearErrors)) {
                $this->addErrors($subModel->getErrors());
            }
            $commonValidResult = $commonValidResult && $currentValidResult;
        }

        return $commonValidResult && parent::validate($attributeNames, $clearErrors);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'          => \Yii::t('order', 'Order ID'),
            'tenant_id'         => \Yii::t('order', 'Tenant ID'),
            'worker_id'         => \Yii::t('order', 'Worker'),
            'city_id'           => \Yii::t('order', 'Branch'),
            'tariff_id'         => \Yii::t('order', 'Tariff'),
            'client_id'         => \Yii::t('order', 'Client ID'),
            'user_create'       => \Yii::t('order', 'User Create'),
            'user_modifed'      => \Yii::t('order', 'User modifed'),
            'status_id'         => \Yii::t('order', 'Status'),
            'address'           => \Yii::t('order', 'Address'),
            'comment'           => \Yii::t('order', 'Comment'),
            'predv_price'       => \Yii::t('order', 'Price'),
            'predv_distance'    => \Yii::t('order', 'Preliminary distance'),
            'predv_time'        => \Yii::t('order', 'Preliminary time'),
            'create_time'       => \Yii::t('order', 'Create Time'),
            'device'            => \Yii::t('order', 'Device'),
            'payment'           => \Yii::t('order', 'Payment'),
            'show_phone'        => \Yii::t('order', 'Show client phone to worker'),
            'parking_id'        => \Yii::t('parking', 'Parking'),
            'preliminary_calc'  => \Yii::t('parking', 'Preliminary calculation'),
            'additional_option' => \Yii::t('order', 'Additional options'),
            'order_time'        => \Yii::t('order', 'Order time'),
            'status_time'       => \Yii::t('order', 'Status time'),
            'phone'             => \Yii::t('order', 'Client phone'),
            'bonus_payment'     => \Yii::t('order', 'Bonus payment'),
            'position_id'       => \Yii::t('order', 'Position Id'),
            'summary_cost'      => \Yii::t('order', 'Summary cost'),
            'update_time'       => \Yii::t('order', 'Update time'),
            'deny_refuse_order' => \Yii::t('order', 'Without the possibility of failure'),
            'car_id'            => \Yii::t('order', 'Car Id'),
            'company_id'        => \Yii::t('order', 'Company ID'),
            'is_fix'            => \Yii::t('order', 'offer my price'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ArrayHelper::merge(parent::attributes(), ['additional_option', 'pan', 'subject']);
    }

    /**
     * Getting local time.
     *
     * @param integer $time
     *
     * @return integer
     */
    private function getOrderTimestamp($time = null)
    {
        if (is_null($time)) {
            $time = time();
        }

        $offset = $this->getOrderOffset();

        return $offset + $time;
    }

    /**
     * Getting local time offset.
     * @return integer
     */
    public function getOrderOffset()
    {
        return City::getTimeOffset($this->city_id);
    }


    public function getStatus()
    {
        return $this->hasOne(OrderStatusRecord::class, ['status_id' => 'status_id']);
    }

    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getClientPassenger()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_passenger_id']);
    }

    public function getOrderExceptCarModels()
    {
        return $this->hasMany(OrderExceptCarModels::class, ['order_id' => 'order_id']);
    }


    private function isNeedCheckOrderTime()
    {
        return in_array($this->status->status_group, [
                OrderStatus::STATUS_GROUP_NEW,
                OrderStatus::STATUS_GROUP_PRE_ORDER,
            ], false)
            && $this->status_id != OrderStatus::STATUS_OVERDUE
            && $this->status_id != OrderStatus::STATUS_MANUAL_MODE;
    }

    /**
     * Update order status
     */
    private function updateOrderStatus()
    {
        if ($this->status_id === OrderStatus::STATUS_OVERDUE
            && $this->order_time > $this->getOrderTimestamp()
        ) {
            $this->status_id = OrderStatus::STATUS_FREE;
        }

        if ($this->isPreOrder()) {
            if (in_array($this->status_id, [
                OrderStatus::STATUS_NEW,
                OrderStatus::STATUS_NOPARKING,
                OrderStatus::STATUS_FREE,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_FREE_FOR_HOSPITAL,
                OrderStatus::STATUS_FREE_FOR_COMPANY,
            ], false)
            ) {
                switch ($this->status_id) {
                    case OrderStatus::STATUS_NEW:
                    case OrderStatus::STATUS_NOPARKING:
                    case OrderStatus::STATUS_FREE:
                        $this->status_id = empty($this->parking_id)
                            ? OrderStatus::STATUS_PRE_NOPARKING : OrderStatus::STATUS_PRE;
                        break;
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT:
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT;
                        break;
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD:
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD;
                        break;
                    case OrderStatus::STATUS_FREE_FOR_HOSPITAL:
                        $this->status_id = OrderStatus::STATUS_PRE_ORDER_FOR_HOSPITAL;
                        break;
                    case OrderStatus::STATUS_FREE_FOR_COMPANY:
                        $this->status_id = OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY;
                        break;
                }
                $this->edit = 1;

            }
        } else {
            if (in_array($this->status_id, [
                OrderStatus::STATUS_PRE,
                OrderStatus::STATUS_PRE_NOPARKING,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
            ], false)
            ) {
                switch ($this->status_id) {
                    case OrderStatus::STATUS_PRE:
                    case OrderStatus::STATUS_PRE_NOPARKING:
                        $this->status_id = empty($this->parking_id)
                            ? OrderStatus::STATUS_NOPARKING : OrderStatus::STATUS_NEW;
                        break;
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT:
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                        break;
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD:
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD;
                        break;
                }
                $this->edit = 0;
            }
        }
        $this->updateOrderStatusHospital();

    }


    public function updateOrderStatusHospital(){

        if($this->device === OrderStatus::TYPE_HOSPITAL){

            if(!in_array($this->status_id, [
                OrderStatus::STATUS_GET_WORKER,
                OrderStatus::STATUS_WORKER_WAITING,
                OrderStatus::STATUS_CLIENT_IS_NOT_COMING_OUT,
                OrderStatus::STATUS_FREE_AWAITING,
                OrderStatus::STATUS_NONE_FREE_AWAITING,
                OrderStatus::STATUS_EXECUTING,
                OrderStatus::STATUS_COMPLETED_PAID,
                OrderStatus::STATUS_COMPLETED_NOT_PAID,
                OrderStatus::STATUS_REJECTED,
                OrderStatus::STATUS_NO_CARS,
                OrderStatus::STATUS_OVERDUE,
                OrderStatus::STATUS_WORKER_LATE,
                OrderStatus::STATUS_EXECUTION_PRE,
                OrderStatus::STATUS_PAYMENT_CONFIRM,
                OrderStatus::STATUS_NO_CARS_BY_DISPATCHER,
                OrderStatus::STATUS_MANUAL_MODE,
                OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER,
                OrderStatus::STATUS_WAITING_FOR_PAYMENT])){
                $this->is_fix = 1;

                if ($this->isPreOrder()) {

                    if($this->worker_id){
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT;
                    }else{
                        if($this->company_id ){
                            $this->status_id = OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY;
                        }
                        elseif(!$this->company_id){
                            $this->status_id = OrderStatus::STATUS_PRE_ORDER_FOR_HOSPITAL;
                        }
                    }

                    $this->edit = 1;
                }else{
                    if($this->worker_id){
                        $this->status_id = OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                    }else{
                        if($this->company_id ){
                            $this->status_id = OrderStatus::STATUS_FREE_FOR_COMPANY;
                        }
                        elseif(!$this->company_id){
                            $this->status_id = OrderStatus::STATUS_FREE_FOR_HOSPITAL;
                        }
                    }
                    $this->edit = 0;
                }
            }
        }
    }



    /**
     * @param int $parkingId
     *
     * @return string
     */
    private function getParkingNameById($parkingId)
    {
        return Parking::find()
            ->where(['parking_id' => $parkingId])
            ->select('name')
            ->scalar();
    }

    /**
     * Determine parking and address coordinates
     *
     * @param array $address
     *
     * @return array
     */
    private function determineParkingAndAddressCoords($address)
    {
        $result = [];
        foreach ($address as $key => $value) {
            if (empty($value['lat']) || empty($value['lon'])) {
                $coords = app()->geocoder->findCoordsByAddress(implode(', ', [
                    $value['city'],
                    $value['street'],
                    $value['house'],
                ]));
                $value['lat'] = $coords['lat'];
                $value['lon'] = $coords['lon'];
            }

            if (empty($value['parking_id'])) {
                $parkingInfo = app()->routeAnalyzer->getParkingByCoords(
                    $this->tenant_id, $this->city_id, $value['lat'], $value['lon'], false);

                if (empty($parkingInfo['error'])) {
                    $value['parking_id'] = (int)$parkingInfo['inside'];
                    $value['parking'] = $this->getParkingNameById($value['parking_id']);

                    if ($key == 'A') {
                        $this->parking_id = $value['parking_id'];
                    }
                }
            } elseif ($key == 'A') {
                $this->parking_id = $value['parking_id'];
            }
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * Is active current order in the redis
     * @return bool
     */
    private function isActiveOrder()
    {
        $order = $this->getActiveOrder();
        $completedStatus = [OrderStatus::STATUS_GROUP_COMPLETED, OrderStatus::STATUS_GROUP_REJECTED];

        return isset($order)
            && isset($order['status']['status_group'])
            && !in_array($order['status']['status_group'], $completedStatus);
    }

    /**
     * Is address was changed
     *
     * @param array $oldAddress
     * @param array $newAddress
     *
     * @return bool
     */
    private function isAddressChanged($oldAddress, $newAddress)
    {
        $addressFields = [
            'city',
            'street',
            'house',
            'housing',
            'parking',
            'parking_id',
            'lat',
            'lon',
        ];

        if (count($oldAddress) != count($newAddress)) {
            return true;
        }

        foreach ($oldAddress as $key => $value) {
            foreach ($addressFields as $field) {
                $oldValue = isset($oldAddress[$key][$field]) ? $oldAddress[$key][$field] : null;
                $newValue = isset($newAddress[$key][$field]) ? $newAddress[$key][$field] : null;

                if ($oldValue != $newValue) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check changed attributes
     * @throws InvalidAttributeValueException
     * @throws \yii\base\InvalidConfigException
     */
    private function checkChangedAttributes()
    {
        $status = OrderStatusRecord::findOne($this->getOldAttribute('status_id'));
        $newStatus = OrderStatusRecord::findOne($this->getAttribute('status_id'));


        $checkAttributes = [
            'order_number',
            'order_time',
            'city_id',
            'phone',
            'client_id',
            'worker_id',
            'address',
            'parking_id',
            'comment',
            'tariff_id',
            'payment',
            'predv_price',
            'additional_option',
            'pan',
        ];

        if ($status instanceof OrderStatusRecord
            && $newStatus instanceof OrderStatusRecord
            && !empty($checkAttributes)
        ) {
            /* @var $manager OrderChangeRuleManager */
            $manager = \Yii::createObject(OrderChangeRuleManager::class);


            if ($status->status_id === $newStatus->status_id
                && Order::isActiveOrderProcessingThroughTheExternalExchange($this->tenant_id, $this->order_id)
                && in_array($status->status_id, OrderStatus::getWorksStatusId(), false)) {
                foreach ($checkAttributes as $attribute) {
                    if ($this->getOldAttribute($attribute) != $this->getAttribute($attribute)) {
                        throw new InvalidAttributeValueException(
                            t('order', 'It is forbidden to edit the value of "{attribute}" when order processed by exchange worker', [
                                'attribute' => $this->getAttributeLabel($attribute),
                            ]));
                    }
                }
            }

            foreach ($checkAttributes as $attribute) {
                switch ($attribute) {
                    case 'address':
                        $error = $this->isAddressChanged(
                                unserialize($this->getOldAttribute('address')),
                                unserialize($this->address))
                            && !$manager->canUpdateAttribute($status, $newStatus, $attribute);
                        break;
                    case 'additional_option':
                        $error = is_array($this->additional_option)
                            && !$manager->canUpdateAttribute($status, $newStatus, $attribute);
                        break;
                    default:
                        $error = $this->getOldAttribute($attribute) != $this->getAttribute($attribute)
                            && !$manager->canUpdateAttribute($status, $newStatus, $attribute);
                }


                if ($error) {

                    throw new InvalidAttributeValueException(
                        t('order', 'It is forbidden to edit the value of "{attribute}"', [
                            'attribute' => $this->getAttributeLabel($attribute),
                        ]));
                }
            }
        }

    }

    /**
     * Check order status
     * @throws InvalidAttributeValueException
     * @throws \yii\base\InvalidConfigException
     */
    private function checkOrderStatus()
    {
        if ($this->getOldAttribute('status_id') != $this->getAttribute('status_id')) {
            $newStatus = OrderStatusRecord::findOne($this->getAttribute('status_id'));
            $oldStatus = OrderStatusRecord::findOne($this->getOldAttribute('status_id'));

            /* @var $manager OrderChangeRuleManager */
            $manager = \Yii::createObject(OrderChangeRuleManager::class);

            if (!$manager->canUpdateStatus($oldStatus, $newStatus)) {
                throw new InvalidAttributeValueException(
                    t('order', 'You can not change the order status to "{status}"', [
                        'status' => t('status_event', t('order', $newStatus->name)),
                    ]));
            }
        }
    }

    /*
     * Check attribute `worker_id`
     * @throws ForbiddenChangeWorkerException
     * @throws \yii\base\InvalidConfigException
     * */
    private function checkCompany()
    {
        if($this->device == OrderStatus::TYPE_HOSPITAL){

            $newCompanyId = $this->getAttribute('company_id');
            $oldCompanyId = $this->getOldAttribute('company_id');
            $statusId = $this->getAttribute('status_id');

            if(empty($newCompanyId) && in_array($statusId, [
                    OrderStatus::STATUS_FREE_FOR_COMPANY,
                    OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY,
                ], false)
            ){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'You need select the company'));
            }

            if($newCompanyId == $oldCompanyId){
                return;
            }

            if(isset($oldCompanyId) && empty($newCompanyId)){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'Before delete a company, you need select action "Unlink the company".'));
            }

            if(isset($oldCompanyId) && isset($newCompanyId) && ($newCompanyId !== $oldCompanyId)){

                throw new ForbiddenChangeWorkerException(
                    t('order', 'Before assigning a company, you need select action "Unlink the company".'));
            }

            if(empty($oldCompanyId) && !empty($newCompanyId)){
                if(!in_array($statusId, [
                    OrderStatus::STATUS_FREE_FOR_COMPANY,
                    OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY,
                ], false)
                ){
                    throw new ForbiddenChangeWorkerException(
                        t('order', 'To assign company to order select action "Assign a company"'));
                }
            }
        }


    }

    /**
     * Check attribute `worker_id`
     * @throws ForbiddenChangeWorkerException
     * @throws \yii\base\InvalidConfigException
     */
    private function checkWorker()
    {
        $newWorkerId = $this->getAttribute('worker_id');
        $oldWorkerId = $this->getOldAttribute('worker_id');
        $statusId = $this->getAttribute('status_id');


        if (empty($newWorkerId) && in_array($statusId, [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
            ], false)
        ) {
            throw new ForbiddenChangeWorkerException(
                t('order', 'You need select the worker'));
        }

        if ($newWorkerId == $oldWorkerId) {
            return;
        }


        if (empty($oldWorkerId) && !empty($newWorkerId)) {
            if (!in_array($statusId, [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
            ], false)
            ) {
                throw new ForbiddenChangeWorkerException(
                    t('order', 'To assign worker to order select action "Assign a worker"'));
            }

            // нельзя назначать на водителя - если его нет на смене  и статус заказа текущий
           /* if(in_array($statusId, [
                OrderStatus::STATUS_NEW,
                OrderStatus::STATUS_OFFER_ORDER,
                OrderStatus::STATUS_WORKER_REFUSED,
                OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_REFUSED_ORDER_ASSIGN,
                OrderStatus::STATUS_FREE,
                OrderStatus::STATUS_NOPARKING,
                OrderStatus::STATUS_NOPARKING,
                OrderStatus::STATUS_MANUAL_MODE,
                OrderStatus::STATUS_OVERDUE,
            ], false)){
                var_dump(25);
                $workerService = $this->getWorkerService();
                $worker = $workerService->getActiveWorker($this->tenant_id, $this->worker->callsign);
                if (empty($worker)) {
                    throw new ForbiddenChangeWorkerException(
                        t('order', 'The driver must be on shift'));
                }
            }*/

            // проверка на наличие водителя
            if(!isset($this->worker) || is_null($this->worker)){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'Worker not exist'));
            }

            $position = array_shift(ArrayHelper::toArray($this->worker->positions));
            $position_id = ArrayHelper::getValue($position, 'position_id');

            // проверка на позицию водителя
            if(!isset($position_id)){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'Worker must have position'));
            }
            $order_city= $this->city_id;

            // больничный заказ - привязываем к компании
            $company_id = ArrayHelper::getValue($this->worker,'tenant_company_id');

            if(!is_int($company_id) ){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'The worker can not accept the order offer. Worker has not company'));
            }
            elseif($this->device == OrderStatus::TYPE_HOSPITAL && (is_int($this->company_id)) && ($company_id != $this->company_id ) ){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'The worker can not accept the order offer. The driver belongs to another company'));
            }

            $this->company_id = $company_id;


            // запись данных в логи
            if( empty($position_id) || is_null($position_id) || empty($order_city)){
                throw new ForbiddenChangeWorkerException(
                    t('order', 'The driver must be have position_id and city'));
            }


            $nodeApi = new ServiceEngine();
            if (!$nodeApi->canOfferOrderToWorker($this->tenant_id, $this->order_id, $this->worker->callsign, $position_id, $order_city )
            ) {
                throw new ForbiddenChangeWorkerException(
                    t('order', 'The worker can not accept the order offer'));
            }
        } else {
            throw new ForbiddenChangeWorkerException(
                t('order', 'You need to freeze order to update worker'));
        }
    }

    private function setClientId()
    {
        $client = $this->getClientByPhone(
            $this->phone, $this->tenant_id, $this->city_id);
        if (empty($client)) {
            throw new ErrorException('Не удалось создать клиента с параметрами: phone:' . $this->phone .
                '; tenant_id: ' . $this->tenant_id . '; city_id: ' . $this->city_id);
        }
        $this->client_id = $client->client_id;
    }

    private function getPreparedAddress()
    {
        return serialize($this->determineParkingAndAddressCoords(
            $this->addressFilter($this->address)));
    }

    private function getNextOrderNumber()
    {
        return $this->getLastOrderNumber() + 1;
    }

    public function getTimeOffset()
    {
        return City::getTimeOffset($this->city_id);
    }

    private function getStatusTime()
    {
        return $this->getOldAttribute('status_id') != $this->status_id ? time() : $this->status_time;
    }

    private function isInvalidOrderTime()
    {
        return $this->isNeedCheckOrderTime() && $this->order_time < $this->getOrderTimestamp();
    }


    /**
     * @throws BlockedCompanyException
     */
    private function checkValidCompany()
    {
        $company = ClientCompany::findOne($this->company_id);

        if ($company !== null && (int)$company->block === 1) {
            throw new BlockedCompanyException(t('order', 'Company is blocked.'));
        }
    }

    /**
     * @throws InvalidClientCard
     */
    private function checkValidCard()
    {
        /** @var Card $card */
        $card = app()->get('card');

        if (!$card->isValid($this->tenant_id, $this->city_id, $this->client_id, $this->pan)) {
            throw new InvalidClientCard();
        }
    }

    protected function setAttributesSubModels()
    {
        foreach ($this->subModels() as $subModel) {
            $this->setAttributes($subModel->getAttributes(), false);
        }
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws BlockedCompanyException
     * @throws InvalidAttributeValueException
     * @throws InvalidParamException
     * @throws ForbiddenChangeOrderTimeException
     * @throws \Exception
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        $this->setAttributesSubModels();

        if($insert){
            $this->updateOrderStatus();
        }

        if (!$insert) {
            if (!$insert && !$this->isActiveOrder()) {
                throw new \Exception(t('order', 'You can not edit the completed order'));
            }


            // search/create client by phone number
            if (empty($this->client_id) || $this->isAttributeChanged('phone')) {
                $this->setClientId();
            }


            if (strpos($this->payment, self::PAYMENT_CORP) !== false) {
                $this->payment = self::PAYMENT_CORP;

                $this->checkValidCompany();
            }


            if (!empty($this->pan)) {
                $this->checkValidCard();
            }

            if (!$this->validate(['tariff_id'])) {
                throw new InvalidAttributeValueException(implode(', ', $this->getFirstErrors()));
            }

            $this->orderAddress->validateAddress();

            if (is_array($this->address)) {
                $this->address = $this->getPreparedAddress();
            }

            if ($insert) {
                $this->order_number = $this->getNextOrderNumber();

                if (!in_array($this->status_id, [
                    OrderStatus::STATUS_NEW,
                    OrderStatus::STATUS_FREE,
                    OrderStatus::STATUS_MANUAL_MODE,
                ], false)
                ) {
                    $this->status_id = OrderStatus::STATUS_NEW;
                }
            }

            $this->order_time = $this->getOrderTime();

            if ($this->isInvalidOrderTime()) {
                $statuses = array_merge(OrderStatus::getStatusWithPickUpTime(), [
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                ]);
                if (in_array($this->status_id, $statuses, false)) {
                    // need for generate current time in method getOrderTime
                    $this->order_time = null;
                    $this->order_time = $this->getOrderTime();
                } else {
                    throw new InvalidOrderTimeException(t('order', 'Order time is incorrect'));
                }
            }

            $this->updateOrderStatus();

            $this->status_time = $this->getStatusTime();

            $this->arOldAttributes = $this->getOldAttributes();

            $this->setCurrencyId();

            $this->time_offset = $this->getTimeOffset();

            $this->checkCompany();
            $this->checkWorker();

            if (!$insert) {
                //$this->checkOrderStatus();
                $this->checkChangedAttributes();
            }
        }


        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            $isAddOptionsChanged = true;
            if (!$insert) {
                if (is_array($this->additional_option)) {
                    OrderHasOptionRecord::deleteAll(['order_id' => $this->order_id]);
                } else {
                    $isAddOptionsChanged = false;
                }

                //Формирование лога обновлений
                //Сравниваем старые и новые значения атрибутов, получаеем новые значения
                $newAttributes = $this->getAttributes();
                // через array_diff нельзя сравнивать вложенные массивы идет явное приведение типа к сроке (string)value
                unset($newAttributes['additional_option']);
                $order_diff = array_diff_assoc($newAttributes, $this->arOldAttributes);

                // todo daimond  если изменился водитель
                /*  if(isset($order_diff['worker_id'])){
                    $orderChange = new OrderChangeData();
                    $orderChange->tenant_id = $this->tenant_id;
                    $orderChange->order_id = $this->order_id;
                    $orderChange->change_field = 'status';
                    $orderChange->change_object_id = (string)$this->worker->callsign;;
                    $orderChange->change_object_type = 'worker';
                    $orderChange->change_subject = 'worker';
                    $orderChange->change_val = Worker::STATUS_ACCEPT_PREORDER;
                    $orderChange->change_time = (int)time();
                }*/


                //Проверяем изменения доп.опций
                if ($isAddOptionsChanged) {
                    $order_diff['additional_option'] = serialize($this->additional_option);
                }


                //Все изменения логируем
                OrderChangeData::userLog($this->order_id, $this->tenant_id, $this->subject, $order_diff);
                //-----------------------------------
            }

            //Сохранение доп. пожеланий
            if ($isAddOptionsChanged) {
                OrderHasOption::manySave($this->additional_option, $this->order_id);
            }

            if (!empty($this->pan)) {
                CardPayment::deleteAll(['order_id' => $this->order_id]);
                (new CardPayment(['order_id' => $this->order_id, 'pan' => $this->pan]))->save();
            }


            //Сохранение заказа в редис
            if (!array_key_exists('status_id', $changedAttributes)
                || $changedAttributes['status_id'] != OrderStatus::STATUS_COMPLETED_NOT_PAID
            ) {
                $redisSaveResult = $this->saveActiveOrder();

                if (($redisSaveResult != 1 && $insert) || ($redisSaveResult != 0 && !$insert)) {
                    throw new ErrorException('Error to save order to redis');
                }
            }
        }
    }

    public function getOrderCostData()
    {
        $orderIds = OrderHasOption::getOptionIds($this->order_id);

        $taxiRouteAnalyzer = app()->routeAnalyzer;
        $routeCostInfo = $taxiRouteAnalyzer->analyzeRoute($this->tenant_id, $this->city_id,
            unserialize($this->address),
            $orderIds, $this->tariff_id, date('d.m.Y H:i:s', $this->order_time));

        if (empty($routeCostInfo)) {
            throw new ErrorException('No $routeCostInfo in OrderRecord::getOrderCostData()');
        }

        $costData = [];
        $result = (array)$routeCostInfo;

        if (!isset($result['tariffInfo']) || empty($result['tariffInfo'])) {
            throw new ErrorException('No tariffInfo in OrderRecord::getOrderCostData()');
        }

        $costData['additionals_cost'] = isset($result['additionalCost']) ? (string)$result['additionalCost'] : null;
        $costData['summary_time'] = isset($result['summaryTime']) ? (string)$result['summaryTime'] : null;
        $costData['summary_distance'] = isset($result['summaryDistance']) ? (string)$result['summaryDistance'] : null;
        $costData['city_time'] = isset($result['cityTime']) ? (string)$result['cityTime'] : null;
        $costData['city_distance'] = isset($result['cityDistance']) ? (string)$result['cityDistance'] : null;
        $costData['city_cost'] = isset($result['cityCost']) ? (string)$result['cityCost'] : null;
        $costData['out_city_time'] = isset($result['outCityTime']) ? (string)$result['outCityTime'] : null;
        $costData['out_city_distance'] = isset($result['outCityDistance']) ? (string)$result['outCityDistance'] : null;
        $costData['out_city_cost'] = isset($result['outCityCost']) ? (string)$result['outCityCost'] : null;
        $costData['enableParkingRatio'] = empty($result['enableParkingRatio']) ? 0 : 1;

        if ($this->is_fix == 1) {
            $costData['summary_cost'] = $this->predv_price;
            $costData['is_fix'] = $this->is_fix;
        } else {
            $costData['is_fix'] = empty($result['isFix']) ? 0 : 1;
            $costData['summary_cost'] = isset($result['summaryCost']) ? (string)$result['summaryCost'] : null;
        }
        $costData['tariffInfo'] = $result['tariffInfo'];
        $costData['start_point_location'] = isset($result['startPointLocation']) ? (string)$result['startPointLocation'] : null;

        return $costData;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->passenger->setAttributes([
            'client_passenger_id'    => $this->getAttribute('client_passenger_id'),
            'client_passenger_phone' => $this->getAttribute('client_passenger_phone')
        ]);

        $this->address = unserialize($this->address);
        if ($this->address === false) {
            $this->address = [
                'A' => [
                    'city'       => '',
                    'city_id'    => '',
                    'street'     => '',
                    'house'      => '',
                    'housing'    => '',
                    'porch'      => '',
                    'apt'        => '',
                    'parking_id' => '',
                    'lat'        => '',
                    'lon'        => '',
                ],
            ];
        }
    }


    /**
     * Find client by phone number and create new client if not found
     *
     * @param string $phone
     * @param int $tenantId
     * @param int $cityId
     *
     * @return Client|bool
     */
    private function getClientByPhone($phone, $tenantId, $cityId)
    {
        $client = Client::find()
            ->joinWith('clientPhones', false)
            ->where(['tenant_id' => $tenantId, 'value' => $phone])
            ->one();

        if (empty($client)) {
            $transaction = app()->db->beginTransaction();
            try {
                $client = new Client([
                    'tenant_id' => $tenantId,
                    'city_id'   => $cityId,
                    'phone'     => [$phone],
                ]);

                $client->save();

                $clientPhone = new ClientPhone([
                    'client_id' => $client->client_id,
                    'value'     => $phone,
                ]);

                if ($clientPhone->save()) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();

                    return false;
                }
            } catch (\Exception $exc) {
                $transaction->rollBack();

                return false;
            }
        }

        return $client;
    }

    public function isPreOrder()
    {
        $pre_order_time = TenantSetting::getSettingValue($this->tenant_id, 'PRE_ORDER', $this->city_id,
            $this->position_id);
        $pre_order_time = $pre_order_time * 60;

        if (empty($pre_order_time)) {
            $pre_order_time = self::PRE_ODRER_TIME;
        }

        $now = $this->getOrderTimestamp();


        return ($this->getOrderTime() - $now) >= $pre_order_time;
    }

    /**
     * Getting last order number.
     * @return false|null|string
     */
    private function getLastOrderNumber()
    {
        return (new Order(['tenantId' => $this->tenant_id]))->getLastOrderNumber();
    }

    private function addressFilter(array $address)
    {
        $index = 0;
        $filteredAddress = [];
        $currentKey = 'A';

        $maxNumberOfAddressPoints = DefaultSettings::getSetting(
            DefaultSettings::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS, 0);

        foreach ($address as $key => $value) {
            if ($index >= $maxNumberOfAddressPoints) {
                break;
            }

            if ($value['lat'] && $value['lon']) {
                $filteredAddress[$currentKey] = $address[$key];
                $currentKey++;
                $index++;
            }
        }

        return $filteredAddress;
    }

    /**
     * @return int
     */
    private function getOrderTime()
    {
        if (!empty($this->order_time)) {
            return $this->order_time;
        }

        if (in_array($this->status_id, OrderStatus::getStatusWithPickUpTime())) {
            return $this->getOrderTimestamp() + $this->getPickUp();
        }

        return $this->getOrderTimestamp();
    }

    /**
     * Время подачи авто.
     * @return integer Time in seconds.
     */
    private function getPickUp()
    {
        if (empty($this->pickUp)) {
            $this->pickUp = TenantSetting::getSettingValue($this->tenant_id, 'PICK_UP', $this->city_id,
                $this->position_id);

            if (empty($this->pickUp)) {
                $this->pickUp = self::PICK_UP_TIME;
            }
        }

        return $this->pickUp;
    }

    /**
     * Saving order to Redis
     * @return mixed
     */
    public function saveActiveOrder()
    {
        $costData = $this->getOrderCostData();
        $tariffId = isset($costData["tariffInfo"]["tariffDataCity"]["tariff_id"]) ? $costData["tariffInfo"]["tariffDataCity"]["tariff_id"] : null;

        $order_query = self::find()
            ->where([
                'order_id' => $this->order_id,
            ])
            ->with([
                'status',
                'tariff',
                'tariff.class'              => function (ActiveQuery $query) {
                    $query->select(['class_id', 'class']);
                },
                'client'                    => function (ActiveQuery $query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang']);
                },
                'userCreated'               => function (ActiveQuery $query) {
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'options.additionalOptions' => function (ActiveQuery $query) use ($tariffId) {
                    $query->select(['id', 'additional_option_id', 'price'])->where([
                        'tariff_id'   => $tariffId,
                        'tariff_type' => "CURRENT",
                    ]);
                },
                'tenant'                    => function (ActiveQuery $query) {
                    $query->select(['domain']);
                },
                'worker',
            ]);

        if(is_int($this->company_id)){
            $order_query->with([
                'worker_company' => function (ActiveQuery $query) use ($tariffId) {
                    $query->select(['tenant_company_id', 'name', 'phone', 'user_contact']);
                }
            ]);
        }
        $order = $order_query->asArray()->one();

        $order['tenant_login'] = $order['tenant']['domain'];
        $order['costData'] = $costData;

        if (!empty($order['worker'])) {
            $workerData = (new ActiveWorkerRepository(['tenantId' => $this->tenant_id]))->getOne($order['worker']['callsign']);
            if (!empty($workerData["worker"])) {
                $order["worker"] = $workerData["worker"];
            }
            if ($workerData["position"] && $workerData["position"]["has_car"]) {
                $order["car_id"] = $workerData["car"]["car_id"];
                $order["car"] = $workerData["car"];
            }
        }

        if (!$this->isNewRecord) {
            $orderOldData = $this->getActiveOrder();
            $order['status']['last_status_id'] = $orderOldData['status']['last_status_id'];
            $order['status']['last_status_group'] = $orderOldData['status']['last_status_group'];


            if (isset($orderOldData['settings'])) {
                $order['settings'] = $orderOldData['settings'];
            }
            if (isset($orderOldData['server_id'])) {
                $order['server_id'] = $orderOldData['server_id'];

            }
            if (isset($orderOldData['process_id'])) {
                $order['process_id'] = $orderOldData['process_id'];
            }

            //@TODO удалить это кусок после релиза ветки `5830_create_order_through_api_order`
            if (isset($orderOldData['offered_workers_list'])) {
                $order['offered_workers_list'] = $orderOldData['offered_workers_list'];
            }

            // массив водителей отказавших от заказа
            if (isset($orderOldData['refuse_workers_list'])) {
                $order['refuse_workers_list'] = $orderOldData['refuse_workers_list'];
            }

            if (isset($orderOldData['workers_offered'])) {
                $order['workers_offered'] = $orderOldData['workers_offered'];
            }

            if (isset($orderOldData['workers_offered_responses'])) {
                $order['workers_offered_responses'] = $orderOldData['workers_offered_responses'];
            }

            if (array_key_exists('originally_preorder', $orderOldData)) {
                $order['originally_preorder'] = $orderOldData['originally_preorder'];
            }

            if (array_key_exists('exchange_info', $orderOldData)) {
                $order['exchange_info'] = $orderOldData['exchange_info'];
            }
        }

        $orderSerialized = serialize($order);
        return (new ActiveOrderRepository(['tenantId' => $this->tenant_id]))->set($this->order_id, $orderSerialized);
    }

    /**
     * Return waiting time, when worker wait for free.
     * @return integer Time in seconds.
     */
    public function getWorkerWaitingTime()
    {
        $waiting_time = 0;

        if (empty($this->costData)) {
            $order = $this->getActiveOrder();

            if (!empty($order['costData'])) {
                $this->costData = $order['costData'];
            }
        }

        $tariffAreaKey = isset($this->costData['start_point_location']) && $this->costData['start_point_location'] == 'in' ?
            'tariffDataCity' : 'tariffDataTrack';
        $tariffWaitingTimeKey = isset($this->costData['tariffInfo']['isDay']) && $this->costData['tariffInfo']['isDay'] ?
            'wait_time_day' : 'wait_time_night';

        if (isset($this->costData['tariffInfo'][$tariffAreaKey][$tariffWaitingTimeKey])) {
            $waiting_time = $this->costData['tariffInfo'][$tariffAreaKey][$tariffWaitingTimeKey];
        }

        return $waiting_time * 60;
    }

    /**
     * Return active order from redis.
     * @return array
     */
    public function getActiveOrder()
    {
        return (new ActiveOrdersProvider(['tenantId' => $this->tenant_id]))->getOne($this->order_id);
    }

    public function getDeviceName()
    {
        switch ($this->device) {
            case self::DEVICE_0:
                $name = t('order', 'Dispatcher');
                break;
            case 'IOS':
                $name = 'IOS';
                break;
            case 'ANDROID':
                $name = 'Android';
                break;
            case 'WORKER':
                $name = t('order', 'Border');
                break;
            default :
                $name = '';
        }

        return $name;
    }

    /**
     * Getting order address
     *
     * @param int $orderId
     *
     * @return array
     */
    public static function getOrderAddress($orderId)
    {
        return self::findOne($orderId)->address;
    }

    public function getFormatStreet($point = 'A')
    {
        $street_name = $this->address[$point]['street'] . ' ' . $this->address[$point]['house'];

        if (!empty($this->address[$point]['housing'])) {
            $street_name .= ' ' . t('order', 'b.') . ' ' . $this->address[$point]['housing'];
        }

        return trim($street_name);
    }

    private function setCurrencyId()
    {
        $this->currency_id = TenantSetting::getSettingValue(
            $this->tenant_id, TenantSetting::CURRENCY_TYPE, $this->city_id);
    }

    public function trySaveOrder()
    {
        $retryCount = app()->params['saveOrder.retryCount'];
        for ($i = 0; $i < $retryCount; $i++) {
            try {
                return $this->save(false);
            } catch (\Exception $ex) {
                if (strpos($ex->getMessage(), 'ui_order__tenant_id__order_number') === false
                    && strpos($ex->getMessage(),'Deadlock found when trying to get lock; try restarting transaction') === false) {
                    throw $ex;
                } else {
                    $this->delay();
                    continue;
                }
            }
        }

        return false;
    }

    protected function delay()
    {
        usleep(mt_rand(0, 500000));
    }

    public function isCardPayment()
    {
        return $this->payment == self::PAYMENT_CARD;
    }

    public function prepareAddress()
    {
        $this->orderAddress->prepareAddress();
    }

    public function isFixPrice()
    {
        return $this->is_fix == 1;
    }

    public function validateAddress()
    {
        $this->orderAddress->validateAddress();
    }

    public function serializeAndPrepareAddress()
    {
        $this->orderAddress->serializeAndPrepareAddress();
    }

    public function prepareStatusForPanel()
    {
        $this->orderStatus->prepareStatusForPanel();
    }

    public function identifyAndSetStatus()
    {
        $this->orderStatus->identifyAndSetStatus();
    }

    public function getOrderTimeForSave()
    {
        return $this->orderTime->getOrderTime();
    }

    public function getClientPassengerId()
    {
        return $this->passenger->client_passenger_id;
    }

    public function setClientPassengerId($value)
    {
        $this->passenger->client_passenger_id = $value;
    }

    public function getClientPassengerPhone()
    {
        return $this->passenger->client_passenger_phone;
    }

    public function setClientPassengerPhone($value)
    {
        $this->passenger->client_passenger_phone = $value;
    }

    public function isGoPassenger()
    {
        return $this->passenger->isGoPassenger();
    }

    /**
     * @return dto\Name
     */
    public function getNamePassenger()
    {
        return $this->passenger->getName();
    }

    /**
     * @return Name
     */
    public function getNameClient()
    {
        $name             = new Name();
        $name->name       = $this->client_name ? $this->client_name : $this->passenger->client_passenger_name;
        $name->secondName = $this->client_secondname ? $this->client_secondname : $this->passenger->client_passenger_secondname;
        $name->lastName   = $this->client_lastname ? $this->client_lastname: $this->passenger->client_passenger_lastname;
        return $name;

       /* $name             = new Name();
        $name->name       = $this->client_name;
        $name->secondName = $this->client_secondname;
        $name->lastName   = $this->client_lastname;

        return $name;*/
    }

    public function getNameFullClient()
    {
        $name             = new Name();
        $name->name       = $this->passenger->client_passenger_name;
        $name->secondName = $this->passenger->client_passenger_secondname;
        $name->lastName   = $this->passenger->client_passenger_lastname;
        return $name;
    }


    public function updateOrderExceptCarModels()
    {
        $currentOrderExceptCarModelsIds = $this->getOrderExceptCarModels()->select('car_model_id')->column();
        $newOrderExceptCarModelsIds     = $this->except_car_models ? $this->except_car_models : [];

        $toInsert                        = [];
        $toInsertOrderExceptCarModelsIds = array_filter(array_diff($newOrderExceptCarModelsIds, $currentOrderExceptCarModelsIds));

        foreach ($toInsertOrderExceptCarModelsIds as $orderExceptCarModelsId) {
            $toInsert[] = [
                'order_id'     => $this->order_id,
                'car_model_id' => $orderExceptCarModelsId,
                'updated_at'   => time(),
                'created_at'   => time(),
            ];
        }

        if ($toInsert) {
            OrderExceptCarModels::getDb()
                ->createCommand()
                ->batchInsert(
                    OrderExceptCarModels::tableName(),
                    ['order_id', 'car_model_id', 'updated_at', 'created_at'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentOrderExceptCarModelsIds, $newOrderExceptCarModelsIds))) {
            OrderExceptCarModels::deleteAll(['order_id' => $this->order_id, 'car_model_id' => $toRemove]);
        }
    }



    private function getWorkerService()
    {
        $service = \Yii::$container->get('workerService');
        return $service;
    }
}
