<?php


namespace app\modules\v1\models;


use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;

abstract class OrdersFactory
{
    const ACTIVE_TYPE = [
        OrderStatus::STATUS_GROUP_NEW,
        OrderStatus::STATUS_GROUP_IN_HAND,
        OrderStatus::STATUS_GROUP_PRE_ORDER,
    ];
    const WARNING_TYPE = [
        OrderStatus::STATUS_GROUP_WARNING,
    ];
    const COMPLETED_TYPE = [
        OrderStatus::STATUS_GROUP_COMPLETED,
        OrderStatus::STATUS_GROUP_REJECTED,
    ];

    /**
     * @param array $params Configuration array for "app\modules\v1\models\order\Orders" instanse.
     * Must be contain a "statusGroup" field
     * @return ActiveOrdersProvider|CompletedOrdersProvider|WarningOrdersProvider
     * @throws NotSupportedException
     * @throws InvalidConfigException
     */
    public static function getInstance($params)
    {
        if (!isset($params['statusGroup'])) {
            throw new InvalidConfigException('The field "statusGroup" must be set');
        }

        if (in_array($params['statusGroup'], self::ACTIVE_TYPE)) {
            $orderObject = (new ActiveOrdersProvider($params));
        } elseif (in_array($params['statusGroup'], self::WARNING_TYPE)) {
            $orderObject = (new WarningOrdersProvider($params));
        } elseif (in_array($params['statusGroup'], self::COMPLETED_TYPE)) {
            $orderObject = (new CompletedOrdersProvider($params));
        } else {
            throw new NotSupportedException('The order group "' . $params['statusGroup'] . '" is not supported');
        }

        return $orderObject;
    }
}