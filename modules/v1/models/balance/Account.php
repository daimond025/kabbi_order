<?php

namespace app\modules\v1\models\balance;

use Yii;

/**
 * Class Account
 * @package app\modules\v1\models\balance
 * @property float balance
 */
class Account extends \yii\db\ActiveRecord
{

    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;
    const TENANT_KIND = 1;
    const DRIVER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;
    const DEFAULT_CURRENCY = 1;
    const CLIENT_BONUS = 7;
    const SYSTEM_BONUS = 8;

    public $sum;
    public $transaction_comment;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }
}
