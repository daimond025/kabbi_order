<?php


namespace app\modules\v1\models\orderDataUpdaters;


use yii\base\Object;

abstract class OrderUpdaterBase extends Object
{
    public $orderId;
    public $updateTime;
    public $uuid;
    public $tenantId;
    public $senderId;
    public $lang;

    /**
     * @param array $data
     *
     * @return mixed
     */
    abstract public function update($data);

    /**
     * Getting prepared response
     *
     * @param int    $code
     * @param string $info
     * @param string $result
     *
     * @return array
     */
    protected function getResponse($code, $info, $result)
    {
        return [
            'uuid'     => $this->uuid,
            'order_id' => $this->orderId,
            'type'     => 'response',
            'code'     => $code,
            'info'     => $info,
            'result'   => $result,
        ];
    }
}