<?php


namespace app\modules\v1\models\orderDataUpdaters;


use app\components\OrderError;
use app\modules\v1\models\Order;
use app\components\exchange\exceptions\ExchangeServiceException;
use app\components\exchange\exceptions\NoRelevantExchangeConnections;
use GuzzleHttp\Exception\ClientException;
use app\components\exchange\ServiceExchange;



class OrderOperatorUpdater extends OrderUpdaterBase
{
    public function update($data)
    {
        $result = (new Order())->update($this->orderId, $this->tenantId, $data, $this->updateTime, $this->lang);

        return $this->getResponse($result['code'], $result['info'], 1);
    }


    public function sendToExchange($data)
    {
        /**
         * @var $serviceExchange ServiceExchange
         */
        $serviceExchange = \Yii::$app->serviceExchange;
        $result = new OrderError();
        try {
            $serviceExchange->sendToExchange($this->tenantId, $this->orderId, $data['subject']);
            $result->setErrorCode(OrderError::SUCCESS)
                ->setErrorMessage(t('exchange', 'Order sent ot exchange', [], $this->lang));
            return $result->getData();
        } catch (ClientException $ex) {
            $responseBody = $ex->getResponse()->getBody()->getContents();
            $responseBody = json_decode($responseBody, true);
            $result->setErrorCode(OrderError::INVALID_VALUE)
                ->setErrorMessage(t('exchange', $responseBody['message'], [], $this->lang));
            return $result->getData();
        } catch (NoRelevantExchangeConnections $ex) {
            $result->setErrorCode(OrderError::NO_RELEVANT_EXCHANGE_CONNECTIONS)
                ->setErrorMessage(t('exchange', 'No active relevant exchange connections', [], $this->lang));
            return $result->getData();
        } catch (ExchangeServiceException $ex) {
            $result->setErrorCode(OrderError::INVALID_VALUE)
                ->setErrorMessage(t('exchange', $ex->getMessage()));
            return $result->getData();
        } catch (\Exception $ex) {
            \Yii::error($ex);
            $result->setErrorCode(OrderError::INTERNAL_ERROR)
                ->setErrorMessage(t('exchange', 'Can not send order to exchange', [], $this->lang));
            return $result->getData();
        }

    }
}