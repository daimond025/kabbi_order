<?php


namespace app\modules\v1\models\orderDataUpdaters;


use app\modules\v1\models\workers\WorkerSetOrderStatus;

class OrderWorkerUpdater extends OrderUpdaterBase
{
    public function update($data)
    {

        $result = (new WorkerSetOrderStatus())->update($this->orderId, $this->tenantId, $data, $this->updateTime,
            $this->lang, $this->uuid);


       // \Yii::warning('daimond_rezult' . json_encode($result) );


        return $this->getResponse($result['code'], $result['info'],
            isset($result['result']) ? $result['result'] : null);
    }
}