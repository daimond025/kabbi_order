<?php

namespace app\modules\v1\models\orderDataUpdaters;

use app\modules\v1\models\client\ClientUpdateOrder;


/**
 * Class OrderWorkerUpdater
 * @package app\modules\v1\models
 */
class OrderClientUpdater extends OrderUpdaterBase
{
    public function update($data)
    {
        $result = (new ClientUpdateOrder())->update(
            $this->tenantId, $this->orderId, $data, $this->updateTime, $this->lang, $this->uuid);

        return $this->getResponse($result['code'], $result['info'],
            isset($result['result']) ? $result['result'] : null);
    }
}