<?php

namespace app\modules\v1\models\orderDataUpdaters;


use app\modules\v1\models\exchange\ExchangeUpdateOrder;

class OrderExchangeUpdater extends OrderUpdaterBase
{

    /**
     * Update order
     * @param array $data
     * @return array|mixed
     */
    public function update($data)
    {
        $result = (new ExchangeUpdateOrder())->update($this->orderId, $this->tenantId, $data, $this->updateTime, $this->lang);
        return $this->getResponse($result['code'], $result['info'], 1);
    }

}