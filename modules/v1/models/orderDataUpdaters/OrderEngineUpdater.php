<?php

namespace app\modules\v1\models\orderDataUpdaters;


use app\components\EngineError;
use app\components\exchange\exceptions\ExchangeServiceException;
use app\components\exchange\exceptions\NoRelevantExchangeConnections;
use app\components\exchange\ServiceExchange;
use app\modules\v1\models\engine\EngineUpdateOrder;
use GuzzleHttp\Exception\ClientException;

class OrderEngineUpdater extends OrderUpdaterBase
{
    /**
     * Update order
     * @param array $data
     * @return array|mixed
     */
    public function update($data)
    {
        $result = (new EngineUpdateOrder())->update($this->orderId, $this->tenantId, $data, $this->updateTime, $this->lang);
        return $this->getResponse($result['code'], $result['info'], 1);
    }

    /**
     * Send order to exchange
     * @param $data
     * @return array
     */
    public function sendToExchange($data)
    {
        /**
         * @var $serviceExchange ServiceExchange
         */
        $serviceExchange = \Yii::$app->serviceExchange;
        $result = new EngineError();
        try {
            $serviceExchange->sendToExchange($this->tenantId, $this->orderId, $data['subject']);
            $result->setErrorCode(EngineError::SUCCESS)
                ->setErrorMessage(t('exchange', 'Order sent ot exchange', [], $this->lang));
            return $result->getData();
        } catch (ClientException $ex) {
            $responseBody = $ex->getResponse()->getBody()->getContents();
            $responseBody = json_decode($responseBody, true);
            $result->setErrorCode(EngineError::INVALID_VALUE)
                ->setErrorMessage(t('exchange', $responseBody['message'], [], $this->lang));
            return $result->getData();
        } catch (NoRelevantExchangeConnections $ex) {
            $result->setErrorCode(EngineError::NO_RELEVANT_EXCHANGE_CONNECTIONS)
                ->setErrorMessage(t('exchange', 'No active relevant exchange connections', [], $this->lang));
            return $result->getData();
        } catch (ExchangeServiceException $ex) {
            $result->setErrorCode(EngineError::INVALID_VALUE)
                ->setErrorMessage(t('exchange', $ex->getMessage()));
            return $result->getData();
        } catch (\Exception $ex) {
            \Yii::error($ex);
            $result->setErrorCode(EngineError::INTERNAL_ERROR)
                ->setErrorMessage(t('exchange', 'Can not send order to exchange', [], $this->lang));
            return $result->getData();
        }

    }
}