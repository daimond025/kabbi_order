<?php

namespace app\modules\v1\models;

use Yii;

/**
 * This is the model class for table "{{%car_model}}".
 *
 * @property string  $model_id
 * @property integer $brand_id
 * @property string  $name
 * @property integer $type_id
 *
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_model}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('car', 'Model ID'),
            'brand_id' => Yii::t('car', 'Brand ID'),
            'name'     => Yii::t('car', 'Name'),
            'type_id'  => Yii::t('car', 'Type'),
        ];
    }

}
