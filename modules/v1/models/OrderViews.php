<?php

namespace app\modules\v1\models;

use app\models\OrderViews as OrderViewsRecord;
use yii\helpers\ArrayHelper;


class OrderViews
{
    /**
     * Allow to save multiple rows
     * @param array $arCity
     * @param array $user_id
     * @param array $tenant_id
     * @return boolean
     */
    public static function batchInsert($arCity, $user_id, $tenant_id)
    {
        if (is_array($arCity) && !empty($arCity)) {
            $insertValue = [];
            $connection = app()->db;

            foreach ($arCity as $city_id) {
                if ($city_id > 0) {
                    $insertValue[] = [$user_id, $tenant_id, $city_id];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()->batchInsert(OrderViewsRecord::tableName(),
                    ['user_id', 'tenant_id', 'city_id'], $insertValue)->execute();

                return true;
            }
        }

        return false;
    }

    public static function refreshRows($user_id, $tenant_id, $user_city_list)
    {
        $order_views = OrderViewsRecord::find()
            ->where(['user_id' => $user_id])
            ->select(['view_id', 'user_id', 'city_id'])
            ->all();

        if (!is_null($order_views)) {
            //Очистка от старых запией
            foreach ($order_views as $order_view) {
                if (!in_array($order_view->city_id, $user_city_list)) {
                    $order_view->delete();
                }
            }

            //Поиск новых
            $order_views_city = ArrayHelper::getColumn($order_views, 'city_id');
            $user_city_list = array_diff($user_city_list, $order_views_city);
        }

        self::batchInsert($user_city_list, $user_id, $tenant_id);
    }
}
