<?php


namespace app\modules\v1\models;


use app\components\exchange\ServiceExchange;
use app\components\OrderError;
use app\models\ActiveOrderRepository;
use app\models\Call;
use app\models\Worker;
use app\modules\v1\models\exceptions\BlockedCompanyException;
use app\modules\v1\models\exceptions\CompleteOrderException;
use app\modules\v1\models\exceptions\ForbiddenChangeOrderTimeException;
use app\modules\v1\models\exceptions\ForbiddenChangeWorkerException;
use app\modules\v1\models\exceptions\InvalidAttributeValueException;
use app\modules\v1\models\exceptions\InvalidOrderTimeException;
use app\modules\v1\models\workers\WorkerSetOrderStatus;
use Yii;
use yii\base\ErrorException;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class Order extends Object
{
    public $tenantId;
    public $cityId;
    public $positionId;
    public $date;

    /**
     * Getting orders.
     *
     * @param string|array $status_group
     * @param integer|null $date 'd.m.Y'
     * @param array $arSort ['sort' => 'asc', 'order' => 'status_id']
     * @param array $filterStatus [1, 2, 3]
     * @param array $arOrderExclusion [1, 2, 3]
     * @param bool $count Count orders
     *
     * @return array|int
     */
    public function getByStatusGroup(
        $status_group,
        $date = null,
        $arSort = [],
        $filterStatus = [],
        $arOrderExclusion = [],
        $count = false
    )
    {
        $cur_date = date('d.m.Y');

        if ($status_group == OrderStatus::STATUS_GROUP_IN_HAND) {
            $status_group = OrderStatus::getSubWorkGroups();
        }

        $query = OrderRecord::find();
        $query->where([
            'tenant_id' => $this->tenantId,
        ]);

        $query->andFilterWhere(['not in', 'order_id', $arOrderExclusion]);

        //Выборка по дате
        if (is_null($date)) {
            $date = $cur_date;
        }

        list($day, $month, $year) = explode('.', $date);
        $first_time = mktime(0, 0, 0, $month, $day, $year);
        $second_time = mktime(23, 59, 59, $month, $day, $year);

        if ($status_group == OrderStatus::STATUS_GROUP_PRE_ORDER) {
            $query->andWhere("((create_time >= $first_time AND create_time <= $second_time) OR (status_time >= $first_time AND status_time <= $second_time))");
        } else {
            $query->andWhere("status_time >= $first_time AND status_time <= $second_time");
        }
        //--------------------------------------------------------------------------------

        $query->with([
            'userCreated' => function (ActiveQuery $sub_query) {
                $sub_query->select(['user_id', 'last_name', 'name']);
            },
            'client'      => function (ActiveQuery $sub_query) {
                $sub_query->select(['client_id', 'last_name', 'name']);
            },
            'status',
            'worker'      => function (ActiveQuery $sub_query) {
                $sub_query->select(['worker_id', 'last_name', 'name', 'callsign', 'phone']);
            },
            'car'         => function (ActiveQuery $sub_query) {
                $sub_query->select(['car_id', 'name', 'gos_number', 'color']);
            },
        ]);

        if ($status_group == OrderStatus::STATUS_GROUP_WARNING) {
            $warning_statuses = !empty($filterStatus) ? $filterStatus : OrderStatus::getWarningStatusId();
            $query->andWhere(['status_id' => $warning_statuses]);
        } else {
            $query->joinWith([
                'status' => function (ActiveQuery $sub_query) use ($status_group, $filterStatus) {
                    $sub_query->andWhere(['status_group' => $status_group]);

                    if (!empty($filterStatus)) {
                        $sub_query->andWhere([OrderRecord::tableName() . '.status_id' => $filterStatus]);
                    }
                },
            ], false);
        }

        $query->andFilterWhere(['city_id' => $this->cityId]);
        $query->andFilterWhere(['position_id' => $this->positionId]);

        if ($count) {
            return $query->count();
        }

        //Сортировка
        if (!isset($arSort['sort'])) {
            $sort = SORT_DESC;
        } else {
            $sort = ($arSort['sort'] == SORT_DESC || $arSort['sort'] == 'desc') ? SORT_DESC : SORT_ASC;
        }

        $order = isset($arSort['order']) ? $arSort['order'] : 'status_time';
        $orderBy = [$order => $sort];

        $query->orderBy($orderBy);

        $query->asArray();

        return $query->all();
    }

    public static function isActiveOrderProcessingThroughTheExternalExchange($tenantId, $orderId)
    {
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $activeOrder = $activeOrderRepository->getOne($orderId);
        return ArrayHelper::getValue($activeOrder, 'exchange_info.order', false) ? true : false;
    }

    /**
     * Getting order info.
     *
     * @param string|array $filter
     *
     * @return array
     */
    public function getDetailData($filter)
    {
        $query = OrderRecord::find()->where($filter);


        $query->with([
            'client'        => function (ActiveQuery $sub_query) {
                $sub_query->select([
                    'client_id',
                    'last_name',
                    'name',
                    'second_name',
                    'black_list',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'fail_dispatcher_order',
                    'photo',
                ]);
                $sub_query->with([
                    'companies' => function (ActiveQuery $sub_query) {
                        $sub_query->select(['company_id', 'name']);
                    },
                ]);
            },
            'userCreated'   => function (ActiveQuery $sub_query) {
                $sub_query->select(['user_id', 'last_name', 'name', 'second_name']);
            },
            'options',
            'city'          => function (ActiveQuery $sub_query) {
                $sub_query->select(['city_id', 'republic_id']);
            },
            'city.republic' => function (ActiveQuery $sub_query) {
                $sub_query->select(['republic_id', 'timezone']);
            },
            'worker',
            'currency',
            'status',
            'car'           => function (ActiveQuery $sub_query) {
                $sub_query->select(['car_id', 'name', 'gos_number', 'color']);
            },
        ]);

        $query->asArray();

        return $query->one();
    }

    /**
     * @return array
     */
    public function getCounts()
    {
        return [
            OrderStatus::STATUS_GROUP_NEW       => $this->getCountsByGroup(OrderStatus::STATUS_GROUP_NEW),
            OrderStatus::STATUS_GROUP_IN_HAND   => $this->getCountsByGroup(OrderStatus::STATUS_GROUP_IN_HAND),
            OrderStatus::STATUS_GROUP_WARNING   => $this->getCountsByGroup(OrderStatus::STATUS_GROUP_WARNING),
            OrderStatus::STATUS_GROUP_COMPLETED => $this->getCountsByGroup(OrderStatus::STATUS_GROUP_COMPLETED),
            OrderStatus::STATUS_GROUP_REJECTED  => $this->getCountsByGroup(OrderStatus::STATUS_GROUP_REJECTED),
        ];
    }

    /**
     * @param string $group
     *
     * @return int
     */
    public function getCountsByGroup($group)
    {
        return OrdersFactory::getInstance([
            'cityId'      => $this->cityId,
            'positionId'  => $this->positionId,
            'statusGroup' => $group,
            'tenantId'    => $this->tenantId,
            'date'        => $this->date,
        ])->getCounts();
    }

    /**
     * Stop offer to driver
     *
     * @param integer $orderNumber
     *
     * @return bool
     */
    public function stopOffer($orderNumber)
    {
        $order = $this->getDetailData(['tenant_id' => $this->tenantId, 'order_number' => $orderNumber]);

        if (empty($order) || $order['status_id'] != OrderStatus::STATUS_OFFER_ORDER) {
            return false;
        } else {
            Yii::$app->redis_pub_sub->executeCommand('PUBLISH', [
                "order_{$order['order_id']}",
                'stop_offer',
            ]);

            return true;
        }
    }

    /**
     * @return false|null|string
     */
    public function getLastOrderNumber()
    {
        $command = Yii::$app->db->createCommand('SELECT MAX(order_number) FROM tbl_order WHERE `tenant_id`=' . $this->tenantId);

        return $command->queryScalar();
    }

    /**
     * Complete the order
     *
     * @param int $orderId
     * @param int $tenantId
     * @param array $data
     * @param int $lastUpdateTime
     * @param string $lang
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    private function completeOrder($orderId, $tenantId, $data, $lastUpdateTime, $lang)
    {
        $workerSetOrderStatus = \Yii::createObject(WorkerSetOrderStatus::class);

        return $workerSetOrderStatus->update($orderId, $tenantId, $data, $lastUpdateTime, $lang);
    }


    /**
     * Updates an existing OrderRecord model.
     * Codes:
     * 100 - Data updated successfully.
     * 101 - Error saving order.
     * 102 - Invalid order time
     * 103 - The order is already updated.
     * 104 - Forbidden change order
     * 105 - Invalid attribute value
     * 106 - Forbidden change worker
     * 107 - Other code errors
     *
     * @param $orderId
     * @param $tenantId
     * @param $data
     * @param $lastUpdateTime
     * @param $lang
     * @return array
     */
    public function update($orderId, $tenantId, $data, $lastUpdateTime, $lang)
    {
        if (!empty($lang)) {
            \Yii::$app->language = $lang;
        }

        // todo простая проверка состояния заказа в редисе
       /* $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $orderDataRedis = $activeOrderRepository->getOne($orderId);
        var_dump($orderDataRedis);
        exit();*/

        $order = $this->getOrder($orderId, $tenantId);
        $updateTime = $order->update_time;
        $order->subject = $data['subject'];
        $oldStatusId = $order->status_id;
        $orderError = new OrderError();



        if ($order->load($data, '')) {
            try {

                if ($lastUpdateTime != $updateTime && false) {
                    $orderError->setErrorCode(OrderError::OLD_DATA)
                        ->setErrorMessage(t('order', 'Error saving order. The order is already updated.'));
                } elseif (in_array($order->status_id, [
                    OrderStatus::STATUS_COMPLETED_PAID,
                    OrderStatus::STATUS_COMPLETED_NOT_PAID,
                ], false)) {
                    if (!in_array($oldStatusId, [
                        OrderStatus::STATUS_GET_WORKER,
                        OrderStatus::STATUS_WORKER_LATE,
                        OrderStatus::STATUS_WORKER_WAITING,
                        OrderStatus::STATUS_CLIENT_IS_NOT_COMING_OUT,
                        OrderStatus::STATUS_FREE_AWAITING,
                        OrderStatus::STATUS_NONE_FREE_AWAITING,
                        OrderStatus::STATUS_EXECUTION_PRE,
                        OrderStatus::STATUS_EXECUTING,
                        OrderStatus::STATUS_WAITING_FOR_PAYMENT,
                        OrderStatus::STATUS_PAYMENT_CONFIRM,
                        OrderStatus::STATUS_PRE_GET_WORKER,
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                        OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION,
                        OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION,
                    ], false)
                    ) {
                        throw new CompleteOrderException(t('order',
                            'You can complete order in executing status group.'));
                    }


                    $worker = Worker::findOne($order->worker_id);


                    $data = [
                        'worker_login'      => isset($worker->callsign) ? $worker->callsign : null,
                        'status_id'         => isset($data['status_id']) ? $data['status_id'] : null,
                        'dispatcher_id'     => isset($data['user_modifed']) ? $data['user_modifed'] : null,
                        'detail_order_data' => isset($data['summary_cost']) ? json_encode(['summary_cost' => $data['summary_cost']]) : null,
                    ];


                    if ($order->payment != OrderRecord::PAYMENT_CASH
                        && $order->status_id == OrderStatus::STATUS_COMPLETED_PAID
                    ) {
                        $data['status_id'] = OrderStatus::STATUS_PAYMENT_CONFIRM;
                    }


                    $result = $this->completeOrder($orderId, $tenantId, $data, $lastUpdateTime, $lang);

                    if (isset($result['result']['set_result']) && $result['result']['set_result'] == 1) {
                        $orderError->setErrorCode(OrderError::SUCCESS)
                            ->setErrorMessage(t('app', 'Data updated successfully'));
                    } else {
                        $message = $data['status_id'] === OrderStatus::STATUS_PAYMENT_CONFIRM
                            ? 'An error occurred while completing the order by non-cash payment method.'
                            : 'Error saving order. Notify to administrator, please.';

                        $orderError->setErrorCode(OrderError::ERROR_SAVING)->setErrorMessage(t('order', $message));
                    }
                } else {

                   $transaction = app()->getDb()->beginTransaction();


                    try {
                        if (!$order->save(false)) {
                            $transaction->rollBack();
                            Yii::error($order->getErrors());
                            Yii::error('Ошибка сохранения заказа', 'order');
                            $orderError->setErrorCode(OrderError::ERROR_SAVING)
                                ->setErrorMessage(t('order', 'Error saving order. Notify to administrator, please.'));
                        } else {
                            $transaction->commit();
                            $orderError->setErrorCode(OrderError::SUCCESS)
                                ->setErrorMessage(t('app', 'Data updated successfully'));
                        }
                    } catch (\Exception $ex) {
                        $transaction->rollBack();
                        throw $ex;
                    }
                }


                $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
                $orderDataRedis = $activeOrderRepository->getOne($orderId);

                if (empty($orderDataRedis)) {
                    $orderError->setErrorCode(OrderError::INTERNAL_ERROR)
                        ->setErrorMessage(t('app', 'No orderDataRedis'));
                }
                if (array_key_exists('exchange_info', $orderDataRedis) && $orderError->getErrorCode() === OrderError::SUCCESS) {
                    /**
                     * @var $serviceExchange ServiceExchange
                     */
                    $serviceExchange = \Yii::$app->serviceExchange;
                    //Если заказ в бирже и его отменили или завершили или заморозили через панель управления, то отменяем его в бирже
                    if (isset($data['status_id'])
                        && in_array($data['status_id'], array_merge(
                            OrderStatus::getRejectedStatusId(),
                            OrderStatus::getCompletedStatusId(),
                            [OrderStatus::STATUS_MANUAL_MODE,
                             OrderStatus::STATUS_NEW
                            ]))
                    ) {
                        try {
                            $serviceExchange->deleteFromExchange($orderDataRedis, $data['subject']);
                        } catch (\Exception $ex) {
                            \Yii::error($ex->getMessage());
                        }
                    } else {
                        try {
                            $serviceExchange->updateAtExchange($orderDataRedis, $data['subject']);
                        } catch (\Exception $ex) {
                            \Yii::error($ex->getMessage());
                        }
                    }
                }
            } catch (BlockedCompanyException $exc) {
                $orderError->setErrorCode(OrderError::COMPANY_BLOCKED_ERROR)
                    ->setErrorMessage($exc->getMessage());
            } catch (CompleteOrderException $exc) {
                $orderError->setErrorCode(OrderError::COMPLETE_ORDER_ERROR)
                    ->setErrorMessage($exc->getMessage());
            } catch (InvalidOrderTimeException $exc) {
                $orderError->setErrorCode(OrderError::INVALID_ORDER_TIME)
                    ->setErrorMessage($exc->getMessage());
            } catch (ForbiddenChangeOrderTimeException $exc) {
                $orderError->setErrorCode(OrderError::FORBIDDEN_CHANGE_ORDER_TIME)
                    ->setErrorMessage($exc->getMessage());
            } catch (InvalidAttributeValueException $exc) {
                $orderError->setErrorCode(OrderError::INVALID_VALUE)
                    ->setErrorMessage($exc->getMessage());
            } catch (ForbiddenChangeWorkerException $exc) {
                $orderError->setErrorCode(OrderError::FORBIDDEN_CHANGE_WORKER)
                    ->setErrorMessage($exc->getMessage());

            } catch (\Exception $exc) {
                Yii::error($exc);
                Yii::error($exc->getMessage(), 'order');

                $orderError->setErrorCode(OrderError::INTERNAL_ERROR)
                    ->setErrorMessage(t('order', 'Error saving order. Notify to administrator, please.'));
            }
        }

        return $orderError->getData();
    }

    /**
     * @param int $orderId
     * @param int $tenantId
     *
     * @return array|null|\yii\db\ActiveRecord|OrderRecord
     */
    private function getOrder($orderId, $tenantId)
    {
        return OrderRecord::findOne(['order_id' => $orderId, 'tenant_id' => $tenantId]);
    }
}