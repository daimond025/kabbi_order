<?php

namespace app\modules\v1\models;


use app\models\OrderHasOption as OrderHasOptionRecord;

class OrderHasOption
{
    /**
     * Getting current additional options
     *
     * @param $orderId
     *
     * @return array
     */
    public static function getOptionIds($orderId)
    {
        $optionIds = OrderHasOptionRecord::find()
            ->select(['option_id'])
            ->where(['order_id' => $orderId])
            ->asArray()
            ->column();

        return empty($optionIds) ? [] : array_values($optionIds);
    }

    /**
     * Allow to save multiple models
     *
     * @param array $arOptions
     * @param int   $order_id
     *
     * @return boolean
     */
    public static function manySave($arOptions, $order_id)
    {
        if (is_array($arOptions) && !empty($arOptions)) {
            $insertValue = [];
            $connection  = app()->db;

            foreach ($arOptions as $option_id) {
                if ($option_id > 0) {
                    $insertValue[] = [$order_id, $option_id];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()->batchInsert(OrderHasOptionRecord::tableName(), ['order_id', 'option_id'],
                    $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
