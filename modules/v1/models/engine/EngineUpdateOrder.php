<?php

namespace app\modules\v1\models\engine;

use yii\base\Object;
use app\components\EngineError;
use app\modules\v1\models\OrderStatus;
use app\models\ActiveOrderRepository;
use app\components\exchange\ServiceExchange;

class EngineUpdateOrder extends Object
{
    /**
     * Update order from engine
     * @param $orderId
     * @param $tenantId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @param string $uuid
     * @return array
     */
    public function update($orderId, $tenantId, $data, $updateTime, $lang, $uuid = '')
    {
        $orderError = new EngineError();
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $orderDataRedis = $activeOrderRepository->getOne($orderId);

        if (empty($orderDataRedis)) {
            $orderError->setErrorCode(EngineError::INTERNAL_ERROR)
                ->setErrorMessage(t('app', 'No orderDataRedis'));
            return $orderError->getData();
        }

        if (array_key_exists('exchange_info', $orderDataRedis)) {
            /**
             * @var $serviceExchange ServiceExchange
             */
            $serviceExchange = \Yii::$app->serviceExchange;
            //Отмена заказа
            if (isset($data['status_id']) && in_array($data['status_id'], array_merge(OrderStatus::getRejectedStatusId(), OrderStatus::getCompletedStatusId()))) {
                try {
                    $serviceExchange->deleteFromExchange($orderDataRedis, $data['subject']);
                } catch (\Exception $ex) {
                    \Yii::error($ex->getMessage());
                }
            } else {
                try {
                    $serviceExchange->updateAtExchange($orderDataRedis, $data['subject']);
                } catch (\Exception $ex) {
                    \Yii::error($ex->getMessage());
                }
            }

        }


        //@todo перенести всю логику редактирования данных заказа в движке в это место
        $orderError->setErrorCode(EngineError::SUCCESS)
            ->setErrorMessage(t('app', 'Data updated successfully'));

        return $orderError->getData();


    }


}