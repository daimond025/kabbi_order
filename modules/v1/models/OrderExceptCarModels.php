<?php

namespace app\modules\v1\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%order_except_car_models}}".
 *
 * @property integer     $id
 * @property integer     $order_id
 * @property integer     $car_model_id
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property CarModel    $carModel
 * @property OrderRecord $order
 */
class OrderExceptCarModels extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_except_car_models}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'car_model_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['car_model_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarModel::className(),
                'targetAttribute' => ['car_model_id' => 'model_id'],
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => OrderRecord::className(),
                'targetAttribute' => ['order_id' => 'order_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'order_id'     => 'Order ID',
            'car_model_id' => 'Car Model ID',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['model_id' => 'car_model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
