<?php

namespace app\modules\v1\models\rules\order;

use app\components\OrderError;
use app\models\ClientCompany;
use app\models\ClientPhone;
use app\models\Message;
use app\models\TaxiTariff;
use app\models\Tenant;
use app\models\TenantCityHasPosition;
use app\models\TenantHasCity;
use app\modules\v1\models\CarModel;
use app\modules\v1\models\City;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\workers\client\Client;
use app\modules\v1\validators\Phone;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string  $phone
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string  $address
 * @property string  $comment
 * @property string  $predv_price
 * @property string  $predv_distance
 * @property string  $predv_time
 * @property string  $device
 * @property string  $order_number
 * @property string  $payment
 * @property integer $show_phone
 * @property string  $create_time
 * @property string  $updated_time
 * @property string  $order_time
 * @property string  $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float   $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 */
class OrderRules
{
    public function getRules(OrderRecord $order)
    {
        return [
            [
                [
                    'tenant_id',
                    'city_id',
                    'tariff_id',
                    'position_id',
                    'orderAction',
                    'phone',
                    'address',
                    'payment',
                ],
                'required',
                'message' => OrderError::MISSING_PARAMETER,
            ],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'city_id',
                    'tariff_id',
                    'phone',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'time_to_client',
                    'predv_time',
                    'client_id',
                    'bonus_payment',
                    'update_time',
                    'car_id',
                    'order_hours',
                    'order_minutes',
                    'order_seconds',
                    'deny_refuse_order',
                    'app_id',
                ],
                'integer',
                'message' => OrderError::BAD_PARAM,
            ],
            [['phone'], 'string', 'max' => 20, 'message' => OrderError::BAD_PARAM],
            [['phone'], Phone::className(), 'message' => OrderError::BAD_PARAM],
            [
                [
                    'comment',
                    'new_add_option',
                    'orderAction',
                    'lang',
                    'payment',
                    'pan',
                    'client_device_token',
                ],
                'string',
                'length' => [0, 2000],
                'message' => OrderError::BAD_PARAM,
            ],
            [
                'tenant_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Tenant::class,
                'message'     => OrderError::INTERNAL_ERROR,
            ],
            [
                'client_id',
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::class,
                'targetAttribute' => ['client_id', 'tenant_id'],
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                'phone',
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ClientPhone::class,
                'targetAttribute' => ['client_id', 'phone' => 'value'],
                'when'            => function (OrderRecord $order) {
                    return (bool)$order->client_id;
                },
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                'city_id',
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TenantHasCity::class,
                'targetAttribute' => ['city_id', 'tenant_id'],
                'filter'          => [
                    'block' => 0,
                ],
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                'position_id',
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TenantCityHasPosition::class,
                'targetAttribute' => ['position_id', 'tenant_id', 'city_id'],
                'message'         => OrderError::INTERNAL_ERROR,
            ],
            [
                'tariff_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => TaxiTariff::class,
                'filter'      => function (ActiveQuery $query) use ($order) {
                    $query->alias('t')
                        ->leftJoin(
                            '{{%taxi_tariff_has_city}} thc',
                            'thc.tariff_id = t.tariff_id'
                        )
                        ->where([
                            't.block'       => 0,
                            't.tenant_id'   => $order->tenant_id,
                            't.tariff_id'   => $order->tariff_id,
                            't.position_id' => $order->position_id,
                            'thc.city_id'   => $order->city_id,
                        ]);
                },
                'message'     => OrderError::TARIFF_ERROR,
            ],
            [
                'company_id',
                'exist',
                'targetClass' => ClientCompany::className(),
                'filter'      => ['block' => '0'],
            ],
            [
                'lang',
                'filter',
                'filter' => function ($value) {
                    if (in_array($value, \Yii::$app->params['supportedLanguages'])) {
                        return $value;
                    }

                    return 'en-US';
                },
            ],
            [
                'address',
                function ($attribute) use ($order) {
                    if (!is_array($order->address)) {
                        $order->addError($attribute, OrderError::EXPECTED_ARRAY);
                    }
                },
            ],
            [
                [
                    'additional_option',
                    'status_reject',
                    'hash',
                    'versionclient',
                ],
                'safe',
            ],
            [
                [
                    'is_fix',
                    'originally_preorder',
                    'order_now',
                ],
                'in',
                'range' => [0, 1],
            ],
            [
                ['payment'],
                'in',
                'range'   => OrderRecord::TYPES_PAYMENT,
                'message' => OrderError::BAD_PARAM,
            ],
            [
                ['order_date'],
                'match',
                'pattern' => '/^\d{4}-\d{2}-\d{2}$/',
            ],
            [
                ['order_time'],
                function ($attribute) use ($order) {

                    if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $order->order_time)
                        || preg_match('/^\d{2}.\d{2}.\d{4} \d{2}:\d{2}:\d{2}$/', $order->order_time)
                    ) {
                        $order->order_time = \Yii::$app->formatter->asTimestamp($order->order_time);

                        return true;
                    } elseif (is_numeric($order->order_time)) {

                        return true;
                    }
                    $order->addError($attribute, OrderError::BAD_PARAM);

                    return false;
                }
            ],
            [
                ['predv_price', 'predv_distance', 'summary_cost', 'predv_price_no_discount'],
                'number',
            ],
            [
                ['device'],
                'string',
                'max' => 10,
            ],
            ['deny_refuse_order', 'default', 'value' => 0],
            ['is_fix', 'default', 'value' => '0'],
            ['edit', 'default', 'value' => '0'],
            ['lang', 'default', 'value' => 'en-US'],
            [
                ['except_car_models'],
                function ($attribute) use ($order) {
                    if (!preg_match('/^[\d,]+$/', $order->except_car_models)) {
                        $order->addError($attribute, OrderError::BAD_PARAM);
                        return false;
                    }

                    $carModelIdsNoTrust = array_unique(explode(',', $order->except_car_models));

                    $order->except_car_models = CarModel::find()
                        ->select(['model_id'])
                        ->where(['model_id' => $carModelIdsNoTrust])
                        ->asArray()
                        ->column();

                    return true;
                },
                'message' => OrderError::BAD_PARAM,
            ],
            [
                ['client_name', 'client_lastname', 'client_secondname'],
                'string',
                'max' => 45,
                'message' => OrderError::BAD_PARAM,
            ],
        ];
    }
}
