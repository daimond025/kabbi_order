<?php

namespace app\modules\v1\models\exceptions;

use yii\base\Exception;

class InvalidOrderTimeException extends Exception
{
}