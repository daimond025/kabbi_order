<?php

namespace app\modules\v1\models\exceptions;

use yii\base\Exception;

/**
 * Class InvalidClientCard
 * @package app\modules\v1\models\exceptions
 */
class InvalidClientCard extends Exception
{
}