<?php

namespace app\modules\v1\models\exceptions;

use yii\base\Exception;

class ForbiddenChangeOrderTimeException extends Exception
{
}