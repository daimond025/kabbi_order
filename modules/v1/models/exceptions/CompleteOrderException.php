<?php

namespace app\modules\v1\models\exceptions;

use yii\base\Exception;

/**
 * Class CompleteOrderException
 * @package app\modules\v1\models\exceptions
 */
class CompleteOrderException extends Exception
{
}