<?php

namespace app\modules\v1\models\exceptions;

use yii\base\Exception;

/**
 * Class BlockedCompanyException
 * @package app\modules\v1\models\exceptions
 */
class BlockedCompanyException extends Exception
{
}