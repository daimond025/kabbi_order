<?php

namespace app\modules\v1\models\exchange;

use app\components\ExchangeError;
use app\models\ActiveOrderRepository;
use app\models\Order;
use app\modules\v1\models\OrderStatus;
use app\components\exchange\ServiceExchange;
use app\modules\v1\models\OrderRecord;
use app\models\OrderStatusRecord;
use app\modules\v1\models\workers\order\OrderDetailCost;
use yii\base\Object;
use Yii;

class ExchangeUpdateOrder extends Object
{
    /**
     * Update order from exchange
     * @param $orderId
     * @param $tenantId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @param string $uuid
     * @return array
     */
    public function update($orderId, $tenantId, $data, $updateTime, $lang, $uuid = '')
    {

        if (isset($data['status_id'])) {
            $statusNewId = (int)$data['status_id'];
            return $this->setOrderStatus($orderId, $tenantId, $statusNewId, $data['subject']);
        }
        $orderError = new ExchangeError();
        $orderError->setErrorCode(ExchangeError::NO_RESULT)
            ->setErrorMessage(t('app', 'Unsupported changed params'));
        return $orderError->getData();
    }

    /**
     * @TODO - доделать смену статусов..придумать что делать если смена статуса запрещена
     * @param $orderId
     * @param $tenantId
     * @param $statusNewId
     * @param $subject
     * @return array
     */
    protected function setOrderStatus($orderId, $tenantId, $statusNewId, $subject)
    {
        $orderError = new ExchangeError();
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $orderDataRedis = $activeOrderRepository->getOne($orderId);
        if (empty($orderDataRedis)) {
            $orderError->setErrorCode(ExchangeError::INTERNAL_ERROR)
                ->setErrorMessage(t('app', 'No orderDataRedis'));
            return $orderError->getData();
        }
        if (!array_key_exists('exchange_info', $orderDataRedis)) {
            $orderError->setErrorCode(ExchangeError::INTERNAL_ERROR)
                ->setErrorMessage(t('app', 'order have not exchange_info'));
            return $orderError->getData();
        }

        $statusOldId = isset($orderDataRedis['status']['status_id']) ? (int)$orderDataRedis['status']['status_id'] : null;
        $statusGroupOld = isset($orderDataRedis['status']['status_group']) ? $orderDataRedis['status']['status_group'] : null;
        $exchangeProgramId = (int)$orderDataRedis['exchange_info']['exchange_program_id'];

        if ($statusNewId === OrderStatus::STATUS_GET_WORKER && (!in_array($statusOldId, [OrderStatus::STATUS_FREE, OrderStatus::STATUS_OVERDUE]))) {
            $orderError->setErrorCode(ExchangeError::FORBIDDEN_CHANGE_STATUS_ID)
                ->setErrorMessage(t('app', $orderError->getErrorMessage()));
            return $orderError->getData();
        }


        //Заказ взял водитель
        if (in_array($statusNewId, [OrderStatus::STATUS_GET_WORKER, OrderStatus::STATUS_WORKER_WAITING, OrderStatus::STATUS_EXECUTING], false)) {
            /**
             * @var $serviceExchange ServiceExchange
             */
            $serviceExchange = \Yii::$app->serviceExchange;

            $exchangeOrderInfo = $serviceExchange->getOrderInfoFromExchange($orderDataRedis);
            // Обновим инфу о заказе в редисе с данными из биржы
            if ($exchangeOrderInfo) {
                $exchangeOrderParams = $this->getFormattedExchangeOrderData($exchangeOrderInfo, $exchangeProgramId);
                $orderDataRedis['exchange_info']['order'] = $exchangeOrderParams;
            }
        }
        //Заказ завершен
        if ($statusNewId === OrderStatus::STATUS_COMPLETED_PAID) {
            /**
             * @var $serviceExchange ServiceExchange
             */
            $serviceExchange = \Yii::$app->serviceExchange;
            $exchangeOrderInfo = $serviceExchange->getOrderInfoFromExchange($orderDataRedis);
            if ($exchangeOrderInfo) {
                $exchangeOrderParams = $this->getFormattedExchangeOrderData($exchangeOrderInfo, $exchangeProgramId);
                if ($exchangeOrderParams['done_price']) {
                    $detailOrderData['summary_cost'] = $exchangeOrderParams['done_price'];
                    OrderDetailCost::orderDetailCostSave($orderDataRedis, $detailOrderData, null, null);
                }
            }
        }
        //Ищем заказ в БД
        $orderDataMysql = $this->getOrder($orderId, $tenantId);
        //$orderDataMysql->subject = $subject;
        $orderDataMysql->status_id = $statusNewId;
        $orderDataMysql->status_time = time();


        // Перезапишем статусы в редисе
        $statusData = OrderStatusRecord::findOne($statusNewId);
        $orderDataRedis['status']['status_id'] = $statusData->status_id;
        $orderDataRedis['status']['name'] = $statusData->name;
        $orderDataRedis['status']['status_group'] = $statusData->status_group;
        $orderDataRedis['status']['dispatcher_sees'] = $statusData->dispatcher_sees;
        $orderDataRedis['status_id'] = $statusData->status_id;
        $orderDataRedis['status_time'] = time();
        $orderDataRedis['update_time'] = time();


        if ($orderDataMysql->save(false)) {
            $activeOrderRepository->set($orderId, serialize($orderDataRedis));
            $orderError->setErrorCode(ExchangeError::SUCCESS)
                ->setErrorMessage(t('app', 'Data updated successfully'));
            return $orderError->getData();
        }
        Yii::error($orderDataMysql->getErrors());
        $orderError->setErrorCode(ExchangeError::INTERNAL_ERROR)
            ->setErrorMessage(t('app', $orderError->getErrorMessage()));
        return $orderError->getData();
    }

    protected function getFormattedExchangeOrderData($exchangeOrderInfo, $exchangeProgramId)
    {
        if ($exchangeProgramId === ServiceExchange::UP_AND_UP_PROGRAM_ID) {
            $exchangeOrderData = [
                'worker'     => null,
                'car'        => null,
                'done_price' => null,
            ];

            if (isset($exchangeOrderInfo['order']['auto'])) {
                $exchangeCar = $exchangeOrderInfo['order']['auto'];
                $exchangeOrderData['car'] = [
                    'external_car_id' => isset($exchangeCar['id']) ? $exchangeCar['id'] : null,
                    'color'           => isset($exchangeCar['vehicle']['color']) ? $exchangeCar['vehicle']['color'] : null,
                    'gos_number'      => isset($exchangeCar['vehicle']['gnumber']) ? $exchangeCar['vehicle']['gnumber'] : null,
                    'name'            => isset($exchangeCar['vehicle']['model']) ? $exchangeCar['vehicle']['model'] : null,
                    'lat'             => isset($exchangeCar['vehicle']['lat']) ? $exchangeCar['vehicle']['lat'] : null,
                    'lon'             => isset($exchangeCar['vehicle']['lon']) ? $exchangeCar['vehicle']['lon'] : null,
                ];
                $exchangeOrderData['worker'] = [
                    'external_worker_id' => null,
                    'name'               => isset($exchangeCar['vehicle']['name']) ? $exchangeCar['vehicle']['name'] : null,
                    'phone'              => isset($exchangeCar['vehicle']['phone']) ? (int)$exchangeCar['vehicle']['phone'] : null,
                ];
            }

            if (isset($exchangeOrderInfo['order']['done_price'])) {
                $exchangeOrderData['done_price'] = $exchangeOrderInfo['order']['done_price'];
            }
            return $exchangeOrderData;
        }
    }

    /**
     * Get Order Record
     * @param int $orderId
     * @param int $tenantId
     *
     * @return Order
     */
    private function getOrder($orderId, $tenantId)
    {
        return Order::findOne(['order_id' => $orderId, 'tenant_id' => $tenantId]);
    }

}