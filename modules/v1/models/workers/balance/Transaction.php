<?php

namespace app\modules\v1\models\workers\balance;

use Yii;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer         $transaction_id
 * @property integer         $type_id
 * @property integer         $sender_acc_id
 * @property integer         $receiver_acc_id
 * @property double          $sum
 * @property integer         $payment_method
 * @property string          $date
 * @property string          $comment
 * @property integer         $user_created
 *
 * @property Operation[]     $operations
 * @property PaymentMethod   $paymentMethod
 * @property Account         $receiverAcc
 * @property Account         $senderAcc
 * @property TransactionType $type
 */
class Transaction extends \yii\db\ActiveRecord
{

    /**
     * Пополнение
     */
    const DEPOSIT_TYPE = 1;

    /**
     * Снятие
     */
    const WITHDRAWAL_TYPE = 2;

    /**
     * Оплата за заказ
     */
    const ORDER_PAYMENT_TYPE = 3;

    //Методы оплаты
    const CASH_PAYMENT = 1;
    const CARD_PAYMENT = 2;
    const PERSONAL_ACCOUNT_PAYMENT = 3;
    const CORP_BALANCE_PAYMENT = 4;
    const TERMINAL_QIWI_PAYMENT = 5;
    const WITHDRAWAL_PAYMENT = 6;
    const TERMINAL_ELECSNET_PAYMENT = 7;

    const PAYMENT_WITHDRAWAL_FOR_TARIFF = 'WITHDRAWAL_FOR_TARIFF';
    const BONUS_PAYMENT = 'BONUS';

    //Коды ошибок
    const SUCCESS_RESPONSE = 100;
    const NOT_SUPPORTED_RESPONSE = 120;
    const SYSTEM_ERROR_RESPONSE = 140;
    const NO_MONEY_RESPONSE = 160;
    const NOT_FOUND_RESPONSE = 180;
    const INVALID_CARD_PAYMENT_PARAMS_RESPONSE = 250;
    const CARD_PAYMENT_FAILED_RESPONSE = 260;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'payment_method', 'user_created'], 'integer'],
            [['receiver_acc_id', 'sum'], 'required'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id'  => 'Transaction ID',
            'type_id'         => 'Type ID',
            'sender_acc_id'   => 'Sender Acc ID',
            'receiver_acc_id' => 'Receiver Acc ID',
            'sum'             => 'Sum',
            'payment_method'  => 'Payment Method',
            'date'            => 'Date',
            'comment'         => 'Comment',
            'user_created'    => 'Dispatcher',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['payment_id' => 'payment_method']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverAcc()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'receiver_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderAcc()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'sender_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransactionType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(\app\modules\tenant\models\User::className(), ['user_id' => 'user_created']);
    }

    /**
     * Проведение транзакции
     *
     * @param array $transactionData
     *
     * @return string Код ответа
     * @see taxi-gearman \console\components\billing\Billing
     */
    public static function createTransaction($transactionData)
    {
        return Yii::$app->gearman->doHigh('Billing', $transactionData);
    }

}
