<?php

namespace app\modules\v1\models\workers\balance;

use yii\base\Object;

/**
 * Class WorkerTransaction
 * @package api\models\balance
 */
class WorkerTransaction extends Object
{
    public function closeOrder($orderId, $statusNewId, $requestId)
    {
        $transactionData = [
            'order_id'        => $orderId,
            'order_status_id' => $statusNewId,
            'request_id'      => $requestId,
        ];

        $responseCode = Transaction::createTransaction($transactionData);
        if ($responseCode == Transaction::SUCCESS_RESPONSE) {
            return true;
        } else {
            \Yii::error("ERROR AT closeOrderTransaction. Error code: " . $responseCode);

            return $responseCode;
        }
    }

}