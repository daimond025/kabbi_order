<?php

namespace app\modules\v1\models\workers\order;

use Yii;

/**
 * This is the model class for table "tbl_order_detail_cost".
 *
 * @property integer $detail_id
 * @property integer $order_id
 * @property string  $accrual_city
 * @property string  $accrual_out
 * @property string  $summary_distance
 * @property string  $summary_cost
 * @property string  $summary_time
 * @property string  $city_time
 * @property string  $city_distance
 * @property string  $city_cost
 * @property string  $city_cost_time
 * @property string  $out_city_time
 * @property string  $out_city_distance
 * @property string  $out_city_cost
 * @property string  $out_city_cost_time
 * @property string  $city_time_wait
 * @property string  $out_time_wait
 * @property string  $before_time_wait
 * @property string  $additional_cost
 * @property string  $is_fix
 * @property string  $city_next_km_price
 * @property string  $city_next_km_price_time
 * @property string  $out_next_km_price
 * @property string  $out_next_km_price_time
 * @property string  $supply_price
 * @property string  $city_wait_time
 * @property string  $out_wait_time
 * @property string  $city_wait_driving
 * @property string  $out_wait_driving
 * @property string  $city_wait_price
 * @property string  $out_wait_price
 * @property string  $time
 * @property string  $distance_for_plant
 * @property string  $planting_price
 * @property string  $planting_include
 * @property string  $planting_include_time
 * @property string  $start_point_location
 * @property string  $city_next_cost_unit
 * @property string  $out_next_cost_unit
 * @property string  $promo_discount_value
 * @property string  $promo_discount_percent
 *
 * @property string  $city_wait_cost
 * @property string  $out_wait_cost
 * @property string  $distance_for_plant_cost
 * @property string  $before_time_wait_cost
 *
 * @property string  $bonus
 * @property string  $writeoff_bonus_id
 *
 * @property string  $commission
 * @property string  $surcharge
 * @property string  $tax
 *
 * @property Order   $order
 */
class OrderDetailCost extends \yii\db\ActiveRecord
{

    public $lang;
    public $tenantId;
    public $workerCallsign;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_detail_cost';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'time'], 'integer'],
            [['accrual_city', 'accrual_out', 'start_point_location'], 'string'],
            [
                [
                    'summary_distance',
                    'summary_cost',
                    'summary_time',
                    'city_time',
                    'city_distance',
                    'city_cost',
                    'out_city_time',
                    'out_city_distance',
                    'out_city_cost',
                    'city_time_wait',
                    'out_time_wait',
                    'before_time_wait',
                    'additional_cost',
                    'is_fix',
                    'supply_price',
                    'city_wait_time',
                    'out_wait_time',
                    'city_wait_driving',
                    'out_wait_driving',
                    'city_wait_price',
                    'out_wait_price',
                    'distance_for_plant',
                    'planting_price',
                    'planting_include',
                    'commission',
                    'surcharge',
                    'tax',
                    'promo_discount_value',
                    'promo_discount_percent',
                ],
                'string',
                'max' => 45,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'detail_id'               => 'Detail ID',
            'order_id'                => 'Order ID',
            'accrual_city'            => 'Accrual City',
            'accrual_out'             => 'Accrual Out',
            'summary_distance'        => 'Summary Distance',
            'summary_cost'            => 'Summary Cost',
            'summary_time'            => 'Summary Time',
            'city_time'               => 'City Time',
            'city_distance'           => 'City Distance',
            'city_cost'               => 'City Cost',
            'city_cost_time'          => 'City Cost Time',
            'out_city_time'           => 'Out City Time',
            'out_city_distance'       => 'Out City Distance',
            'out_city_cost'           => 'Out City Cost',
            'out_city_cost_time'      => 'Out City Cost Time',
            'city_time_wait'          => 'City Time Wait',
            'out_time_wait'           => 'Out Time Wait',
            'before_time_wait'        => 'Before Time Wait',
            'additional_cost'         => 'Additional Cost',
            'is_fix'                  => 'Is Fix',
            'city_next_km_price'      => 'City Next Km Price',
            'out_next_km_price'       => 'Out Next Km Price',
            'supply_price'            => 'Supply Price',
            'city_wait_time'          => 'City Wait Time',
            'out_wait_time'           => 'Out Wait Time',
            'city_wait_driving'       => 'City Wait Driving',
            'out_wait_driving'        => 'Out Wait Driving',
            'city_wait_price'         => 'City Wait Price',
            'out_wait_price'          => 'Out Wait Price',
            'time'                    => 'Time',
            'distance_for_plant'      => 'Distance For Plant',
            'planting_price'          => 'Planting Price',
            'planting_include'        => 'Planting Include',
            'planting_include_time'   => 'Planting Include Time',
            'start_point_location'    => 'Start Point Location',
            'city_wait_cost'          => 'City Wait Cost',
            'out_wait_cost'           => 'Out Wait Cost',
            'distance_for_plant_cost' => 'Distance For Plant Cost',
            'before_time_wait_cost'   => 'Before Time Wait Cost',
            'city_next_cost_unit'     => 'City Next Cost Unit',
            'out_next_cost_unit'      => 'Out Next Cost Unit',
            'bonus'                   => 'Bonus',
            'writeoff_bonus_id'       => 'Writeoff bonus id',
            'commission'              => 'Commission',
            'surcharge'               => 'Surcharge',
            'tax'                     => 'Tax',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * Кастомное сохранение детализации стоимости заказа с реализацией транзакций
     *
     * @param array  $orderDataRedis
     * @param array  $detailOrderData
     * @param int    $workerCallsign
     * @param string $lang
     *
     * @return bool
     */
    public static function orderDetailCostSave($orderDataRedis, $detailOrderData, $workerCallsign, $lang)
    {
        $tariffData                    = isset($orderDataRedis["costData"]["tariffInfo"]) ? $orderDataRedis["costData"]["tariffInfo"] : null;
        $detailCost                    = new OrderDetailCost();
        $detailCost->order_id          = isset($orderDataRedis["order_id"]) ? (int)$orderDataRedis["order_id"] : null;
        $detailCost->tenantId          = isset($orderDataRedis["tenant_id"]) ? (int)$orderDataRedis["tenant_id"] : null;
        $detailCost->workerCallsign    = $workerCallsign;
        $detailCost->lang              = $lang;
        $detailCost->summary_time      = isset($detailOrderData["summary_time"]) ? (string)$detailOrderData["summary_time"] : null;
        $detailCost->summary_distance  = isset($detailOrderData["summary_distance"]) ? (string)$detailOrderData["summary_distance"] : null;


        $detailCost->summary_cost      = isset($detailOrderData["summary_cost"]) ? (string)$detailOrderData["summary_cost"] : null;
        $detailCost->commission        = isset($detailOrderData["commission"]) ? (string)$detailOrderData["commission"] : null;
        $detailCost->surcharge         = isset($detailOrderData["surcharge"]) ? (string)$detailOrderData["surcharge"] : null;
        $detailCost->tax               = isset($detailOrderData["tax"]) ? (string)$detailOrderData["tax"] : null;
        $detailCost->bonus             = isset($detailOrderData["bonus"]) ? (string)$detailOrderData["bonus"] : null;
        $detailCost->writeoff_bonus_id = isset($detailOrderData["writeoff_bonus_id"]) ? (string)$detailOrderData["writeoff_bonus_id"] : null;

        $detailCost->city_time      = isset($detailOrderData["city_time"]) ? (string)$detailOrderData["city_time"] : null;
        $detailCost->city_distance  = isset($detailOrderData["city_distance"]) ? (string)$detailOrderData["city_distance"] : null;
        $detailCost->city_cost      = isset($detailOrderData["city_cost"]) ? (string)$detailOrderData["city_cost"] : null;
        $detailCost->city_cost_time = isset($detailOrderData["city_cost_time"]) ? (string)$detailOrderData["city_cost_time"] : null;

        $detailCost->out_city_time      = isset($detailOrderData["out_city_time"]) ? (string)$detailOrderData["out_city_time"] : null;
        $detailCost->out_city_distance  = isset($detailOrderData["out_city_distance"]) ? (string)$detailOrderData["out_city_distance"] : null;
        $detailCost->out_city_cost      = isset($detailOrderData["out_city_cost"]) ? (string)$detailOrderData["out_city_cost"] : null;
        $detailCost->out_city_cost_time = isset($detailOrderData["out_city_cost_time"]) ? (string)$detailOrderData["out_city_cost_time"] : null;

        $detailCost->city_time_wait     = isset($detailOrderData["city_time_wait"]) ? (string)$detailOrderData["city_time_wait"] : null;
        $detailCost->out_time_wait      = isset($detailOrderData["out_time_wait"]) ? (string)$detailOrderData["out_time_wait"] : null;
        $detailCost->before_time_wait   = isset($detailOrderData["before_time_wait"]) ? (string)$detailOrderData["before_time_wait"] : null;
        $detailCost->distance_for_plant = isset($detailOrderData["distance_for_plant"]) ? (string)$detailOrderData["distance_for_plant"] : null;

        $detailCost->city_wait_cost          = isset($detailOrderData["city_wait_cost"]) ? (string)$detailOrderData["city_wait_cost"] : null;
        $detailCost->out_wait_cost           = isset($detailOrderData["out_wait_cost"]) ? (string)$detailOrderData["out_wait_cost"] : null;
        $detailCost->distance_for_plant_cost = isset($detailOrderData["distance_for_plant_cost"]) ? (string)$detailOrderData["distance_for_plant_cost"] : null;
        $detailCost->before_time_wait_cost   = isset($detailOrderData["before_time_wait_cost"]) ? (string)$detailOrderData["before_time_wait_cost"] : null;

        $detailCost->promo_discount_value   = isset($detailOrderData["promo_discount_value"]) ? (string)$detailOrderData["promo_discount_value"] : null;
        $detailCost->promo_discount_percent = isset($detailOrderData["promo_discount_percent"]) ? (string)$detailOrderData["promo_discount_percent"] : null;

        $detailCost->time = (int)time();

        if (!empty($tariffData)) {
            $detailCost->accrual_city = isset($tariffData["tariffDataCity"]["accrual"]) ? (string)$tariffData["tariffDataCity"]["accrual"] : null;
            $detailCost->accrual_out  = isset($tariffData["tariffDataTrack"]["accrual"]) ? (string)$tariffData["tariffDataTrack"]["accrual"] : null;

            $detailCost->additional_cost = isset($orderDataRedis["costData"]["additionals_cost"]) ? (string)$orderDataRedis["costData"]["additionals_cost"] : null;
            $detailCost->is_fix          = isset($orderDataRedis["costData"]["is_fix"]) ? (string)$orderDataRedis["costData"]["is_fix"] : null;

            // todo
            $detailCost->summary_cost   = (((int)$detailCost->is_fix === 1 ) && !empty($orderDataRedis['costData']['summary_cost'])) ? (string)$orderDataRedis['costData']['summary_cost'] : $detailCost->summary_cost;

            $detailCost->start_point_location = isset($orderDataRedis["costData"]["start_point_location"]) ? (string)$orderDataRedis["costData"]["start_point_location"] : null;

            if (isset($orderDataRedis["costData"]["start_point_location"])) {
                if ($orderDataRedis["costData"]["start_point_location"] == "in") {
                    if (isset($tariffData["isDay"])) {
                        if ($tariffData["isDay"] == 1) {
                            $detailCost->planting_price        = isset($tariffData["tariffDataCity"]["planting_price_day"]) ? (string)$tariffData["tariffDataCity"]["planting_price_day"] : null;
                            $detailCost->planting_include      = isset($tariffData["tariffDataCity"]["planting_include_day"]) ? (string)$tariffData["tariffDataCity"]["planting_include_day"] : null;
                            $detailCost->planting_include_time = isset($tariffData["tariffDataCity"]["planting_include_day_time"]) ? (string)$tariffData["tariffDataCity"]["planting_include_day_time"] : null;
                        } else {
                            $detailCost->planting_price        = isset($tariffData["tariffDataCity"]["planting_include_night"]) ? (string)$tariffData["tariffDataCity"]["planting_price_night"] : null;
                            $detailCost->planting_include      = isset($tariffData["tariffDataCity"]["planting_include_night"]) ? (string)$tariffData["tariffDataCity"]["planting_include_night"] : null;
                            $detailCost->planting_include_time = isset($tariffData["tariffDataCity"]["planting_include_night_time"]) ? (string)$tariffData["tariffDataCity"]["planting_include_night_time"] : null;
                        }
                    }
                }
                if ($orderDataRedis["costData"]["start_point_location"] == "out") {
                    if (isset($tariffData["isDay"])) {
                        if ($tariffData["isDay"] == 1) {
                            $detailCost->planting_price        = isset($tariffData["tariffDataTrack"]["planting_price_day"]) ? (string)$tariffData["tariffDataTrack"]["planting_price_day"] : null;
                            $detailCost->planting_include      = isset($tariffData["tariffDataTrack"]["planting_include_day"]) ? (string)$tariffData["tariffDataTrack"]["planting_include_day"] : null;
                            $detailCost->planting_include_time = isset($tariffData["tariffDataTrack"]["planting_include_day_time"]) ? (string)$tariffData["tariffDataTrack"]["planting_include_day_time"] : null;
                        } else {
                            $detailCost->planting_price        = isset($tariffData["tariffDataTrack"]["planting_include_night"]) ? (string)$tariffData["tariffDataTrack"]["planting_price_night"] : null;
                            $detailCost->planting_include      = isset($tariffData["tariffDataTrack"]["planting_include_night"]) ? (string)$tariffData["tariffDataTrack"]["planting_include_night"] : null;
                            $detailCost->planting_include_time = isset($tariffData["tariffDataTrack"]["planting_include_night_time"]) ? (string)$tariffData["tariffDataTrack"]["planting_include_night_time"] : null;
                        }
                    }
                }
            }

            if (isset($tariffData["isDay"])) {
                if ($tariffData["isDay"] == 1) {
                    $detailCost->city_next_km_price      = isset($tariffData["tariffDataCity"]["next_km_price_day"]) ? (string)$tariffData["tariffDataCity"]["next_km_price_day"] : null;
                    $detailCost->city_next_km_price_time = isset($tariffData["tariffDataCity"]["next_km_price_day_time"]) ? (string)$tariffData["tariffDataCity"]["next_km_price_day_time"] : null;
                    $detailCost->city_next_cost_unit     = isset($tariffData["tariffDataCity"]["next_cost_unit_day"]) ? (string)$tariffData["tariffDataCity"]["next_cost_unit_day"] : null;

                    $detailCost->out_next_km_price      = isset($tariffData["tariffDataTrack"]["next_km_price_day"]) ? (string)$tariffData["tariffDataTrack"]["next_km_price_day"] : null;
                    $detailCost->out_next_km_price_time = isset($tariffData["tariffDataTrack"]["next_km_price_day_time"]) ? (string)$tariffData["tariffDataTrack"]["next_km_price_day_time"] : null;
                    $detailCost->out_next_cost_unit     = isset($tariffData["tariffDataTrack"]["next_cost_unit_day"]) ? (string)$tariffData["tariffDataTrack"]["next_cost_unit_day"] : null;


                    $detailCost->supply_price = isset($tariffData["tariffDataTrack"]["supply_price_day"]) ? (string)$tariffData["tariffDataTrack"]["supply_price_day"] : null;

                    $detailCost->city_wait_time = isset($tariffData["tariffDataCity"]["wait_time_day"]) ? (string)$tariffData["tariffDataCity"]["wait_time_day"] : null;
                    $detailCost->out_wait_time  = isset($tariffData["tariffDataTrack"]["wait_time_day"]) ? (string)$tariffData["tariffDataTrack"]["wait_time_day"] : null;

                    $detailCost->city_wait_driving = isset($tariffData["tariffDataCity"]["wait_driving_time_day"]) ? (string)$tariffData["tariffDataCity"]["wait_driving_time_day"] : null;
                    $detailCost->out_wait_driving  = isset($tariffData["tariffDataTrack"]["wait_driving_time_day"]) ? (string)$tariffData["tariffDataTrack"]["wait_driving_time_day"] : null;

                    $detailCost->city_wait_price = isset($tariffData["tariffDataCity"]["wait_price_day"]) ? (string)$tariffData["tariffDataCity"]["wait_price_day"] : null;
                    $detailCost->out_wait_price  = isset($tariffData["tariffDataTrack"]["wait_price_day"]) ? (string)$tariffData["tariffDataTrack"]["wait_price_day"] : null;
                } else {
                    $detailCost->city_next_km_price      = isset($tariffData["tariffDataCity"]["next_km_price_night"]) ? (string)$tariffData["tariffDataCity"]["next_km_price_night"] : null;
                    $detailCost->city_next_km_price_time = isset($tariffData["tariffDataCity"]["next_km_price_night_time"]) ? (string)$tariffData["tariffDataCity"]["next_km_price_night_time"] : null;
                    $detailCost->city_next_cost_unit     = isset($tariffData["tariffDataCity"]["next_cost_unit_night"]) ? (string)$tariffData["tariffDataCity"]["next_cost_unit_night"] : null;

                    $detailCost->out_next_km_price      = isset($tariffData["tariffDataTrack"]["next_km_price_night"]) ? (string)$tariffData["tariffDataTrack"]["next_km_price_night"] : null;
                    $detailCost->out_next_km_price_time = isset($tariffData["tariffDataTrack"]["next_km_price_night_time"]) ? (string)$tariffData["tariffDataTrack"]["next_km_price_night_time"] : null;
                    $detailCost->out_next_cost_unit     = isset($tariffData["tariffDataTrack"]["next_cost_unit_night"]) ? (string)$tariffData["tariffDataTrack"]["next_cost_unit_night"] : null;

                    $detailCost->supply_price = isset($tariffData["tariffDataTrack"]["supply_price_night"]) ? (string)$tariffData["tariffDataTrack"]["supply_price_night"] : null;

                    $detailCost->city_wait_time = isset($tariffData["tariffDataCity"]["supply_price_night"]) ? (string)$tariffData["tariffDataCity"]["supply_price_night"] : null;
                    $detailCost->out_wait_time  = isset($tariffData["tariffDataTrack"]["supply_price_night"]) ? (string)$tariffData["tariffDataTrack"]["supply_price_night"] : null;

                    $detailCost->city_wait_driving = isset($tariffData["tariffDataCity"]["wait_driving_time_night"]) ? (string)$tariffData["tariffDataCity"]["wait_driving_time_night"] : null;
                    $detailCost->out_wait_driving  = isset($tariffData["tariffDataTrack"]["wait_driving_time_night"]) ? (string)$tariffData["tariffDataTrack"]["wait_driving_time_night"] : null;

                    $detailCost->city_wait_price = isset($tariffData["tariffDataCity"]["wait_price_night"]) ? (string)$tariffData["tariffDataCity"]["wait_price_night"] : null;
                    $detailCost->out_wait_price  = isset($tariffData["tariffDataTrack"]["wait_price_night"]) ? (string)$tariffData["tariffDataTrack"]["wait_price_night"] : null;
                }
            }
        }
        if ($detailCost->save()) {
            return true;
        } else {
            Yii::error($detailCost->errors);
        }

        return false;
    }

}
