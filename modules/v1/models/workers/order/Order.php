<?php

namespace app\modules\v1\models\workers\order;

use app\components\settings\OrderSetting;
use app\models\TenantSetting;
use app\modules\v1\models\workers\client\Client;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property string            $order_id
 * @property string            $tenant_id
 * @property string            $worker_id
 * @property string            $city_id
 * @property string            $tariff_id
 * @property string            $user_id
 * @property string            $user_create
 * @property integer           $status_id
 * @property string            $user_modifed
 * @property string            $company_id
 * @property string            $parking_id
 * @property string            $address
 * @property string            $commentc
 * @property string            $predv_price
 * @property string            $device
 * @property string            $order_number
 * @property string            $payment
 * @property integer           $show_phone
 * @property integer           $viewed
 * @property string            $create_time
 * @property string            $order_time
 * @property string            $status_time
 * @property string            $time_to_client
 * @property string            $phone
 * @property integer           $time_offset
 * @property integer           $is_fix
 * @property integer           $tenant_company_id
 *
 * @property ClientReview[]    $clientReviews
 * @property Exchange[]        $exchanges
 * @property City              $city
 * @property ClientCompany     $company
 * @property Worker            $worker
 * @property OrderStatus       $status
 * @property Parking           $parking
 * @property TaxiTariff        $tariff
 * @property Tenant            $tenant
 * @property User              $userCreate
 * @property User              $userModifed
 * @property OrderChange[]     $orderChanges
 * @property OrderHasOption[]  $orderHasOptions
 * @property CarOption[]       $options
 * @property OrderStatusTime[] $orderStatusTimes
 * @property ParkingOrder[]    $parkingOrders
 */
class Order extends \yii\db\ActiveRecord
{

    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_1 = 'IOS';
    const DEVICE_2 = 'ANDROID';
    const DEVICE_WORKER = 'WORKER';

    const PAYMENT_CASH = 'CASH';
    const PAYMENT_CARD = 'CARD';

    const PRE_ODRER_TIME = 1800;
    const PICK_UP_TIME = 300;
    const STATUS_NEW_FREE = 4;
    const STATUS_NEW_EXPIRED = 52;
    const STATUS_NEW_PRDV = 6;
    const STATUS_NEW_PRDV_FREE = 15;
    const STATUS_NEW_PRDV_NO_PARKING = 16;

    public $additional_option = [];
    // public $phone;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $status_reject;
    private $pickUp;
    private $orderOffset;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'status_id'], 'required'],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'city_id',
                    'tariff_id',
                    'client_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'order_time',
                    'time_offset',
                ],
                'integer',
            ],
            [['address', 'comment', 'payment', 'phone'], 'string'],
            [
                ['show_phone', 'additional_option', 'order_date', 'order_hours', 'order_minutes', 'status_reject'],
                'safe',
            ],
            [['predv_price'], 'number'],
            [['is_fix'], 'in', 'range' => [0, 1]],
            ['is_fix', 'default', 'value' => '0'],
            [['device'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'         => 'Order ID',
            'tenant_id'        => 'Tenant ID',
            'worker_id'        => 'Worker ID',
            'city_id'          => 'City ID',
            'tariff_id'        => 'Tariff',
            'client_id'        => 'Client ID',
            'user_create'      => 'User Create',
            'user_modifed'     => 'User Modifed',
            'status_id'        => 'Status',
            'address'          => 'Address',
            'comment'          => 'Comment',
            'predv_price'      => 'Predv Price',
            'create_time'      => 'Create Time',
            'device'           => 'Device',
            'payment'          => 'Payment',
            'show_phone'       => 'Show client phone to worker',
            'parking_id'       => 'Parking',
            'preliminary_calc' => 'Preliminary calculation',
            'phone'            => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReview()
    {
        return $this->hasOne(\app\models\client\ClientReview::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(Exchange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParking()
    {
        return $this->hasOne(\app\models\parking\Parking::className(), ['parking_id' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(\app\models\order\OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(\app\models\tariff\TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\app\models\tenant\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(\app\models\tenant\User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModifed()
    {
        return $this->hasOne(\app\models\tenant\User::className(), ['user_id' => 'user_modifed']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\app\models\address\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(\app\models\order\OrderChange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHasOptions()
    {
        return $this->hasMany(\app\models\order\OrderHasOption::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(\app\models\car\CarOption::className(),
            ['option_id' => 'option_id'])->viaTable('{{%order_has_option}}', ['order_id' => 'order_id']);
    }

    //    /**
    //     * @return \yii\db\ActiveQuery
    //     */
    //    public function getDriverShift()
    //    {
    //        return $this->hasMany(\app\models\driver\DriverShift::className(), ['driver_id' => 'driver_id']);
    //    }

    public function getOrderDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id']);
    }

    public static function getLastOrderNumber($tenantId)
    {
        $connection = Yii::$app->db;
        $command    = $connection->createCommand('SELECT MAX(order_number) FROM ' . Order::tableName() . 'WHERE `tenant_id`=' . $tenantId);

        return $command->queryScalar();
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->isNewRecord) {
            $retryCount = \Yii::$app->params['saveOrder.retryCount'];

            for ($i = 0; $i < $retryCount; $i++) {
                $this->order_number = Order::getLastOrderNumber($this->tenant_id) + 1;
                try {
                    return parent::save($runValidation, $attributeNames);
                } catch (Exception $ex) {
                    if (strpos($ex->getMessage(), 'ui_order__tenant_id__order_number') === false) {
                        throw $ex;
                    } else {
                        continue;
                    }
                }
            }

            return false;
        }

        return parent::save($runValidation, $attributeNames);
    }

    public static function filterAddress($addressArray)
    {
        if (is_array($addressArray)) {
            $newArray = ['address' => []];
            foreach ($addressArray['address'] as $address) {
                $newAddress = [];
                foreach ($address as $key => $paramVal) {
                    if (!empty($paramVal)) {
                        $paramVal = trim(strip_tags(stripcslashes($paramVal)));

                        if ($key == "city") {
                            $paramVal = mb_substr($paramVal, 0, 50, 'utf-8');
                        }
                        if ($key == "street") {
                            $paramVal = mb_substr($paramVal, 0, 100, 'utf-8');
                        }
                        if (in_array($key, ['house', 'housing', 'porch', 'apt'])) {
                            $paramVal = mb_substr($paramVal, 0, 10, 'utf-8');
                        }
                    }
                    $newAddress[$key] = $paramVal;
                }
                $newArray['address'] [] = $newAddress;
            }

            return $newArray;
        }
    }

    /**
     * Фильтрация отдачи инфы по заказу, доделать работу с допиками и с их сохранением
     *
     * @param array  $orderData
     * @param int    $tenantId
     * @param string $language
     *
     * @return array
     */
    public static function filterOrderData($orderData, $tenantId, $language)
    {
        //Проверям, нужно ли добавить инфу по бонсуам клиента
        if (!empty($orderData['bonus_payment'])
            && $orderData['payment'] != 'CORP_BALANCE'
        ) {
            $bonusdata = Client::getBonusPayment(
                $tenantId,
                $orderData['order_id'],
                $orderData['client_id'],
                $orderData['currency_id'],
                $orderData['tariff_id']
            );
        } else {
            $bonusdata = [];
        }

        $positionId = $orderData['position_id'];
        $cityId     = $orderData['city_id'];

        $propertyForDelete = [
            'city_id',
            'tariff_id',
            'parking_id',
            'worker_id',
            'car_id',
            'user_id',
            'user_create',
            'status_id',
            'user_modifed',
            'create_time',
            'update_time',
            'client_device_token',
            'userCreated',
            'city',
            'worker',
            'car',
            'call_warning_id',
            'status_time',
            'time_to_client',
            'tenant_id',
            'company_id',
            'client_id',
            'predv_price',
            'predv_distance',
            'predv_time',
        ];

        foreach ($propertyForDelete as $property) {
            if (array_key_exists($property, $orderData)) {
                unset($orderData[$property]);
            }
        }

        if (isset($orderData['settings']['ORDER_OFFER_SEC'])) {
            $offerSec = $orderData['settings']['ORDER_OFFER_SEC'];
            unset($orderData['settings']);
            $orderData['offer_sec'] = $offerSec;
        }

        if (isset($orderData['status']['dispatcher_sees'])) {
            unset($orderData['status']['dispatcher_sees']);
        }
        if (isset($orderData['tariff']['tenant_id'])) {
            unset($orderData['tariff']['tenant_id']);
        }
        if (isset($orderData['tariff']['block'])) {
            unset($orderData['tariff']['block']);
        }
        if (isset($orderData['tariff']['class']['class'])) {
            $orderData['tariff']['class']['class'] = Yii::t('car', $orderData['tariff']['class']['class'], null,
                $language);
        }

        if (!empty($orderData['options'])) {
            $newOptions = [];
            foreach ($orderData['options'] as $optionData) {
                if (isset($optionData["name"])) {
                    $optionData["name"] = Yii::t('car-options', $optionData["name"], null, $language);
                }
                if (isset($optionData["additionalOptions"])) {
                    $additionalOptions = $optionData["additionalOptions"];
                    if (isset($additionalOptions[0])) {
                        if (isset($additionalOptions[0]["price"])) {
                            $optionData["price"] = $additionalOptions[0]["price"];
                        }
                    }
                }
                unset($optionData["additionalOptions"]);
                unset($optionData["option_id"]);
                $newOptions[] = $optionData;
            }
            $orderData['options'] = $newOptions;
        }

        if (isset($orderData['client']['client_id'])) {
            unset($orderData['client']['client_id']);
        }


        //Подчистим цену
        if (isset($orderData['costData']['city_time'])) {
            unset($orderData['costData']['city_time']);
        }
        if (isset($orderData['costData']['city_distance'])) {
            unset($orderData['costData']['city_distance']);
        }
        if (isset($orderData['costData']['city_cost'])) {
            unset($orderData['costData']['city_cost']);
        }

        if (isset($orderData['costData']['out_city_time'])) {
            unset($orderData['costData']['out_city_time']);
        }
        if (isset($orderData['costData']['out_city_distance'])) {
            unset($orderData['costData']['out_city_distance']);
        }
        if (isset($orderData['costData']['out_city_cost'])) {
            unset($orderData['costData']['out_city_cost']);
        }

        $showWorkerPhone = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_WORKER_PHONE, $cityId, $positionId);

        if (isset($showWorkerPhone)) {
            if ($showWorkerPhone == "0") {
                if (isset($orderData["phone"])) {
                    unset($orderData["phone"]);
                }
            }
        } else {
            if (isset($orderData["phone"])) {
                unset($orderData["phone"]);
            }
        }
        unset($orderData['show_phone']);

        try {
            $orderData['address'] = unserialize($orderData['address']);
        } catch (\Exception $exc) {
            Yii::error($exc);
        }

        if (isset($orderData['address'])) {
            $showForWorkerEndAddress = TenantSetting::getSettingValue(
                $tenantId, TenantSetting::SETTING_SHOW_WORKER_ADDRESS, $cityId, $positionId);
            if (empty($showForWorkerEndAddress)) {
                if (isset($orderData['address']['B'])) {
                    unset($orderData['address']['B']);
                }
                if (isset($orderData['address']['C'])) {
                    unset($orderData['address']['C']);
                }
                if (isset($orderData['address']['D'])) {
                    unset($orderData['address']['D']);
                }
                if (isset($orderData['address']['E'])) {
                    unset($orderData['address']['E']);
                }
            }
        }
        $showForWorkerFioClient = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_WORKER_CLIENT, $cityId, $positionId);
        if (empty($showForWorkerFioClient)) {
            unset($orderData['client']);
        }
        $orderData['order_time'] = date('d.m.Y H:i', $orderData['order_time']);

        //Нужно отфильтровать тарифы от лишних полей в зависимости от isDay
        if (isset($orderData['costData']['tariffInfo']['isDay'])) {
            $isDay         = $orderData['costData']['tariffInfo']['isDay'];
            $tariffInCity  = isset($orderData['costData']['tariffInfo']['tariffDataCity']) ? $orderData['costData']['tariffInfo']['tariffDataCity'] : null;
            $tariffOutCity = isset($orderData['costData']['tariffInfo']['tariffDataTrack']) ? $orderData['costData']['tariffInfo']['tariffDataTrack'] : null;
            $tariffArr     = [
                'inCity'  => $tariffInCity,
                'outCity' => $tariffOutCity,
            ];

            $newTariffArr = [];
            foreach ($tariffArr as $key => $tariff) {
                if ($isDay == 1) {
                    $propertyForDelete = [
                        'planting_price_night',
                        'planting_include_night',
                        'next_km_price_night',
                        'min_price_night',
                        'supply_price_night',
                        'wait_time_night',
                        'wait_driving_time_night',
                        'wait_price_night',
                        'speed_downtime_night',
                        'rounding_night',
                        'allow_day_night',
                        'active',
                        'next_cost_unit_night',
                        'next_km_price_night_time',
                        'planting_include_night_time',
                    ];
                } else {
                    $propertyForDelete = [
                        'planting_price_day',
                        'planting_include_day',
                        'next_km_price_day',
                        'min_price_day',
                        'supply_price_day',
                        'wait_time_day',
                        'wait_driving_time_day',
                        'wait_price_day',
                        'speed_downtime_day',
                        'rounding_day',
                        'allow_day_night',
                        'active',
                        'next_cost_unit_day',
                        'next_km_price_day_time',
                        'planting_include_day_time',
                    ];
                }
                foreach ($propertyForDelete as $property) {
                    if (array_key_exists($property, $tariff)) {
                        unset($tariff["$property"]);
                    }
                }
                $newTariffArr[$key] = $tariff;
            }
            if (isset($newTariffArr['inCity']['accrual'])) {
                if ($newTariffArr['inCity']['accrual'] == "INTERVAL") {
                    if (isset($newTariffArr['inCity']['next_km_price_day'])) {
                        $nextKm                                      = unserialize($newTariffArr['inCity']['next_km_price_day']);
                        $newTariffArr['inCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['inCity']['next_km_price_night'])) {
                            $nextKm                                        = unserialize($newTariffArr['inCity']['next_km_price_night']);
                            $newTariffArr['inCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }
            }
            if (isset($newTariffArr['outCity']['accrual'])) {
                if ($newTariffArr['outCity']['accrual'] == "INTERVAL") {
                    if (isset($newTariffArr['outCity']['next_km_price_day'])) {
                        $nextKm                                       = unserialize($newTariffArr['outCity']['next_km_price_day']);
                        $newTariffArr['outCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['outCity']['next_km_price_night'])) {
                            $nextKm                                         = unserialize($newTariffArr['outCity']['next_km_price_night']);
                            $newTariffArr['outCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }
            }

            $orderData['costData']['tariffInfo']['tariffDataCity']  = $newTariffArr['inCity'];
            $orderData['costData']['tariffInfo']['tariffDataTrack'] = $newTariffArr['outCity'];
        }
        $orderData['bonusData'] = $bonusdata;

        return $orderData;
    }

}
