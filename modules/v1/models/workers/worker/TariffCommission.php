<?php

namespace app\modules\v1\models\workers\worker;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "tbl_tariff_commission".
 *
 * @property integer            $id
 * @property integer            $option_id
 * @property string             $type
 * @property string             $value
 * @property string             $from
 * @property string             $to
 * @property string             $kind
 * @property string             $tax
 *
 * @property WorkerOptionTariff $option
 */
class TariffCommission extends ActiveRecord
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_MONEY = 'MONEY';

    const CLASS_FIXED = 'FIXED';
    const CLASS_INTERVAL = 'INTERVAL';

    const KIND_FIXED = 'FIXED';
    const KIND_CASH = 'CASH';
    const KIND_CARD = 'CARD';
    const KIND_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const KIND_CORP_BALANCE = 'CORP_BALANCE';
    const KIND_BORDER = 'BORDER';

    const KIND_ALL = [
        self::KIND_FIXED,
        self::KIND_CASH,
        self::KIND_CARD,
        self::KIND_PERSONAL_ACCOUNT,
        self::KIND_CORP_BALANCE,
        self::KIND_BORDER,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tariff_commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id'], 'required'],
            [['option_id'], 'integer'],
            [['type', 'kind'], 'string'],
            [['value', 'from', 'to', 'tax'], 'number', 'min' => 0],
            [
                ['option_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerOptionTariff::className(),
                'targetAttribute' => ['option_id' => 'option_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'option_id' => 'Option ID',
            'type'      => 'Type',
            'value'     => 'Value',
            'from'      => 'From',
            'to'        => 'To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(WorkerOptionTariff::className(), ['option_id' => 'option_id']);
    }

}
