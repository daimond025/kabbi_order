<?php

namespace app\modules\v1\models\workers\worker;


/**
 * This is the model class for table "tbl_worker_option_tariff".
 *
 * @property integer                $option_id
 * @property integer                $tariff_id
 * @property string                 $tariff_type
 * @property string                 $active_date
 * @property string                 $increase_order_sum
 * @property string                 $increase_sum_fix
 * @property string                 $increase_sum_fix_type
 * @property string                 $increase_sum_limit
 * @property int                    $sort
 *
 * @property WorkerOptionDiscount[] $workerOptionDiscounts
 * @property TariffCommission[]     $tariffCommissions
 */
class WorkerOptionTariff extends \yii\db\ActiveRecord
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_MONEY = 'MONEY';

    const OPTION_TYPE_CURRENT = 'CURRENT';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_option_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'tariff_type'], 'required'],
            [['tariff_id', 'sort'], 'integer'],
            [['tariff_type', 'commission_type', 'active_date'], 'string'],
            [['commission'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id'       => 'Option ID',
            'tariff_id'       => 'Tariff ID',
            'tariff_type'     => 'Tariff Type',
            'commission_type' => 'Commission Type',
            'commission'      => 'Commission',
            'active_date'     => 'Active Date',
            'sort'            => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOptionDiscounts()
    {
        return $this->hasMany(WorkerOptionDiscount::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffCommissions()
    {
        return $this->hasMany(TariffCommission::className(), ['option_id' => 'option_id']);
    }

}
