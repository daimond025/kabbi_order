<?php

namespace app\modules\v1\models\workers\worker;

use app\modules\v1\models\workers\car\CarOption;
use Yii;

/**
 * This is the model class for table "tbl_worker_option_discount".
 *
 * @property integer            $discount_id
 * @property integer            $car_option_id
 * @property integer            $option_id
 * @property string             $discount_line_type
 * @property string             $discount_order_type
 * @property string             $discount_line = '
 * @property string             $discount_order
 *
 * @property CarOption          $carOption
 * @property WorkerOptionTariff $option
 */
class WorkerOptionDiscount extends \yii\db\ActiveRecord
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_MONEY = 'MONEY';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_option_discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_option_id', 'option_id'], 'required'],
            [['car_option_id', 'option_id'], 'integer'],
            [['discount_line_type', 'discount_order_type'], 'string'],
            [['discount_line', 'discount_order'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'discount_id'         => 'Discount ID',
            'car_option_id'       => 'Car Option ID',
            'option_id'           => 'Option ID',
            'discount_line_type'  => 'Discount Line Type',
            'discount_order_type' => 'Discount Order Type',
            'discount_line'       => 'Discount Line',
            'discount_order'      => 'Discount Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'car_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(WorkerOptionTariff::className(), ['option_id' => 'option_id']);
    }
}
