<?php

namespace app\modules\v1\models\workers;

use app\modules\v1\models\City;
use yii\base\Object;

/**
 * Class TimeHelper
 * @package v3\components
 */
class TimeHelper extends Object
{
    /**
     * Getting current time
     * @return int
     */
    function getCurrentTime()
    {
        return time();
    }

    public static function getCurrentTimeInCity($cityId)
    {
        $timeUtc    = time();
        $offsetCity = City::getTimeOffset($cityId);

        return $timeUtc + $offsetCity;
    }

}