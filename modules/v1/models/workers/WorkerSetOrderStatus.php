<?php

namespace app\modules\v1\models\workers;

use app\components\exchange\ServiceExchange;
use app\components\WorkerErrorCode;
use app\models\ActiveOrderRepository;
use app\models\OrderStatusRecord;
use app\models\TenantSetting;
use app\models\Worker;
use app\modules\v1\components\services\order\OrderService;
use app\modules\v1\components\services\promo\PromoService;
use app\modules\v1\components\services\promo\Setting;
use app\modules\v1\components\services\order\orderUpdate\exceptions\OrderUpdateException;
use app\modules\v1\components\services\order\orderUpdate\OrderInfo;
use app\modules\v1\components\services\order\orderUpdate\OrderUpdateAddress;
use app\modules\v1\components\services\order\orderUpdate\OrderUpdatePredvPrice;
use app\modules\v1\components\services\order\orderUpdate\OrderValidate;
use app\modules\v1\models\City;
use app\modules\v1\models\OrderStatus;
use app\modules\v1\models\workers\balance\WorkerTransaction;
use app\modules\v1\models\workers\order\Order;
use app\modules\v1\models\workers\order\OrderDetailCost;
use app\modules\v1\models\workers\worker\TariffCommission;
use app\modules\v1\models\workers\worker\WorkerOptionTariff;
use app\modules\v1\models\workers\worker\WorkerShift;
use paymentGate\PaymentGateService;
use paymentGate\ProfileService;
use referralSystem\exceptions\OrderWithDetailCostNotFoundException;
use referralSystem\exceptions\ReferralSystemException;
use referralSystem\exceptions\ReferralSystemNotFoundException;
use referralSystem\models\OrderRepository;
use referralSystem\models\ReferralSystemRepository;
use referralSystem\ReferralSystemService;
use Yii;
use yii\base\Exception;
use yii\base\Object;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\OrderChangeData;

/**
 * Class WorkerSetOrderStatus
 * @package app\modules\v1\models\workers
 */
class WorkerSetOrderStatus extends Object
{

    /** @var $orderService OrderService */
    protected $orderService;

    public function init()
    {
        parent::init();

        $this->orderService = Yii::$container->get(OrderService::class);
    }

    /**
     *  Getting worker service
     * @return WorkerRepository
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function getWorkerService()
    {
        /**
         * @var WorkerRepository $service
         */
        $service = \Yii::$container->get('workerService');
        return $service;
    }

    /**
     * @return WorkerTariffService
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function getWorkerTariffService()
    {
        /**
         * @var WorkerTariffService $service
         */
        $service = \Yii::$container->get('workerTariffService');
        return $service;
    }

    /**
     * @return TimeHelper
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function getTimeHelper()
    {
        /**
         * @var TimeHelper $service
         */
        $service = \Yii::$container->get('timeHelper');
        return $service;
    }

    /**
     * @param       $errorCode
     * @param array $info
     *
     * @return array
     */
    private function getPreparedResponse($errorCode, array $info = [])
    {
        return ArrayHelper::merge(WorkerErrorCode::getErrorData($errorCode), [
            'result' => $info,
        ]);
    }

    /**
     * Update order
     * @param $orderId
     * @param $tenantId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @param string $uuid
     * @return array
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function update($orderId, $tenantId, $data, $updateTime, $lang, $uuid = '')
    {
        if (isset($data['status_id'])) {
            return $this->setOrderStatus($orderId, $tenantId, $data, $updateTime, $lang, $uuid);
        }
        return $this->updateOrder($orderId, $tenantId, $data, $updateTime, $lang, $uuid);

    }

    /**
     * @param int $tenantId
     * @param int $callsign
     * @param int $shiftId
     * @param array $pauseData
     *
     * @return array
     */
    private function checkAndStopPauseAndReturnData($tenantId, $callsign, $shiftId, $pauseData)
    {
        $count = count($pauseData);
        if ($count && $pauseData[$count - 1]['pause_end'] === null) {
            $pauseData[$count - 1]['pause_end'] = (string)time();

            $shift = WorkerShift::findOne($shiftId);
            if ($shift !== null) {
                $shift->pause_data = serialize($pauseData);
                if (!$shift->save()) {
                    $error = implode('; ', $shift->getFirstErrors());
                    \Yii::error("Stop unclosed pause error: tenantId={$tenantId}, callsign={$callsign}, shiftId={$shiftId}, error={$error}");
                }
            }
        }

        return $pauseData;
    }

    /**
     * Setup order status
     * \Yii::warning('daimond' . $callsignInOrder ."|||" . $workerCallsign ); - пример логирования
     *
     * @param int $orderId
     * @param int $tenantId
     * @param array $data
     * @param int $updateTime
     * @param string $lang
     * @param string $uuid
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    private function setOrderStatus($orderId, $tenantId, $data, $updateTime, $lang, $uuid = '')
    {
        $workerCallsign = (int)$data['worker_login'];
        $statusNewId = (int)$data['status_id'];
        $timeToClient = isset($data['time_to_client']) ? (int)$data['time_to_client'] : null;
        $detailOrderData = isset($data['detail_order_data']) ? $data['detail_order_data'] : null;
        $dispatcherId = isset($data['dispatcher_id']) ? $data['dispatcher_id'] : null;

        //Детализация заказа
        if ($detailOrderData !== null) {
            $detailOrderData = Json::decode($detailOrderData);
        }


        $workerService = $this->getWorkerService();
        $workerTariffService = $this->getWorkerTariffService();


        // Инфа по заказу из редиса
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $orderDataRedis = $activeOrderRepository->getOne($orderId);


        $statusOldId = isset($orderDataRedis['status']['status_id']) ? (int)$orderDataRedis['status']['status_id'] : null;
        $statusGroupOld = isset($orderDataRedis['status']['status_group']) ? $orderDataRedis['status']['status_group'] : null;

        $callsignInOrder = isset($orderDataRedis['worker']['callsign']) ? (int)$orderDataRedis['worker']['callsign'] : null;
        $offeredWorkers = isset($orderDataRedis['workers_offered']) ? $orderDataRedis['workers_offered'] : [];

        $edit = (isset($orderDataRedis['edit']) && in_array((int)$orderDataRedis['edit'], [ 0, 1 ]))
            ? (int)$orderDataRedis['edit'] : 0;

        // если статус не поменялся  - нет надобности проводить событие через цикл
        if(empty($orderDataRedis) ||
            ($statusNewId === $statusOldId && (!empty($callsignInOrder) && !empty($workerCallsign) && ((int)$callsignInOrder === (int)$workerCallsign)))
            || (in_array($statusGroupOld, [
                OrderStatus::STATUS_GROUP_COMPLETED,
                OrderStatus::STATUS_GROUP_REJECTED,
            ], false) )){
            return $this->getPreparedResponse(WorkerErrorCode::ORDER_REPEAT_STATUS, ['set_result' => 2]);
        }
        elseif (empty($orderDataRedis)) {
            return $this->getPreparedResponse(WorkerErrorCode::EMPTY_DATA_IN_DATABASE, ['set_result' => 0]);
        }

        //@todo  удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
        if (isset($orderDataRedis['offered_workers_list'])) {
            $offeredWorkers = $orderDataRedis['offered_workers_list'];
        }


        if ($statusGroupOld === OrderStatus::STATUS_GROUP_PRE_ORDER
            && $statusNewId === OrderStatus::STATUS_REFUSED_ORDER_ASSIGN
        ) {
            $statusNewId = OrderStatus::STATUS_PRE_REFUSE_WORKER;
        } elseif ($statusGroupOld !== OrderStatus::STATUS_GROUP_PRE_ORDER
            && $statusNewId === OrderStatus::STATUS_PRE_REFUSE_WORKER
        ) {
            $statusNewId = OrderStatus::STATUS_REFUSED_ORDER_ASSIGN;
        }

        if ($statusNewId !== (int)$data['status_id']) {
            \Yii::$app->apiLogger->log('Status was changed (' . $data['status_id'] . ' => ' . $statusNewId . ')');
        }

        // daimond025 если водитель хочет выполнить заказ прям сейчас  - без того что время уже подошло
        if ($statusNewId === OrderStatus::STATUS_EXECUTION_PRE
            && in_array($statusOldId, [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                OrderStatus::STATUS_PRE_GET_WORKER,
            ], false)
            && ($edit == OrderStatus::EDIT_NOT_PERMIT)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::FORBIDDEN_ACTION, ['set_result' => 0]);
        }


        if ($statusNewId === OrderStatus::STATUS_WORKER_REFUSED
            && in_array($statusOldId, [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                OrderStatus::STATUS_PRE_GET_WORKER,
                OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION,
                OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION,
                OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION,
                OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION,
            ], false)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::FORBIDDEN_ACTION, ['set_result' => 0]);
        }


        if (in_array($statusNewId, [
            OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION,
            OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION,
        ], false)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::FORBIDDEN_ACTION, ['set_result' => 0]);
        }


        /// daimond025
        if ( ($callsignInOrder !== $workerCallsign
                || !in_array($statusOldId,[
                OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION,
                OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION
            ], false))
            && in_array($statusNewId, [
                OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION,
                OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION,
            ], false)
        ) {
            return $this->getPreparedResponse(
                $callsignInOrder !== $workerCallsign ? WorkerErrorCode::ORDER_IS_BUSY : WorkerErrorCode::FORBIDDEN_ACTION,
                ['set_result' => 0]);
        }


        // Если это предварительный заказ и исполнитель хочет его взять,
        // то проверить не занят ли он
        if ($statusNewId === OrderStatus::STATUS_PRE_GET_WORKER
            && !in_array($statusOldId, [
                OrderStatus::STATUS_PRE,
                OrderStatus::STATUS_PRE_NOPARKING,
                OrderStatus::STATUS_PRE_ORDER_FOR_COMPANY,
                OrderStatus::STATUS_PRE_ORDER_FOR_HOSPITAL,
            ], false)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, ['set_result' => 0]);
        }

        if ($statusNewId === OrderStatus::STATUS_PRE_REFUSE_WORKER
            && (!in_array($statusOldId, [
                    OrderStatus::STATUS_PRE_GET_WORKER,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                    OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION,
                ], false)
                || $callsignInOrder !== $workerCallsign)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
        }

        //Если экшн от водителя
        if (OrderStatus::isStatusWorkerAction($statusNewId)) {

            //Водитель берет заказ который он уже выполнял ранее, сразу возвращаем позитивный результат
            if ($statusNewId === OrderStatus::STATUS_GET_WORKER
                && $workerCallsign === $callsignInOrder
                && in_array($statusGroupOld, [OrderStatus::STATUS_GROUP_EXECUTING, OrderStatus::STATUS_GROUP_CAR_ASSIGNED], false)) {
                return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
            }


            //TODO daimond025
            if ($statusNewId === OrderStatus::STATUS_GET_WORKER && $statusGroupOld !== OrderStatus::STATUS_GROUP_NEW) {
                return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, ['set_result' => 0]);
            }


            if ($statusNewId === OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT
                && !in_array($statusOldId, [OrderStatus::STATUS_FREE, OrderStatus::STATUS_OVERDUE], false)) {
                return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, ['set_result' => 0]);
            }


            //Если исполнитель хочет взять заказ во время предложения, проверяем ему ли он предлагается
            if ($statusNewId === OrderStatus::STATUS_GET_WORKER
                && $statusOldId === OrderStatus::STATUS_OFFER_ORDER
                && (!in_array($workerCallsign, $offeredWorkers, false))
            ) {
                return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, ['set_result' => 0]);
            }

            // нельзя перепрыгивать через статусы
            if(($statusNewId === OrderStatus::STATUS_WORKER_WAITING) && ($statusOldId === OrderStatus::STATUS_OFFER_ORDER )){
                return $this->getPreparedResponse(WorkerErrorCode::ORDER_SELECT_DRIVER, ['set_result' => 0]);
            }

            if (($statusNewId === OrderStatus::STATUS_GET_WORKER
                    && ((!empty($callsignInOrder) && $callsignInOrder !== $workerCallsign)
                        || $statusOldId === OrderStatus::STATUS_MANUAL_MODE))
                || //(!in_array($statusNewId, [OrderStatus::STATUS_GET_WORKER , OrderStatus::STATUS_WORKER_WAITING,OrderStatus::STATUS_EXECUTING,  OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT])
                (!empty($callsignInOrder) && !empty($workerCallsign) && ((int)$callsignInOrder !== (int)$workerCallsign))
            ) {
                return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, ['set_result' => 0]);
            }

        }


        // принуждение к заказу водитедя
        $denyRefuseOrder = isset($orderDataRedis['deny_refuse_order']) ? $orderDataRedis['deny_refuse_order'] : null;
        if ($denyRefuseOrder === 1
            && in_array($statusNewId, [
                OrderStatus::STATUS_WORKER_REFUSED,
                OrderStatus::STATUS_PRE_REFUSE_WORKER,
                OrderStatus::STATUS_REFUSED_ORDER_ASSIGN,
            ], false)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::DENY_REFUSE_ORDER, ['set_result' => 0]);
        }

        // нельзя отклонить заказ больничный заказ
        $order_type = ArrayHelper::getValue($orderDataRedis, 'device');

        if($order_type === OrderStatus::TYPE_HOSPITAL
            && in_array($statusNewId, [
                OrderStatus::STATUS_WORKER_REFUSED,
                OrderStatus::STATUS_PRE_REFUSE_WORKER,
                OrderStatus::STATUS_REFUSED_ORDER_ASSIGN,
            ], false)
            ){
            return $this->getPreparedResponse(WorkerErrorCode::DENY_REFUSE_ORDER, ['set_result' => 0]);
        }


        // Если отказ от заказа, то если заказ сейчас не его,
        // то не переключать статус, сказать все ок
        if ($statusNewId === OrderStatus::STATUS_WORKER_REFUSED
            && (empty($callsignInOrder) || $callsignInOrder !== $workerCallsign)
        ) {
            return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
        }

        //Ищем заказ в БД
        $orderDataMysql = Order::findOne($orderId);

        if (isset($statusNewId)) {
            // Перезапишем статусы в редисе и мускуле
            $statusData = OrderStatusRecord::findOne($statusNewId);

            // ограничения на порядок выполнения заказа по группам  - по порядку
            if (OrderStatus::isStatusWorkerAction($statusNewId) && !OrderStatus::isGroupStatusChain($statusData->status_group , $statusGroupOld)) {
                return $this->getPreparedResponse(WorkerErrorCode::FORBIDDEN_ACTION, ['set_result' => 0]);
            }

            $orderDataMysql->status_id = $statusNewId;
            $orderDataMysql->status_time = time();

            $orderDataRedis['status']['status_id'] = $statusData->status_id;
            $orderDataRedis['status']['name'] = $statusData->name;
            $orderDataRedis['status']['status_group'] = $statusData->status_group;
            $orderDataRedis['status']['dispatcher_sees'] = $statusData->dispatcher_sees;
            $orderDataRedis['status_id'] = $statusData->status_id;
            $orderDataRedis['status_time'] = time();

            $worker = $workerService->getActiveWorker($tenantId, $workerCallsign);

            if ($statusNewId === OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT) {
                if (empty($worker)) {
                    return $this->getPreparedResponse(WorkerErrorCode::SHIFT_IS_CLOSED, [
                        'set_result' => 0,
                    ]);
                }

                if ($worker['worker']['status'] === Worker::STATUS_BLOCKED) {
                    return $this->getPreparedResponse(
                        WorkerErrorCode::WORKER_BLOCKED, ['set_result' => 0]
                    );
                }

                if (!$this->orderService->isAllowedSetStatusAssignedAtOrderSoft(
                    $tenantId, $workerCallsign, $orderDataRedis['city_id'], $orderDataRedis['position_id'])
                ) {
                    return $this->getPreparedResponse(WorkerErrorCode::ORDER_ASSIGN_LIMIT, [
                        'set_result' => 0,
                    ]);
                }

                //Обновим у заказа в редисе  и в мускуле исполнителя
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;

                $orderDataRedis['worker_id'] = $worker['worker']['worker_id'];
                $orderDataRedis['car_id'] = $carId;
                $orderDataRedis['worker'] = $worker['worker'];
                $orderDataRedis['car'] = isset($worker['car']) ? $worker['car'] : null;

                $orderDataMysql->worker_id = $worker['worker']['worker_id'];
                $orderDataMysql->car_id = $carId;

            }

            // Если исполнитель принял прямой заказ, то нужно поменять ему статус в редисе,
            // остальные статусы (FREE,OFFER_ORDER,BLOCKED) ставит ему система
            if ($statusNewId === OrderStatus::STATUS_GET_WORKER) {
                if (empty($worker)) {
                    return $this->getPreparedResponse(WorkerErrorCode::SHIFT_IS_CLOSED, [
                        'set_result' => 0,
                    ]);
                }

                if ($worker['worker']['status'] === Worker::STATUS_BLOCKED) {
                    return $this->getPreparedResponse(WorkerErrorCode::WORKER_BLOCKED, ['set_result' => 0]);
                }

                $active_orders = ArrayHelper::getValue($worker, 'worker.active_orders', []);

                if ($worker['worker']['status'] === Worker::STATUS_ON_ORDER
                    && ( !(empty($worker['worker']['last_order_id']) || $worker['worker']['last_order_id'] == $orderId) || (count($active_orders) > 0))
                    && ($order_type !== OrderStatus::TYPE_HOSPITAL)
                ) {
                    return $this->getPreparedResponse(WorkerErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER,
                        ['set_result' => 0]);
                }

                #todo больничная поездка
                if($order_type === OrderStatus::TYPE_HOSPITAL){

                    #todo времянка - только для определенных водителй
                    $order_yandex_temp = strtoupper(ArrayHelper::getValue($worker, 'worker.yandex_account_number', ''));
                    if( ($worker['worker']['status'] === Worker::STATUS_ON_ORDER
                        && ($order_yandex_temp !==  OrderStatus::TYPE_HOSPITAL) )
                    ){
                        return $this->getPreparedResponse(WorkerErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER,
                            ['set_result' => 0]);
                    }

                    // ограничение на количество
                    $limit_pre_order =  $this->orderService->isMaxCountActiveOrders($active_orders, $tenantId, $orderDataRedis['city_id'], $orderDataRedis['position_id']);
                    if(!$limit_pre_order){
                        return $this->getPreparedResponse(WorkerErrorCode::ORDER_ASSIGN_LIMIT, [
                            'set_result' => 0,
                        ]);
                    }
                }

                //Обновим у заказа в редисе  и в мускуле исполнителя
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;

                $orderDataRedis['worker_id'] = $worker['worker']['worker_id'];
                $orderDataRedis['car_id'] = $carId;
                $orderDataRedis['worker'] = $worker['worker'];
                $orderDataRedis['car'] = isset($worker['car']) ? $worker['car'] : null;

                $orderDataMysql->worker_id = $worker['worker']['worker_id'];
                $orderDataMysql->car_id = $carId;

                //Сохраняем время подачи авто
                if (isset($timeToClient)) {
                    $orderDataRedis['time_to_client'] = $timeToClient;
                    $orderDataMysql->time_to_client = $timeToClient;
                }

                // Обновим статус исполнителя
                $worker['worker']['status'] = Worker::STATUS_ON_ORDER;
                if (isset($worker['worker']['pause_data'])) {
                    $worker['worker']['pause_data'] = $this->checkAndStopPauseAndReturnData($tenantId,
                        $workerCallsign,
                        $worker['worker']['worker_shift_id'], $worker['worker']['pause_data']);
                    $worker['worker']['break_reason'] = null;
                }

                //Сбросим счетчик отказов подряд
                $worker['worker']['in_row_reject'] = 0;

                if ($workerService->saveActiveWorker($tenantId, $workerCallsign, $worker)) {
                    $orderChange = new OrderChangeData();
                    $orderChange->tenant_id = $tenantId;
                    $orderChange->order_id = $orderId;
                    $orderChange->change_field = 'status';
                    $orderChange->change_object_id = (string)$workerCallsign;
                    $orderChange->change_object_type = 'worker';
                    $orderChange->change_subject = 'worker';
                    $orderChange->change_val = Worker::STATUS_ON_ORDER;
                    $orderChange->change_time = time();

                    if (!$orderChange->save()) {
                        Yii::error($orderChange->errors);
                    }
                }
            }

            // Если исполнитель принял предварительный прямой
            if ($statusNewId === OrderStatus::STATUS_PRE_GET_WORKER) {
                if (empty($worker)) {
                    return $this->getPreparedResponse(WorkerErrorCode::SHIFT_IS_CLOSED, [
                        'set_result' => 0,
                    ]);
                }

                if ($worker['worker']['status'] === Worker::STATUS_BLOCKED) {
                    return $this->getPreparedResponse(WorkerErrorCode::WORKER_BLOCKED, ['set_result' => 0]);
                }

                $timeHelper = $this->getTimeHelper();
                if (!empty($worker['worker']['preorder_end_block'])
                    && $timeHelper->getCurrentTime() < $worker['worker']['preorder_end_block']
                ) {
                    return $this->getPreparedResponse(WorkerErrorCode::WORKER_PRE_ORDER_BLOCKED, [
                        'set_result'   => 0,
                        'unblock_time' => (int)$worker['worker']['preorder_end_block']
                            + (int)City::getTimeOffset($worker['worker']['city_id']),
                    ]);
                }

                //Обновим у заказа в редисе и в мускуле исполнителя
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;

                $orderDataRedis['worker_id'] = $worker['worker']['worker_id'];
                $orderDataRedis['car_id'] = $carId;
                $orderDataRedis['worker'] = $worker['worker'];
                $orderDataRedis['car'] = isset($worker['car']) ? $worker['car'] : null;

                $orderDataMysql->worker_id = $worker['worker']['worker_id'];
                $orderDataMysql->car_id = $carId;

                $orderChange = new OrderChangeData();
                $orderChange->tenant_id = $tenantId;
                $orderChange->order_id = $orderId;
                $orderChange->change_field = 'status';
                $orderChange->change_object_id = (string)$workerCallsign;
                $orderChange->change_object_type = 'worker';
                $orderChange->change_subject = 'worker';
                $orderChange->change_val = Worker::STATUS_ACCEPT_PREORDER;
                $orderChange->change_time = (int)time();

                if (!$orderChange->save()) {
                    Yii::error($orderChange->errors);
                }
            }

            // Если исполнитель отказался от предварительного заказа
            if ($statusNewId === OrderStatus::STATUS_PRE_REFUSE_WORKER) {
                $orderChange = new OrderChangeData();
                $orderChange->tenant_id = $tenantId;
                $orderChange->order_id = $orderId;
                $orderChange->change_field = 'status';
                $orderChange->change_object_id = (string)$workerCallsign;
                $orderChange->change_object_type = 'worker';
                $orderChange->change_subject = 'worker';
                $orderChange->change_val = Worker::STATUS_REFUSE_PREORDER;
                $orderChange->change_time = (int)time();

                if (!$orderChange->save()) {
                    Yii::error($orderChange->errors);
                }
            }

            // исполнитель начал выполнять предварительный заказ
            if ($statusNewId === OrderStatus::STATUS_EXECUTION_PRE) {
                if (!empty($worker)) {
                    if ($orderDataRedis['worker_id'] != $worker['worker']['worker_id']) {
                        return $this->getPreparedResponse(WorkerErrorCode::ORDER_IS_BUSY, [
                            'set_result' => 0,
                        ]);
                    }


                    $order_type = ArrayHelper::getValue($orderDataRedis, 'device');

                    #todo  ограничение на небольничные заказы
                    if ($worker['worker']['status'] === Worker::STATUS_ON_ORDER
                        && !(empty($worker['worker']['last_order_id']) || $worker['worker']['last_order_id'] == $orderId)
                        && ($order_type !== OrderStatus::TYPE_HOSPITAL)
                    ) {
                        return $this->getPreparedResponse(WorkerErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER,
                            ['set_result' => 0]);
                    }

                    #todo больничная поездка
                    if($order_type === OrderStatus::TYPE_HOSPITAL){

                        #todo времянка - только для определенных водителй
                        $order_yandex_temp = strtoupper(ArrayHelper::getValue($worker, 'worker.yandex_account_number', ''));
                        $actice_order = ArrayHelper::getValue($worker, 'worker.active_orders', []);

                        if( ($worker['worker']['status'] === Worker::STATUS_ON_ORDER
                            && ($order_yandex_temp !==  OrderStatus::TYPE_HOSPITAL) )
                        ){
                            return $this->getPreparedResponse(WorkerErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER,
                                ['set_result' => 0]);
                        }

                        // ограничение на количество
                        $limit_pre_order =  $this->orderService->isMaxCountActiveOrders($actice_order, $tenantId, $orderDataRedis['city_id'], $orderDataRedis['position_id']);
                        if(!$limit_pre_order){
                            return $this->getPreparedResponse(WorkerErrorCode::ORDER_ASSIGN_LIMIT, [
                                'set_result' => 0,
                                ]);
                        }
                    }

                    //Обновим у заказа в редисе и в мускуле исполнителя
                    $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;

                    $orderDataRedis['worker_id'] = $worker['worker']['worker_id'];
                    $orderDataRedis['car_id'] = $carId;
                    $orderDataRedis['worker'] = $worker['worker'];
                    $orderDataRedis['car'] = isset($worker['car']) ? $worker['car'] : null;

                    $orderDataMysql->worker_id = $worker['worker']['worker_id'];
                    $orderDataMysql->car_id = $carId;

                    //Сохраняем время подачи авто
                    if (isset($timeToClient)) {
                        $orderDataMysql->time_to_client = $timeToClient;
                        $orderDataRedis['time_to_client'] = $timeToClient;
                    }

                    // Обновим статус исполнителя
                    $worker['worker']['status'] = Worker::STATUS_ON_ORDER;
                    if (isset($worker['worker']['pause_data'])) {
                        $worker['worker']['pause_data'] = $this->checkAndStopPauseAndReturnData($tenantId,
                            $workerCallsign, $worker['worker']['worker_shift_id'], $worker['worker']['pause_data']);
                        $worker['worker']['break_reason'] = null;
                    }

                    if ($workerService->saveActiveWorker($tenantId, $workerCallsign, $worker)) {
                        $orderChange = new OrderChangeData();
                        $orderChange->tenant_id = $tenantId;
                        $orderChange->order_id = $orderId;
                        $orderChange->change_field = 'status';
                        $orderChange->change_object_id = (string)$workerCallsign;
                        $orderChange->change_object_type = 'worker';
                        $orderChange->change_subject = 'worker';
                        $orderChange->change_val = Worker::STATUS_ON_ORDER;
                        $orderChange->change_time = (int)time();

                        if (!$orderChange->save()) {
                            Yii::error($orderChange->errors);
                        }
                    }
                }
            }


            // общие действия для некторых статусов
            if(in_array($statusNewId, [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_GET_WORKER,
                OrderStatus::STATUS_PRE_GET_WORKER,
                OrderStatus::STATUS_EXECUTION_PRE,
            ])){
                $company_id = ArrayHelper::getValue($worker,'worker.tenant_company_id');
                if($company_id){
                    $this->setOrderByCompanyWorker($company_id, $orderDataMysql, $orderDataRedis, $worker );
                }
            }

            if ($dispatcherId !== null) {
                try {
                    OrderChangeData::saveChangeData(
                        $tenantId,
                        $orderId,
                        implode('_', [OrderChangeData::SUBJECT_DISPATCHER, $dispatcherId]),
                        'status_id',
                        $statusNewId,
                        $orderId,
                        OrderChangeData::OBJECT_TYPE_ORDER
                    );
                } catch (\Exception $ex) {
                    \Yii::error($ex->getMessage());
                }
            }
        }

        // Запишем детализацию заказа
        if (isset($detailOrderData)) {
            $worker = $workerService->getActiveWorker($tenantId, $workerCallsign);

            $time = $orderDataMysql->order_time;
            $kind = $orderDataMysql->device === Order::DEVICE_WORKER
                ? TariffCommission::KIND_BORDER : $orderDataMysql->payment;

            $workerTariffId = null;
            $cityId = null;
            $carId = null;

            if (empty($worker['worker'])) {
                $lastShift = $workerService->getLastShift($tenantId, $workerCallsign);
                if (empty($lastShift)) {
                    $message = "Worker tariff not found because shift is closed and last shift not found tenantId={$tenantId}, callsign={$workerCallsign}, orderId={$orderId}";
                    \Yii::$app->apiLogger->log($message);
                    \Yii::error($message, 'worker-commission');
                } else {
                    $workerTariffId = $lastShift->tariff_id;
                    $cityId = $lastShift->city_id;
                    $carId = $lastShift->car_id;
                }
            } // fix remove after worker api was released (branch: worker_tariff_improvement)
            elseif (isset($worker['worker']['tariff_option_id'])) {
                $workerOptionTariff = WorkerOptionTariff::findOne($worker['worker']['tariff_option_id']);
                $workerTariffId = isset($workerOptionTariff->tariff_id) ? $workerOptionTariff->tariff_id : null;
                $cityId = $worker['worker']['city_id'];
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;
            } else {
                $workerTariffId = $worker['worker']['worker_tariff_id'];
                $cityId = $worker['worker']['city_id'];
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;
            }

            $summaryCost = (float)$detailOrderData['summary_cost'];

            try {
                $surcharge = $workerTariffService->getTariffSurcharge($workerTariffId, $time, $summaryCost);
            } catch (\Exception $ex) {
                $surcharge = 0;
                \Yii::$app->apiLogger->log('Calculation of worker surcharge was failed: ' . $ex->getMessage());
                Yii::error($ex, 'worker-commission');
            }

            try {
                $tax = $workerTariffService->getTariffTax($kind, $workerTariffId, $time, $summaryCost + $surcharge);
            } catch (\Exception $ex) {
                $tax = 0;
                \Yii::$app->apiLogger->log('Calculation of order tax was failed: ' . $ex->getMessage());
                Yii::error($ex, 'worker-commission');
            }

            $promoSetting = new Setting($orderDataRedis, $summaryCost - $tax + $surcharge);
            $promoService = new PromoService($promoSetting);

            try {
                if ($promoService->isCommissionForWorker()) {

                    $commission = $promoService->getCommissionForWorker();
                } else {
                    $commission = $workerTariffService->getTariffCommission(
                        $tenantId,
                        $cityId,
                        $workerTariffId,
                        $time,
                        $carId,
                        $kind,
                        $summaryCost - $tax + $surcharge);
                }
            } catch (\Exception $ex) {
                $commission = 0;
                \Yii::$app->apiLogger->log('Calculation of worker commission was failed: ' . $ex->getMessage());
                Yii::error($ex, 'worker-commission');
            }

            if (empty($detailOrderData['summary_cost'])) {
                $detailOrderData['summary_cost'] = $summaryCost;
            }

            if ($orderDataMysql->payment === Order::PAYMENT_CARD && !empty($detailOrderData['bonus'])) {
                $detailOrderData['bonus'] = $this->getCorrectedBonusSum($tenantId, $cityId, $summaryCost, $detailOrderData['bonus']);
            }

            $detailOrderData['surcharge'] = $surcharge;
            $detailOrderData['tax'] = $tax;
            $detailOrderData['commission'] = $commission;

            $saveDetailResult = OrderDetailCost::orderDetailCostSave(
                $orderDataRedis, $detailOrderData, $workerCallsign, $lang);

            if ($saveDetailResult) {

                if ($promoService->isHasPromoCode()) {
                    $promoService->startProcessingPromotionCode();
                }

                $orderRepository = new OrderRepository();
                $referralSystemRepository = new ReferralSystemRepository(\Yii::$app->getDb());
                $service = new ReferralSystemService($orderRepository, $referralSystemRepository);

                try {
                    $service->calculateBonus($orderId);
                    \Yii::$app->apiLogger->log('Referral bonus was calculated');
                } catch (OrderWithDetailCostNotFoundException $ignore) {
                } catch (ReferralSystemNotFoundException $ignore) {
                } catch (ReferralSystemException $ex) {
                    \Yii::$app->apiLogger->log('Calculation of referrer bonus was failed (' . $ex->getMessage() . ')');
                    \Yii::error($ex, 'referral-system');
                }
            }
        }

        //Оплата произведена через кеш, выполняем транзакциюи возвращаем ответ
        if ($statusNewId === OrderStatus::STATUS_COMPLETED_PAID
            || $statusNewId === OrderStatus::STATUS_COMPLETED_NOT_PAID
        ) {

            if ($orderDataRedis['worker']['tenant_company_id']) {
                $orderDataMysql->tenant_company_id = $orderDataRedis['worker']['tenant_company_id'];
            }

            /**
             * @var $workerTransaction WorkerTransaction
             */
            $workerTransaction = Yii::$container->get(WorkerTransaction::className());

            $closeResultCode = $workerTransaction->closeOrder($orderId, $statusNewId, $uuid);
            if ($orderDataMysql->save()) {
                $activeOrderRepository->set($orderId, serialize($orderDataRedis));

                return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
            }
        }


        //Сохраняем данные в мускул, в редис
        if ($orderDataMysql->save()) {
            $orderDataRedis['update_time'] = $orderDataMysql->update_time;
            $activeOrderRepository->set($orderId, serialize($orderDataRedis));

            //Удаляем заказ из биржы
            if (array_key_exists('exchange_info', $orderDataRedis)) {
                /**
                 * @var $serviceExchange ServiceExchange
                 */
                $serviceExchange = \Yii::$app->serviceExchange;
                try {
                    $serviceExchange->deleteFromExchange($orderDataRedis, $data['subject']);
                } catch (\Exception $ex) {
                    \Yii::error($ex->getMessage());
                }
            }

            //Перезапишем время подачи авто
            if (isset($timeToClient)) {
                $orderChange = new OrderChangeData();
                $orderChange->tenant_id = $tenantId;
                $orderChange->order_id = $orderId;
                $orderChange->change_field = 'time_to_client';
                $orderChange->change_object_id = (string)$orderId;
                $orderChange->change_object_type = 'order';
                $orderChange->change_subject = 'worker';
                $orderChange->change_val = (string)$timeToClient;
                $orderChange->change_time = (int)time();

                if (!$orderChange->save()) {
                    Yii::error($orderChange->errors);
                }
            }

            //Если нажали  - подтверждение оплаты, проводим транзакцию и проставляем статус оплачен/не оплачен
            if ($statusNewId === OrderStatus::STATUS_PAYMENT_CONFIRM) {
                if ($orderDataMysql->payment !== 'CASH') {
                    /**
                     * @var $workerTransaction WorkerTransaction
                     */
                    $workerTransaction = Yii::$container->get(WorkerTransaction::className());

                    $closeResultCode = $workerTransaction->closeOrder($orderId, $statusNewId, $uuid);
                    if ($closeResultCode === true) {
                        // Инфа по заказу из редиса
                        $orderDataRedis = $activeOrderRepository->getOne($orderId);
                        //Ищем заказ в БД
                        $orderDataMysql = Order::findOne($orderId);

                        if ($orderDataRedis && $orderDataMysql) {
                            // Перезапишем статус на - выполнен оплачен
                            $orderPaidStatus = OrderStatus::STATUS_COMPLETED_PAID;

                            // Перезапишем статусы в редисе и мускуле
                            $statusData = OrderStatusRecord::findOne($orderPaidStatus);

                            $statusForRedis = [
                                'status_id'       => $statusData->status_id,
                                'name'            => $statusData->name,
                                'status_group'    => $statusData->status_group,
                                'dispatcher_sees' => $statusData->dispatcher_sees,
                            ];
                            $orderDataRedis['status'] = $statusForRedis;
                            $orderDataRedis['status_id'] = $statusData->status_id;
                            $orderDataRedis['status_time'] = time();

                            $orderDataMysql->status_id = $orderPaidStatus;
                            $orderDataMysql->status_time = time();

                            if ($orderDataMysql->save()) {
                                $activeOrderRepository->set($orderId, serialize($orderDataRedis));

                                return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
                            }
                        } else {
                            return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 0]);
                        }
                    } else {
                        // Инфа по заказу из редиса
                        $orderDataRedis = $activeOrderRepository->getOne($orderId);

                        //Ищем заказ в БД
                        $orderDataMysql = Order::findOne($orderId);

                        if ($orderDataRedis && $orderDataMysql) {
                            $currentPaymentType = $orderDataMysql->payment;
                            if ($currentPaymentType !== 'CASH') {
                                $orderDataRedis['payment'] = 'CASH';
                                $orderDataMysql->payment = 'CASH';

                                try {
                                    OrderChangeData::saveChangeData($tenantId, $orderId,
                                        OrderChangeData::SUBJECT_WORKER, 'payment', Order::PAYMENT_CASH,
                                        $orderId, OrderChangeData::OBJECT_TYPE_ORDER);
                                } catch (\Exception $e) {
                                    \Yii::error("Save order change data error (change payment to cash). Error={$e->getMessage()}");
                                }

                                if ($orderDataMysql->save()) {
                                    $activeOrderRepository->set($orderId, serialize($orderDataRedis));
                                }
                            }
                        }

                        return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 0]);
                    }
                } else {
                    return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 0]);
                }
            }

            return $this->getPreparedResponse(WorkerErrorCode::OK, ['set_result' => 1]);
        }

        \Yii::error(implode(", ", $orderDataMysql->getFirstErrors()), 'set_order_status');

        return $this->getPreparedResponse(WorkerErrorCode::INTERNAL_ERROR, ['set_result' => 0]);
    }

    /**
     * Update order
     * @param $orderId
     * @param $tenantId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @param string $uuid
     * @return array
     * @throws Exception
     * @throws \yii\db\Exception
     */
    private function updateOrder($orderId, $tenantId, $data, $updateTime, $lang, $uuid = '')
    {

        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);

        $orderDataRedis = $activeOrderRepository->getOne($orderId);
        $orderDataMysql = Order::findOne($orderId);
        $orderDataMysql->order_date = Date('Y-m-d H:i:s', $orderDataMysql->order_time);

        $orderInfo = new OrderInfo($orderId, $tenantId, $data, $lang);

        try {
            (new OrderValidate($orderInfo, $orderDataMysql, $orderDataRedis))->runValidate();

            $transaction = \Yii::$app->db->beginTransaction();

            (new OrderUpdateAddress($orderInfo, $orderDataMysql, $orderDataRedis))->runUpdate();
            (new OrderUpdatePredvPrice($orderInfo, $orderDataMysql, $orderDataRedis))->runUpdate();

            $transaction->commit();

            $filteredOrder = Order::filterOrderData($orderDataRedis, $tenantId, $lang);

            return $this->getPreparedResponse(WorkerErrorCode::OK, [
                'set_result' => 1,
                'cost_data'  => isset($filteredOrder['costData']) ? $filteredOrder['costData'] : [],
            ]);

        } catch (OrderUpdateException $e) {
            return $this->getPreparedResponse($e->getMessage(), ['set_result' => 0]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::error($e, 'update_order');

            return $this->getPreparedResponse(WorkerErrorCode::INTERNAL_ERROR, ['set_result' => 0]);
        }

    }

    /**
     * @param int $tenantId
     * @param int $cityId
     * @param float $summaryCost
     * @param float $bonus
     *
     * @return float
     */
    private function getCorrectedBonusSum($tenantId, $cityId, $summaryCost, $bonus)
    {
        try {
            /** @var ProfileService $profileService */
            $profileService = \Yii::$container->get(ProfileService::class);

            /** @var PaymentGateService $paymentGateService */
            $paymentGateService = \Yii::$container->get(PaymentGateService::class);

            $profile = $profileService->getProfile($tenantId, $cityId);
            $paymentGate = $paymentGateService->get($profile);

            $cardBingingSum = isset($paymentGate['cardBindingSum']) ? (float)$paymentGate['cardBindingSum'] : 0;
            if ($cardBingingSum > 0 && $summaryCost - $bonus < $cardBingingSum) {
                $correctedValue = $summaryCost - $cardBingingSum;
                \Yii::$app->apiLogger->log("Bonus sum was corrected: old=$bonus, new=$correctedValue");

                return $correctedValue;
            }
        } catch (\Exception $e) {
            $message = "Correction of bonus sum was failed: tenantId=$tenantId, cityId=$cityId, bonus=$bonus, error={$e->getMessage()}";
            \Yii::$app->apiLogger->log($message);
            Yii::error($e, 'bonus-sum-correction');
        }

        return $bonus;
    }

    public function setOrderByCompanyWorker($company_id = 0, Order &$orderDataMysql, &$orderDataRedis = [], $workerDataRedis = [])
    {
        if($company_id){
            $orderDataMysql->company_id = $company_id;
            $orderDataMysql->tenant_company_id = $company_id;
        }
        $workerCompanyId = ArrayHelper::getValue($workerDataRedis, 'worker.tenant_company.company_id', '') ;
        $workerCompanyName = ArrayHelper::getValue($workerDataRedis, 'worker.tenant_company.name');
        $workerCompanyPhone = ArrayHelper::getValue($workerDataRedis, 'worker.tenant_company.phone') ;
        $workerCompanyUser_contact = ArrayHelper::getValue($workerDataRedis,'worker.tenant_company.user_contact');

        $orderDataRedis['worker_company']['tenant_company_id'] = $workerCompanyId;
        $orderDataRedis['worker_company']['name'] = $workerCompanyName ;
        $orderDataRedis['worker_company']['phone'] = $workerCompanyPhone;
        $orderDataRedis['worker_company']['user_contact'] = $workerCompanyUser_contact;

    }

}