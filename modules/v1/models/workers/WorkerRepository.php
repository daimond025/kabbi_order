<?php

namespace app\modules\v1\models\workers;

use app\models\Worker;
use app\modules\v1\models\workers\worker\WorkerShift;
use yii\base\Object;

/**
 * Class WorkerService
 * @package app\modules\v1\models\workers
 */
class WorkerRepository extends Object
{

    /**
     * Получение исполнителя из редиса
     *
     * @param string $tenantId
     * @param string $workerCallsign
     *
     * @return array|false
     */
    public function getActiveWorker($tenantId, $workerCallsign)
    {
        $workerData = \Yii::$app->redis_workers
            ->executeCommand('hget', [$tenantId, $workerCallsign]);
        $workerData = unserialize($workerData);

        if (!empty($workerData)) {
            return $workerData;
        } else {
            return false;
        }
    }

    /**
     * Save worker to redis
     *
     * @param int   $tenantId
     * @param int   $workerCallsign
     * @param array $worker
     *
     * @return bool
     */
    public function saveActiveWorker($tenantId, $workerCallsign, $worker)
    {
        $workerData = serialize($worker);
        $result     = \Yii::$app->redis_workers->executeCommand(
            'hset', [$tenantId, $workerCallsign, $workerData]);

        return $result == 0 || $result == 1;
    }

    /**
     * @param int $tenantId
     * @param int $workerCallsign
     *
     * @return array|null|\yii\db\ActiveRecord
     *
     */
    public function getLastShift($tenantId, $workerCallsign)
    {
        $workerId = Worker::find()
            ->select(['worker_id'])
            ->where([
                'tenant_id' => $tenantId,
                'callsign'  => $workerCallsign,
            ])
            ->scalar();

        return WorkerShift::find()
            ->where([
                'worker_id' => $workerId,
            ])
            ->orderBy(['start_work' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    public function isWorkerOnShift($tenantId, $workerCallsign)
    {
        $shift_data = \Yii::$app->redis_workers
            ->executeCommand('hget', [$tenantId, $workerCallsign]);

        return !empty($shift_data);
    }

    public function getWorker($tenantId, $workerCallsign)
    {
        return Worker::find()
            ->alias('t')
            ->joinWith('tenant')
            ->where([
                't.tenant_id' => $tenantId,
                't.callsign'  => $workerCallsign,
            ])
            ->asArray()
            ->one();
    }

    /**
     * Getting worker shift id from Redis
     *
     * @param string $tenantLogin
     * @param int    $workerCallsign
     *
     * @return string
     */
    public function getWorkerShift($tenantLogin, $workerCallsign)
    {
        $shiftKey = $tenantLogin . '_' . $workerCallsign;

        return \Yii::$app->redis_worker_shift->executeCommand('GET', [$shiftKey]);
    }
}
