<?php

namespace app\modules\v1\models\workers;

use app\modules\v1\models\workers\car\CarHasOption;
use app\modules\v1\models\workers\tenant\TenantHasCarOption;
use app\modules\v1\models\workers\worker\TariffCommission;
use app\modules\v1\models\workers\worker\WorkerOptionDiscount;
use app\modules\v1\models\workers\worker\WorkerOptionTariff;
use yii\base\Object;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class WorkerTariffService
 * @package app\modules\v1\models\workers
 */
class WorkerTariffService extends Object
{
    /**
     * @param int $workerTariffId
     * @param int $time
     *
     * @return ActiveRecord
     */
    private function getWorkerOptionTariff($workerTariffId, $time)
    {
        $date = new \DateTime();
        $date->setTimestamp($time);

        $dateFull    = $date->format('d.m.Y');
        $dateShort   = $date->format('d.m');
        $dayOfWeek   = date('l', strtotime($dateFull));
        $currentTime = $date->format('H:i');

        $optionId = $this->getTariffOptionId($workerTariffId, [$dayOfWeek, $dateShort, $dateFull], $currentTime);

        return WorkerOptionTariff::findOne($optionId);
    }

    /**
     * @param int    $tariffId
     * @param array  $currentDays
     * @param string $currentTime
     *
     * @return int
     */
    private function getTariffOptionId($tariffId, $currentDays, $currentTime)
    {
        $currentOptionId = WorkerOptionTariff::find()
            ->select('option_id')
            ->where([
                'tariff_id'   => $tariffId,
                'tariff_type' => WorkerOptionTariff::OPTION_TYPE_CURRENT,
            ])
            ->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])
            ->scalar();

        $options = WorkerOptionTariff::find()
            ->select(['option_id', 'active_date'])
            ->where([
                'and',
                ['tariff_id' => $tariffId],
                ['!=', 'tariff_type', WorkerOptionTariff::OPTION_TYPE_CURRENT],
            ])
            ->asArray()
            ->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])
            ->all();

        if (!empty($options)) {
            foreach ($options as $option) {
                $dates = empty($option['active_date']) ? [] : explode(';', $option['active_date']);

                foreach ($dates as $date) {
                    $parsedDate = explode('|', $date);
                    if (!in_array((string)$parsedDate[0], $currentDays, true)) {
                        continue;
                    }

                    $timeInterval = empty($parsedDate[1]) ? ['00:00', '23:59'] : explode('-', $parsedDate[1]);
                    if ($timeInterval[0] <= $currentTime && $timeInterval[1] >= $currentTime) {
                        return $option['option_id'];
                    }
                }
            }
        }

        return $currentOptionId;
    }

    /**
     * Getting tariff surcharge
     *
     * @param int   $tariffId
     * @param int   $time
     * @param float $summaryCost
     *
     * @return float
     */
    public function getTariffSurcharge($tariffId, $time, $summaryCost = 0.0)
    {
        $workerOptionTariff = $this->getWorkerOptionTariff($tariffId, $time);

        $increaseOrderSum   = (float)$workerOptionTariff->increase_order_sum;
        $increaseSumFix     = (float)$workerOptionTariff->increase_sum_fix;
        $increaseSumFixType = $workerOptionTariff->increase_sum_fix_type;
        $increaseSumLimit   = (float)$workerOptionTariff->increase_sum_limit;

        $fullOrderSum = $summaryCost < $increaseOrderSum ? $increaseOrderSum : $summaryCost;
        if ($workerOptionTariff->increase_sum_limit === null || $fullOrderSum < $increaseSumLimit) {

            $fullOrderSum += $increaseSumFixType === WorkerOptionTariff::TYPE_PERCENT
                ? $fullOrderSum * $increaseSumFix / 100 : $increaseSumFix;
        }

        return $fullOrderSum - $summaryCost;
    }

    /**
     * Getting tariff tax
     *
     * @param string $kind
     * @param int    $tariffId
     * @param int    $time
     * @param float  $summaryCost
     *
     * @return float
     */
    public function getTariffTax($kind, $tariffId, $time, $summaryCost = 0.0)
    {
        $workerOptionTariff = $this->getWorkerOptionTariff($tariffId, $time);
        $groupedCommissions = empty($workerOptionTariff->tariffCommissions)
            ? [] : ArrayHelper::index($workerOptionTariff->tariffCommissions, null, 'kind');

        $commission = null;
        if ($kind === TariffCommission::KIND_CORP_BALANCE
            && !empty($groupedCommissions[TariffCommission::KIND_FIXED])
        ) {
            $commission = current($groupedCommissions[TariffCommission::KIND_FIXED]);
        } elseif (!empty($groupedCommissions[$kind])) {
            $commission = current(array_filter($groupedCommissions[$kind],
                function ($commission) use ($summaryCost) {
                    $from = isset($commission->from) ? $commission->from : 0;
                    $to   = isset($commission->to) ? $commission->to : null;

                    return !(($from > $summaryCost) || (isset($to) && $summaryCost >= $to));
                }));
        }

        $tax = empty($commission->tax) || $commission->tax < 0 ? 0 : (float)$commission->tax;

        return $summaryCost * $tax / 100;
    }


    /**
     * Getting tariff commission
     *
     * @param int    $tenantId
     * @param int    $cityId
     * @param int    $tariffId
     * @param int    $time
     * @param int    $carId
     * @param string $kind
     * @param float  $summaryCost
     *
     * @return float
     */
    public function getTariffCommission($tenantId, $cityId, $tariffId, $time, $carId, $kind, $summaryCost = 0.0)
    {
        $workerOptionTariff = $this->getWorkerOptionTariff($tariffId, $time);
        $groupedCommissions = empty($workerOptionTariff->tariffCommissions)
            ? [] : ArrayHelper::index($workerOptionTariff->tariffCommissions, null, 'kind');

        $commission = null;
        if (!empty($groupedCommissions[TariffCommission::KIND_FIXED])) {
            $commission = current($groupedCommissions[TariffCommission::KIND_FIXED]);
        } elseif (!empty($groupedCommissions[$kind])) {
            $commission = current(array_filter($groupedCommissions[$kind],
                function ($commission) use ($summaryCost) {
                    $from = isset($commission->from) ? $commission->from : 0;
                    $to   = isset($commission->to) ? $commission->to : null;

                    return !(($from > $summaryCost) || (isset($to) && $summaryCost >= $to));
                }));
        }

        $commissionValue = 0;
        if (isset($commission->type, $commission->value)) {
            $value = $commission->value <= 0 ? 0 : (float)$commission->value;
            if ($commission->type === TariffCommission::TYPE_MONEY) {
                $commissionValue = $value;
            } elseif ($commission->type === TariffCommission::TYPE_PERCENT) {
                $commissionValue = $summaryCost * $value / 100;
            }
        }

        $carOptions = CarHasOption::find()
            ->select('option_id')
            ->where(['car_id' => $carId])
            ->asArray()
            ->all();

        $tenantCarOptions = TenantHasCarOption::find()
            ->select('option_id')
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'is_active' => 1,
            ])
            ->asArray()
            ->all();

        $workerOptionDiscount = WorkerOptionDiscount::find()
            ->select(['discount_order_type', 'discount_order'])
            ->where(['option_id' => $workerOptionTariff['option_id']])
            ->andWhere([
                'car_option_id' => isset($carOptions) ? ArrayHelper::getColumn($carOptions, 'option_id') : null,
            ])
            ->andWhere([
                'car_option_id' => isset($tenantCarOptions) ? ArrayHelper::getColumn($tenantCarOptions,
                    'option_id') : null,
            ])
            ->asArray()
            ->all();

        $discountCost    = 0;
        $discountPercent = 0;
        if (is_array($workerOptionDiscount)) {
            foreach ($workerOptionDiscount as $discount) {
                if ($discount['discount_order_type'] == WorkerOptionDiscount::TYPE_MONEY) {
                    $discountCost += (float)$discount['discount_order'];
                } elseif ($discount['discount_order_type'] == WorkerOptionDiscount::TYPE_PERCENT) {
                    $discountPercent += (float)$discount['discount_order'];
                }
            }
        }

        $commissionValue = $commissionValue - $discountCost - ($commissionValue * $discountPercent / 100);
        //        if ($commissionValue > $summaryCost) {
        //            $commissionValue = $summaryCost;
        //        } else
        if ($commissionValue < 0) {
            $commissionValue = 0;
        }

        return $commissionValue;
    }

}