<?php

namespace app\modules\v1\models\workers\client;

use app\models\ClientPhone;
use bonusSystem\BonusSystem;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_client".
 *
 * @property string                $client_id
 * @property string                $tenant_id
 * @property string                $city_id
 * @property string                $photo
 * @property string                $last_name
 * @property string                $name
 * @property string                $second_name
 * @property string                $email
 * @property integer               $black_list
 * @property integer               $priority
 * @property string                $create_time
 * @property integer               $active
 * @property string                $success_order
 * @property string                $fail_driver_order
 * @property string                $fail_client_order
 * @property string                $birth
 * @property integer               $password
 * @property string                $device
 * @property integer               $app_id
 * @property string                $device_token
 * @property string                $lang
 *
 * @property Tenant                $tenant
 * @property ClientHasBonus[]      $clientHasBonuses
 * @property ClientBonus[]         $bonuses
 * @property ClientHasCompany[]    $clientHasCompanies
 * @property ClientCompany[]       $companies
 * @property ClientPhone[]         $clientPhones
 * @property ClientReview[]        $clientReviews
 * @property ClientReviewRaiting[] $clientReviewRaitings
 * @property OrderChange[]         $orderChanges
 * @property OrderHistory[]        $orderHistories
 */
class Client extends \yii\db\ActiveRecord
{
    public $phone;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'required'],
            [
                [
                    'tenant_id',
                    'city_id',
                    'black_list',
                    'priority',
                    'active',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'password',
                ],
                'integer',
            ],
            [['create_time', 'birth'], 'safe'],
            [['photo'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'         => 'Client ID',
            'tenant_id'         => 'Tenant ID',
            'city_id'           => 'City ID',
            'photo'             => 'Photo',
            'last_name'         => 'Last Name',
            'name'              => 'Name',
            'second_name'       => 'Second Name',
            'email'             => 'Email',
            'black_list'        => 'Black List',
            'priority'          => 'Priority',
            'create_time'       => 'Create Time',
            'active'            => 'Active',
            'success_order'     => 'Success Order',
            'fail_worker_order' => 'Fail Worker Order',
            'fail_client_order' => 'Fail Client Order',
            'birth'             => 'Birth',
            'password'          => 'Password',
        ];
    }

    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * Getting bonus payment data
     *
     * @param int $tenantId
     * @param int $orderId
     * @param int $clientId
     * @param int $currencyId
     * @param int $clientTariffId
     *
     * @return array
     */
    public static function getBonusPayment($tenantId, $orderId, $clientId, $currencyId, $clientTariffId)
    {
        try {
            /** @var $bonusSystem BonusSystem */
            $bonusSystem = \Yii::createObject(BonusSystem::class, [$tenantId]);

            if ($bonusSystem->isUDSGameBonusSystem()
                && !$bonusSystem->isRegisteredOrderForWriteOff($orderId)
            ) {
                return [];
            }

            $clientBonusBalance = $bonusSystem->getBalance($clientId, $currencyId);
            $paymentStrategy    = $bonusSystem->getPaymentStrategy($clientTariffId);

            return [
                'bonus_id'              => $paymentStrategy->getBonusId(),
                'payment_method'        => $paymentStrategy->getPaymentMethod(),
                'max_payment_type'      => $paymentStrategy->getMaxPaymentType(),
                'max_payment'           => $paymentStrategy->getMaxPayment(),
                'client_bonus_balance ' => $clientBonusBalance,
            ];
        } catch (\Exception $ex) {
            return [];
        }
    }

    public static function find()
    {
        return new ClientQuery(get_called_class());
    }

}

