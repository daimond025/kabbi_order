<?php

namespace app\modules\v1\models\workers\client;

use app\models\ClientPhone;
use yii\db\ActiveQuery;

class ClientQuery extends ActiveQuery
{
    public function byPhone($phone)
    {
        return $this->andWhere([ClientPhone::tableName() . '.value' => $phone]);
    }

    public function byTenantId($tenantId)
    {
        return $this->andWhere([Client::tableName() . '.tenant_id' => $tenantId]);
    }
}
