<?php


namespace app\modules\v1\models;


use app\models\OrderDetailCost as OrderDetailCostRecord;
use yii\base\Object;

class OrderDetailCost extends Object
{
    public $orderId;

    public function getData()
    {
        return OrderDetailCostRecord::find()->asArray()->where(['order_id' => $this->orderId])->one();
    }
}