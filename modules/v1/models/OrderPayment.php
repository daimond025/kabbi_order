<?php


namespace app\modules\v1\models;


class OrderPayment
{
    const PAYMENT_CARD = 'CARD';
    const PAYMENT_PERSON = 'PERSONAL_ACCOUNT';
    const PAYMENT_CASH = 'CASH';
    const PAYMENT_CORP = 'CORP_BALANCE';

    public static function getPaymentVariants()
    {
        return [
            self::PAYMENT_CASH   => t('order', 'Cash'),
            self::PAYMENT_CARD   => t('order', 'Bank card'),
            self::PAYMENT_PERSON => t('order', 'Personal account'),
            self::PAYMENT_CORP   => t('order', 'Corporate balance'),
        ];
    }

    /**
     * OrderRecord payment for select in order card.
     * @param bool $showCardPayment
     * @return array
     */
    public static function getPaymentList($showCardPayment = false)
    {
        $payments = [
            self::PAYMENT_CASH   => t('order', 'Cash'),
            self::PAYMENT_PERSON => t('order', 'Personal account'),
        ];
        if ($showCardPayment) {
            $payments[self::PAYMENT_CARD] = t('order', 'Bank card');
        }

        return $payments;
    }

    /**
     * @param string $key
     * @param string $companyName
     * @return mixed|string
     */
    public static function getPaymentNameByKey($key, $companyName)
    {
        $paymentList = self::getPaymentList(true);

        if ($key === self::PAYMENT_CORP) {
            $paymentName = t('order', 'Balance{name}', ['name' => " ($companyName)"]);
        } elseif (isset($paymentList[$key])) {
            $paymentName = $paymentList[$key];
        } else {
            $paymentName = '';
        }

        return $paymentName;
    }
}