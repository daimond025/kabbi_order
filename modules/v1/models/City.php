<?php


namespace app\modules\v1\models;


use yii\base\Object;
use app\models\City as CityRecord;
use yii\db\ActiveQuery;

class City extends Object
{
    private static $_timeOffset;

    /**
     * City time offset.
     * @param integer $city_id
     * @return integer
     */
    public static function getTimeOffset($city_id)
    {
        if (!isset(self::$_timeOffset[$city_id])) {
            $city = CityRecord::find()
                ->where(['city_id' => $city_id])
                ->with([
                    'republic' => function (ActiveQuery $query) {
                        $query->select(['republic_id', 'timezone']);
                    }
                ])
                ->one();

            if (empty($city)) {
                return 0;
            }

            self::$_timeOffset[$city_id] = !empty($city->republic->timezone) ? $city->republic->timezone * 3600 : 0;
        }

        return self::$_timeOffset[$city_id];
    }
}