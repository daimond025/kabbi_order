<?php

namespace app\modules\v1\models\client;

use app\models\City;
use app\models\Tenant;
use yii\db\ActiveRecord;

/**
 * Class Company
 * @package app\modules\v1\models\client
 * @property int block
 */
class Company extends ActiveRecord
{

    const BLOCK = 1;
    const NOT_BLOCK = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getHasClient()
    {
        return $this->hasMany(ClientHasCompany::className(), ['company_id' => 'company_id']);
    }
}
