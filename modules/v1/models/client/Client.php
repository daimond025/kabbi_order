<?php

namespace app\modules\v1\models\client;

use yii\db\ActiveRecord;

class Client extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%client}}';
    }

    public function rules()
    {
        return [];
    }
}
