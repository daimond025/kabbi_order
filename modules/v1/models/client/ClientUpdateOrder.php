<?php

namespace app\modules\v1\models\client;

use app\components\ClientErrorCode;
use app\components\exchange\ServiceExchange;
use app\components\repositories\OrderRepository;
use app\models\ActiveOrderRepository;
use app\models\OrderStatusRecord;
use app\modules\v1\delegations\address\Courier;
use app\modules\v1\extensions\codeGenerator\CodeGenerator;
use app\modules\v1\models\exceptions\BlockedCompanyException;
use app\modules\v1\models\exceptions\InvalidClientCard;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class ClientUpdateOrder
 * @package app\modules\v1\models\client
 */
class ClientUpdateOrder extends Object
{

    /** @var OrderRepository */
    protected $orderRepository;
    /** @var CodeGenerator */
    protected $codeGenerator;

    public function init()
    {
        $this->orderRepository = \Yii::createObject(OrderRepository::class);
        $this->codeGenerator = \Yii::createObject(CodeGenerator::class, [range(0, 9)]);

        parent::init();
    }

    /**
     * @param       $errorCode
     * @param array $info
     *
     * @return array
     */
    private function getPreparedResponse($errorCode, array $info = [])
    {
        return ArrayHelper::merge(ClientErrorCode::getErrorData($errorCode), [
            'result' => $info,
        ]);
    }

    /**
     * Reject order
     * @param $tenantId
     * @param $orderId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @return array
     * @throws \Exception
     */
    private function rejectOrder($tenantId, $orderId, $data, $updateTime, $lang)
    {
        $statusNewId = (int)$data['status_id'];

        // Инфа по заказу из редиса
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $orderDataRedis = $activeOrderRepository->getOne($orderId);

        if (empty($orderDataRedis)) {
            return $this->getPreparedResponse(ClientErrorCode::EMPTY_DATA_IN_DATABASE, ['reject_result' => 0]);
        }

        $statusGroup = (string)$orderDataRedis['status']['status_group'];
        if (in_array($statusGroup, [
            OrderStatus::STATUS_GROUP_NEW,
            OrderStatus::STATUS_GROUP_CAR_ASSIGNED,
            OrderStatus::STATUS_GROUP_PRE_ORDER,
            OrderStatus::STATUS_GROUP_CAR_AT_PLACE,
        ])) {
            $orderDataMysql = Order::findOne($orderId);
            if (empty($orderDataMysql)) {
                return $this->getPreparedResponse(ClientErrorCode::EMPTY_DATA_IN_DATABASE, ['reject_result' => 0]);
            }

            $orderDataMysql->status_id = $statusNewId;
            if ($orderDataMysql->save()) {
                $statusData = OrderStatusRecord::findOne($statusNewId);
                $orderDataMysql->status_id = $statusNewId;
                $orderDataMysql->status_time = time();
                $orderDataRedis['status']['status_id'] = $statusData->status_id;
                $orderDataRedis['status']['name'] = $statusData->name;
                $orderDataRedis['status']['status_group'] = $statusData->status_group;
                $orderDataRedis['status']['dispatcher_sees'] = $statusData->dispatcher_sees;
                $orderDataRedis['status_id'] = $statusData->status_id;
                $orderDataRedis['status_time'] = time();
                $activeOrderRepository->set($orderId, serialize($orderDataRedis));

                if (array_key_exists('exchange_info', $orderDataRedis)) {
                    /**
                     * @var $serviceExchange ServiceExchange
                     */
                    $serviceExchange = \Yii::$app->serviceExchange;
                    try {
                        $serviceExchange->deleteFromExchange($orderDataRedis, $data['subject']);
                    } catch (\Exception $ex) {
                        \Yii::error($ex->getMessage());
                    }
                }
                return $this->getPreparedResponse(ClientErrorCode::OK, ['reject_result' => 1]);
            } else {
                return $this->getPreparedResponse(ClientErrorCode::INTERNAL_ERROR, ['reject_result' => 0]);
            }
        }

        switch ($statusGroup) {
            case OrderStatus::STATUS_GROUP_REJECTED :
                return $this->getPreparedResponse(ClientErrorCode::ORDER_IS_REJECTED, ['reject_result' => 0]);
            case OrderStatus::STATUS_GROUP_COMPLETED :
                return $this->getPreparedResponse(ClientErrorCode::ORDER_IS_COMPLETED, ['reject_result' => 0]);
            default:
                return $this->getPreparedResponse(ClientErrorCode::OK, ['reject_result' => 0]);
        }
    }

    /**
     * Update
     * @param $tenantId
     * @param $orderId
     * @param $data
     * @param $updateTime
     * @param $lang
     * @param string $uuid
     * @return array
     * @throws \Exception
     */
    public function update($tenantId, $orderId, $data, $updateTime, $lang, $uuid = '')
    {
        if (isset($data['status_id']) && in_array($data['status_id'], OrderStatus::getRejectedStatusIdByClient())) {
            return $this->rejectOrder($tenantId, $orderId, $data, $updateTime, $lang);
        } else {
            return $this->updateOrder($orderId, $tenantId, $data, $updateTime, $lang, $uuid);
        }
    }


    private function updateOrder($orderId, $tenantId, $data, $lastUpdateTime, $lang, $uuid = '')
    {

        if (!empty($lang)) {
            \Yii::$app->language = $lang;
        }

        $order = $this->getOrder($orderId, $tenantId);
        $updateTime = $order->update_time;
        $order->subject = $data['subject'];


        if ($lastUpdateTime != $updateTime) {
            return $this->getPreparedResponse(ClientErrorCode::INTERNAL_ERROR);
        }

        if (in_array($order->status_id, OrderStatus::getFinishedStatusId())
            || $order->status_id === OrderStatus::STATUS_WAITING_FOR_PAYMENT
        ) {
            return $this->getPreparedResponse(ClientErrorCode::INTERNAL_ERROR);
        }

        if (isset($data['payment'])) {

            switch ($data['payment']) {

                case OrderRecord::PAYMENT_CORP:
                    if (!isset($data['company_id'])) {
                        return $this->getPreparedResponse(ClientErrorCode::MISSING_INPUT_PARAMETER);
                    }
                    $order->company_id = $data['company_id'];
                    break;

                case OrderRecord::PAYMENT_CARD:
                    if (isset($data['pan'])) {
                        $order->pan = $data['pan'];
                    }
                    break;

                case OrderRecord::PAYMENT_CASH:
                case OrderRecord::PAYMENT_PERSONAL_ACCOUNT:
                    break;

                default:
                    return $this->getPreparedResponse(ClientErrorCode::BAD_PARAM);
            }

            $order->payment = $data['payment'];
        }

        if (isset($data['address'])) {
            if (!is_array($data['address'])) {
                $data_temp['address'] = unserialize($data['address']);

                if(!$data_temp['address']){

                    $data['address']  = json_decode($data['address'], true);
                }
            }

            $oldAddress = $order->address;

            $data['address']['A'] = $order->address['A'];
            $order->address = $data['address'];

            $order->address = $this->rewritingConfirmCodes($order->address, $oldAddress, $order);

            if (isset($data['predv_price'])) {
                $order->predv_price = $data['predv_price'];
            }
            if (isset($data['predv_distance'])) {
                $order->predv_distance = $data['predv_distance'];
            }
            if (isset($data['predv_time'])) {
                $order->predv_time = $data['predv_time'];
            }
        }

        $transaction = app()->getDb()->beginTransaction();
        try {
            if (!$order->save(false)) {
                $transaction->rollBack();

                \Yii::error($order->getErrors());
                \Yii::error('Ошибка сохранения заказа', 'order');

                return $this->getPreparedResponse(ClientErrorCode::INTERNAL_ERROR);
            } else {
                $transaction->commit();
                $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
                $orderDataRedis = $activeOrderRepository->getOne($orderId);
                if (array_key_exists('exchange_info', $orderDataRedis)) {
                    /**
                     * @var $serviceExchange ServiceExchange
                     */
                    $serviceExchange = \Yii::$app->serviceExchange;
                    try {
                        $serviceExchange->updateAtExchange($orderDataRedis, $data['subject']);
                    } catch (\Exception $ex) {
                        \Yii::error($ex->getMessage());
                    }
                }
                return $this->getPreparedResponse(ClientErrorCode::OK);
            }

        } catch (BlockedCompanyException $ex) {
            $transaction->rollBack();
            return $this->getPreparedResponse(ClientErrorCode::BAD_PARAM);

        } catch (InvalidClientCard $ex) {
            $transaction->rollBack();
            return $this->getPreparedResponse(ClientErrorCode::INVALID_PAN);

        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex->getMessage());
            return $this->getPreparedResponse(ClientErrorCode::INTERNAL_ERROR);
        }
    }

    protected function rewritingConfirmCodes($newAddresses, $oldAddresses, OrderRecord $order)
    {
        $settingCurrentOrder = $this->orderRepository->getSettingsActiveOrderByData(
            $order->tenant_id,
            $order->order_id
        );

        foreach ($oldAddresses as $point => $oldAddress) {
            if ($point == 'A') {
                $newAddresses[$point] = $oldAddress;
                continue;
            }

            if ($settingCurrentOrder->requirePointConfirmationCode) {
                if ($code = ArrayHelper::getValue($oldAddresses[$point], Courier::FIELD_CONFIRMATION_CODE)) {
                    $newAddresses[$point][Courier::FIELD_CONFIRMATION_CODE] = $code;
                } else {
                    $newAddresses[$point][Courier::FIELD_CONFIRMATION_CODE] =
                        $this->codeGenerator->getUniqueCode();
                }
            }

        }

        return $newAddresses;
    }

    /**
     * @param int $orderId
     * @param int $tenantId
     *
     * @return array|null|\yii\db\ActiveRecord|OrderRecord
     */
    private function getOrder($orderId, $tenantId)
    {
        return OrderRecord::findOne(['order_id' => $orderId, 'tenant_id' => $tenantId]);
    }
}