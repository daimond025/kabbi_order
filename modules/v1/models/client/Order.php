<?php

namespace app\modules\v1\models\client;

use yii\db\ActiveRecord;


/**
 * Class Order
 * @package app\modules\v1\models\client
 */
class Order extends ActiveRecord
{
    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_1 = 'IOS';
    const DEVICE_2 = 'ANDROID';
    const PRE_ODRER_TIME = 1800;
    const PICK_UP_TIME = 300;


    public static function tableName()
    {
        return '{{%order}}';
    }

    public function rules()
    {
        return [
            //            ['tenant_id', 'exist', 'targetClass' => '\api\models\tenant\Tenant'],
            //            ['city_id', 'exist', 'targetClass' => '\api\models\city\City'],
            //            [
            //                'tariff_id',
            //                'exist',
            //                'targetClass'     => '\api\models\taxi_tariff\TaxiTariff',
            //                'targetAttribute' => ['tariff_id', 'tenant_id'],
            //            ],
            //            ['client_id', 'exist', 'targetClass' => '\api\models\client\Client'],
            //            [
            //                'parking_id',
            //                'exist',
            //                'targetClass'     => '\api\models\parking\Parking',
            //                'targetAttribute' => ['parking_id', 'tenant_id', 'city_id'],
            //            ],
            [['predv_price', 'predv_time', 'predv_distance', 'client_device_token', 'comment'], 'safe'],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            ['bonus_payment', 'in', 'range' => [0, 1]],
            [['order_number', 'create_time', 'status_time', 'company_id'], 'number'],
            [
                [
                    'predv_price',
                    'predv_time',
                    'predv_distance',
                    'client_device_token',
                    'comment',
                    'order_number',
                    'create_time',
                    'status_time',
                    'payment',
                    'status_id',
                    'order_time',
                    'address',
                    'currency_id',
                    'position_id',
                    'time_offset',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%order_has_option}}',
            ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->getIsNewRecord()) {
            $retryCount = app()->params['saveOrder.retryCount'];
            for ($i = 0; $i < $retryCount; $i++) {
                $this->order_number = self::getLastOrderNumber($this->tenant_id) + 1;
                try {
                    return parent::save($runValidation, $attributeNames);
                } catch (\Exception $ex) {
                    if (strpos($ex->getMessage(), 'ui_order__tenant_id__order_number') === false) {
                        throw $ex;
                    } else {
                        continue;
                    }
                }
            }

            return false;
        }

        return parent::save($runValidation, $attributeNames);;
    }

}