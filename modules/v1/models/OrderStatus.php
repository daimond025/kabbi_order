<?php

namespace app\modules\v1\models;

use app\models\OrderStatusRecord as OrderStatusRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order_status}}".
 *
 */
class OrderStatus
{
    const TYPE_HOSPITAL = 'HOSPITAL';

    const STATUS_GROUP_NEW = 'new';
    const STATUS_GROUP_PRE_ORDER = 'pre_order';
    const STATUS_GROUP_EXECUTING = 'executing';
    const STATUS_GROUP_COMPLETED = 'completed';
    const STATUS_GROUP_REJECTED = 'rejected';
    const STATUS_GROUP_CAR_ASSIGNED = 'car_assigned';
    const STATUS_GROUP_CAR_AT_PLACE = 'car_at_place';
    const STATUS_GROUP_WARNING = 'warning';
    const STATUS_GROUP_IN_HAND = 'in_hands';

    const STATUS_GROUP_CHAIN = [
        1 => [self::STATUS_GROUP_NEW , self::STATUS_GROUP_PRE_ORDER ],
        2 => [self::STATUS_GROUP_CAR_ASSIGNED],
        3 => [self::STATUS_GROUP_CAR_AT_PLACE],
        4 => [self::STATUS_GROUP_EXECUTING],
        5 => [self::STATUS_GROUP_COMPLETED],

    ];

    const EDIT_PERMIT = 0;
    const EDIT_NOT_PERMIT = 1;

    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_WORKER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_GET_WORKER = 7;
    const STATUS_PRE_REFUSE_WORKER = 10;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_WORKER = 17;
    const STATUS_WORKER_WAITING = 26;
    const STATUS_CLIENT_IS_NOT_COMING_OUT = 27;
    const STATUS_FREE_AWAITING = 29;
    const STATUS_NONE_FREE_AWAITING = 30;
    const STATUS_EXECUTING = 36;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_REJECTED = 39;
    const STATUS_NO_CARS = 40;
    const STATUS_OVERDUE = 52;
    const STATUS_WORKER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_PAYMENT_CONFIRM = 106;
    const STATUS_NO_CARS_BY_DISPATCHER = 107;
    const STATUS_MANUAL_MODE = 108;
    const STATUS_DRIVER_IGNORE_ORDER_OFFER = 109;
    const STATUS_WAITING_FOR_PAYMENT = 110;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT = 111;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD = 112;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT = 113;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_HARD = 114;
    const STATUS_REFUSED_ORDER_ASSIGN = 115;
    const STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION = 116;
    const STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION = 117;
    const STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION = 118;
    const STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION = 119;

    const STATUS_PRE_ORDER_FOR_HOSPITAL = 203;
    const STATUS_FREE_FOR_HOSPITAL = 204;

    const STATUS_PRE_ORDER_FOR_COMPANY = 205;
    const STATUS_FREE_FOR_COMPANY = 206;

    const CACHE_KEY = 'tbl_order_status';

    public static function getNewStatusId()
    {
        return [1, 2, 3, 4, 5, 52, 108, 109, 113, 114, 115];
    }

    public static function getWorksStatusId()
    {
        return [17, 26, 27, 29, 30, 36, 54, 55, 106, 110];
    }

    public static function getWarningStatusId()
    {
        return [5, 10, 16, 27, 30, 38, 45, 46, 47, 48, 52, 54];
    }

    public static function getPreOrderStatusId()
    {
        return [6, 7, 10, 16, 111, 112, 116, 117, 118, 119];
    }

    public static function getCompletedStatusId()
    {
        return [37, 38];
    }

    public static function getRejectedStatusId()
    {
        return [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 107];
    }

    public static function getRejectedStatusGroup($status_id)
    {
        if (in_array($status_id, [39, 40, 41, 42])) {
            return t('status_event', 'Rejected by the client');
        } elseif (in_array($status_id, [43, 44, 45, 46, 47, 48, 49, 50, 51, 107])) {
            return t('status_event', 'Rejected by the dispatcher');
        };

        return null;
    }

    public static function getFinishedStatusId()
    {
        return array_merge(self::getCompletedStatusId(), self::getRejectedStatusId());
    }

    public static function getRejectedStatusIdByClient()
    {
        return [39, 40, 41, 42, 43, 44];
    }

    public static function getRejectedStatusIdByWorker()
    {
        return [45, 46, 47, 48];
    }

    public static function getStatusWithPickUpTime()
    {
        return [
            OrderStatus::STATUS_NEW,
            OrderStatus::STATUS_FREE,
            OrderStatus::STATUS_FREE_FOR_HOSPITAL,
            OrderStatus::STATUS_FREE_FOR_COMPANY,
            OrderStatus::STATUS_NOPARKING,
        ];
    }

    public static function getBlockEditStatuses()
    {
        return [1, 2, 3, 109];
    }

    /**
     * For order update card.
     * @return array
     */
    public static function getCountdownSatusList()
    {
        return [1, 4, 17, 26, 52, 54, 113, 114];
    }

    /**
     * Allow to know order status group.
     *
     * @param integer $status_id
     *
     * @return array Statuses
     */
    public static function getStatusGroup($status_id)
    {
        $arStatusGroup = [];

        if (in_array($status_id, self::getWarningStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_WARNING;
        }
        if (in_array($status_id, self::getNewStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_NEW;
        }
        if (in_array($status_id, self::getWorksStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_IN_HAND;
        }
        if (in_array($status_id, self::getPreOrderStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_PRE_ORDER;
        }
        if (in_array($status_id, self::getCompletedStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_COMPLETED;
        }
        if (in_array($status_id, self::getRejectedStatusId())) {
            $arStatusGroup[] = self::STATUS_GROUP_REJECTED;
        }

        return $arStatusGroup;
    }

    /**
     * Группы, которые входят в раздел "В работе"
     * @return array
     */
    public static function getSubWorkGroups()
    {
        return [
            self::STATUS_GROUP_CAR_ASSIGNED,
            self::STATUS_GROUP_CAR_AT_PLACE,
            self::STATUS_GROUP_EXECUTING,
        ];
    }

    /**
     * Группы, у которых необходимо выводить счетчик новых заказов
     * @return array
     */
    public static function getGroupsForCalculating()
    {
        return [
            self::STATUS_GROUP_NEW,
            self::STATUS_GROUP_IN_HAND,
            self::STATUS_GROUP_WARNING,
            self::STATUS_GROUP_PRE_ORDER,
        ];
    }

    /**
     * Группы, которые хранятся в редисе
     * @return array
     */
    public static function getGroupsFromRedis()
    {
        return [
            self::STATUS_GROUP_NEW,
            self::STATUS_GROUP_IN_HAND,
            self::STATUS_GROUP_WARNING,
            self::STATUS_GROUP_PRE_ORDER,
        ];
    }

    public static function isAllowedStatusForPanel($statusId)
    {
        if (!in_array($statusId, [
            OrderStatus::STATUS_NEW,
            OrderStatus::STATUS_FREE,
            OrderStatus::STATUS_MANUAL_MODE,
        ])
        ) {
            return false;
        }

        return true;
    }

    public static function isStatusManualMode($statusId)
    {
        return $statusId == OrderStatus::STATUS_MANUAL_MODE;
    }

    public static function isWorkerGetOrRefuseOrder($statusId)
    {
        return in_array($statusId, [
            OrderStatus::STATUS_PRE_GET_WORKER,
            OrderStatus::STATUS_PRE_REFUSE_WORKER
        ]);
    }

    public static function isStatusGroupPreOrder($statusGroup)
    {
        return $statusGroup == OrderStatus::STATUS_GROUP_PRE_ORDER;
    }

    public static function isOrderWithPickUpTime($statusId)
    {
        return in_array($statusId, OrderStatus::getStatusWithPickUpTime());
    }

    /**
     * Список статусов из БД (Кешируется).
     * @return array
     */
    public static function getStatusData()
    {
        $cache = Yii::$app->cache;
        $data  = $cache->get(self::CACHE_KEY);

        if ($data === false) {
            $data = OrderStatusRecord::find()->asArray()->all();
            $cache->set(self::CACHE_KEY, $data);
        }

        return $data;
    }

    /**
     * Группы статусов при которых на карте отображается водитель.
     * @return array
     */
    public static function getStatusGroupsForMapWorkerType()
    {
        return [self::STATUS_GROUP_CAR_ASSIGNED, self::STATUS_GROUP_CAR_AT_PLACE, self::STATUS_GROUP_EXECUTING];
    }

    /**
     * Группы статусов при которых на карте отображается маршрут заказа.
     * @return array
     */
    public static function getStatusGroupsForMapRouteType()
    {
        return [self::STATUS_GROUP_COMPLETED];
    }

    /**
     * Is statusId set by worker
     *
     * @param int $statusId
     *
     * @return boolean
     */
    public static function isStatusWorkerAction($statusId)
    {
        return in_array($statusId, [
            self::STATUS_GET_WORKER,
            self::STATUS_WORKER_WAITING,
            self::STATUS_EXECUTING,
            self::STATUS_COMPLETED_PAID,
            self::STATUS_COMPLETED_NOT_PAID,
            self::STATUS_PAYMENT_CONFIRM,
            self::STATUS_EXECUTION_PRE,
            self::STATUS_WAITING_FOR_PAYMENT,
            self::STATUS_REFUSED_ORDER_ASSIGN,
            self::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT,
        ], true);
    }

    public static function isGroupStatusChain($groupNew, $groupOld ){
        // в этих группах ничего не ищем
        if(in_array($groupNew, [self::STATUS_GROUP_COMPLETED, self::STATUS_GROUP_NEW])){
            return true;
        }

        // индекс  групп
        $indexOld = 0;
        $indexNew = 0;
        foreach( self::STATUS_GROUP_CHAIN as $index => $group){
            if(is_array($group) && (in_array($groupNew, $group))){
                $indexNew = (int)$index; continue;
            }
            elseif(is_array($group) && (in_array($groupOld, $group))){
                $indexOld = (int)$index; continue;
            }
            if($indexOld > 0 && $indexNew > 0) break;
        }

        $difference  = (int)($indexNew - $indexOld);
        if((($indexOld == 0) || ($indexNew === 0)) || ($difference == 1)){
            return true;
        }

        return false;
    }
}
