<?php


namespace app\modules\v1\models\orderEvents;

use app\modules\v1\models\orderDataUpdaters\OrderEngineUpdater;
use app\modules\v1\models\orderDataUpdaters\OrderOperatorUpdater;
use yii\base\NotSupportedException;

class OrderSendToExchangeEvent extends OrderUpdateEvent
{

    /**
     * @param array $data
     * @return array
     * @throws NotSupportedException
     */
    public function execute(array $data)
    {
        $updaterConfig = [
            'orderId'    => $data['order_id'],
            'updateTime' => $data['last_update_time'],
            'uuid'       => $data['uuid'],
            'tenantId'   => $data['tenant_id'],
            'lang'       => $data['lang'],
        ];
        $service = $data['sender_service'];

        switch ($service) {
            case self::OPERATOR_SERVICE:
                $object = new OrderOperatorUpdater($updaterConfig);
                break;
            case self::ENGINE_SERVICE:
                $object = new OrderEngineUpdater($updaterConfig);
                break;
            default:
                throw new NotSupportedException('The service "' . $service . '" is not supported');
        }
        $updateParams = $this->getChangedParams($data);
        return $object->sendToExchange($updateParams);
    }

}