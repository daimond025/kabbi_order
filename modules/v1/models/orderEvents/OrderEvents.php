<?php


namespace app\modules\v1\models\orderEvents;


use yii\base\Object;
use app\models\OrderChangeData as OrderChangeDataRecord;

class OrderEvents extends Object
{
    public $orderId;

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getEvents()
    {
        return OrderChangeDataRecord::find()->asArray()->where(['order_id' => $this->orderId])->all();
    }
}