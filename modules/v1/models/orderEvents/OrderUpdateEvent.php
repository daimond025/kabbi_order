<?php


namespace app\modules\v1\models\orderEvents;


use app\modules\v1\models\orderDataUpdaters\OrderClientUpdater;
use app\modules\v1\models\orderDataUpdaters\OrderEngineUpdater;
use app\modules\v1\models\orderDataUpdaters\OrderOperatorUpdater;
use app\modules\v1\models\orderDataUpdaters\OrderWorkerUpdater;
use app\modules\v1\models\orderDataUpdaters\OrderExchangeUpdater;
use yii\base\NotSupportedException;

class OrderUpdateEvent extends OrderEventBase
{

    /**
     * Update the order
     *
     * @param array $data
     *
     * @return array
     * @throws NotSupportedException
     */
    public function execute(array $data)
    {
        $updaterConfig = [
            'orderId'    => $data['order_id'],
            'updateTime' => $data['last_update_time'],
            'uuid'       => $data['uuid'],
            'tenantId'   => $data['tenant_id'],
            'lang'       => $data['lang'],
        ];

        $service = $data['sender_service'];



        switch ($service) {
            case self::OPERATOR_SERVICE:
                $object = new OrderOperatorUpdater($updaterConfig);
                break;
            case self::WORKER_SERVICE:
                $object = new OrderWorkerUpdater($updaterConfig);
                break;
            case self::CLIENT_SERVICE:
                $object = new OrderClientUpdater($updaterConfig);
                break;
            case self::ENGINE_SERVICE:
                $object = new OrderEngineUpdater($updaterConfig);
                break;
            case self::EXCHANGE_SERVICE:
                $object = new OrderExchangeUpdater($updaterConfig);
                break;
            default:
                throw new NotSupportedException('The service "' . $service . '" is not supported');
        }

        $updateParams = $this->getChangedParams($data);
        return $object->update($updateParams);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function getChangedParams(array $data)
    {
        $changedParams = $data['params'];

        switch ($data['sender_service']) {
            case self::OPERATOR_SERVICE:
                $changedParams['user_modifed'] = $data['change_sender_id'];
                $changedParams['subject'] = 'disputcher_' . $data['change_sender_id'];
                break;

            case self::WORKER_SERVICE:
                $changedParams['worker_login'] = $data['change_sender_id'];
                $changedParams['subject'] = 'worker_' . $data['change_sender_id'];
                break;

            case self::CLIENT_SERVICE:
                $changedParams['subject'] = 'client_' . $data['change_sender_id'];
                break;
            case self::ENGINE_SERVICE:
                $changedParams['subject'] = 'system';
                break;
            case self::EXCHANGE_SERVICE:
                $changedParams['subject'] = 'exchange';
                break;
        }


        return $changedParams;
    }
}