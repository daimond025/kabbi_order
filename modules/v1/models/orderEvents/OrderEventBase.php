<?php


namespace app\modules\v1\models\orderEvents;


abstract class OrderEventBase
{
    const OPERATOR_SERVICE = 'operator_service';
    const WORKER_SERVICE = 'worker_service';
    const CLIENT_SERVICE = 'client_service';
    const ENGINE_SERVICE = 'engine_service';
    const EXCHANGE_SERVICE = 'exchange_service';


    /**
     * Execute
     * @param array $data
     * @return array
     */
    abstract public function execute(array $data);


}