<?php


namespace app\modules\v1\models;


use app\modules\v1\models\interfaces\OrderDataProviderInterface;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class WarningOrdersProvider extends OrdersProviderBase implements OrderDataProviderInterface
{
    /**
     * @return array
     */
    public function getList()
    {
        $activeOrders   = $this->getActiveOrders();
        $orderExclusion = ArrayHelper::getColumn($activeOrders, 'order_id');

        return ArrayHelper::merge($activeOrders,
            (new Order([
                'tenantId'   => $this->tenantId,
                'cityId'     => $this->cityId,
                'positionId' => $this->positionId,
            ]))->getByStatusGroup($this->statusGroup, $this->date, [], [], $orderExclusion));
    }

    /**
     * @return int
     */
    public function getCounts()
    {
        return count($this->getList());
    }

    /**
     * @return array
     */
    private function getActiveOrders()
    {
        return (new ActiveOrdersProvider([
            'cityId'      => $this->cityId,
            'positionId'  => $this->positionId,
            'statusGroup' => $this->statusGroup,
            'tenantId'    => $this->tenantId,
            'date'        => $this->date,
            'allowCityId' => $this->allowCityId,
        ]))->getList();
    }
}