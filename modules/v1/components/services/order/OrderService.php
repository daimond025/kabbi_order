<?php

namespace app\modules\v1\components\services\order;


use app\models\TenantSetting;
use app\modules\v1\components\repositories\WorkerActiveOrderRepository;

class OrderService
{
    /** @var $workerActiveOrderRepository WorkerActiveOrderRepository */
    protected $workerActiveOrderRepository;

    public function __construct()
    {
        $this->workerActiveOrderRepository = \Yii::$container->get(WorkerActiveOrderRepository::class);

    }

    /**
     * Is allowed to worker set status assigned at order soft
     * @param $tenantId
     * @param $workerCallsign
     * @param $cityId
     * @param $positionId
     * @return bool
     */
    public function isAllowedSetStatusAssignedAtOrderSoft($tenantId, $workerCallsign, $cityId, $positionId)
    {
        $isAllowed = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_RESERVE_URGENT_ORDERS, $cityId, $positionId
        );

        if ($isAllowed) {

            $maxCountOrders = (int)TenantSetting::getSettingValue(
                $tenantId, TenantSetting::SETTING_MAX_COUNT_RESERVE_URGENT_ORDERS, $cityId, $positionId
            );

            $currentCountOrders = count($this->workerActiveOrderRepository->getActiveOrdersByWorker($tenantId, $workerCallsign));

            if ($maxCountOrders > $currentCountOrders) {
                return true;
            }

        }

        return false;
    }

    public function isMaxCountActiveOrders($active_orders, $tenantId,  $cityId, $positionId){
        $isAllowed = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_RESERVE_URGENT_ORDERS, $cityId, $positionId
        );

        if ($isAllowed) {

            $maxCountOrders = (int)TenantSetting::getSettingValue(
                $tenantId, TenantSetting::SETTING_MAX_COUNT_RESERVE_URGENT_ORDERS, $cityId, $positionId
            );

            $currentCountOrders = count($active_orders);

            if ($maxCountOrders > $currentCountOrders) {
                return true;
            }

        }

        return false;
    }

}