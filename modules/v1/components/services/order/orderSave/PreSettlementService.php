<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\components\TaxiRouteAnalyzer;
use app\modules\v1\models\OrderRecord;

class PreSettlementService
{

    public function getOrderCostData(OrderRecord $order)
    {
        /** @var TaxiRouteAnalyzer $taxiRouteAnalyzer */
        $taxiRouteAnalyzer = app()->routeAnalyzer;

      /*  $temp = unserialize($order->address);
        unset($temp['B']);
        unset($temp['A']['lat']);
        unset($temp['A']['lon']);
        $temp['A']['city_id'] = ;
        var_dump( $temp);
        exit();*/

        $routeCostInfo     = $taxiRouteAnalyzer->analyzeRoute(
            $order->tenant_id,
            $order->city_id,
            unserialize($order->address),
            $order->additional_option,
            $order->tariff_id,
            date('d.m.Y H:i:s', $order->order_time),
            $order->client_id
        );



        if (empty($routeCostInfo)) {
            throw new \RuntimeException('No $routeCostInfo in Order::getOrderCostData()');
        }

        $costData = [];
        $result   = (array)$routeCostInfo;

        if (!isset($result['tariffInfo']) || empty($result['tariffInfo'])) {
            throw new \RuntimeException('No tariffInfo in Order::getOrderCostData()');
        }


        $costData['additionals_cost']     = isset($result['additionalCost']) ? (string)$result['additionalCost'] : null;
        $costData['summary_time']         = isset($result['summaryTime']) ? (string)$result['summaryTime'] : null;
        $costData['summary_distance']     = isset($result['summaryDistance']) ? (string)$result['summaryDistance'] : null;
        $costData['city_time']            = isset($result['cityTime']) ? (string)$result['cityTime'] : null;
        $costData['city_distance']        = isset($result['cityDistance']) ? (string)$result['cityDistance'] : null;
        $costData['city_cost']            = isset($result['cityCost']) ? (string)$result['cityCost'] : null;
        $costData['out_city_time']        = isset($result['outCityTime']) ? (string)$result['outCityTime'] : null;
        $costData['out_city_distance']    = isset($result['outCityDistance']) ? (string)$result['outCityDistance'] : null;
        $costData['out_city_cost']        = isset($result['outCityCost']) ? (string)$result['outCityCost'] : null;
        $costData['enable_parking_ratio'] = empty($result['enableParkingRatio']) ? 0 : 1;

        if ($order->isFixPrice()) {
            $costData['summary_cost'] = $order->predv_price;
            $costData['is_fix']       = $order->is_fix;
        } else {
            $costData['is_fix']       = empty($result['isFix']) ? 0 : 1;
            $costData['summary_cost'] = isset($result['summaryCost']) ? (string)$result['summaryCost'] : null;
        }

        $costData['summary_cost_no_discount'] = isset($result['summaryCostNoDiscount']) ? (string)$result['summaryCostNoDiscount'] : null;
        $costData['tariffInfo']               = $result['tariffInfo'];
        $costData['start_point_location']     = isset($result['startPointLocation']) ? (string)$result['startPointLocation'] : null;

        return $costData;
    }
}
