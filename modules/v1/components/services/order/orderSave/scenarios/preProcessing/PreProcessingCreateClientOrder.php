<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\preProcessing;

use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\components\services\order\orderSave\AddressService;
use app\modules\v1\components\services\order\orderSave\OrderService;
use app\modules\v1\components\services\order\orderSave\paymentService\PaymentService;
use app\modules\v1\components\services\order\orderSave\PreSettlementService;
use app\modules\v1\components\services\order\orderSave\PromoService;
use app\modules\v1\components\services\order\orderSave\StatusService;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;
use app\modules\v1\models\workers\TimeHelper;

class PreProcessingCreateClientOrder extends AbstractPreProcessing
{

    public function run(OrderRecord $order)
    {

        $this->testClientBlackList($order->phone, $order->tenant_id);

        $this->setOrderTime($order);

        $order->validateAddress();
        $order->serializeAndPrepareAddress();
        $order->parking_id  = $this->getParkingId($order);
        $order->currency_id = $order->settings->getCurrencyId();
        $this->setStatusIdAndCorrectionOrderTime($order);


        $this->makePreSettlement($order);

        $order->order_number    = $this->prepareOrderNumber($order);
        $order->status_time     = $this->getStatusTime();
        $order->arOldAttributes = $order->getOldAttributes();
        $order->time_offset     = $order->getTimeOffset();

        $this->paymentService->testPaymentByClient($order);
        $order->promo_code_id = $this->promoService->findSuitablePromoCodeId($order);
    }

    protected function getParkingId(OrderRecord $order)
    {
        $address = unserialize($order->address);

        return (string)$address['A']['parking_id'];
    }

    protected function setOrderTime(OrderRecord $order)
    {

        if (empty($order->order_time)) {
            $order->order_time = TimeHelper::getCurrentTimeInCity($order->city_id);
        }

    }

    protected function setStatusIdAndCorrectionOrderTime(OrderRecord $order)
    {
        $settingPreOrderTime = $order->settings->getPreOrderTimeInSecond();

        $now    = TimeHelper::getCurrentTimeInCity($order->city_id);

        $pickUp = $order->settings->getPickUp();


        if ($order->order_time - $now >= $settingPreOrderTime) {
            $order->status_id = empty($order->parking_id)
                ? OrderStatus::STATUS_PRE_NOPARKING
                : OrderStatus::STATUS_PRE;
            $order->edit = 1;
        } else {
            $order->status_id = empty($order->parking_id)
                ? OrderStatus::STATUS_NOPARKING
                : OrderStatus::STATUS_NEW;
        }

        if ($order->order_time - $pickUp < $now) {
            $order->order_time = $now + $pickUp;
        }
    }
}
