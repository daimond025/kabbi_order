<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\preProcessing;

use app\modules\v1\components\services\order\orderSave\ActionManager;
use app\modules\v1\models\OrderRecord;

class PreProcessingFactory
{
    public static function getPreProcessing(OrderRecord $order)
    {
        switch ($order->orderAction) {
            case ActionManager::ACTION_NEW_ORDER:
            case ActionManager::ACTION_FREEZE_ORDER:
            case ActionManager::ACTION_FREE_ORDER:
                return \Yii::createObject(PreProcessingCreateOrder::class);

            case ActionManager::ACTION_EXECUTING:
                return \Yii::createObject(PreProcessingExecutingOrder::class);

            case ActionManager::ACTION_NEW_ORDER_CLIENT:
                return \Yii::createObject(PreProcessingCreateClientOrder::class);
            default:
                throw new \RuntimeException('Error orderAction indefined');
        }
    }
}
