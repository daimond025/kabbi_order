<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\preProcessing;

use app\modules\v1\models\OrderRecord;

class PreProcessingCreateOrder extends AbstractPreProcessing
{
    public function run(OrderRecord $order)
    {
        $order->identifyAndSetStatus();

        $order->validateAddress();
        $order->serializeAndPrepareAddress();
        $order->prepareStatusForPanel();


        $order->order_number = $this->prepareOrderNumber($order);
        $order->order_time   = $order->getOrderTimeForSave();

        $this->statusService->updateOrderStatus($order);

        $order->status_time     = $this->getStatusTime();
        $order->arOldAttributes = $order->getOldAttributes();
        $order->currency_id     = $order->settings->getCurrencyId();

        $order->time_offset     = $order->getTimeOffset();
        $this->paymentService->testPaymentByClient($order);
        $order->promo_code_id   = $this->promoService->findSuitablePromoCodeId($order);


    }
}
