<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\preProcessing;

use app\components\OrderError;
use app\models\Worker;
use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\components\services\order\orderSave\AddressService;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\components\services\order\orderSave\OrderService;
use app\modules\v1\components\services\order\orderSave\paymentService\PaymentService;
use app\modules\v1\components\services\order\orderSave\PreSettlementService;
use app\modules\v1\components\services\order\orderSave\PromoService;
use app\modules\v1\components\services\order\orderSave\StatusService;
use app\modules\v1\components\services\order\orderSave\WorkerPrepareService;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\workers\TimeHelper;

class PreProcessingExecutingOrder extends AbstractPreProcessing
{
    protected $workerPrepareService;

    public function __construct(
        AddressService $addressService,
        StatusService $statusService,
        PromoService $promoService,
        OrderService $orderService,
        ClientService $clientService,
        PreSettlementService $preSettlementService,
        WorkerPrepareService $workerPrepareService,
        PaymentService $paymentService
    ) {
        parent::__construct(
            $addressService,
            $statusService,
            $promoService,
            $orderService,
            $clientService,
            $preSettlementService,
            $paymentService
        );

        $this->workerPrepareService = $workerPrepareService;
    }

    public function run(OrderRecord $order)
    {
        $this->workerPrepareService->prepareWorker($order);
        $this->testClientBlackList($order->phone, $order->tenant_id);

        $order->identifyAndSetStatus();

        $order->validateAddress();
        $order->serializeAndPrepareAddress();
        $order->order_number = $this->prepareOrderNumber($order);
        $order->order_time   = TimeHelper::getCurrentTimeInCity($order->city_id);
        $order->status_time  = $this->getStatusTime();

        $this->makePreSettlement($order);

        $order->arOldAttributes = $order->getOldAttributes();
        $order->currency_id     = $order->settings->getCurrencyId();
        $order->time_offset     = $order->getTimeOffset();

        $this->paymentService->testPaymentByClient($order);
        $order->promo_code_id   = $this->promoService->findSuitablePromoCodeId($order);
    }
}
