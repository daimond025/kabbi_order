<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\preProcessing;

use app\components\OrderError;
use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\components\services\order\orderSave\AddressService;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\components\services\order\orderSave\OrderService;
use app\modules\v1\components\services\order\orderSave\paymentService\PaymentService;
use app\modules\v1\components\services\order\orderSave\PreSettlementService;
use app\modules\v1\components\services\order\orderSave\PromoService;
use app\modules\v1\components\services\order\orderSave\StatusService;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;
use yii\db\Expression;

abstract class AbstractPreProcessing
{
    /** @var AddressService */
    protected $addressService;

    /** @var StatusService */
    protected $statusService;

    /** @var PromoService */
    protected $promoService;

    /** @var OrderService */
    protected $orderService;

    /** @var ClientService */
    protected $clientService;

    /** @var PreSettlementService */
    protected $preSettlementService;

    /** @var PaymentService */
    protected $paymentService;

    public function __construct(
        AddressService $addressService,
        StatusService $statusService,
        PromoService $promoService,
        OrderService $orderService,
        ClientService $clientService,
        PreSettlementService $preSettlementService,
        PaymentService $paymentService
    ) {
        $this->addressService       = $addressService;
        $this->statusService        = $statusService;
        $this->promoService         = $promoService;
        $this->orderService         = $orderService;
        $this->clientService        = $clientService;
        $this->preSettlementService = $preSettlementService;
        $this->paymentService       = $paymentService;
    }

    abstract public function run(OrderRecord $order);

    protected function makePreSettlement(OrderRecord $order)
    {

        $costData              = $this->preSettlementService->getOrderCostData($order);

        $order->predv_price    = $costData['summary_cost'];
        $order->predv_time     = $costData['summary_time'];
        $order->predv_distance = $costData['summary_distance'];


        if ($costData['summary_cost'] != $costData['summary_cost_no_discount']) {
            $order->predv_price_no_discount = $costData['summary_cost_no_discount'];
        }
    }

    protected function testClientBlackList($phone, $tenant_id)
    {
        if ($this->clientService->isHasClientBlackList($phone, $tenant_id)) {
            throw new UserException(OrderError::BLACK_LIST);
        }
    }

    protected function getStatusTime()
    {
        return time();
    }

    protected function prepareOrderNumber(OrderRecord $order)
    {
        return new Expression("(SELECT * FROM (SELECT COALESCE (MAX(order_number), 0) FROM tbl_order WHERE tenant_id = {$order->tenant_id} FOR UPDATE) AS t) + 1");
    }
}
