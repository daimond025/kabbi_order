<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\postProcessing;

use app\modules\v1\components\services\order\orderSave\ActionManager;
use app\modules\v1\models\OrderRecord;

class PostProcessingFactory
{
    public static function getPostProcessing(OrderRecord $order)
    {
        switch ($order->orderAction) {
            case ActionManager::ACTION_NEW_ORDER:
            case ActionManager::ACTION_FREEZE_ORDER:
            case ActionManager::ACTION_FREE_ORDER:
            case ActionManager::ACTION_EXECUTING:
                return \Yii::createObject(PostProcessingForMostCases::class);

            case ActionManager::ACTION_NEW_ORDER_CLIENT:
                return \Yii::createObject(PostProcessingCreateClientOrder::class);
            default:
                throw new \RuntimeException('Error orderAction indefined');
        }
    }
}
