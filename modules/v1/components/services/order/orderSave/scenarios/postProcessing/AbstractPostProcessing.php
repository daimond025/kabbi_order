<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\postProcessing;

use app\models\Call;
use app\modules\v1\components\services\order\orderSave\AddressService;
use app\modules\v1\components\services\order\orderSave\OrderService;
use app\modules\v1\models\OrderHasOption;
use app\modules\v1\models\OrderRecord;

abstract class AbstractPostProcessing
{
    /** @var OrderService */
    protected $orderService;
    /** @var AddressService */
    protected $addressService;

    public function __construct(
        OrderService $orderService,
        AddressService $addressService
    ) {
        $this->orderService   = $orderService;
        $this->addressService = $addressService;
    }

    abstract public function run(OrderRecord $order);

    public function runAfterTransaction(OrderRecord $order)
    {
        $order->order_number = (int)$this->getOrderNumber($order);

        $this->orderService->distributeOrder($order);

        $orderJson = json_encode($order->getAttributes(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


        \Yii::info(
            "[order] order_id={$order->order_id},client_id={$order->client_id} Order was created ({$orderJson})",
            'app-log'
        );
    }

    protected function getOrderNumber(OrderRecord $order)
    {
        return OrderRecord::find()
            ->select(['order_number'])
            ->where(['order_id' => $order->order_id])
            ->scalar();
    }

    protected function processingCallId(OrderRecord $order)
    {
        $call_id = $order->call_id;
        if (!empty($call_id)) {
            $call = Call::findOne(['uniqueid' => $call_id]);
            if (!empty($call) && empty($call->order_id)) {
                $call->order_id = $order->order_id;
                $call->save(false, ['order_id']);
            }
        }
    }

    protected function saveAdditionalOptions(OrderRecord $order)
    {
        OrderHasOption::manySave($order->additional_option, $order->order_id);
    }
}
