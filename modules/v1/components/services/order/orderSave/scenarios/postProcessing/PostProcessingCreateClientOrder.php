<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\postProcessing;

use app\components\ClientErrorCode;
use app\components\helpers\ApiClientHelper;
use app\components\OrderError;
use app\models\CardPayment;
use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\components\services\order\orderSave\AddressService;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\components\services\order\orderSave\OrderService;
use app\modules\v1\models\OrderRecord;
use bonusSystem\BonusSystem;
use bonusSystem\exceptions\ClientIsNotRegisteredException;
use bonusSystem\exceptions\GetCustomerException;

class PostProcessingCreateClientOrder extends AbstractPostProcessing
{
    /** @var ClientService */
    protected $clientService;

    public function __construct(
        OrderService $orderService,
        AddressService $addressService,
        ClientService $clientService
    ) {
        parent::__construct($orderService, $addressService);

        $this->clientService = $clientService;
    }

    public function run(OrderRecord $order)
    {
        $this->updateDataClient($order);

        $this->bonusProcessing($order);

        $this->processingPayment($order);

        $this->saveAdditionalOptions($order);


        $this->orderService->saveOrder($order);
        $this->addressService->saveAddressToHistoryClient($order);

        $this->processingCallId($order);
        $order->updateOrderExceptCarModels();
    }

    protected function processingPayment(OrderRecord $order)
    {
        if ($order->isCardPayment()) {
            $cardPayment           = new CardPayment();
            $cardPayment->pan      = $order->pan;
            $cardPayment->order_id = $order->order_id;

            if (!$cardPayment->save()) {
                \Yii::error(implode("\n", $cardPayment->getFirstErrors()), 'bank-card');
                throw new UserException(ClientErrorCode::INTERNAL_ERROR);
            }
        }
    }

    protected function updateDataClient(OrderRecord $order)
    {
        $this->clientService->updateDeviceAndCityData(
            $order->client,
            $order->device,
            $order->client_device_token,
            $order->city_id,
            $order->lang,
            $order->app_id
        );
    }

    protected function bonusProcessing(OrderRecord $order)
    {
        $bonusSystem = new BonusSystem($order->tenant_id);

        if (ApiClientHelper::isVersionAppForBonusSystem($order->versionclient)) {
            try {

                if ($bonusSystem->isUDSGameBonusSystem()
                    && $bonusSystem->isRegisteredClient($order->client_id)
                ) {
                    $bonusPromoCode = $order->bonus_payment
                        ? $order->bonus_promo_code
                        : null;


                    $bonusSystem->registerOrder(
                        $order->order_id,
                        $bonusPromoCode
                    );
                }

            } catch (GetCustomerException $e) {
                throw new UserException(OrderError::INVALID_PROMO_CODE);
            } catch (ClientIsNotRegisteredException $ex) {
                throw new UserException(OrderError::BONUS_SYSTEM_IS_NOT_ACTIVATED);
            } catch (\Exception $ex) {
                \Yii::error($ex);
            }
        }
    }
}
