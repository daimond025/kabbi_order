<?php

namespace app\modules\v1\components\services\order\orderSave\scenarios\postProcessing;

use app\modules\v1\models\OrderRecord;

class PostProcessingForMostCases extends AbstractPostProcessing
{
    public function run(OrderRecord $order)
    {
        $this->saveAdditionalOptions($order);
        $this->orderService->saveOrder($order);

        $this->addressService->saveAddressToHistoryClient($order);

        $this->processingCallId($order);


        $order->updateOrderExceptCarModels();
    }
}
