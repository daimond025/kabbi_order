<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;

class OrderStatusDelegate
{
    protected $order;

    public function __construct(OrderRecord $order)
    {
        $this->order = $order;
    }

    public function identifyAndSetStatus()
    {
        switch ($this->order->orderAction) {
            case ActionManager::ACTION_NEW_ORDER:
                $this->order->status_id = OrderStatus::STATUS_NEW;
                break;
            case ActionManager::ACTION_FREE_ORDER:
                $this->order->status_id = OrderStatus::STATUS_FREE;
                break;
            case ActionManager::ACTION_FREEZE_ORDER:
                $this->order->status_id = OrderStatus::STATUS_MANUAL_MODE;
                break;
            case ActionManager::ACTION_ASSIGN_ORDER:
                $this->order->status_id = (int)$this->order->deny_refuse_order === 1
                    ? OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD
                    : OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                break;
            case ActionManager::ACTION_COMPLETE_PAY_ORDER:
                $this->order->status_id = OrderStatus::STATUS_COMPLETED_PAID;
                break;
            case ActionManager::ACTION_COMPLETE_NO_PAY_ORDER:
                $this->order->status_id = OrderStatus::STATUS_COMPLETED_NOT_PAID;
                break;
            case ActionManager::ACTION_REJECT_ORDER:
                $this->order->status_id = $this->order->status_reject;
                break;
            case ActionManager::ACTION_EXECUTING:
                $this->order->status_id = OrderStatus::STATUS_EXECUTING;
        }
    }

    public function prepareStatusForPanel()
    {
        if (!OrderStatus::isAllowedStatusForPanel($this->order->status_id)) {
            $this->order->status_id = OrderStatus::STATUS_NEW;
        }
    }
}
