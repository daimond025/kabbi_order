<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\components\repositories\OrderRepository;
use app\components\repositories\WorkerShiftRepository;
use app\components\serviceEngine\ServiceEngine;
use app\modules\v1\models\OrderRecord;

class OrderService
{
    /** @var OrderRepository */
    protected $orderRepository;

    /** @var WorkerShiftRepository */
    protected $workerShiftRepository;

    /** @var PreSettlementService */
    protected $preSettlement;

    /** @var ServiceEngine */
    protected $serviceEngine;

    public function __construct(
        OrderRepository $orderRepository,
        WorkerShiftRepository $workerShiftRepository,
        ServiceEngine $serviceEngine,
        PreSettlementService $preSettlement
    ) {
        $this->orderRepository       = $orderRepository;
        $this->workerShiftRepository = $workerShiftRepository;
        $this->serviceEngine         = $serviceEngine;
        $this->preSettlement         = $preSettlement;
    }

    /**
     * @param OrderRecord $order
     *
     * @throws \RuntimeException
     */
    public function distributeOrder(OrderRecord $order)
    {
        try {
            $resultSendOrder = $this->serviceEngine->neworderAuto($order->order_id, $order->tenant_id);
        } catch (\Exception $e) {
            \Yii::error("Receive error neworderAuto: orderId={$order->order_id} (Error:{$e->getMessage()})");
            $resultSendOrder = false;
        }

        if (!$resultSendOrder) {
            $error = null;
            try {
                $this->orderRepository->deleteOrder($order);
            } catch (\Exception $ex) {
                $isOrderDeleted = false;
                $error = $ex->getMessage();
            }

            if (empty($isOrderDeleted)) {
                \Yii::error(
                    "Error delete new order from MySql, after engine was failed: orderId={$order->order_id} (Error:{$error})",
                    'order'
                );
                \Yii::info(
                    "[order] order_id={$order->order_id},user_id={$order->user_create} Error delete new order from MySql, after engine was failed (Error:{$error})",
                    'app-log'
                );
            }
            \Yii::info(
                "[order] order_id={$order->order_id},user_id={$order->user_create} Error to save order to nodejs",
                'app-log'
            );
            throw new \RuntimeException('Error to save order to service_engine');
        }
    }

    public function saveOrder(OrderRecord $order)
    {
        $redisSaveResult = $this->orderRepository->saveOrder(
            $order->tenant_id,
            $order->order_id,
            $this->getOrderSerialize($order)
        );


        if (($redisSaveResult != 1)) {
            throw new \RuntimeException('Error to save order to redis');
        }
    }

    protected function getOrderSerialize(OrderRecord $order)
    {
        $costData        = $this->preSettlement->getOrderCostData($order);
        $exceptCarModels = $order->except_car_models;

        $tariffId = isset($costData["tariffInfo"]["tariffDataCity"]["tariff_id"])
            ? $costData["tariffInfo"]["tariffDataCity"]["tariff_id"]
            : null;

        $order = $this->orderRepository->getOrder($order->order_id, $tariffId);

        $order['tenant_login']    = $order['tenant']['domain'];
        $order['costData']        = $costData;
        $order['exceptCarModels'] = $exceptCarModels;

        if (!empty($order['worker'])) {
            $workerData      = $this->workerShiftRepository->getActiveWorker(
                $order['worker']['callsign'],
                $order['tenant_id']
            );
            $order["car_id"] = $workerData["car"]["car_id"];
            $order["worker"] = $workerData["worker"];
            $order["car"]    = $workerData["car"];
        }
        return serialize($order);
    }
}
