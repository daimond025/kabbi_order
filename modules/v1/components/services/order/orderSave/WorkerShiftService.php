<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\models\Worker;
use app\modules\v1\models\workers\worker\WorkerShift;
use app\modules\v1\models\workers\WorkerRepository;

class WorkerShiftService
{

    protected $workerRepository;

    public function __construct(WorkerRepository $workerRepository)
    {
        $this->workerRepository = $workerRepository;
    }

    public function setPauseData($tenantId, $tenantLogin, $workerCallsign, $comment, $action)
    {

        $worker = $this->workerRepository->getActiveWorker($tenantId, $workerCallsign);

        if (empty($worker)) {
            return;
        }

        $pauseData = [];

        if (!empty($worker['worker']['pause_data'])) {
            $pauseData = $worker['worker']['pause_data'];
            $count     = count($pauseData);

            if ($pauseData[$count - 1]['pause_end'] === null) {
                $pauseData[$count - 1]['pause_end'] = (string)time();
            }
        }

        if ($action === Worker::ACTION_PAUSE) {
            $pauseData[] = [
                'pause_start'  => (string)time(),
                'pause_end'    => null,
                'pause_reason' => $comment,
            ];
        }

        $worker['worker']['pause_data'] = $pauseData;
        $this->workerRepository->saveActiveWorker($tenantId, $workerCallsign, $worker);

        $shift = WorkerShift::findone($this->workerRepository->getWorkerShift($tenantLogin, $workerCallsign));

        if (!empty($shift)) {
            $shift->pause_data = serialize($pauseData);
            $shift->save();
        }
    }
}
