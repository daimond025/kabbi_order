<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;

class StatusService
{

    public function prepareStatusForPanel($statusId)
    {
        if (!OrderStatus::isAllowedStatusForPanel($statusId)) {
            return OrderStatus::STATUS_NEW;
        }

        return $statusId;
    }

    public function updateOrderStatus(OrderRecord $order)
    {
        if (OrderStatus::isStatusManualMode($order->status_id)) {
            return;
        }

        if (OrderStatus::isWorkerGetOrRefuseOrder($order->status_id)) {
            return;
        }

        if ($order->orderTime->isPreOrder()) {
            if (!OrderStatus::isStatusGroupPreOrder($order->status->status_group)) {
                $order->status_id = empty($order->parking_id)
                    ? OrderStatus::STATUS_PRE_NOPARKING : OrderStatus::STATUS_PRE;
            }
            $order->edit = 1;
        } elseif (OrderStatus::isStatusGroupPreOrder($order->status->status_group)) {
            $order->status_id  = empty($order->parking_id)
                ? OrderStatus::STATUS_NOPARKING : OrderStatus::STATUS_NEW;
            $order->order_time = $order->getOrderTimeForSave();
        }
    }
}
