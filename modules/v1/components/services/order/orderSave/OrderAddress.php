<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\models\Address;
use app\models\DefaultSettings;
use app\models\Parking;
use app\models\TenantSetting;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\delegations\address\Courier;
use app\modules\v1\extensions\codeGenerator\CodeGenerator;
use app\modules\v1\models\OrderRecord;
use yii\helpers\ArrayHelper;

class OrderAddress
{

    public $placeIds;
    protected $order;
    protected $lang;
    /** @var Courier */
    protected $courier;

    public function __construct(OrderRecord $order)
    {
        $this->order = $order;
        $this->courier = \Yii::createObject(Courier::class, [$order]);

        $this->tenant_id = null;
        $this->city_id = null;
        $this->lang = null;
    }

    public function serializeAndPrepareAddress()
    {
        $this->prepareAddress();
        $this->order->address = $this->sanitizeAddress($this->order->address);

        $this->order->address = serialize($this->determineParkingAndAddressCoords($this->order));

    }

    public function validateAddress()
    {
        $this->prepareComponent();
        $this->prepareAddress();
        $deteilsAddress = $this->deteilAddress();
        $preparedAddresses = [];

        foreach($this->order->address as $address){
            $newAddress = [];
            foreach($address as $key => $paramVal){
                if(!empty($paramVal)){
                    $paramVal = trim(strip_tags(stripcslashes($paramVal)));

                    if($key == "city"){
                        $paramVal = mb_substr($paramVal, 0, 50, 'utf-8');
                    }
                    if($key == "street"){
                        $paramVal = mb_substr($paramVal, 0, 100, 'utf-8');
                    }
                    if(in_array($key, ['house', 'housing', 'porch', 'apt'])){
                        $paramVal = mb_substr($paramVal, 0, 10, 'utf-8');
                    }
                }
                $newAddress[$key] = $paramVal;
            }

            $addressData['city_id'] = ArrayHelper::getValue($newAddress, 'city_id');
            $addressData['city'] = ArrayHelper::getValue($newAddress, 'city');
            $addressData['street'] = ArrayHelper::getValue($newAddress, 'street');
            $addressData['housing'] = ArrayHelper::getValue($newAddress, 'housing');
            $addressData['house'] = ArrayHelper::getValue($newAddress, 'house');
            $addressData['porch'] = ArrayHelper::getValue($newAddress, 'porch');
            $addressData['apt'] = ArrayHelper::getValue($newAddress, 'apt');
            $addressData['lat'] = ArrayHelper::getValue($newAddress, 'lat');
            $addressData['lon'] = ArrayHelper::getValue($newAddress, 'lon');


            $addressData['lon'] = ArrayHelper::getValue($newAddress, 'lon');

            $placeId = ArrayHelper::getValue($newAddress, 'placeId');
            if(!is_null($placeId) &&  is_array($deteilsAddress[$placeId])){
                $deteilAdres = $deteilsAddress[$placeId];
                $addressData['house_n'] = ArrayHelper::getValue($deteilAdres, 'house');
                $addressData['street_n'] = ArrayHelper::getValue($deteilAdres, 'street');
                $addressData['city_n'] = ArrayHelper::getValue($deteilAdres, 'city');
                $addressData['code_n'] = ArrayHelper::getValue($deteilAdres, 'code');
                $addressData['name_n'] = ArrayHelper::getValue($deteilAdres, 'name');
                $addressData['placeId'] = $placeId;
            }else{
                $addressData['house_n'] ='';
                $addressData['street_n'] = '';
                $addressData['city_n'] = '';
                $addressData['code_n'] = '';
                $addressData['name_n'] = '';
            }

            $addressData[Courier::FIELD_CONFIRMATION_CODE]
                = ArrayHelper::getValue($address, Courier::FIELD_CONFIRMATION_CODE);
            $addressData[Courier::FIELD_COMMENT]
                = ArrayHelper::getValue($address, Courier::FIELD_COMMENT);
            $addressData[Courier::FIELD_PHONE]
                = ArrayHelper::getValue($address, Courier::FIELD_PHONE);

            $preparedAddresses[] = $addressData;
        }

        if(empty($preparedAddresses)){
            throw (new UserException(\Yii::t('order', 'Missing address')));
        }

        $this->order->address = $preparedAddresses;

        $this->courier->validate();
    }

    public function getPlaces()
    {
        $places = [];
        foreach($this->order->address as $key => $address){
            $placeId = ArrayHelper::getValue($address, 'placeId');
            if(!is_null($placeId) && !empty($placeId)){
                $places[] = trim($placeId);
            }
        }
        $this->placeIds = $places;
    }

    public function deteilAddress()
    {
        // unput place_ids
        $this->getPlaces();

        $placesDbs = $this->getDbAddress();
        // check
        $placesIdDB = array_keys($placesDbs);

        $result = [];

        foreach($this->placeIds as $placeId){
            // get address from google
            if(!in_array($placeId, $placesIdDB)){
                $address = $this->getGeoDeteils($placeId);
                $result[$placeId] = self::formatedPlace($address);
                $this->saveDeteilsAddress($placeId, $address);
            }// nedd update deteils address
            elseif(Address::needUpdate($placesDbs[$placeId])){
                $address = $this->getGeoDeteils($placeId);
                $result[$placeId] = self::formatedPlace($address);
                $this->updateDeteilsAddres($placesDbs[$placeId], $placeId,  $address);
            }else{
                $result[$placeId] = self::formatedPlace(
                    isset($placesDbs[$placeId]->address) ? $placesDbs[$placeId]->address : [] );
            }
        }
        return $result;
    }

    public static function formatedPlace($address){
        return [
            'house' => mb_substr(ArrayHelper::getValue($address, 'house', ''), 0, 50, 'utf-8'),
            'street' => mb_substr(ArrayHelper::getValue($address, 'street', ''), 0, 50, 'utf-8'),
            'city' => mb_substr(ArrayHelper::getValue($address, 'city', ''), 0, 50, 'utf-8'),
            'country' => mb_substr(ArrayHelper::getValue($address, 'country', ''), 0, 50, 'utf-8'),
            'code' => ArrayHelper::getValue($address, 'code', ''),
            'lat' => ArrayHelper::getValue($address, 'lat', ''),
            'lon' => ArrayHelper::getValue($address, 'lon', ''),
            'name' => ArrayHelper::getValue($address, 'name', '')
        ];

    }

    public function getGeoDeteils($placeId)
    {
        $address = app()->geoService->placeDeteils(
            $this->order->tenant_id,
            $this->order->city_id,
            $this->lang,
            $placeId
        );
        return $address;
    }

    /**
     * function update geoData
     *
     * @param Address $geoDeteils
     *
     * @param string $placeId
     *
     * @param string $address
     *
     * @return bool
     * */
    public function updateDeteilsAddres($geoDeteils, $placeId, $address)
    {
        $geoDeteils->place_id = $placeId;
        $geoDeteils->address = $address;
        return $geoDeteils->save();

    }

    public function saveDeteilsAddress($placeId, $address)
    {
        $geoDeteils = new Address();
        $geoDeteils->place_id = $placeId;
        $geoDeteils->address = $address;
        return $geoDeteils->save();
    }

    /*
     * @return array app\models\Address
     * */
    public function getDbAddress()
    {
        $placesDbs = Address::find()
            ->where(['in', 'place_id', $this->placeIds])
            ->all();

        $result = [];
        foreach($placesDbs as $places){
            $result[$places->place_id] = $places;
        }
        return $result;
    }

    public function prepareComponent(){
        $this->tenant_id = $this->order->tenant_id;
        $this->city_id = $this->order->city_id;
        $this->lang = TenantSetting::getSettingValue($this->tenant_id, 'LANGUAGE', $this->city_id );
    }
    public function prepareAddress()
    {
        if(is_array($this->order->address)){
            return;
        }

        $parsedAddress = unserialize($this->order->address);
        if($parsedAddress === false){
            throw new \RuntimeException('Error address unserialize');
        }

        $this->order->address = $parsedAddress;
    }

    protected function sanitizeAddress(array $address)
    {
        $index = 0;
        $filteredAddress = [];
        $currentKey = 'A';

        $maxNumberOfAddressPoints = DefaultSettings::getSetting(
            DefaultSettings::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS,
            0
        );

        foreach($address as $key => $value){
            if($index >= $maxNumberOfAddressPoints){
                break;
            }

            if($value['lat'] && $value['lon']){
                $filteredAddress[$currentKey] = $address[$key];
                $currentKey++;
                $index++;
            }
        }

        return $filteredAddress;
    }

    protected function determineParkingAndAddressCoords(OrderRecord $order)
    {
        $result = [];
        foreach($order->address as $key => $value){
            if(empty($value['lat']) || empty($value['lon'])){
                $coords = app()->geocoder->findCoordsByAddress(implode(', ', [
                    $value['city'],
                    $value['street'],
                    $value['house'],
                ]));
                $value['lat'] = $coords['lat'];
                $value['lon'] = $coords['lon'];
            }

            if(empty($value['parking_id'])){
                $parkingInfo = app()->routeAnalyzer->getParkingByCoords(
                    $order->tenant_id,
                    $order->city_id,
                    $value['lat'],
                    $value['lon'],
                    false
                );

                if(empty($parkingInfo['error'])){
                    $value['parking_id'] = (int)$parkingInfo['inside'];
                    $value['parking'] = $this->getParkingNameById($value['parking_id']);

                    if($key == 'A'){
                        $order->parking_id = $value['parking_id'];
                    }
                }
            }elseif($key == 'A'){
                $order->parking_id = $value['parking_id'];
            }
            $result[$key] = $value;
        }
        return $result;
    }

    protected function getParkingNameById($parkingId)
    {
        return Parking::find()
            ->where(['parking_id' => $parkingId])
            ->select('name')
            ->scalar();
    }
}
