<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService\tests;

use app\components\OrderError;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\balance\Account;
use app\modules\v1\models\client\ClientHasCompany;
use app\modules\v1\models\OrderRecord;

class CorpBalanceTest implements PaymentTestInterface
{
    public function runTest(OrderRecord $order)
    {
        /** @var ClientHasCompany $company */
        $company = ClientHasCompany::find()
            ->where([
                'client_id'                                   => $order->client_id,
                ClientHasCompany::tableName() . '.company_id' => $order->company_id,
            ])
            ->joinWith('company')
            ->one();

        if ($company && $company->company && $company->company->block == 0) {
            /** @var Account $corpAccount */
            $corpAccount = Account::findOne([
                'owner_id'    => $company->company_id,
                'tenant_id'   => $order->tenant_id,
                'acc_kind_id' => Account::COMPANY_KIND,
                'currency_id' => $order->currency_id,
            ]);
            if (!$corpAccount) {
                throw new UserException(OrderError::BAD_PAY_TYPE);
            } else {
                if ($order->predv_price > $corpAccount->balance + $company->company->credits) {
                    throw new UserException(OrderError::NO_MONEY);
                }
            }
        } else {
            throw new UserException(OrderError::BAD_PAY_TYPE);
        }
    }
}
