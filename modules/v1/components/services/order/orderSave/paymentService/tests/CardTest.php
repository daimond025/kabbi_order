<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService\tests;

use app\components\Card;
use app\components\OrderError;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\OrderRecord;

class CardTest implements PaymentTestInterface
{
    public function runTest(OrderRecord $order)
    {
        /* @var $card Card */
        $card = \Yii::$app->card;


        try {
            $isValid = $card->isValid(
                $order->tenant_id,
                $order->city_id,
                $order->client_id,
                $order->pan
            );

        } catch (\Exception $ex) {
            \Yii::error($ex->getCode() . "\n" . $ex->getMessage(), 'bank-card');
            throw new UserException(OrderError::INTERNAL_ERROR);
        }
        if (!$isValid) {
            throw new UserException(OrderError::INVALID_PAN);
        }
    }
}
