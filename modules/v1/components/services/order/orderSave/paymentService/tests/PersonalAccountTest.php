<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService\tests;

use app\components\OrderError;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\balance\Account;
use app\modules\v1\models\OrderRecord;

class PersonalAccountTest implements PaymentTestInterface
{
    public function runTest(OrderRecord $order)
    {
        $account = Account::findOne([
            'owner_id'    => $order->client_id,
            'tenant_id'   => $order->tenant_id,
            'acc_kind_id' => Account::CLIENT_KIND,
            'currency_id' => $order->currency_id,
        ]);
        if (!$account) {
            throw new UserException(OrderError::BAD_PAY_TYPE);
        }
        if ($order->predv_price > $account->balance) {
            throw new UserException(OrderError::NO_MONEY);
        }
    }
}
