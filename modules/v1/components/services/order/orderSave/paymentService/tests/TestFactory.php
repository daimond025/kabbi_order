<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService\tests;

use app\components\OrderError;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\OrderRecord;

class TestFactory
{
    /**
     * @param OrderRecord $order
     *
     * @return PaymentTestInterface
     * @throws UserException
     */
    public function getTest(OrderRecord $order)
    {

        switch ($order->payment) {
            case OrderRecord::PAYMENT_CORP:
                return new CorpBalanceTest();
            case OrderRecord::PAYMENT_PERSONAL_ACCOUNT:
                return new PersonalAccountTest();
            case OrderRecord::PAYMENT_CARD:
                return new CardTest();
            case OrderRecord::PAYMENT_CASH:
                return new CashTest();
            default:
                throw new UserException(OrderError::BAD_PAY_TYPE);
        }
    }
}
