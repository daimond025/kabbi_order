<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService\tests;

use app\modules\v1\models\OrderRecord;

interface PaymentTestInterface
{
    public function runTest(OrderRecord $order);
}
