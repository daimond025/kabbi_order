<?php

namespace app\modules\v1\components\services\order\orderSave\paymentService;

use app\modules\v1\components\services\order\orderSave\paymentService\tests\TestFactory;
use app\modules\v1\models\OrderRecord;

class PaymentService
{

    /** @var TestFactory */
    protected $testFactory;

    public function __construct(
        TestFactory $testFactory
    ) {
        $this->testFactory = $testFactory;
    }

    public function testPaymentByClient(OrderRecord $order)
    {
        $test = $this->testFactory->getTest($order);
        $test->runTest($order);
    }
}
