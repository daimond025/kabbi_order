<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\components\OrderError;
use app\components\services\error\exception\UserRequestException;
use app\modules\v1\components\services\client\ClientService;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\components\services\order\orderSave\scenarios\postProcessing\AbstractPostProcessing;
use app\modules\v1\components\services\order\orderSave\scenarios\postProcessing\PostProcessingFactory;
use app\modules\v1\components\services\order\orderSave\scenarios\preProcessing\AbstractPreProcessing;
use app\modules\v1\components\services\order\orderSave\scenarios\preProcessing\PreProcessingFactory;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\services\order\PassengerService;
use yii\base\ErrorException;

class CreateOrderService
{

    protected $clientService;
    protected $passengerService;

    public function __construct(
        ClientService $clientService,
        PassengerService $passengerService
    ) {
        $this->clientService = $clientService;
        $this->passengerService = $passengerService;
    }

    /**
     * @param OrderRecord $order
     * @return OrderRecord
     * @throws UserRequestException
     * @throws \yii\db\Exception
     */
    public function createOrder(OrderRecord $order)
    {
        if ($order->validate() && $order->isNewRecord) {
            $order->client_id = $this->clientService->getClientIdByOrder($order);
            $this->passengerService->preparePassenger($order);


            /** @var AbstractPreProcessing $dataPreProcessingService */
            $dataPreProcessingService = PreProcessingFactory::getPreProcessing($order);

            /** @var AbstractPostProcessing $dataPostProcessingService */
            $dataPostProcessingService = PostProcessingFactory::getPostProcessing($order);


            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $dataPreProcessingService->run($order);
                if (!$order->trySaveOrder()) {
                    throw new ErrorException(
                        'Error save order. Post: ' . json_encode(
                            $order->getAttributes(),
                            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
                        )
                    );
                }

                $dataPostProcessingService->run($order);


            } catch (UserException $e) {
                $transaction->rollBack();
                \Yii::error(json_encode($e->getMessage()));
                throw new UserRequestException($e->getMessage());

            } catch (\Exception $e) {
                $transaction->rollBack();
                \Yii::error($e->getMessage());
                throw $e;
            }

            $transaction->commit();

            $dataPostProcessingService->runAfterTransaction($order);

        } else {
            throw new UserRequestException($order->getErrors(), 0, null, $order);
        }

        return $order;
    }
}
