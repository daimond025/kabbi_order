<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\models\ClientAddressHistory;
use app\models\DefaultSettings;
use app\models\Parking;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\OrderRecord;

class AddressService
{

    public function saveAddressToHistoryClient(OrderRecord $order)
    {
        $order->prepareAddress();

        foreach ($order->address as $address) {
            $this->updateOrCreateClientAddressHistoryModel($address, $order->client_id, $order->city_id);
        }
    }

    protected function updateOrCreateClientAddressHistoryModel($address, $clientId, $cityId)
    {
        if (!isset($address['city'])
            || !isset($address['house'])
            || !isset($address['street'])
        ) {
            return true;
        }

        $model = ClientAddressHistory::findOne([
            'client_id' => $clientId,
            'city_id'   => $cityId,
            'lat'       => $address['lat'],
            'lon'       => $address['lon'],
        ]);

        if ($model) {
            $model->attributes = [
                'client_id' => $clientId,
                'city_id'   => $cityId,
                'city'      => !empty($address['code_n']) ? $address['city_n'] : $address['city'],
                'street'    => !empty($address['code_n']) ? $address['street_n'] : $address['street'],
                'house'     => !empty($address['code_n']) ? $address['house_n'] : $address['house'],
                'lat'       => $address['lat'],
                'lon'       => $address['lon'],
                'placeId'       => !empty($address['placeId']) ?  $address['placeId'] : '',
            ];

            if (!$model->updateTime()) {
                \Yii::error('Error update time order in history.');

                return false;
            }

            return true;
        } else {
            $model             = new ClientAddressHistory();
            $model->attributes = [
                'client_id' => $clientId,
                'city_id'   => $cityId,
                'city'      => !empty($address['code_n']) ? $address['city_n'] : $address['city'],
                'street'    => !empty($address['code_n']) ? $address['street_n'] : $address['street'],
                'house'     => !empty($address['code_n']) ? $address['house_n'] : $address['house'],
                'lat'       => $address['lat'],
                'lon'       => $address['lon'],
                'placeId'       => !empty($address['placeId']) ?  $address['placeId'] : '',
            ];


        }

        if (!$model->save()) {
            \Yii::error('Error save address in history.');

            return false;
        }

        return true;
    }
}
