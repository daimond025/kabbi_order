<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\modules\v1\models\OrderChangeRuleManager;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;

/**
 * Class ActionManager
 * @package frontend\modules
 */
class ActionManager
{

    protected $order;
    protected $orderChangeRuleManager;

    const ACTION_UPDATE_ORDER = 'UPDATE_ORDER';
    const ACTION_COPY_ORDER = 'COPY_ORDER';
    const ACTION_STOP_OFFER = 'STOP_OFFER_ORDER';
    const ACTION_NEW_ORDER = 'NEW_ORDER';
    const ACTION_FREE_ORDER = 'FREE_ORDER';
    const ACTION_FREEZE_ORDER = 'FREEZE_ORDER';
    const ACTION_ASSIGN_ORDER = 'ASSIGN_ORDER';
    const ACTION_COMPLETE_PAY_ORDER = 'COMPLETE_PAY_ORDER';
    const ACTION_COMPLETE_NO_PAY_ORDER = 'COMPLETE_NO_PAY_ORDER';
    const ACTION_REJECT_ORDER = 'REJECT_ORDER';
    const ACTION_SEND_TO_EXCHANGE = 'SEND_TO_EXCHANGE';
    const ACTION_EXECUTING = 'EXECUTING_ORDER';
    const ACTION_NEW_ORDER_CLIENT = 'NEW_ORDER_CLIENT';

    public function __construct(
        OrderRecord $order,
        OrderChangeRuleManager $orderChangeRuleManager
    ) {
        $this->order = $order;
        $this->orderChangeRuleManager = $orderChangeRuleManager;
    }

    /**
     * Getting available actions
     *
     * @param int $statusId
     *
     * @return array
     */
    public function getAvailableActions($statusId)
    {
        // CREATE ORDER
        if ($statusId === null) {
            return [
                self::ACTION_NEW_ORDER,
                self::ACTION_FREE_ORDER,
                self::ACTION_FREEZE_ORDER,
            ];
        }

        switch ($statusId) {
            // MANUAL GROUP
            case OrderStatus::STATUS_MANUAL_MODE:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_NEW_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_ASSIGN_ORDER,
                    self::ACTION_REJECT_ORDER,

                ];

            // NEW GROUP
            case OrderStatus::STATUS_NEW:
            case OrderStatus::STATUS_NOPARKING:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_STOP_OFFER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                ];

            // OFFER ORDER GROUP
            case OrderStatus::STATUS_OFFER_ORDER:
            case OrderStatus::STATUS_WORKER_REFUSED:
            case OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_STOP_OFFER,
                ];

            // FREE GROUP
            case OrderStatus::STATUS_FREE:
            case OrderStatus::STATUS_OVERDUE:
            case OrderStatus::STATUS_REFUSED_ORDER_ASSIGN:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_NEW_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_SEND_TO_EXCHANGE,
                    self::ACTION_ASSIGN_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // NEW PRE GROUP
            case OrderStatus::STATUS_PRE:
            case OrderStatus::STATUS_PRE_NOPARKING:
            case OrderStatus::STATUS_PRE_REFUSE_WORKER:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_ASSIGN_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // ASSIGNED PRE GROUP
            case OrderStatus::STATUS_PRE_GET_WORKER:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD:
            case OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];
            case OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION:
                return [
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // ASSIGNED GROUP
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // CAR ASSIGNED GROUP
            case OrderStatus::STATUS_GET_WORKER:
            case OrderStatus::STATUS_WORKER_LATE:
            case OrderStatus::STATUS_EXECUTION_PRE:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // CAR AT PLACE GROUP
            case OrderStatus::STATUS_NONE_FREE_AWAITING:
            case OrderStatus::STATUS_FREE_AWAITING:
            case OrderStatus::STATUS_CLIENT_IS_NOT_COMING_OUT:
            case OrderStatus::STATUS_WORKER_WAITING:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // EXECUTING GROUP
            case OrderStatus::STATUS_EXECUTING:
            case OrderStatus::STATUS_PAYMENT_CONFIRM:
            case OrderStatus::STATUS_WAITING_FOR_PAYMENT:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            default:
                return [
                    self::ACTION_COPY_ORDER,
                ];
        }
    }

    /**
     * Getting available attributes
     *
     * @param int $statusId
     *
     * @return array
     */
    public function getAvailableAttributes($statusId)
    {
        $attributes = $this->getAttributeInformation($statusId);

        $result = [];
        foreach ($attributes as $key => $value) {
            if ($value) {
                $result[] = $key;
            }
        }

        return $result;
    }

    /**
     * Applying action to order
     *
     * @param OrderRecord $order
     */
    public function applyActionToOrder(OrderRecord $order)
    {
        switch ($order->orderAction) {
            case self::ACTION_NEW_ORDER:
                $order->status_id = OrderStatus::STATUS_NEW;
                break;
            case self::ACTION_FREE_ORDER:
                $order->status_id = OrderStatus::STATUS_FREE;
                break;
            case self::ACTION_FREEZE_ORDER:
                $order->status_id = OrderStatus::STATUS_MANUAL_MODE;
                break;
            case self::ACTION_ASSIGN_ORDER:
                $order->status_id = (int)$order->deny_refuse_order === 1
                    ? OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD
                    : OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                break;
            case self::ACTION_COMPLETE_PAY_ORDER:
                $order->status_id = OrderStatus::STATUS_COMPLETED_PAID;
                break;
            case self::ACTION_COMPLETE_NO_PAY_ORDER:
                $order->status_id = OrderStatus::STATUS_COMPLETED_NOT_PAID;
                break;
            case self::ACTION_REJECT_ORDER:
                $order->status_id = $order->status_reject;
                break;
            case self::ACTION_EXECUTING:
                $order->status_id = OrderStatus::STATUS_EXECUTING;
        }
    }

    /**
     * Getting attribute information
     *
     * @param int $statusId
     *
     * @return array
     */
    private function getAttributeInformation($statusId)
    {
        return $this->orderChangeRuleManager->getAvailableAttributes(
            $statusId,
            array_keys($this->order->getAttributes())
        );
    }
}
