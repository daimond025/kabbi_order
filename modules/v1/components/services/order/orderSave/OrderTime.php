<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\components\OrderError;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\OrderStatus;
use app\modules\v1\models\workers\TimeHelper;

class OrderTime
{
    /** @var OrderRecord */
    protected $order;

    public function __construct(OrderRecord $order)
    {
        $this->order = $order;
    }

    public function getOrderTime()
    {
        if ($orderTime = $this->getOrderTimeIfHasOrderTime()) {
            return $orderTime;
        }

        if ($orderTime = $this->getOrderTimeIfHasOrderDate()) {
            return $orderTime;
        }

        if ($orderTime = $this->getOrderTimeWithPickUp()) {
            return $orderTime;
        }

        return TimeHelper::getCurrentTimeInCity($this->order->city_id);
    }

    public function isPreOrder()
    {
        $preOrderTime = $this->order->settings->getPreOrderTimeInSecond();

        if (empty($preOrderTime)) {
            $preOrderTime = OrderRecord::PRE_ODRER_TIME;
        }

        $now = TimeHelper::getCurrentTimeInCity($this->order->city_id);

        return $this->getOrderTime() - $now >= $preOrderTime;
    }

    protected function isInvalidOrderTime()
    {
        return in_array($this->order->status->status_group, [
                OrderStatus::STATUS_GROUP_NEW,
                OrderStatus::STATUS_GROUP_PRE_ORDER,
            ], false)
            && $this->order->status_id != OrderStatus::STATUS_OVERDUE
            && $this->order->status_id != OrderStatus::STATUS_MANUAL_MODE
            && $this->order->order_time < TimeHelper::getCurrentTimeInCity($this->order->city_id);
    }

    protected function getOrderTimeIfHasOrderTime()
    {
        if ($this->order->order_time) {
            if ($this->isInvalidOrderTime()) {
                return $this->getOrderTimeWithPickUpOrThrowError();
            }

            return $this->order->order_time;
        }

        return null;
    }

    protected function getOrderTimeIfHasOrderDate()
    {
        if (empty($this->order->order_now) && $this->isHasOrderDate()) {
            list($year, $month, $day) = explode('-', $this->order->order_date);

            $this->order->order_time = mktime(
                $this->order->order_hours,
                $this->order->order_minutes,
                $this->order->order_seconds,
                $month,
                $day,
                $year
            );

            if ($this->isInvalidOrderTime()) {
                return $this->getOrderTimeWithPickUpOrThrowError();
            }

            return $this->order->order_time;
        }

        return null;
    }

    protected function getOrderTimeWithPickUpOrThrowError()
    {
        if ($orderTime = $this->getOrderTimeWithPickUp()) {
            return $orderTime;
        }

        throw new UserException(OrderError::ORDER_TIME_INCORRECT);
    }

    protected function getOrderTimeWithPickUp()
    {
        if (OrderStatus::isOrderWithPickUpTime($this->order->status_id)) {
            return TimeHelper::getCurrentTimeInCity($this->order->city_id)
                + $this->order->settings->getPickUp();
        }

        return null;
    }

    protected function isHasOrderDate()
    {
        return $this->order->order_date
            && $this->order->order_hours
            && $this->order->order_minutes;
    }

}
