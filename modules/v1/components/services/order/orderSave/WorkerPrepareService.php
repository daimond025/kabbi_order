<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\components\OrderError;
use app\models\Worker;
use app\modules\v1\components\services\order\orderSave\exceptions\UserException;
use app\modules\v1\models\OrderRecord;
use app\modules\v1\models\workers\WorkerRepository;

class WorkerPrepareService
{
    protected $workerService;
    protected $workerShiftService;

    public function __construct(
        WorkerRepository $workerService,
        WorkerShiftService $workerShiftService
    ) {
        $this->workerService = $workerService;
        $this->workerShiftService = $workerShiftService;
    }

    public function prepareWorker(OrderRecord $order)
    {
        $worker = $this->workerService
            ->getActiveWorker($order->tenant_id, $order->worker->callsign);

        $this->testWorkerOnShift($worker);
        $this->testWorkerBlocked($worker);
        $this->testWorkerOnOrder($worker);
        $this->prepareIfWorkerOnBreak($worker);
    }

    protected function testWorkerOnShift($worker)
    {
        if (!$worker) {
            throw new UserException(OrderError::WORKER_SHIFT_IS_CLOSED);
        }
    }

    protected function testWorkerBlocked($worker)
    {
        if (Worker::isWorkerBlocked($worker)) {
            throw new UserException(OrderError::WORKER_BLOCKED);
        }
    }

    protected function testWorkerOnOrder($worker)
    {
        if (Worker::isWorkerOnOrder($worker)) {
            throw new UserException(OrderError::WORKER_ALREADY_HAS_ACTIVE_ORDER);
        }
    }

    protected function prepareIfWorkerOnBreak($worker)
    {
        if (Worker::isWorkerOnBreak($worker)) {
            $worker['worker']['status']       = Worker::STATUS_FREE;
            $worker['worker']['break_reason'] = null;
            $this->workerService->saveActiveWorker(
                $worker['worker']['tenant_id'],
                $worker['worker']['callsign'],
                $worker
            );

            $this->workerShiftService->setPauseData(
                $worker['worker']['tenant_id'],
                $worker['worker']['tenant_login'],
                $worker['worker']['callsign'],
                null,
                Worker::ACTION_WORK
            );
        }
    }
}
