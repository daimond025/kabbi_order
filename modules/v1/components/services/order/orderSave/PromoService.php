<?php

namespace app\modules\v1\components\services\order\orderSave;

use app\models\TaxiTariff;
use app\modules\promocode\components\services\PromoCodeService;
use app\modules\promocode\models\forms\FindSuitablePromoCodeForm;
use app\modules\v1\models\OrderRecord;

class PromoService
{
    protected $promoCodeService;

    public function __construct(
        PromoCodeService $promoCodeService
    ) {
        $this->promoCodeService = $promoCodeService;
    }

    public function findSuitablePromoCodeId(OrderRecord $order)
    {
        $form = new FindSuitablePromoCodeForm([
            'tenant_id'    => $order->tenant_id,
            'client_id'    => $order->client_id,
            'order_time'   => $order->order_time,
            'city_id'      => $order->city_id,
            'position_id'  => $order->position_id,
            'car_class_id' => $this->getCarClassId($order),
        ]);


        $suitablePromoCode = $this->promoCodeService->getPromoCodeForCriteria($form);

        if ($suitablePromoCode) {
            return $suitablePromoCode->code_id;
        }

        return null;
    }

    protected function getCarClassId(OrderRecord $order)
    {
        /** @var TaxiTariff $tariff */
        $tariff = TaxiTariff::findOne(['tariff_id' => $order->tariff_id]);

        return $tariff->class->class_id;
    }
}
