<?php

namespace app\modules\v1\components\services\order\orderUpdate;


use app\components\repositories\OrderRepository;
use app\components\WorkerErrorCode;
use app\models\DefaultSettings;
use app\models\TenantSetting;
use app\modules\v1\components\services\order\orderUpdate\exceptions\OrderUpdateException;
use app\modules\v1\delegations\address\Courier;
use app\modules\v1\extensions\codeGenerator\CodeGenerator;
use app\modules\v1\models\workers\order\Order;
use yii\helpers\ArrayHelper;

class OrderUpdateAddress extends OrderUpdateAbstract
{

    const FILED_CHANGE = 'address';

    /** @var OrderRepository */
    protected $orderRepository;
    /** @var CodeGenerator */
    protected $codeGenerator;

    public function __construct(
        OrderInfo $orderInfo,
        Order $orderDataMysql,
        array $orderDataRedis
    ) {
        $this->orderRepository = \Yii::createObject(OrderRepository::class);
        $this->codeGenerator = \Yii::createObject(CodeGenerator::class, [range(0, 9)]);

        parent::__construct($orderInfo, $orderDataMysql, $orderDataRedis);
    }


    public function runUpdate()
    {

        $orderDataRedis = null;
        $orderDataMysql = null;
        $orderDataMysql = $this->orderDataMysql;
        $orderDataRedis =& $this->orderDataRedis;


        if (!array_key_exists(static::FILED_CHANGE, $this->orderInfo->getData())) {
            return false;
        }

        $address = $this->orderInfo->getData()[static::FILED_CHANGE];

        $taxiRouteAnalyzer = \Yii::$app->routeAnalyzer;

        $cityId       = $orderDataMysql->city_id;
        $orderDate    = $orderDataMysql->order_date;
        $tariffId     = $orderDataMysql->tariff_id;
        $geocoderType = TenantSetting::getSettingValue(
            $this->orderInfo->getTenantId(), TenantSetting::GEOCODER_TYPE, $cityId);
        $geocoderType = empty($geocoderType) ? 'ru' : $geocoderType;

        $oldAddress                         = unserialize($orderDataMysql->address);
        $addressArray                       = json_decode($address, true);
        $addressArray                       = Order::filterAddress($addressArray);
        $addressArray[static::FILED_CHANGE] = array_merge([$oldAddress['A']], $addressArray[static::FILED_CHANGE]);
        $this->prepareCourierFields(
            $addressArray,
            $orderDataMysql->tenant_id,
            $orderDataMysql->order_id,
            $oldAddress
        );

        $newAddressArray = [];
        $addressData     = [];

        $i          = 0;
        $currentPoint = 'A';

        $maxNumberOfAddressPoints = DefaultSettings::getSetting(
            DefaultSettings::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS, 0);

        foreach ($addressArray['address'] as $addressRaw) {
            if ($i > $maxNumberOfAddressPoints - 1) {
                break;
            }

            $addressData['city_id'] = isset($addressRaw['city_id']) ? $addressRaw['city_id'] : null;
            $addressData['city']    = isset($addressRaw['city']) ? $addressRaw['city'] : null;
            $addressData['street']  = isset($addressRaw['street']) ? $addressRaw['street'] : null;
            $addressData['housing'] = isset($addressRaw['housing']) ? $addressRaw['housing'] : null;
            $addressData['house']   = isset($addressRaw['house']) ? $addressRaw['house'] : null;
            $addressData['porch']   = isset($addressRaw['porch']) ? $addressRaw['porch'] : null;
            $addressData['apt']     = isset($addressRaw['apt']) ? $addressRaw['apt'] : null;
            $addressData['lat']     = isset($addressRaw['lat']) ? $addressRaw['lat'] : null;
            $addressData['lon']     = isset($addressRaw['lon']) ? $addressRaw['lon'] : null;


            $addressData[Courier::FIELD_PHONE] =
                ArrayHelper::getValue($addressRaw, Courier::FIELD_PHONE);
            $addressData[Courier::FIELD_COMMENT] =
                ArrayHelper::getValue($addressRaw, Courier::FIELD_COMMENT);
            $addressData[Courier::FIELD_CONFIRMATION_CODE] =
                ArrayHelper::getValue($addressRaw, Courier::FIELD_CONFIRMATION_CODE);


            $parkingInfo               = $taxiRouteAnalyzer->getParkingByCoords($this->orderInfo->getTenantId(),
                $cityId,
                $addressData['lat'], $addressData['lon'], false);
            $addressData['parking']    = isset($parkingInfo['parking_name']) ? $parkingInfo['parking_name'] : null;
            $addressData['parking_id'] = isset($parkingInfo['inside']) ? $parkingInfo['inside'] : null;

            $newAddressArray[$currentPoint] = $addressData;
            $currentPoint++;
            $i++;
        }

        $newAddress                           = serialize($newAddressArray);
        $orderDataRedis[static::FILED_CHANGE] = $newAddress;
        $orderDataMysql->address              = $newAddress;

        $addOptions = [];
        if (isset($orderDataRedis['options']) && is_array($orderDataRedis['options'])) {
            $addOptions = ArrayHelper::getColumn($orderDataRedis['options'], 'option_id');
        }

        $routeCostInfo = $taxiRouteAnalyzer->analyzeRoute(
            $this->orderInfo->getTenantId(), $cityId, $newAddressArray, $addOptions, $tariffId, $orderDate);

        if (empty($routeCostInfo)) {
            \Yii::error("Ошибка анализатора маршрута, невозможно отредактировать заказ (метод update_order). Ответ: " . $routeCostInfo);
            throw new OrderUpdateException(WorkerErrorCode::INTERNAL_ERROR);
        }

        $costData = [];
        if (!empty($routeCostInfo)) {
            $result                           = (array)$routeCostInfo;
            $costData['additionals_cost']     = isset($result['additionalCost']) ? (string)$result['additionalCost'] : '0';
            $costData['summary_time']         = isset($result['summaryTime']) ? (string)$result['summaryTime'] : '0';
            $costData['summary_distance']     = isset($result['summaryDistance']) ? (string)$result['summaryDistance'] : '0';
            $costData['summary_cost']         = isset($result['summaryCost']) ? (string)$result['summaryCost'] : '0';
            $costData['city_time']            = isset($result['cityTime']) ? (string)$result['cityTime'] : null;
            $costData['city_distance']        = isset($result['cityDistance']) ? (string)$result['cityDistance'] : null;
            $costData['city_cost']            = isset($result['cityCost']) ? (string)$result['cityCost'] : null;
            $costData['out_city_time']        = isset($result['outCityTime']) ? (string)$result['outCityTime'] : null;
            $costData['out_city_distance']    = isset($result['outCityDistance']) ? (string)$result['outCityDistance'] : null;
            $costData['out_city_cost']        = isset($result['outCityCost']) ? (string)$result['outCityCost'] : null;
            $costData['is_fix']               = empty($result['isFix']) ? 0 : 1;
            $costData['start_point_location'] = isset($result['startPointLocation']) ? (string)$result['startPointLocation'] : null;
            $costData['tariffInfo']           = isset($result['tariffInfo']) ? $result['tariffInfo'] : null;
        }

        $orderDataMysql->is_fix         = $costData['is_fix'] === 1 ? 1: 0;
        $orderDataMysql->predv_price    = $costData['summary_cost'];
        $orderDataMysql->predv_time     = $costData['summary_time'];
        $orderDataMysql->predv_distance = $costData['summary_distance'];

        $orderDataRedis['costData'] = $costData;

        $this->saveChanges();

        return true;
    }

    public function prepareCourierFields(&$addresses, $tenantId, $orderId, $oldAddress)
    {
        $settingCurrentOrder = $this->orderRepository->getSettingsActiveOrderByData($tenantId, $orderId);

        $oldAddress = $this->redefinitionKeyOldAddress($oldAddress);

        $usedCodes = $this->getConfirmationCodesFromOrder($oldAddress);
        $this->codeGenerator->setUsedCodes($usedCodes);

        foreach ($addresses['address'] as $point => $address) {
            if ($settingCurrentOrder->orderFormAddPointComment) {
                $addresses['address'][$point][Courier::FIELD_COMMENT] = $this->clipString(
                    Courier::MAX_LENGTH_COMMENT,
                    ArrayHelper::getValue($address, Courier::FIELD_COMMENT)
                );
            }

            if ($settingCurrentOrder->orderFormAddPointPhone) {
                $addresses['address'][$point][Courier::FIELD_PHONE] = $this->clipString(
                    Courier::MAX_LENGTH_PHONE,
                    ArrayHelper::getValue($address, Courier::FIELD_PHONE)
                );
            }

            if ($settingCurrentOrder->requirePointConfirmationCode) {
                if ($code = ArrayHelper::getValue($oldAddress[$point], Courier::FIELD_CONFIRMATION_CODE)) {
                    $addresses['address'][$point][Courier::FIELD_CONFIRMATION_CODE] = $code;
                } else {
                    $addresses['address'][$point][Courier::FIELD_CONFIRMATION_CODE] =
                        $this->codeGenerator->getUniqueCode();
                }
            }
        }
    }

    protected function redefinitionKeyOldAddress($oldAddresses)
    {
        $addresses = [];
        foreach ($oldAddresses as $oldAddress) {
            $addresses[] = $oldAddress;
        }

        return $addresses;
    }

    /**
     * @param $addresses
     *
     * @return array
     */
    protected function getConfirmationCodesFromOrder($addresses)
    {

        $usedCodes = [];

        foreach ($addresses as $address) {
            if ($code = ArrayHelper::getValue($address, Courier::FIELD_CONFIRMATION_CODE)) {
                $usedCodes[] = $code;
            }
        }

        return $usedCodes;
    }

    /**
     * @param int    $maxLength
     * @param string $string
     *
     * @return string
     */
    protected function clipString($maxLength, $string)
    {
        return mb_substr($string, 0, $maxLength, 'utf-8');
    }
}
