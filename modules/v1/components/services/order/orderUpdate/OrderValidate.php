<?php

namespace app\modules\v1\components\services\order\orderUpdate;


use app\components\WorkerErrorCode;
use app\modules\v1\components\services\order\orderUpdate\exceptions\OrderUpdateException;
use app\modules\v1\models\OrderStatus;
use app\modules\v1\models\workers\order\Order;

class OrderValidate
{

    protected $orderInfo;
    protected $orderDataMysql;
    protected $orderDataRedis;

    public function __construct(OrderInfo $orderInfo, Order $orderDataMysql, array &$orderDataRedis)
    {
        $this->orderInfo = $orderInfo;
        $this->orderDataMysql = $orderDataMysql;
        $this->orderDataRedis =& $orderDataRedis;
    }

    public function runValidate()
    {
        $orderDataRedis = null;
        $orderDataMysql = null;
        $orderDataMysql = $this->orderDataMysql;
        $orderDataRedis =& $this->orderDataRedis;

        if (empty($orderDataRedis) || empty($orderDataMysql)) {
            throw new OrderUpdateException(WorkerErrorCode::EMPTY_DATA_IN_DATABASE);
        }

        if (isset($orderDataRedis['worker']['callsign'])
            && $orderDataRedis['worker']['callsign'] != $this->orderInfo->getWorkerCallsign()
        ) {
            throw new OrderUpdateException(WorkerErrorCode::ORDER_IS_BUSY);
        }

        $statusGroup = isset($orderDataRedis['status']['status_group'])
            ? $orderDataRedis['status']['status_group'] : null;
        if (!in_array($statusGroup, [
            OrderStatus::STATUS_GROUP_CAR_ASSIGNED,
            OrderStatus::STATUS_GROUP_CAR_AT_PLACE,
            OrderStatus::STATUS_GROUP_EXECUTING,
        ], false)
        ) {
            throw new OrderUpdateException(WorkerErrorCode::FORBIDDEN_ACTION);
        }


        return true;
    }


}