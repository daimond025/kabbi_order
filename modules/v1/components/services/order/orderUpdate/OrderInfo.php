<?php

namespace app\modules\v1\components\services\order\orderUpdate;


class OrderInfo
{

    protected $orderId;
    protected $tenantId;
    protected $data;
    protected $lang;

    public function __construct(
        $orderId,
        $tenantId,
        $data,
        $lang
    )
    {
        $this->orderId = $orderId;
        $this->tenantId = $tenantId;
        $this->data = $data;
        $this->lang = $lang;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return mixed
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    public function getWorkerCallsign()
    {
        return (int)$this->data['worker_login'];
    }

}