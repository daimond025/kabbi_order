<?php

namespace app\modules\v1\components\services\order\orderUpdate;


use app\components\WorkerErrorCode;
use app\modules\v1\components\services\order\orderUpdate\exceptions\OrderUpdateException;
use app\modules\v1\models\OrderStatus;

class OrderUpdatePredvPrice extends OrderUpdateAbstract
{

    const FILED_CHANGE = 'predv_price';


    public function runUpdate()
    {

        $orderDataRedis = null;
        $orderDataMysql = null;
        $orderDataMysql = $this->orderDataMysql;
        $orderDataRedis =& $this->orderDataRedis;

        if (!array_key_exists(static::FILED_CHANGE, $this->orderInfo->getData())) {
            return false;
        }


        $newPredvPrice = $this->orderInfo->getData()[static::FILED_CHANGE];

        if($orderDataMysql->device == OrderStatus::TYPE_HOSPITAL){
            throw new OrderUpdateException(WorkerErrorCode::FORBIDDEN_ACTION);

        }

        $orderDataRedis[static::FILED_CHANGE] = $newPredvPrice;
        $orderDataRedis['is_fix'] = 1;
        $orderDataRedis['costData']['summary_cost'] = $newPredvPrice;
        $orderDataRedis['costData']['is_fix'] = 1;
        $orderDataMysql->predv_price = $newPredvPrice;
        $orderDataMysql->is_fix = "1";

        $this->saveChanges();

        return true;

    }


}