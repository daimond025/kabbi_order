<?php

namespace app\modules\v1\components\services\order\orderUpdate;


use app\models\ActiveOrderRepository;
use app\models\OrderChangeData;
use app\modules\v1\models\workers\order\Order;
use yii\base\Exception;

abstract class OrderUpdateAbstract
{

    const FILED_CHANGE = '';

    protected $orderInfo;
    protected $orderDataMysql;
    protected $orderDataRedis;
    protected $activeOrderRepository;


    public function __construct(OrderInfo $orderInfo, Order $orderDataMysql, array &$orderDataRedis)
    {
        $this->orderInfo = $orderInfo;
        $this->orderDataMysql = $orderDataMysql;
        $this->orderDataRedis =& $orderDataRedis;

        $this->activeOrderRepository = new ActiveOrderRepository(['tenantId' => $orderInfo->getTenantId()]);
    }



    protected function saveChanges()
    {

        $orderDataRedis = null;
        $orderDataMysql = null;
        $orderDataMysql = $this->orderDataMysql;
        $orderDataRedis =& $this->orderDataRedis;

        if (!$orderDataMysql->save()) {
            throw new Exception(implode($orderDataMysql->getFirstErrors()));
        } else {

            $changeVal = is_array($this->orderInfo->getData()[static::FILED_CHANGE])
                ? serialize($this->orderInfo->getData()[static::FILED_CHANGE])
                : $this->orderInfo->getData()[static::FILED_CHANGE];

            $orderChange                     = new OrderChangeData();
            $orderChange->tenant_id          = $this->orderInfo->getTenantId();
            $orderChange->order_id           = $this->orderInfo->getOrderId();
            $orderChange->change_field       = static::FILED_CHANGE;
            $orderChange->change_object_id   = (string)$this->orderInfo->getOrderId();
            $orderChange->change_object_type = 'order';
            $orderChange->change_subject     = 'worker';
            $orderChange->change_val         = $changeVal;
            $orderChange->change_time        = time();

            if (!$orderChange->save()) {
                \Yii::error($orderChange->errors);
            }

            $result = $this->activeOrderRepository->set($this->orderInfo->getOrderId(), serialize($orderDataRedis));
            if ($result != 0 && $result != 1) {
                throw new Exception('An error occurred when saving the order in Redis');
            }
        }

        return true;
    }

}