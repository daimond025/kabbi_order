<?php

namespace app\modules\v1\components\services\promo;


use app\modules\promocode\models\entities\PromoCode;
use app\modules\v1\components\services\promo\exceptions\PromoServiceException;

class Setting
{

    protected $orderDataRedis;
    protected $summaryCost;

    /* @var $promoCode PromoCode */
    protected $promoCode;

    public function __construct(array $orderDataRedis, $summaryCost)
    {
        $this->orderDataRedis = $orderDataRedis;
        $this->summaryCost = $summaryCost;
    }

    public function getWorkerId()
    {
        return $this->orderDataRedis['worker_id'];
    }

    public function getPositionId()
    {
        return $this->orderDataRedis['position_id'];
    }

    public function getCarClassId()
    {
        if ($this->orderDataRedis['tariff']['class']) {
            return $this->orderDataRedis['tariff']['class']['class_id'];
        }
        return false;
    }

    public function getOrderId()
    {
        return $this->orderDataRedis['order_id'];
    }

    public function getTenantId()
    {
        return $this->orderDataRedis['tenant_id'];
    }

    public function getPromoCodeId()
    {
        return $this->orderDataRedis['promo_code_id'];
    }

    public function getSummaryCost()
    {
        return $this->summaryCost;
    }

    public function getClientId()
    {
        return $this->orderDataRedis['client_id'];
    }


    /**
     * @return PromoCode|static
     * @throws PromoServiceException
     */
    public function getPromoCode()
    {
        if (!$this->getPromoCodeId()) {
            throw new PromoServiceException('Error call function');
        }

        if (!$this->promoCode instanceof PromoCode) {
            $this->promoCode = PromoCode::findOne(['code_id' => $this->getPromoCodeId()]);
        }

        return $this->promoCode;
    }
}