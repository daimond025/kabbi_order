<?php

namespace app\modules\v1\components\services\promo;


use app\components\repositories\PositionRepository;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoBonusOperation;
use app\modules\promocode\models\entities\PromoClient;
use app\modules\promocode\models\entities\PromoCode;
use app\modules\v1\components\services\promo\exceptions\PromoServiceException;

class PromoService
{

    protected $setting;

    protected $isOperationClientBonuses = false;
    protected $isOperationWorkerBonuses = false;
    protected $isCommissionWorker = false;


    public function __construct(Setting $setting)
    {
        $this->setting = $setting;

        if ($this->isHasPromoCode()) {
            $this->isOperationWorkerBonuses = $this->isBonusWorker();
            $this->isOperationClientBonuses = $this->isBonusClient();
        }
    }

    public function startProcessingPromotionCode()
    {
        if ($this->isOperationWorkerBonuses) {
            $this->appointmentBonusesWorker();
        }

        if ($this->isOperationClientBonuses) {
            $this->appointmentBonusesClient();
        }

        $this->changeStatusPromoCode();

    }

    public function isCommissionForWorker()
    {
        if (!$this->isHasPromoCode()) {
            return false;
        }

        if ($this->setting->getPromoCode()->promo->type_id == Promo::FIELD_PROMO_TYPE_DRIVERS) {
            if ($this->setting->getPromoCode()->worker_id == $this->setting->getWorkerId()) {
                return true;
            }
        }


        return false;
    }

    public function getCommissionForWorker()
    {
        $discount = $this->setting->getPromoCode()->promo->worker_commission;

        $this->isCommissionWorker = true;

        return $this->setting->getSummaryCost() / 100 * $discount;
    }

    public function isHasPromoCode()
    {
        if (!$this->setting->getPromoCodeId()) {
            return false;
        }

        return true;
    }

    protected function appointmentBonusesWorker()
    {

        $promoBonus = $this->setting->getSummaryCost() / 100 * $this->setting->getPromoCode()->promo->worker_bonuses;

        $promoBonusOperation = new PromoBonusOperation([
            'order_id'      => $this->setting->getOrderId(),
            'worker_id'     => $this->setting->getPromoCode()->worker_id,
            'bonus_subject' => PromoBonusOperation::BONUS_SUBJECT_WORKER,
            'promo_bonus'   => $promoBonus,
        ]);

        if ($promoBonusOperation->save()) {
            return true;
        }

        if ($promoBonusOperation->hasErrors()) {
            $error = 'Has error: ' . json_encode($promoBonusOperation->getErrors());
            throw new PromoServiceException($error);
        }

        throw new PromoServiceException('Error save bonus operation');

    }

    protected function appointmentBonusesClient()
    {

        $promoBonusOperation = new PromoBonusOperation([
            'order_id'      => $this->setting->getOrderId(),
            'client_id'     => $this->setting->getClientId(),
            'bonus_subject' => PromoBonusOperation::BONUS_SUBJECT_CLIENT,
            'promo_bonus'   => $this->setting->getPromoCode()->promo->client_bonuses,
        ]);

        if ($promoBonusOperation->save()) {
            return true;
        }

        if ($promoBonusOperation->hasErrors()) {
            $error = 'Has error: ' . json_encode($promoBonusOperation->getErrors());
            throw new PromoServiceException($error);
        }

        throw new PromoServiceException('Error save bonus operation');
    }


    protected function isBonusWorker()
    {


        if ($this->isHasPromoCode()) {
            if ($this->setting->getPromoCode()->promo->type_id == Promo::FIELD_PROMO_TYPE_DRIVERS) {
                if ($this->setting->getPromoCode()->worker_id != $this->setting->getWorkerId()) {
                    return true;
                }
            }
        }

        return false;
    }


    protected function isBonusClient()
    {

        if ($this->isHasPromoCode()) {
            if ($this->setting->getPromoCode()->promo->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_BONUSES) {
                return true;
            }
        }

        return false;
    }

    public function changeStatusPromoCode()
    {

        if (!$this->isNeedChangeStatus()) {
            return false;
        }

        $promoClient = PromoClient::findOne([
            'client_id' => $this->setting->getClientId(),
            'code_id'   => $this->setting->getPromoCodeId(),
        ]);

        $promoClient->status_code_id = PromoCode::STATUS_USED;

        if ($promoClient->update(false, ['status_code_id'])) {
            return true;
        }

        if ($promoClient->hasErrors()) {
            $error = 'Has error: ' . json_encode($promoClient->getErrors());
            throw new PromoServiceException($error);
        }

        throw new PromoServiceException('Error update status code');
    }

    protected function isNeedChangeStatus()
    {
        return $this->setting->getPromoCode()->promo->action_count_id == Promo::FIELD_TYPE_ACTION_COUNT_ONCE;
    }

}