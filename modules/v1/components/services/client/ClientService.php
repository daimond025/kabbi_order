<?php

namespace app\modules\v1\components\services\client;

use app\models\Client;
use app\models\ClientPhone;
use app\modules\v1\models\dto\Name;
use app\modules\v1\models\OrderRecord;

class ClientService
{
    public function getClientIdByOrder(OrderRecord $order)
    {
        if (empty($order->client_id)) {
            /** @var Client $client */
            $client = $this->findClientByPhoneAndCreateIfNotExists(
                $order->phone,
                $order->tenant_id,
                $order->city_id,
                $order->getNameClient()
            );

            return $client->client_id;
        }

        return $order->client_id;
    }

    public function isHasClientBlackList($phone, $tenantId)
    {
        return ClientPhone::find()
            ->alias('cp')
            ->joinWith('client c')
            ->where([
                'cp.value'     => $phone,
                'c.tenant_id'  => $tenantId,
                'c.black_list' => 1,
            ])
            ->exists();
    }

    public function updateDeviceAndCityData(Client $client, $typeClient, $deviceToken, $cityId, $lang, $appId)
    {
        $typeClient = strtoupper($typeClient);


        if (in_array($typeClient, Client::CLIENT_TYPE_LIST)) {
            $client->device = $typeClient;
        }

        if (!empty($cityId)) {
            $client->city_id = $cityId;
        }

        $client->app_id       = $appId;
        $client->device_token = $deviceToken;
        $client->lang         = $lang;

        $this->deleteRepeatDeviceToken($client);

        return $client->save();
    }

    public function deleteRepeatDeviceToken($client)
    {
        if ($client->device_token !== null) {
            /** @var Client[] $clients */
            $clients = Client::find()
                ->where(['device_token' => $client->device_token])
                ->andWhere(['not', ['client_id' => $client->client_id]])
                ->all();
            foreach ($clients as $client) {
                $client->device_token = null;
                $client->update(false, ['device_token']);
            }
        }
    }

    /**
     * @param $phone
     * @param $tenantId
     * @param $cityId
     * @param $name
     *
     * @return Client
     * @throws \yii\db\Exception
     */
    public function findClientByPhoneAndCreateIfNotExists($phone, $tenantId, $cityId, Name $name = null)
    {
        $client = Client::find()
            ->joinWith('clientPhones', false)
            ->byPhone($phone)
            ->byTenantId($tenantId)
            ->one();



        if (empty($client)) {
            $transaction = app()->db->beginTransaction();
            try {
                $client              = new Client();
                $client->tenant_id   = $tenantId;
                $client->city_id     = $cityId;
                $client->phone[]     = $phone;
                $client->name        = $name ? $name->name : null;
                $client->last_name   = $name ? $name->lastName : null;
                $client->second_name = $name ? $name->secondName : null;

                $client->save();

                $clientPhone            = new ClientPhone();
                $clientPhone->client_id = $client->client_id;
                $clientPhone->value     = $phone;

                if ($clientPhone->save()) {
                    $transaction->commit();
                } else {
                    throw new \RuntimeException('Не удалось создать клиента с параметрами: phone:' . $phone .
                        '; tenant_id: ' . $tenantId . '; city_id: ' . $cityId, 'order');
                }
            } catch (\Exception $exc) {
                $transaction->rollback();
                \Yii::error($exc->getMessage());

                throw new \RuntimeException($exc->getMessage());
            }
        } else{
            $client->name        = $name ? $name->name : null;
            $client->last_name   = $name ? $name->lastName : null;
            $client->second_name = $name ? $name->secondName : null;

            if ($client->save(false)) {
            } else {
                throw new \RuntimeException('Не удалось создать клиента с параметрами: phone:' . $phone .
                    '; tenant_id: ' . $tenantId . '; city_id: ' . $cityId, 'order');
            }
        }
        return $client;
    }
}
