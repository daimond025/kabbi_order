<?php

namespace app\modules\v1\components\repositories;


class WorkerActiveOrderRepository
{

    /**
     * Get active orders ids by worker
     * @param $tenantId
     * @param $workerCallsign
     * @return array
     */
    public function getActiveOrdersByWorker($tenantId, $workerCallsign)
    {
        return \Yii::$app->redis_worker_orders->executeCommand('SMEMBERS', ["{$tenantId}_{$workerCallsign}"]);

    }


}