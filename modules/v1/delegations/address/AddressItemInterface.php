<?php

namespace app\modules\v1\delegations\address;

interface AddressItemInterface
{
    public function validate();
}
