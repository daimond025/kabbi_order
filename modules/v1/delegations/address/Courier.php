<?php

namespace app\modules\v1\delegations\address;

use app\modules\v1\extensions\codeGenerator\CodeGenerator;
use app\modules\v1\models\OrderRecord;
use yii\helpers\ArrayHelper;

class Courier implements AddressItemInterface
{

    const FIELD_CONFIRMATION_CODE = 'confirmation_code';
    const FIELD_PHONE = 'phone';
    const FIELD_COMMENT = 'comment';

    const MAX_LENGTH_PHONE = 20;
    const MAX_LENGTH_COMMENT = 2000;

    /** @var CodeGenerator */
    protected $codeGen;

    protected $order;

    protected $address;

    public function __construct(OrderRecord $order)
    {
        $this->order = $order;

        $this->codeGen = \Yii::createObject(CodeGenerator::class, [range(0, 9)]);

        if (!$this->order->isNewRecord) {
            $this->codeGen->setUsedCodes($this->getConfirmationCodesFromOrder());
        }
    }

    /**
     * @return array
     */
    protected function getConfirmationCodesFromOrder()
    {
        $this->order->prepareAddress();

        $usedCodes = [];

        foreach ($this->order->address as $address) {
            if ($code = ArrayHelper::getValue($address, self::FIELD_CONFIRMATION_CODE)) {
                $usedCodes[] = $code;
            }
        }

        return $usedCodes;
    }

    public function validate()
    {
        $this->order->prepareAddress();
        $this->address = $this->order->address;

        $numberPointAddress = 1;

        foreach ($this->order->address as $point => $address) {
            if ($this->order->isNewRecord) {
                $this->validateForSave($address, $numberPointAddress, $point);
            } else {
                $this->validateForUpdate($address, $numberPointAddress, $point);
            }

            $numberPointAddress++;
        }

        $this->order->address = $this->address;
    }

    protected function validateForSave($address, $numberPointAddress, $point)
    {
        if ($this->order->settings->getOrderFromAddPointPhone()) {
            $this->address[$point][self::FIELD_PHONE] = $this->clipString(
                self::MAX_LENGTH_PHONE,
                ArrayHelper::getValue($address, self::FIELD_PHONE)
            );

        }

        if ($this->order->settings->getOrderFromAddPointComment()) {
            $this->address[$point][self::FIELD_COMMENT] = $this->clipString(
                self::MAX_LENGTH_COMMENT,
                ArrayHelper::getValue($address, self::FIELD_COMMENT)
            );
        }

        if ($this->isMakeUniqueCodeForNewOrder($numberPointAddress)) {
            $this->address[$point][self::FIELD_CONFIRMATION_CODE] = $this->codeGen->getUniqueCode();
        }
    }

    protected function validateForUpdate($address, $numberPointAddress, $point)
    {
        $this->address[$point][self::FIELD_CONFIRMATION_CODE]
            = preg_replace("/[^0-9]/", '', $this->address[$point][self::FIELD_CONFIRMATION_CODE]);

        if ($this->order->settings->getSettingsActiveOrder()->orderFormAddPointPhone) {
            $this->address[$point][self::FIELD_PHONE] = $this->clipString(
                self::MAX_LENGTH_PHONE,
                ArrayHelper::getValue($address, self::FIELD_PHONE)
            );
        }

        if ($this->order->settings->getSettingsActiveOrder()->orderFormAddPointComment) {
            $this->address[$point][self::FIELD_COMMENT] = $this->clipString(
                self::MAX_LENGTH_COMMENT,
                ArrayHelper::getValue($address, self::FIELD_COMMENT)
            );
        }

        if ($this->isMakeUniqueCodeForActiveOrder($numberPointAddress, $address)) {
            $this->address[$point][self::FIELD_CONFIRMATION_CODE] = $this->codeGen->getUniqueCode();
        }
    }

    /**
     * @param int    $maxLength
     * @param string $string
     *
     * @return string
     */
    protected function clipString($maxLength, $string)
    {
        return mb_substr($string, 0, $maxLength, 'utf-8');
    }

    protected function isMakeUniqueCodeForActiveOrder($numberPointAddress, $address)
    {
        return $numberPointAddress > 1
            && $this->order->settings->getSettingsActiveOrder()->requirePointConfirmationCode
            && is_null(ArrayHelper::getValue($address, self::FIELD_CONFIRMATION_CODE));
    }

    protected function isMakeUniqueCodeForNewOrder($numberPointAddress)
    {
        return $numberPointAddress > 1 && $this->order->settings->getRequirePointConfirmationCode();
    }
}
