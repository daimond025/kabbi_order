<?php

namespace app\modules\v1\exceptions;

class ExceptionFormatting
{
    public static function getFormattedString($string, array $context)
    {
        $stringContext = json_encode(
            $context,
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );

        return $string . ' ' . $stringContext;
    }
}
