<?php

namespace app\modules\promocode\models\forms;


use app\models\CarClass;
use app\models\City;
use app\models\Client;
use app\models\Position;
use app\models\Tenant;
use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\components\repositories\CityRepository;
use app\modules\promocode\components\repositories\ClientRepository;
use yii\base\Model;

class FindSuitablePromoCodeForm extends Model
{
    public $tenant_id;
    public $client_id;
    public $order_time;
    public $city_id;
    public $position_id;
    public $car_class_id;

    public function rules()
    {
        return [
            [
                ['tenant_id', 'client_id', 'order_time', 'city_id', 'position_id'],
                'required',
                'message' => CodesBodyResponse::BAD_PARAM,
            ],
            [
                ['tenant_id', 'client_id', 'city_id', 'position_id'],
                'integer',
                'message' => CodesBodyResponse::BAD_PARAM,
            ],

            ['order_time', 'filter', 'filter' => [$this, 'filterOrderTime']],

            ['order_time', 'integer', 'min' => 1, 'message' => CodesBodyResponse::BAD_PARAM,],

            [
                ['car_class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => 'class_id',
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_CAR_CLASS,
            ],

            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_TENANT,
            ],

            [
                ['client_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id' => 'client_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_CLIENT,
            ],

            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_CITY,
            ],

            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_POSITION,
            ],

            [
                'client_id',
                function ($attribute) {
                    if (!ClientRepository::existsClientAtTenant($this->client_id, $this->tenant_id)) {
                        $this->addError($attribute, CodesBodyResponse::CLIENT_DO_NOT_BELONG_TENANT);
                    }
                },
            ],
        ];
    }

    public function filterOrderTime($value)
    {
        $timeOffset = CityRepository::getTimeOffset($this->city_id);

        return app()->formatter->asTimestamp($value) - $timeOffset;
    }
}