<?php

namespace app\modules\promocode\models\forms;


use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\models\entities\PromoCode;
use yii\base\Model;

class PromoCodeInfo extends Model
{

    public $code_id;

    public function rules()
    {
        return [
            [['code_id'], 'required', 'message' => CodesBodyResponse::BAD_PARAM],
            [['code_id'], 'integer', 'message' => CodesBodyResponse::BAD_PARAM],

            [
                ['code_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PromoCode::className(),
                'targetAttribute' => ['code_id' => 'code_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_PROMO_CODE,
            ],

        ];
    }

}