<?php

namespace app\modules\promocode\models\forms;


use app\models\Client;
use app\models\Tenant;
use app\modules\promocode\components\exceptions\ExceptionRuntime;
use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\components\repositories\ClientRepository;
use app\modules\promocode\components\repositories\OrderRepository;
use app\modules\promocode\components\repositories\PromoCodeRepository;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoClient;
use app\modules\promocode\models\entities\PromoCode;
use yii\base\Model;

class ActivatePromoCodeForm extends Model
{
    public $tenant_id;
    public $client_id;
    public $promo_code;

    /* @var $promoCode PromoCode */
    protected $promoCode;

    /* @var $clientRepository ClientRepository */
    protected $clientRepository;

    public function init()
    {
        $this->clientRepository = \Yii::$container->get(ClientRepository::class);
        parent::init();
    }

    public function rules()
    {
        return [
            [['tenant_id', 'client_id', 'promo_code'], 'required', 'message' => CodesBodyResponse::BAD_PARAM],
            [['tenant_id', 'client_id'], 'integer', 'message' => CodesBodyResponse::BAD_PARAM],

            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_TENANT,
            ],

            [
                ['client_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id' => 'client_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_CLIENT,
            ],

            [
                ['promo_code'],
                function ($attribute) {

                    $client = $this->clientRepository->getClient($this->client_id);
                    $this->promoCode = PromoCodeRepository::getPromoCodeAtTenant($this->tenant_id, $this->promo_code);

                    // Существует ли код вообще
                    if (!$this->promoCode) {
                        $this->addError($attribute, CodesBodyResponse::NO_CODE_IN_DATABASE);
                    }

                    // Принадлежит ли клиент тенанту
                    if (!ClientRepository::existsClientAtTenant( $this->client_id, $this->tenant_id)) {
                        $this->addError($attribute, CodesBodyResponse::CLIENT_DO_NOT_BELONG_TENANT);
                    }

                    // Город промокода должен соответствовать городу клиента
                    if ($client->city_id != $this->promoCode->promo->city) {
                        $this->addError($attribute, CodesBodyResponse::CODE_FOR_ANOTHER_CITY);
                    }





                    // Если клиент повоторно активирует тот же код
                    $promoCurrentClient = PromoClient::findOne([
                        'client_id' => $this->client_id,
                        'code_id'   => $this->promoCode->code_id,
                    ]);
                    if ($promoCurrentClient) {
                        switch ($promoCurrentClient->status_code_id) {
                            case PromoCode::STATUS_USED:
                                $this->addError($attribute, CodesBodyResponse::THIS_CODE_USED);
                                break;
                            case PromoCode::STATUS_EXPIRED:
                                $this->addError($attribute, CodesBodyResponse::THIS_CODE_EXPIRED);
                                break;
                            case PromoCode::STATUS_ACTIVATED:
                                $this->addError($attribute, CodesBodyResponse::THIS_CODE_ACTIVATED_CLIENT_EARLIER);
                                break;
                        }
                    }

                    // Активна система или Заблокирована
                    if ($this->promoCode->promo->blocked == 1) {
                        $this->addError($attribute, CodesBodyResponse::ACTION_PERIOD_CODE_PASSED);
                    }

                    // Проверка периода действия
                    switch ($this->promoCode->promo->period_type_id) {
                        case Promo::FIELD_PERIOD_NO_LIMITED:
                            break;
                        case Promo::FIELD_PERIOD_LIMITED:
                            if (time() > $this->promoCode->promo->period_end) {
                                $this->addError($attribute, CodesBodyResponse::ACTION_PERIOD_CODE_PASSED);
                            }
                            break;
                    }

                    // Проверка на новых клиентов
                    switch ($this->promoCode->promo->action_clients_id) {
                        case Promo::FIELD_TYPE_ACTION_CLIENT_ALL:
                            break;
                        case Promo::FIELD_TYPE_ACTION_CLIENT_NEW:
                            if (OrderRepository::getCountOrders(['client_id' => $this->client_id]) > 0) {
                                $this->addError($attribute, CodesBodyResponse::CODE_ONLY_FOR_NEW_CLIENTS);
                            }
                            break;

                    }

                    // Проверка если промокод активируется только в случае если у клиента нет других промокодов
                    if ($this->promoCode->promo->action_no_other_codes == 1) {
                        if (PromoCodeRepository::getCountPromoCodesAtClient($this->client_id) > 0) {
                            $this->addError($attribute,
                                CodesBodyResponse::CODE_ONLY_FOR_CLIENTS_NOT_USED_OTHER_CODES);
                        }
                    }

                    // Проверка на наличие текущих действующих промокодов
                    $allActiveCodes = PromoCodeRepository::getAllActiveCodes($this->client_id);
                    if (count($allActiveCodes) > 0) {
                        $this->addError($attribute, CodesBodyResponse::CLIENT_USED_ANOTHER_CODE);
                    }


                    // Промо код типа "генерация" должен принадлежать только одному клиенту
                    if ($this->promoCode->promo->type_id == Promo::FIELD_TYPE_PROMO_GENERATION) {
                        if (count($this->promoCode->promoClients) > 0) {
                            $this->addError($attribute, CodesBodyResponse::THIS_CODE_ACTIVATED_OTHER_CLIENT);
                        }
                    }


                },
            ],
        ];
    }

    public function getPromoCode()
    {
        $this->checkPromoCode();

        return $this->promoCode;
    }


    protected function checkPromoCode()
    {
        if (!$this->promoCode instanceof PromoCode) {
            throw new ExceptionRuntime('Error procedure for calling methods');
        }
    }


}
