<?php

namespace app\modules\promocode\models\forms;


use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\models\entities\Promo;
use yii\base\Model;

class GeneratedPromoCodesForm extends Model
{
    public $tenant_id;
    public $city_id;
    public $position_id;
    public $car_classes_id;
    public $promo_id;
    public $count_symbols_in_code;
    public $symbols;
    public $count_codes;
    public $type_id;

    /* @var $promoAction Promo */
    protected $promoAction;

    public function rules()
    {
        return [
            [
                [
                    'tenant_id',
                    'city_id',
                    'position_id',
                    'promo_id',
                    'count_symbols_in_code',
                    'symbols',
                    'type_id',
                ],
                'required',
                'message' => CodesBodyResponse::BAD_PARAM,
            ],
            [
                ['tenant_id', 'city_id', 'position_id', 'promo_id', 'count_symbols_in_code', 'count_codes', 'type_id'],
                'integer',
                'message' => CodesBodyResponse::BAD_PARAM,
            ],
            [['car_classes_id', 'symbols'], 'each', 'rule' => ['integer'], 'message' => CodesBodyResponse::BAD_PARAM],
        ];
    }

    /**
     * @return Promo
     */
    public function getPromoAction()
    {
        if (!$this->promoAction instanceof Promo) {
            $this->promoAction = new Promo([
                'tenant_id'             => $this->tenant_id,
                'position_id'           => $this->position_id,
                'promo_id'              => $this->promo_id,
                'count_symbols_in_code' => $this->count_symbols_in_code,
                'symbols'               => $this->symbols,
                'count_codes'           => $this->count_codes,
                'type_id'               => $this->type_id,
            ]);
            $this->promoAction->setCity($this->city_id);
            $this->promoAction->setCarClasses($this->car_classes_id);
        }

        return $this->promoAction;
    }
}