<?php

namespace app\modules\promocode\models\forms;


use app\models\Worker;
use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use yii\base\Model;

class WorkerRegistrationForm extends Model
{
    public $worker_id;
    public $cities_id;
    public $positions_id;
    public $car_classes_id;

    public function rules()
    {
        return [
            [['worker_id'], 'required', 'message' => CodesBodyResponse::BAD_PARAM],
            [['worker_id'], 'integer', 'message' => CodesBodyResponse::BAD_PARAM],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
                'message'         => CodesBodyResponse::EMPTY_DATA_IN_DATABASE_WORKER,
            ],
        ];
    }
}