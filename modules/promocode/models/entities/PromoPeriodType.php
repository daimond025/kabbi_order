<?php

namespace app\modules\promocode\models\entities;

use Yii;

/**
 * This is the model class for table "{{%promo_period_type}}".
 *
 * @property integer $period_type_id
 * @property string $name
 */
class PromoPeriodType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_period_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'period_type_id' => 'Period Type ID',
            'name' => 'Name',
        ];
    }
}
