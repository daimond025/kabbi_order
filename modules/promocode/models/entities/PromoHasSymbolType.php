<?php

namespace app\modules\promocode\models\entities;

use Yii;

/**
 * This is the model class for table "{{%promo_has_symbol_type}}".
 *
 * @property integer $promo_id
 * @property integer $symbol_type_id
 *
 * @property Promo $promo
 * @property PromoSymbolType $symbolType
 */
class PromoHasSymbolType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_has_symbol_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_id', 'symbol_type_id'], 'required'],
            [['promo_id', 'symbol_type_id'], 'integer'],
            [['promo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promo::className(), 'targetAttribute' => ['promo_id' => 'promo_id']],
            [['symbol_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoSymbolType::className(), 'targetAttribute' => ['symbol_type_id' => 'symbol_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promo_id' => 'Promo ID',
            'symbol_type_id' => 'Symbol Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSymbolType()
    {
        return $this->hasOne(PromoSymbolType::className(), ['symbol_type_id' => 'symbol_type_id']);
    }
}
