<?php

namespace app\modules\promocode\models\entities;

use Yii;

/**
 * This is the model class for table "{{%promo_action_clients}}".
 *
 * @property integer $action_clients_id
 * @property string $name
 *
 * @property Promo[] $promos
 */
class PromoActionClients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action_clients}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_clients_id' => 'Action Clients ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['action_clients_id' => 'action_clients_id']);
    }
}
