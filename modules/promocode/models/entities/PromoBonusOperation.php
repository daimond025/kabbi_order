<?php

namespace app\modules\promocode\models\entities;

use app\models\Client;
use app\models\Worker;
use app\models\Order;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_bonus_operation}}".
 *
 * @property integer $bonus_operation_id
 * @property integer $order_id
 * @property integer $client_id
 * @property integer $worker_id
 * @property string  $bonus_subject
 * @property integer $completed
 * @property integer $promo_bonus
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Client  $client
 * @property Order   $order
 * @property Worker  $worker
 */
class PromoBonusOperation extends \yii\db\ActiveRecord
{

    const BONUS_SUBJECT_WORKER = 'worker';
    const BONUS_SUBJECT_CLIENT = 'client';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_bonus_operation}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'bonus_subject', 'promo_bonus'], 'required'],
            [['order_id', 'client_id', 'worker_id', 'completed', 'created_at', 'updated_at'], 'integer'],
            [['bonus_subject'], 'string'],
            [['promo_bonus'], 'number'],
            [
                ['client_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id' => 'client_id'],
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Order::className(),
                'targetAttribute' => ['order_id' => 'order_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_operation_id' => 'Bonus Operation ID',
            'order_id'           => 'Order ID',
            'client_id'          => 'Client ID',
            'worker_id'          => 'Worker ID',
            'bonus_subject'      => 'Bonus Subject',
            'completed'          => 'Completed',
            'promo_bonus'        => 'Promo Bonus',
            'created_at'         => 'Created At',
            'updated_at'         => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
