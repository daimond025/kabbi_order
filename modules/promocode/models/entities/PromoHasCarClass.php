<?php

namespace app\modules\promocode\models\entities;

use app\models\CarClass;
use Yii;

/**
 * This is the model class for table "{{%promo_has_car_class}}".
 *
 * @property integer  $car_class_id
 * @property integer  $promo_id
 *
 * @property Promo    $promo
 * @property CarClass $class
 */
class PromoHasCarClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_has_car_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_class_id', 'promo_id'], 'required'],
            [['car_class_id', 'promo_id'], 'integer'],
            [
                ['car_class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['class_id' => 'car_class_id'],
            ],
            [
                ['promo_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['promo_id' => 'promo_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_class_id' => 'Class ID',
            'promo_id'     => 'Promo ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'car_class_id']);
    }
}
