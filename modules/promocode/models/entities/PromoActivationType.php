<?php

namespace app\modules\promocode\models\entities;

use Yii;

/**
 * This is the model class for table "{{%promo_activation_type}}".
 *
 * @property integer $activation_type_id
 * @property string $name
 *
 * @property Promo[] $promos
 */
class PromoActivationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_activation_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activation_type_id' => 'Activation Type ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['activation_type_id' => 'activation_type_id']);
    }
}
