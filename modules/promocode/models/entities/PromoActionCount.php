<?php

namespace app\modules\promocode\models\entities;

use Yii;

/**
 * This is the model class for table "{{%promo_action_count}}".
 *
 * @property integer $action_count_id
 * @property string $name
 *
 * @property Promo[] $promos
 */
class PromoActionCount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_action_count}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_count_id' => 'Action Count ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['action_count_id' => 'action_count_id']);
    }
}
