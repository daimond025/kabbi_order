<?php

namespace app\modules\promocode\models\entities;

use app\models\Client;
use app\models\CarClass;
use app\models\Worker;
use app\models\Order;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property integer       $code_id
 * @property integer       $promo_id
 * @property string        $code
 * @property integer       $created_at
 * @property integer       $updated_at
 * @property integer       $worker_id
 * @property integer       $car_class_id
 *
 * @property Promo         $promo
 * @property Order[]       $orders
 * @property Client        $client
 * @property Worker        $worker
 * @property CarClass      $carClass
 * @property PromoClient[] $promoClients
 */
class PromoCode extends \yii\db\ActiveRecord
{

    public $status_time;

    const STATUS_ACTIVATED = 2;
    const STATUS_USED = 3;
    const STATUS_EXPIRED = 4;


    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_code}}';
    }

    public function extraFields()
    {
        return [
            'promo' => 'promo'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_id', 'code'], 'required'],
            [['promo_id', 'worker_id', 'car_class_id'], 'integer'],
            [['code'], 'string', 'max' => 255],
            [
                ['promo_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['promo_id' => 'promo_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
            [
                ['car_class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['car_class_id' => 'class_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_id'  => 'Code ID',
            'promo_id' => 'Promo ID',
            'code'     => t('promo', 'Code'),
            'is_used'  => 'Is Used',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['promo_code_id' => 'code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasMany(Client::className(), ['client_id' => 'client_id'])
            ->viaTable(PromoClient::tableName(), ['code_id' => 'code_id']);
    }

    public function getPromoClients()
    {
        return $this->hasMany(PromoClient::className(), ['code_id' => 'code_id']);
    }

    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    public function getCarClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'car_class_id']);
    }

}
