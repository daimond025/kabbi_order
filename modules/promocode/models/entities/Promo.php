<?php

namespace app\modules\promocode\models\entities;

use app\models\CarClass;
use app\models\City;
use app\models\Position;
use app\models\Tenant;

/**
 * This is the model class for table "{{%promo}}".
 *
 * @property integer              $promo_id
 * @property integer              $tenant_id
 * @property integer              $type_id
 * @property integer              $position_id
 * @property string               $name
 * @property integer              $period_type_id
 * @property integer              $period_start
 * @property integer              $period_end
 * @property integer              $action_count_id
 * @property integer              $action_clients_id
 * @property integer              $action_no_other_codes
 * @property integer              $activation_type_id
 * @property integer              $client_bonuses
 * @property integer              $client_discount
 * @property integer              $worker_commission
 * @property integer              $worker_bonuses
 * @property integer              $blocked
 * @property integer              $created_at
 * @property integer              $updated_at
 * @property integer              $count_codes
 * @property integer              $count_symbols_in_code
 *
 * @property PromoActionClients   $actionClients
 * @property PromoActionCount     $actionCount
 * @property PromoActivationType  $activationType
 * @property PromoType            $type
 * @property PromoHasCity[]       $promoHasCities
 * @property PromoHasSymbolType[] $promoHasSymbolTypes
 * @property PromoCode[]          $promoCodes
 * @property PromoSymbolType[]    $symbolTypes
 * @property City[]               $promoCities
 * @property Tenant[]             $tenant
 * @property Position[]           $position
 * @property CarClass[]           $carSavedClasses
 *
 * @property integer[]            $cities
 * @property integer[]            $symbols
 * @property string[]             $codes
 * @property string[]             $carClasses
 *
 * @property integer              $city
 *
 * @property string               $dateStart
 * @property string               $dateEnd
 * @property string               $timeStart
 * @property string               $timeEnd
 */
class Promo extends \yii\db\ActiveRecord
{
    const FIELD_PROMO_TYPE_MANUALLY = 1;
    const FIELD_PROMO_TYPE_GENERATION = 2;
    const FIELD_PROMO_TYPE_DRIVERS = 3;

    const FIELD_PERIOD_NO_LIMITED = 1;
    const FIELD_PERIOD_LIMITED = 2;

    const FIELD_TYPE_ACTIVATION_BONUSES = 1;
    const FIELD_TYPE_ACTIVATION_DISCOUNT = 2;

    const FIELD_TYPE_ACTION_COUNT_ONCE = 1;
    const FIELD_TYPE_ACTION_COUNT_EACH = 2;
    const FIELD_TYPE_ACTION_COUNT_ONCE_ACTIVATION = 3;

    const FIELD_TYPE_ACTION_CLIENT_NEW = 1;
    const FIELD_TYPE_ACTION_CLIENT_ALL = 2;

    const FIELD_TYPE_PROMO_MANUALLY = 1;
    const FIELD_TYPE_PROMO_GENERATION = 2;
    const FIELD_TYPE_PROMO_DRIVERS = 3;

    const FIELD_TYPE_PERIOD_NOT = 1;
    const FIELD_TYPE_PERIOD_YES = 2;

    const FIELD_ACTION_NO_OTHER_CODES = 1;

    public $car_class_id;

    const BONUS_ACCRUAL_AFTER_ACTIVATE = 'AFTER_ACTIVATE';
    const BONUS_ACCRUAL_AFTER_ORDER = 'AFTER_ORDER';

    protected $_codes;
    protected $_symbols;
    protected $_classes;
    protected $_cities;

    protected $_dateStart;
    protected $_dateEnd;
    protected $_timeStart;
    protected $_timeEnd;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo}}';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->updateCities();
            $this->updateCarClasses();
            $this->updateSymbols();
        }

    }


    public function beforeSave($insert)
    {
        $this->tenant_id = user()->tenant_id;
        if ($this->type_id == self::FIELD_TYPE_PROMO_MANUALLY) {
            $this->setSymbols([]);
        }

        if ($this->period_type_id == Promo::FIELD_PERIOD_LIMITED) {
            $dateStart          = \DateTime::createFromFormat(
                'd.m.YH:i', $this->_dateStart . $this->_timeStart
            );
            $this->period_start = $dateStart->format('U');

            $dateEnd          = \DateTime::createFromFormat(
                'd.m.YH:i', $this->_dateEnd . $this->_timeEnd
            );
            $this->period_end = $dateEnd->format('U');
        } else {
            $this->period_start = null;
            $this->period_end   = null;
        }


        return parent::beforeSave($insert);
    }


    /**
     * @return string|array
     */
    public function &getCodes()
    {
        if ($this->_codes === null) {
            if ($this->type_id == self::FIELD_TYPE_PROMO_MANUALLY) {
                $this->_codes = $this->promoCodes[0]->code;
            }
        }

        return $this->_codes;
    }

    public function setCodes(&$values)
    {
        $this->_codes =& $values;
    }


    /**
     * @deprecated
     */
    public function setCities($values)
    {
        $this->_cities = $values;
    }

    /**
     * @return array
     * @deprecated
     */
    public function getCities()
    {
        if (!$this->_cities) {
            $this->_cities = $this
                ->getPromoCities()
                ->select('city_id')
                ->column();
        }

        return $this->_cities;
    }

    public function setCity($values)
    {
        $this->_cities = [$values];
    }

    public function getCity()
    {
        if (!$this->_cities) {
            $this->_cities = $this
                ->getPromoCities()
                ->select('city_id')
                ->column();
        }

        return $this->_cities[0];
    }


    public function setCarClasses($values)
    {
        $this->_classes = $values;
    }

    public function getCarClasses()
    {
        if (!$this->_classes) {
            $this->_classes = $this
                ->getSavedCarClasses()
                ->select('class_id')
                ->column();
        }

        return $this->_classes;
    }


    public function setSymbols($values)
    {
        $this->_symbols = $values;
    }

    public function getSymbols()
    {
        if (!$this->_symbols) {
            $this->_symbols = $this
                ->getSymbolTypes()
                ->select('symbol_type_id')
                ->column();
        }

        return $this->_symbols;
    }


    public function getDateStart()
    {
        if ($this->_dateStart === null) {
            if ($this->period_start === null) {
                return date('d.m.Y', time());
            } else {
                return date('d.m.Y', $this->period_start);
            }
        }

        return $this->_dateStart;
    }

    public function setDateStart($value)
    {
        $this->_dateStart = $value;
    }


    public function getDateEnd()
    {
        if ($this->_dateEnd === null) {
            if ($this->period_end === null) {
                return date('d.m.Y', time() + 3600 * 24 * 30);
            } else {
                return date('d.m.Y', $this->period_end);
            }
        }

        return $this->_dateEnd;
    }

    public function setDateEnd($value)
    {
        $this->_dateEnd = $value;
    }


    public function getTimeStart()
    {
        if ($this->_timeStart === null) {
            if ($this->period_start === null) {
                return date('H:i', time());
            } else {
                return date('H:i', $this->period_start);
            }
        }

        return $this->_timeStart;
    }

    public function setTimeStart($value)
    {
        $this->_timeStart = $value;
    }


    public function getTimeEnd()
    {
        if ($this->_timeEnd === null) {
            if ($this->period_end === null) {
                return date('H:i', time());
            } else {
                return date('H:i', $this->period_end);
            }
        }

        return $this->_timeEnd;
    }

    public function setTimeEnd($value)
    {
        $this->_timeEnd = $value;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionClients()
    {
        return $this->hasOne(PromoActionClients::className(), ['action_clients_id' => 'action_clients_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionCount()
    {
        return $this->hasOne(PromoActionCount::className(), ['action_count_id' => 'action_count_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivationType()
    {
        return $this->hasOne(PromoActivationType::className(), ['activation_type_id' => 'activation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PromoType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoHasCities()
    {
        return $this->hasMany(PromoHasCity::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoHasSymbolTypes()
    {
        return $this->hasMany(PromoHasSymbolType::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSymbolTypes()
    {
        return $this->hasMany(PromoSymbolType::className(), ['symbol_type_id' => 'symbol_type_id'])
            ->viaTable('{{%promo_has_symbol_type}}', ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])
            ->viaTable('{{%promo_has_city}}', ['promo_id' => 'promo_id']);
    }

    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getPeriodType()
    {
        return $this->hasOne(PromoPeriodType::className(), ['period_type_id' => 'period_type_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function getSavedCarClasses()
    {
        return $this->hasMany(CarClass::className(), ['class_id' => 'car_class_id'])
            ->viaTable('{{%promo_has_car_class}}', ['promo_id' => 'promo_id']);
    }

    public function getPromoCodes()
    {
        return $this->hasMany(PromoCode::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @param array $workers
     *
     * @return PromoCode|PromoCode[]
     */
    public function saveCodes($workers = null)
    {
        if (is_array($this->getCodes())) {
            /* @var $codes PromoCode */
            $codes = [];
            foreach ($this->getCodes() as $key => $code) {
                $models = new PromoCode([
                    'code'         => $code,
                    'promo_id'     => $this->promo_id,
                    'worker_id'    => is_null($workers) ? null : $workers[$key]['worker_id'],
                    'car_class_id' => is_null($workers)
                        ? null
                        : array_key_exists('car_class_id', $workers[$key])
                            ? $workers[$key]['car_class_id']
                            : null,
                ]);
                $models->save();
                $codes[] = $models;
            }

            return $codes;
        } else {
            $models = new PromoCode([
                'code'     => $this->getCodes(),
                'promo_id' => $this->promo_id,
            ]);
            $models->save();

            return $models;
        }
    }


    public function updateCities()
    {
        $currentCitiesIds = $this->getPromoCities()->select('city_id')->column();
        $newCitiesIds     = $this->getCities();

        $toInsert = [];
        foreach (array_filter(array_diff($newCitiesIds, $currentCitiesIds)) as $cityId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'city_id' => $cityId];
        }

        if ($toInsert) {
            PromoHasCity::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasCity::tableName(),
                    ['promo_id', 'city_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentCitiesIds, $newCitiesIds))) {
            PromoHasCity::deleteAll(['promo_id' => $this->promo_id, 'city_id' => $toRemove]);
        }
    }


    public function updateCarClasses()
    {
        $currentClassesIds = $this->getSavedCarClasses()->select('class_id')->column();
        $newClassesIds     = $this->getCarClasses();

        $toInsert = [];
        foreach (array_filter(array_diff($newClassesIds, $currentClassesIds)) as $classId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'car_class_id' => $classId];
        }


        if ($toInsert) {
            PromoHasCarClass::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasCarClass::tableName(),
                    ['promo_id', 'car_class_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentClassesIds, $newClassesIds))) {
            PromoHasCarClass::deleteAll(['promo_id' => $this->promo_id, 'car_class_id' => $toRemove]);
        }
    }


    public function updateSymbols()
    {
        $currentSymbolsIds = $this->getSymbolTypes()->select('symbol_type_id')->column();
        $newSymbolsIds     = $this->getSymbols();

        $toInsert = [];
        foreach (array_filter(array_diff($newSymbolsIds, $currentSymbolsIds)) as $symbolId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'symbol_type_id' => $symbolId];
        }


        if ($toInsert) {
            PromoHasSymbolType::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasSymbolType::tableName(),
                    ['promo_id', 'symbol_type_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentSymbolsIds, $newSymbolsIds))) {
            PromoHasSymbolType::deleteAll(['promo_id' => $this->promo_id, 'symbol_type_id' => $toRemove]);
        }
    }


}
