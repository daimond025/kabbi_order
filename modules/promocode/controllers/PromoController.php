<?php

namespace app\modules\promocode\controllers;


use app\modules\promocode\components\extensions\apiBodyResponse\apiBodyResponse;
use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\components\services\GenerationPromoCodeService;
use app\modules\promocode\components\services\promoAction\PromoActionService;
use app\modules\promocode\components\services\PromoCodeService;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\forms\ActivatePromoCodeForm;
use app\modules\promocode\models\forms\FindSuitablePromoCodeForm;
use app\modules\promocode\models\forms\GeneratedPromoCodesForm;
use app\modules\promocode\models\forms\PromoCodeInfo;
use app\modules\promocode\models\forms\WorkerRegistrationForm;
use yii\rest\Controller;

class PromoController extends Controller
{
    protected $apiBodyResponse;
    protected $promoCodeService;

    public function __construct($id, $module, apiBodyResponse $apiBodyResponse, $config = [])
    {
        $this->apiBodyResponse = $apiBodyResponse;
        parent::__construct($id, $module, $config);
    }


    public function actionActivate_promo_code()
    {
        $form = new ActivatePromoCodeForm();
        $form->load(post(), '');

        $transaction = app()->db->beginTransaction();
        try {

            if (!$form->validate()) {
                return $this->apiBodyResponse->getErrorResponseOfForm($form);
            }

            (new PromoCodeService())->activatedPromoCode($form);
            $transaction->commit();

            return $this->apiBodyResponse->getGoodResponse();

        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::error($e->getMessage());

            return $this->apiBodyResponse->getErrorResponse(CodesBodyResponse::INTERNAL_ERROR);
        }

    }

    public function actionFind_suitable_promo_code(
        $tenant_id,
        $client_id,
        $order_time,
        $city_id,
        $position_id,
        $car_class_id = null
    ) {
        $form = new FindSuitablePromoCodeForm();
        $form->load(compact('tenant_id', 'client_id', 'order_time', 'city_id', 'position_id', 'car_class_id'), '');

        if (!$form->validate()) {
            return $this->apiBodyResponse->getErrorResponseOfForm($form);
        }

        $suitablePromoCode = (new PromoCodeService())->getPromoCodeForCriteria($form);
        if (is_null($suitablePromoCode)) {
            return $this->apiBodyResponse->getErrorResponse(CodesBodyResponse::NONE_PROMO_CODE);
        }

        return $this->apiBodyResponse->getGoodResponseUsingDataProviderForOneModel($suitablePromoCode);

    }

    public function actionPromo_code_info($code_id)
    {
        $form = new PromoCodeInfo();
        $form->load(compact('code_id'), '');

        if (!$form->validate()) {
            return $this->apiBodyResponse->getErrorResponseOfForm($form);
        }

        $infoPromoCode = (new PromoCodeService())->getInfoPromoCode($form);

        return $this->apiBodyResponse->getGoodResponse($infoPromoCode);
    }

    public function actionWorker_registration($worker_id)
    {
        $form = new WorkerRegistrationForm();
        $form->load(compact('worker_id'), '');

        if (!$form->validate()) {
            return $this->apiBodyResponse->getErrorResponseOfForm($form);
        }

        /* @var $promoActionService PromoActionService */
        $promoActionService = \Yii::$container->get(PromoActionService::class);

        try {
            $promoActionService->setSuitablePromoActionForWorker($form);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            return $this->apiBodyResponse->getErrorResponse(CodesBodyResponse::INTERNAL_ERROR);
        }

        return $this->apiBodyResponse->getGoodResponse();
    }


    public function actionGenerated_promo_codes()
    {

        $form = new GeneratedPromoCodesForm();
        $form->load(post(), '');

        if (!$form->validate()) {
            return $this->apiBodyResponse->getErrorResponseOfForm($form);
        }

        $generator = new GenerationPromoCodeService();

        try {
            switch ($form->getPromoAction()->type_id) {
                case Promo::FIELD_TYPE_PROMO_GENERATION:
                    $generator->createdGenerateCodes($form->getPromoAction());
                    break;

                case Promo::FIELD_TYPE_PROMO_DRIVERS:
                    $generator->createdDriverCodes($form->getPromoAction());
                    break;
            }

        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            return $this->apiBodyResponse->getErrorResponse(CodesBodyResponse::INTERNAL_ERROR);
        }


        return $this->apiBodyResponse->getGoodResponse();
    }

}