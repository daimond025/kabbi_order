<?php

namespace app\modules\promocode;

use app\modules\promocode\components\extensions\apiBodyResponse\apiBodyResponse;
use app\modules\promocode\components\extensions\apiBodyResponse\CodesBodyResponse;
use app\modules\promocode\components\services\GenerationPromoCodeService;
use app\modules\promocode\components\services\promoAction\PromoActionService;
use app\modules\promocode\components\services\PromoCodeService;

/**
 * promocode module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\promocode\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setupDependencies();
    }

    /**
     * Setup dependencies
     */
    private function setupDependencies()
    {
        \Yii::$container->setSingleton(apiBodyResponse::class, function () {
            return new apiBodyResponse(new CodesBodyResponse());
        });

        \Yii::$container->setSingleton(PromoCodeService::class, function () {
            return new PromoActionService(new GenerationPromoCodeService());
        });
    }
}
