<?php

namespace app\modules\promocode\components\extensions\apiBodyResponse;


use app\modules\promocode\components\exceptions\ExceptionRuntime;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\rest\Serializer;

class apiBodyResponse
{
    /* @var $codes CodesBodyResponse */
    protected $codes;

    public function __construct(CodesBodyResponse $codes)
    {
        $this->codes = $codes;
    }

    public function getErrorResponseOfForm(Model $form)
    {
        if (!$form->hasErrors()) {
            throw new ExceptionRuntime('Method call failed');
        }

        foreach (array_keys($form->getAttributes()) as $attribute) {
            if(!is_null($code = $form->getFirstError($attribute))) {
                return $this->codes->getResponseData($code);
            }
        }

        throw new ExceptionRuntime('Method call failed');
    }


    public function getGoodResponse($result = null)
    {
        return $this->codes->getResponseData(CodesBodyResponse::OK, $result);
    }

    public function getGoodResponseUsingDataProviderForOneModel($result)
    {
        $dataProvider = new ActiveDataProvider();
        $dataProvider->setModels([$result]);

        $serializer = new Serializer();
        $res = $serializer->serialize($dataProvider);

        return $this->codes->getResponseData(CodesBodyResponse::OK, current($res));
    }

    public function getErrorResponse($code)
    {
        return $this->codes->getResponseData($code);
    }

}