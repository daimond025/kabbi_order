<?php

namespace app\modules\promocode\components\extensions\apiBodyResponse;


class CodesBodyResponse
{

    const OK = 0;
    const INTERNAL_ERROR = 1;
    const INCORRECTLY_SIGNATURE = 2;
    const UNKNOWN_REQUEST = 3;
    const BAD_PARAM = 4;
    const MISSING_INPUT_PARAMETER = 5;
    const EMPTY_VALUE = 6;
    const EMPTY_DATA_IN_DATABASE = 7;
    const BLACK_LIST = 8;
    const ACCESS_DENIED = 9;

    const REQUEST_PROCESSING = 20;

    const WORKER_BLOCKED = 80;
    const OLD_APP_VERSION = 82;
    const CITY_BLOCKED = 84;

    const FORBIDDEN_ACTION = 100;

    const EMPTY_DATA_IN_DATABASE_TENANT = 101;
    const EMPTY_DATA_IN_DATABASE_CLIENT = 102;
    const NO_CODE_IN_DATABASE = 103;
    const CODE_IN_DATABASE = 103;
    const THIS_CODE_ACTIVATED_CLIENT_EARLIER = 104;
    const THIS_CODE_USED = 105;
    const THIS_CODE_EXPIRED = 106;
    const THIS_CODE_ACTIVATED_OTHER_CLIENT = 107;
    const CLIENT_DO_NOT_BELONG_TENANT = 108;
    const ACTION_PERIOD_CODE_PASSED = 109;
    const CODE_ONLY_FOR_CLIENTS_NOT_USED_OTHER_CODES = 110;
    const CODE_ONLY_FOR_NEW_CLIENTS = 111;
    const CLIENT_USED_CODE_ANOTHER_WORKER = 121;

    const EMPTY_DATA_IN_DATABASE_CITY = 112;
    const EMPTY_DATA_IN_DATABASE_POSITION = 113;
    const NONE_PROMO_CODE = 114;

    const EMPTY_DATA_IN_DATABASE_PROMO_CODE = 115;

    const EMPTY_DATA_IN_DATABASE_WORKER = 116;
    const EMPTY_DATA_IN_DATABASE_CAR_CLASS = 117;
    const WORKER_NOT_HAS_CITY = 118;
    const WORKER_NOT_HAS_POSITION = 119;
    const WORKER_NOT_HAS_CAR_CLASS = 120;

    const NOT_PROMO_ACTION = 122;
    const CLIENT_USED_ANOTHER_CODE = 123;
    const CODE_FOR_ANOTHER_CITY = 124;


    const CODE_MESSAGES = [
        self::OK                                         => 'OK',
        self::INTERNAL_ERROR                             => 'INTERNAL_ERROR',
        self::INCORRECTLY_SIGNATURE                      => 'INCORRECTLY_SIGNATURE',
        self::UNKNOWN_REQUEST                            => 'UNKNOWN_REQUEST',
        self::BAD_PARAM                                  => 'BAD_PARAM',
        self::MISSING_INPUT_PARAMETER                    => 'MISSING_INPUT_PARAMETER',
        self::EMPTY_VALUE                                => 'EMPTY_VALUE',
        self::EMPTY_DATA_IN_DATABASE                     => 'EMPTY_DATA_IN_DATABASE',
        self::BLACK_LIST                                 => 'BLACK_LIST',
        self::ACCESS_DENIED                              => 'ACCESS_DENIED',
        self::WORKER_BLOCKED                             => 'WORKER_BLOCKED',
        self::OLD_APP_VERSION                            => 'OLD_APP_VERSION',
        self::CITY_BLOCKED                               => 'CITY_BLOCKED',
        self::FORBIDDEN_ACTION                           => 'FORBIDDEN_ACTION',
        self::REQUEST_PROCESSING                         => 'REQUEST_PROCESSING',
        self::EMPTY_DATA_IN_DATABASE_TENANT              => 'EMPTY_DATA_IN_DATABASE_TENANT',
        self::EMPTY_DATA_IN_DATABASE_CLIENT              => 'EMPTY_DATA_IN_DATABASE_CLIENT',
        self::NO_CODE_IN_DATABASE                        => 'NO_CODE_IN_DATABASE',
        self::THIS_CODE_ACTIVATED_CLIENT_EARLIER         => 'THIS_CODE_ACTIVATED_CLIENT_EARLIER',
        self::THIS_CODE_USED                             => 'THIS_CODE_USED',
        self::THIS_CODE_EXPIRED                          => 'THIS_CODE_EXPIRED',
        self::THIS_CODE_ACTIVATED_OTHER_CLIENT           => 'THIS_CODE_ACTIVATED_OTHER_CLIENT',
        self::CLIENT_DO_NOT_BELONG_TENANT                => 'CLIENT_DO_NOT_BELONG_TENANT',
        self::ACTION_PERIOD_CODE_PASSED                  => 'ACTION_PERIOD_CODE_PASSED',
        self::CODE_ONLY_FOR_CLIENTS_NOT_USED_OTHER_CODES => 'CODE_ONLY_FOR_CLIENTS_NOT_USED_OTHER_CODES',
        self::CODE_ONLY_FOR_NEW_CLIENTS                  => 'CODE_ONLY_FOR_NEW_CLIENTS',
        self::EMPTY_DATA_IN_DATABASE_CITY                => 'EMPTY_DATA_IN_DATABASE_CITY',
        self::EMPTY_DATA_IN_DATABASE_POSITION            => 'EMPTY_DATA_IN_DATABASE_POSITION',
        self::NONE_PROMO_CODE                            => 'NONE_PROMO_CODE',
        self::EMPTY_DATA_IN_DATABASE_PROMO_CODE          => 'EMPTY_DATA_IN_DATABASE_PROMO_CODE',
        self::EMPTY_DATA_IN_DATABASE_WORKER              => 'EMPTY_DATA_IN_DATABASE_WORKER',
        self::EMPTY_DATA_IN_DATABASE_CAR_CLASS           => 'EMPTY_DATA_IN_DATABASE_CAR_CLASS',
        self::WORKER_NOT_HAS_CITY                        => 'WORKER_NOT_HAS_CITY',
        self::WORKER_NOT_HAS_POSITION                    => 'WORKER_NOT_HAS_POSITION',
        self::WORKER_NOT_HAS_CAR_CLASS                   => 'WORKER_NOT_HAS_CAR_CLASS',
        self::CLIENT_USED_CODE_ANOTHER_WORKER            => 'CLIENT_USED_CODE_ANOTHER_WORKER',
        self::NOT_PROMO_ACTION                           => 'NOT_PROMO_ACTION',
        self::CLIENT_USED_ANOTHER_CODE                   => 'CLIENT_USED_ANOTHER_CODE',
        self::CODE_FOR_ANOTHER_CITY                      => 'CODE_FOR_ANOTHER_CITY',
    ];

    /**
     * Getting error data by code
     *
     * @param $codeResponse
     * @param $result
     *
     * @return array
     */
    public static function getResponseData($codeResponse, $result = null)
    {
        return [
            'code'   => $codeResponse,
            'info'   => self::CODE_MESSAGES[$codeResponse],
            'result' => $result,
        ];
    }
}