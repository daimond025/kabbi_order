<?php

namespace app\modules\promocode\components\extensions\codes;

use app\modules\promocode\components\extensions\codes\exceptions\ExceptionParameters;
use app\modules\promocode\components\extensions\codes\interfaces\CreatedSymbolsInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;
use app\modules\promocode\components\extensions\codes\symbols\Digits;
use app\modules\promocode\components\extensions\codes\symbols\EmptySymbol;
use app\modules\promocode\components\extensions\codes\symbols\LatinLowercase;
use app\modules\promocode\components\extensions\codes\symbols\SymbolDecorator;


class CreatedSymbols implements CreatedSymbolsInterface
{

    const STORE_SYMBOL_CLASS = [
        LatinLowercase::TYPE_NUMBER => LatinLowercase::class,
        Digits::TYPE_NUMBER         => Digits::class,
    ];


    /**
     * @param SettingInterface $setting
     *
     * @return SymbolInterface
     * @throws ExceptionParameters
     */
    public function getSymbols(SettingInterface $setting)
    {
        $symbols = new EmptySymbol();
        foreach ($setting->getNumbersTypeSymbols() as $numberTypeSymbol) {
            $this->testTypeSymbol($numberTypeSymbol);

            $symbolClass = self::STORE_SYMBOL_CLASS[$numberTypeSymbol];
            /* @var $symbols SymbolDecorator */
            $symbols = new $symbolClass($symbols, $setting);
        }

        return $symbols;

    }

    protected function testTypeSymbol($numberTypeSymbol)
    {
        if (!array_key_exists($numberTypeSymbol, self::STORE_SYMBOL_CLASS)) {
            throw new ExceptionParameters(
                t('promo', 'Your request does not fit the requirements.')
            );
        }
    }

}