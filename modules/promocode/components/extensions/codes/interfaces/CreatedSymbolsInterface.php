<?php

namespace app\modules\promocode\components\extensions\codes\interfaces;


interface CreatedSymbolsInterface
{
    /**
     * @param SettingInterface $setting
     *
     * @return SymbolInterface
     */
    public function getSymbols(SettingInterface $setting);
}