<?php
namespace app\modules\promocode\components\extensions\codes\interfaces;


interface SettingInterface
{
    public function getNeedCountCodes();

    public function getNeedCountSymbols();

    public function getNumbersTypeSymbols();

    /**
     * @return SymbolInterface
     */
    public function getTypeSymbols();
}