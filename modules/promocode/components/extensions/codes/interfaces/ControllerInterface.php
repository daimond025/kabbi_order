<?php

namespace app\modules\promocode\components\extensions\codes\interfaces;


interface ControllerInterface
{
    public function checkoutBeforeGenerationCodes();

    public function isValid();

    public function getCountLackCodes();

    public function setSetting(SettingInterface $setting);

    public function setStoreCodes(StoreCodesInterface $storeCodes);
}