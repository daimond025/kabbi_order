<?php
namespace app\modules\promocode\components\extensions\codes\interfaces;


interface StoreCodesInterface
{
    /**
     * @return array
     */
    public function &getCodes();

    public function pushCode($code);

    public function setCodes(array &$codes);

    public function getCurrentCountCodes();

    public function deleteDuplicateCodes(array $codes);
}