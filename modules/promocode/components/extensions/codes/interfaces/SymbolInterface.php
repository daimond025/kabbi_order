<?php

namespace app\modules\promocode\components\extensions\codes\interfaces;


interface SymbolInterface
{

    public function getTypeNumber();

    public function getSymbols();

    public function countSymbolsInTypes();

    public function getRegexp();
}