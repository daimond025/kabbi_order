<?php

namespace app\modules\promocode\components\extensions\codes;


use app\modules\promocode\components\extensions\codes\interfaces\ControllerInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;

class CodesGenerator
{

    /* @var $setting SettingInterface */
    protected $setting;
    /* @var $controller ControllerInterface */
    protected $controller;

    public function __construct(SettingInterface $settings, ControllerInterface $controller)
    {
        $this->setting    = $settings;
        $this->controller = $controller;
    }

    public function getCodes()
    {
        $storeCodes  = new StoreCodes();
        $codeCreator = new CodesCreator($this->setting);
        $this->controller->setSetting($this->setting);
        $this->controller->setStoreCodes($storeCodes);

        $this->controller->checkoutBeforeGenerationCodes();

        while (!$this->controller->isValid()) {

            for ($i = 0; $i < $this->controller->getCountLackCodes(); $i++) {
                $storeCodes->pushCode($codeCreator->createCode());
            }

        }

        return $storeCodes->getCodes();
    }


}