<?php

namespace app\modules\promocode\components\extensions\codes;


use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;

class CodesCreator
{
    protected $usedCodes = [];

    /* @var $setting SettingInterface */
    protected $setting;


    public function __construct(SettingInterface $setting)
    {
        $this->setting = $setting;
    }

    public function createCode()
    {

        $arSymbols = $this->setting->getTypeSymbols()->getSymbols();

        do {
            $randomSymbols = $arSymbols;
            shuffle($randomSymbols);
            $randomSymbols = array_slice($randomSymbols, 0, $this->setting->getNeedCountSymbols());
            $code          = implode('', $randomSymbols);

        } while ($this->checkForUsed($code));

        $this->usedCodes[] = $code;

        return $code;
    }

    protected function checkForUsed($code)
    {
        return in_array($code, $this->usedCodes);
    }


}