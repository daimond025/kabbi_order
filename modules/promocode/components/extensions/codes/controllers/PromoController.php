<?php

namespace app\modules\promocode\components\extensions\codes\controllers;


use app\modules\promocode\components\extensions\codes\exceptions\ExceptionParameters;
use app\modules\promocode\components\extensions\codes\exceptions\ExceptionRuntime;
use app\modules\promocode\components\extensions\codes\interfaces\ControllerInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\StoreCodesInterface;
use app\modules\promocode\components\extensions\codes\settings\PromoSetting;
use app\modules\promocode\components\repositories\PromoCodeRepository;


class PromoController implements ControllerInterface
{
    /* @var $setting PromoSetting */
    protected $setting;
    /* @var $storeCodes StoreCodesInterface */
    protected $storeCodes;

    protected $countLackCodes;

    public function setSetting(SettingInterface $setting)
    {
        $this->setting = $setting;
    }

    public function setStoreCodes(StoreCodesInterface $storeCodes)
    {
        $this->storeCodes = $storeCodes;
    }


    public function checkoutBeforeGenerationCodes()
    {

        $regexp = sprintf(
            '^[%s]{%d}$',
            $this->setting->getTypeSymbols()->getRegexp(),
            $this->setting->getNeedCountSymbols()
        );

        $countCodesInDb    = PromoCodeRepository::getCountCodesByRegexp($regexp, $this->setting->getTenantId());
        $countAllUsedCodes = $this->setting->getNeedCountCodes() + $countCodesInDb;
        $countVariants     = $this->setting->getTypeSymbols()->countSymbolsInTypes() ** $this->setting->getNeedCountSymbols();

        if ($countVariants < $this->setting->getNeedCountCodes()) {
            throw new ExceptionParameters(
                t('promo', 'The number of generated codes is not possible with so many characters in each code.')
            );
        }

        if ($countVariants < $countAllUsedCodes) {
            throw new ExceptionParameters(
                t('promo',
                    'With the codes already created with the same parameters, you can create a maximum of {count} codes.',
                    ['count' => $countVariants - $countCodesInDb])
            );
        }

        return true;
    }

    public function isValid()
    {
        if (!$this->setting instanceof SettingInterface) {
            throw new ExceptionRuntime('Error procedure for calling methods');
        }

        if (!$this->testCountCodes()) {
            return false;
        }

        if ($this->storeCodes->getCurrentCountCodes() > 0) {
            $duplicateCodes = PromoCodeRepository::getDuplicateCodes(
                $this->storeCodes->getCodes(),
                $this->setting->getTenantId()
            );
            $this->storeCodes->deleteDuplicateCodes($duplicateCodes);
        }

        if (!$this->testCountCodes()) {
            return false;
        }

        return true;
    }

    public function getCountLackCodes()
    {
        if (is_null($this->countLackCodes)) {
            throw new ExceptionRuntime('Error procedure for calling methods');
        }

        return $this->countLackCodes;
    }


    protected function testCountCodes()
    {
        if ($this->storeCodes->getCurrentCountCodes() != $this->setting->getNeedCountCodes()) {
            $this->countLackCodes = $this->setting->getNeedCountCodes() - $this->storeCodes->getCurrentCountCodes();

            return false;
        }

        return true;
    }
}