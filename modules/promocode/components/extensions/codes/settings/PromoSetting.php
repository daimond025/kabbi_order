<?php

namespace app\modules\promocode\components\extensions\codes\settings;

use app\modules\promocode\components\extensions\codes\CreatedSymbols;
use app\modules\promocode\components\extensions\codes\exceptions\ExceptionParameters;
use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;

class PromoSetting implements SettingInterface
{

    protected $countSymbols;
    protected $needCountCodes;
    protected $numbersTypeSymbols;
    /* @var $symbols SymbolInterface */
    protected $symbols;
    protected $tenantId;

    public function __construct(
        $countSymbols,
        array $numberTypeSymbols,
        $tenantId,
        $needCountCodes = 1
    ) {
        $this->countSymbols       = $countSymbols;
        $this->needCountCodes     = $needCountCodes;
        $this->numbersTypeSymbols = $numberTypeSymbols;
        $this->tenantId           = $tenantId;
    }

    public function getNumbersTypeSymbols()
    {
        return $this->numbersTypeSymbols;
    }

    public function getNeedCountCodes()
    {
        return $this->needCountCodes;
    }

    public function getNeedCountSymbols()
    {
        return $this->countSymbols;
    }

    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @return SymbolInterface
     * @throws ExceptionParameters
     */
    public function getTypeSymbols()
    {
        if (!$this->symbols instanceof SymbolInterface) {
            $this->symbols = (new CreatedSymbols())
                ->getSymbols($this);
        }

        return $this->symbols;
    }

}