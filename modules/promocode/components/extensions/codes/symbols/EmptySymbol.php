<?php
namespace app\modules\promocode\components\extensions\codes\symbols;

use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;

class EmptySymbol implements SymbolInterface
{

    public function getTypeNumber()
    {
        return 0;
    }


    public function countSymbolsInTypes()
    {
        return 0;
    }


    public function getSymbols()
    {
        return [];
    }

    public function getRegexp()
    {
        return '';
    }


}