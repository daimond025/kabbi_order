<?php

namespace app\modules\promocode\components\extensions\codes\symbols;


use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;

class Digits extends SymbolDecorator
{

    const TYPE_NUMBER = 2;

    public function __construct(SymbolInterface $symbol, SettingInterface $setting)
    {
        parent::__construct($symbol, $setting);
        $this->setArraySymbols(range(0, 9));
        $this->setRegexp('0-9');
        $this->setTypeNumber(self::TYPE_NUMBER);
    }


}
