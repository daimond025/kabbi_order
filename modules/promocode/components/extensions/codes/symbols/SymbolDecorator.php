<?php

namespace app\modules\promocode\components\extensions\codes\symbols;

use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;

abstract class SymbolDecorator implements SymbolInterface
{

    protected $setting;
    protected $arraySymbols;
    protected $symbol;
    protected $regexp;
    protected $typeNumber;

    public function __construct(SymbolInterface $symbol, SettingInterface $setting)
    {
        $this->symbol  = $symbol;
        $this->setting = $setting;
    }

    public function setArraySymbols(array $typeSymbols)
    {
        return $this->arraySymbols = $typeSymbols;
    }

    public function setTypeNumber($typeNumber)
    {
        $this->typeNumber = $typeNumber;
    }

    public function getArraySymbols()
    {
        return $this->arraySymbols;
    }

    public function setRegexp($regexp)
    {
        $this->regexp = $regexp;
    }

    protected function prepareSymbols()
    {
        $symbols = [];

        for ($i = 0; $i < $this->setting->getNeedCountSymbols(); $i++) {
            $symbols = array_merge($symbols, $this->getArraySymbols());
        }

        return $symbols;
    }

    public function countSymbolsInTypes()
    {
        return count($this->getArraySymbols()) + $this->symbol->countSymbolsInTypes();
    }

    public function getSymbols()
    {
        return array_merge(
            $this->prepareSymbols(),
            $this->symbol->getSymbols()
        );
    }

    public function getRegexp()
    {
        return $this->regexp . $this->symbol->getRegexp();
    }

    public function getTypeNumber()
    {
        return $this->typeNumber;
    }


}