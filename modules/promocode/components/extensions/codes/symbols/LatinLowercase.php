<?php

namespace app\modules\promocode\components\extensions\codes\symbols;

use app\modules\promocode\components\extensions\codes\interfaces\SettingInterface;
use app\modules\promocode\components\extensions\codes\interfaces\SymbolInterface;

class LatinLowercase extends SymbolDecorator
{

    const TYPE_NUMBER = 1;

    public function __construct(SymbolInterface $symbol, SettingInterface $setting)
    {
        parent::__construct($symbol, $setting);
        $this->setArraySymbols(range('a', 'z'));
        $this->setRegexp('a-z');
        $this->setTypeNumber(self::TYPE_NUMBER);
    }


}
