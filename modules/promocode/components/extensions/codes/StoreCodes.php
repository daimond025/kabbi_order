<?php

namespace app\modules\promocode\components\extensions\codes;


use app\modules\promocode\components\extensions\codes\exceptions\ExceptionRuntime;
use app\modules\promocode\components\extensions\codes\interfaces\StoreCodesInterface;

class StoreCodes implements StoreCodesInterface
{

    protected $codes = [];

    public function &getCodes()
    {
        return $this->codes;
    }

    public function pushCode($code)
    {
        $this->codes[] = $code;
    }

    public function setCodes(array &$codes)
    {
        $this->codes = $codes;
    }

    public function getCurrentCountCodes()
    {
        return count($this->codes);
    }

    public function deleteDuplicateCodes(array $codes)
    {
        foreach ($codes as $code) {
            $key = array_search($code['code'], $this->codes);
            if (!$key) {
                throw new ExceptionRuntime('Error procedure for calling methods');
            }
            unset($this->codes[$key]);
        }
    }
}