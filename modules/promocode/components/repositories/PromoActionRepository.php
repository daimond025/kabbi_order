<?php

namespace app\modules\promocode\components\repositories;


use app\models\Position;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoCode;
use app\modules\promocode\models\entities\PromoHasCity;

class PromoActionRepository
{
    public static function getAllPromoActionsByCriteria(
        $tenantId,
        array $citiesId,
        array $positionsId,
        $typeId,
        $carClassesId = []
    ) {
        $query = Promo::find()->alias('p')
            ->select('`p`.*, `phcc`.`car_class_id` AS car_class_id')
            ->distinct()
            ->leftJoin(
                PromoHasCity::tableName() . ' phc',
                '`p`.`promo_id` = `phc`.`promo_id`'
            )
            ->leftJoin(
                PromoCode::tableName() . ' pc',
                '`p`.`promo_id` = `pc`.`promo_id`'
            )
            ->leftJoin(
                Position::tableName() . ' ps',
                '`p`.`position_id` = `ps`.`position_id`'
            )
            ->leftJoin(
                '{{%promo_has_car_class}} phcc',
                '`p`.`promo_id` = `phcc`.`promo_id`'
            )
            ->where([
                '`p`.`type_id`'            => $typeId,
                '`p`.`tenant_id`'          => $tenantId,
                '`p`.`position_id`'        => $positionsId,
                '`phc`.`city_id`'          => $citiesId,
                '`ps`.`transport_type_id`' => null,
            ]);

        if ($carClassesId) {
            $query
                ->orWhere([
                    '`p`.`type_id`'         => $typeId,
                    '`p`.`tenant_id`'       => $tenantId,
                    '`p`.`position_id`'     => $positionsId,
                    '`phc`.`city_id`'       => $citiesId,
                    '`phcc`.`car_class_id`' => $carClassesId,
                ]);
        }

        $res = $query->createCommand()->queryAll();

        $promos = [];
        foreach ($res as $promo) {
            $promos[] = new Promo($promo);
        }

        return $promos;
    }

    public static function getPromoActionsAtWorker($workerId)
    {
        $query = Promo::find()->alias('p')
            ->select('`p`.*, `phcc`.`car_class_id` AS car_class_id')
            ->distinct()
            ->leftJoin(
                PromoCode::tableName() . ' pc',
                '`p`.`promo_id` = `pc`.`promo_id`'
            )
            ->leftJoin(
                '{{%promo_has_car_class}} phcc',
                '`p`.`promo_id` = `phcc`.`promo_id`'
            )
            ->where(['`pc`.`worker_id`' => $workerId]);

        $res = $query->createCommand()->queryAll();

        $promos = [];
        foreach ($res as $promo) {
            $promos[] = new Promo($promo);
        }

        return $promos;
    }


}
