<?php

namespace app\modules\promocode\components\repositories;


use app\models\Client;

class ClientRepository
{
    public static function existsClientAtTenant($clientId, $tenantId)
    {
        return Client::find()
            ->where([
                'client_id' => $clientId,
                'tenant_id' => $tenantId
            ])
            ->exists();
    }


    /**
     * @param       $clientId
     * @param array $conditions
     *
     * @return null|Client
     */
    public function getClient($clientId, $conditions = [])
    {

        return Client::find()
            ->where(array_merge([
                'client_id' => $clientId
            ], $conditions))
            ->one();

    }
}