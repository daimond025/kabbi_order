<?php

namespace app\modules\promocode\components\repositories;


use app\models\City;
use yii\db\ActiveQuery;

class CityRepository
{

    protected static $_timeOffset;

    public static function getTimeOffset($city_id)
    {
        if (!isset(self::$_timeOffset[$city_id])) {
            /* @var $city City */
            $city = City::find()
                ->where(['city_id' => $city_id])
                ->with([
                    'republic' => function (ActiveQuery $query) {
                        $query->select([
                            'republic_id',
                            'timezone',
                        ]);
                    },
                ])
                ->one();

            self::$_timeOffset[$city_id] = !empty($city) && !empty($city->republic->timezone) ? $city->republic->timezone * 3600 : 0;
        }

        return self::$_timeOffset[$city_id];
    }
}