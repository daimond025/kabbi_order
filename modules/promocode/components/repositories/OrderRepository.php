<?php
namespace app\modules\promocode\components\repositories;


use app\modules\v1\models\workers\order\Order;

class OrderRepository
{
    public static function getCountOrders($conditions)
    {
        return (int)Order::find()->where($conditions)->count();
    }
}