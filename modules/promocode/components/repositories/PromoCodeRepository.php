<?php

namespace app\modules\promocode\components\repositories;


use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoClient;
use app\modules\promocode\models\entities\PromoCode;

class PromoCodeRepository
{

    /**
     * @param $tenantId
     * @param $code
     *
     * @return PromoCode
     */
    public static function getPromoCodeAtTenant($tenantId, $code)
    {
        /* @var $code PromoCode */
        $code = PromoCode::find()->alias('c')
            ->leftJoin(
                Promo::tableName() . ' `p`',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->with('promo', 'promoClients')
            ->where([
                'p.tenant_id' => $tenantId,
                'p.blocked'   => 0,
                'c.code'      => $code,
            ])
            ->one();

        return $code;
    }

    /**
     * @param $clientId
     *
     * @return PromoCode[]
     */
    public static function getAllPromoCodesAtClient($clientId)
    {
        return PromoCode::find()->alias('c')
            ->joinWith('promoClients cl', false)
            ->joinWith('promo p')
            ->where([
                'cl.client_id' => $clientId
            ])
            ->all();
    }

    public static function getCountPromoCodesAtClient($clientId, $conditions = [])
    {
        return (int)PromoCode::find()->alias('c')
            ->leftJoin(
                PromoClient::tableName() . ' ct',
                '`c`.`code_id` = `ct`.`code_id`'
            )
            ->leftJoin(
                Promo::tableName() . ' p',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->where(array_merge([
                'ct.client_id' => $clientId,
            ], $conditions))
            ->count();
    }

    /**
     * @param $tenant_id
     * @param $client_id
     * @param $order_time
     * @param $city_id
     * @param $position_id
     * @param $car_class_id
     *
     * @return array
     */
    public static function getAllPromoCodesByCriteria(
        $tenant_id,
        $client_id,
        $order_time,
        $city_id,
        $position_id,
        $car_class_id
    ) {
        $sqlCarClass = $car_class_id ? 'AND `phcc`.`car_class_id` = :car_class_id' : '';

        $sql = "
SELECT `c`.*, `cl`.`status_time`
FROM `tbl_promo_code` `c`
LEFT JOIN `tbl_promo_client` `cl` ON `c`.`code_id` = `cl`.`code_id`
LEFT JOIN `tbl_promo` `p` ON `c`.`promo_id` = `p`.`promo_id`
LEFT JOIN `tbl_promo_has_city` `phc` ON `p`.`promo_id` = `phc`.`promo_id`
LEFT JOIN `tbl_promo_has_car_class` `phcc` ON `p`.`promo_id` = `phcc`.`promo_id`
LEFT JOIN `tbl_city` `ct` ON `phc`.`city_id` = `ct`.`city_id`
WHERE (
    `p`.`tenant_id` = :tenant_id 
    AND `cl`.`client_id` = :client_id
    AND `ct`.`city_id` = :city_id
    AND `p`.`position_id` = :position_id
    AND `cl`.`status_code_id` = :status_active
    AND `p`.`period_type_id` = :period_type_id_yes
    AND `p`.`period_start` <= :order_time
    AND `p`.`period_end` > :order_time
    $sqlCarClass
    AND `p`.`blocked` = 0
) OR (
    `p`.`tenant_id` = :tenant_id
    AND `cl`.`client_id` = :client_id
    AND `ct`.`city_id` = :city_id
    AND `p`.`position_id` = :position_id
    AND `cl`.`status_code_id` = :status_active
    AND `p`.`period_type_id` = :period_type_id_not
    $sqlCarClass
    AND `p`.`blocked` = 0
)
";


        $codes = \Yii::$app->db->createCommand($sql, [
            ':tenant_id'          => $tenant_id,
            ':client_id'          => $client_id,
            ':city_id'            => $city_id,
            ':position_id'        => $position_id,
            ':period_type_id_yes' => Promo::FIELD_TYPE_PERIOD_YES,
            ':period_type_id_not' => Promo::FIELD_TYPE_PERIOD_NOT,
            ':order_time'         => $order_time,
            ':status_active'      => PromoCode::STATUS_ACTIVATED,
            ':car_class_id'       => $car_class_id,
        ])->queryAll();

        $promoCodes = [];
        foreach ($codes as $key => $code) {
            $promoCodes[$key] = new PromoCode($code);
        }

        return $promoCodes;
    }

    /**
     * @param $client_id
     *
     * @return PromoCode[]
     */
    public static function getAllActiveCodes($client_id)
    {
        $sql = <<<SQL
SELECT `c`.*
FROM `tbl_promo_code` `c`
LEFT JOIN `tbl_promo_client` `cl`    ON `c`.`code_id` = `cl`.`code_id`
LEFT JOIN `tbl_promo` `p`            ON `c`.`promo_id` = `p`.`promo_id`
LEFT JOIN `tbl_promo_has_city` `phc` ON `p`.`promo_id` = `phc`.`promo_id`
WHERE (
    `cl`.`client_id` = :client_id
    AND `cl`.`status_code_id` = :status_active
    AND `p`.`period_type_id` = :period_type_id_yes
    AND `p`.`period_start` <= :order_time
    AND `p`.`period_end` > :order_time
    AND `p`.`blocked` = 0
) OR (
    `cl`.`client_id` = :client_id
    AND `cl`.`status_code_id` = :status_active
    AND `p`.`period_type_id` = :period_type_id_not
    AND `p`.`blocked` = 0
)
SQL;
        $codes = \Yii::$app->db->createCommand($sql, [
            ':client_id'          => $client_id,
            ':period_type_id_yes' => Promo::FIELD_TYPE_PERIOD_YES,
            ':period_type_id_not' => Promo::FIELD_TYPE_PERIOD_NOT,
            ':order_time'         => time(),
            ':status_active'      => PromoCode::STATUS_ACTIVATED,
        ])->queryAll();


        $promoCodes = [];
        foreach ($codes as $key => $code) {
            $promoCodes[$key] = new PromoCode($code);
        }

        return $promoCodes;
    }

    /**
     * @param $code_id
     *
     * @return PromoCode
     */
    public static function getPromoCodeInfo($code_id)
    {
        /* @var $promoCode PromoCode */
        $promoCode = PromoCode::find()
            ->with('promo')
            ->where(['code_id' => $code_id])
            ->one();
        return $promoCode;
    }

    public static function getDuplicateCodes(array &$codes, $tenantId)
    {
        return PromoCode::find()->alias('c')
            ->select('`c`.`code`')
            ->leftJoin(
                Promo::tableName() . ' `p`',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->where(['`c`.`code`' => $codes])
            ->andWhere(['`p`.`tenant_id`' => $tenantId])
            ->asArray()
            ->all();
    }

    public static function getCountCodesByRegexp($regexp, $tenantId)
    {
        return PromoCode::find()->alias('c')
            ->leftJoin(
                Promo::tableName() . ' `p`',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->where(['REGEXP BINARY', '`c`.`code`', $regexp])
            ->andWhere(['`p`.`tenant_id`' => $tenantId])
            ->count();
    }
}