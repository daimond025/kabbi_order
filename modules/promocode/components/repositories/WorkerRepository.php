<?php

namespace app\modules\promocode\components\repositories;


use app\models\Car;
use app\models\CarClass;
use app\models\City;
use app\models\Position;
use app\models\Worker;
use yii\db\Query;

class WorkerRepository
{
    public static function getCitiesHasWorker($workerId)
    {
        return City::find()->alias('c')
            ->leftJoin(
                '{{%worker_has_city}} whc',
                '`c`.`city_id` = `whc`.`city_id`'
            )
            ->where(['`whc`.`worker_id`' => $workerId])
            ->all();
    }

    public static function getPositionsHasWorker($workerId)
    {
        return Position::find()->alias('p')
            ->leftJoin(
                '{{%worker_has_position}} whp',
                '`p`.`position_id` = `whp`.`position_id`'
            )
            ->where(['`whp`.`worker_id`' => $workerId])
            ->all();
    }

    public static function getCarClassHasWorker($workerId)
    {
        return CarClass::find()->alias('cc')
            ->leftJoin(
                Car::tableName() . 'c',
                '`cc`.`class_id` = `c`.`class_id`'
            )
            ->leftJoin(
                '{{%worker_has_car}} whc',
                '`c`.`car_id` = `whc`.`car_id`'
            )
            ->leftJoin(
                '{{%worker_has_position}} whp',
                '`whc`.`has_position_id` = `whp`.`id`'
            )
            ->where(['`whp`.`worker_id`' => $workerId])
            ->all();
    }

    /**
     * @param $tenantId integer
     * @param $cityId integer
     * @param $positionId integer
     * @param $carClasses integer[]
     *
     * @return array
     */
    public static function getWorkersForPromoCodes($tenantId, $cityId, $positionId, $carClasses = null)
    {
        $query = Worker::find()
            ->alias('w')
            ->leftJoin('{{%worker_has_city}} `whc`', '`w`.`worker_id` = `whc`.`worker_id`')
            ->leftJoin('{{%worker_has_position}} `whp`', '`w`.`worker_id` = `whp`.`worker_id`')
            ->where([
                '`w`.`tenant_id`'     => $tenantId,
                '`whc`.`city_id`'     => $cityId,
                '`whp`.`position_id`' => $positionId,
            ]);

        if ($carClasses) {
            $subQuery = (new Query())->select('whcr.*')
                ->from('{{%worker_has_car}} whcr')
                ->leftJoin(Car::tableName(). ' `c`', '`c`.`car_id` = `whcr`.`car_id`')
                ->groupBy(['`whcr`.`has_position_id`', '`c`.`class_id`']);

            $query
                ->select('`w`.*, `c`.`class_id` AS car_class_id')
                ->leftJoin(['whcr' => $subQuery], '`whp`.`id` = `whcr`.`has_position_id`')
                ->leftJoin(Car::tableName() . ' `c`', '`whcr`.`car_id` = `c`.`car_id`')
                ->andWhere(['`c`.`class_id`' => $carClasses]);
        }

        return $query->createCommand()->queryAll();
    }
}