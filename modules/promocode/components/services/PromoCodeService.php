<?php

namespace app\modules\promocode\components\services;


use app\components\services\transaction\TransactionModel;
use app\modules\promocode\components\repositories\PromoCodeRepository;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoClient;
use app\modules\promocode\models\entities\PromoCode;
use app\modules\promocode\models\forms\ActivatePromoCodeForm;
use app\modules\promocode\models\forms\FindSuitablePromoCodeForm;
use app\modules\promocode\models\forms\PromoCodeInfo;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use app\components\services\transaction\TransactionService;

class PromoCodeService
{
    /* @var $transactionService TransactionService */
    protected $transactionService;

    public function __construct()
    {
        $this->transactionService = \Yii::$container->get(TransactionService::class);
    }


    public function activatedPromoCode(ActivatePromoCodeForm $form)
    {

        $promoClient = new PromoClient([
            'client_id'      => $form->client_id,
            'code_id'        => $form->getPromoCode()->code_id,
            'status_code_id' => PromoCode::STATUS_ACTIVATED,
            'status_time'    => time(),
        ]);

        $promoClient->save();

        $this->bonusAccrualIfExist($promoClient);

    }


    public function getPromoCodeForCriteria(FindSuitablePromoCodeForm $form)
    {
        $promoCodes = PromoCodeRepository::getAllPromoCodesByCriteria(
            $form->tenant_id,
            $form->client_id,
            $form->order_time,
            $form->city_id,
            $form->position_id,
            $form->car_class_id
        );


        return $this->selectionOnePromoCode($promoCodes);
    }

    /**
     * @param $promoCodes PromoCode[]
     *
     * @return PromoCode
     */
    protected function selectionOnePromoCode($promoCodes)
    {
        if (!$promoCodes) {
            return null;
        }


        // выдаем самый первый активированный промокод
        ArrayHelper::multisort($promoCodes, 'status_time', SORT_ASC);

        return $promoCodes[0];
    }

    public function getInfoPromoCode(PromoCodeInfo $form)
    {
        $infoCode  = [];
        $promoCode = PromoCodeRepository::getPromoCodeInfo($form->code_id);

        $infoCode['code']             = $promoCode;
        $infoCode['promo']            = $promoCode->promo;
        $infoCode['promoCarClassIds'] = $promoCode->promo->carClasses;

        return $infoCode;
    }

    protected function bonusAccrualIfExist(PromoClient $promoClient)
    {
        $promo = $promoClient->promoCode->promo;

        if (!$this->isBonusAccrual($promo)) {
            return false;
        }

        $transactionModel = (new TransactionModel())->fillDataForPromoBonus($promoClient);

        if ($this->changeStatusPromoCode($promoClient)) {
            $this->transactionService->createTransaction($transactionModel);
        } else {
            $error = "ERROR AT changeStatusPromoCode.";
            \Yii::error($error);
            throw new Exception($error);
        }
    }

    protected function changeStatusPromoCode(PromoClient $promoClient)
    {
        $promoClient->status_code_id = PromoCode::STATUS_USED;
        return $promoClient->update(false, ['status_code_id']);
    }

    protected function isBonusAccrual(Promo $promo)
    {
        if ($promo->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_BONUSES
            && $promo->action_count_id == Promo::FIELD_TYPE_ACTION_COUNT_ONCE_ACTIVATION
        ) {
            return true;
        }

        return false;
    }

}