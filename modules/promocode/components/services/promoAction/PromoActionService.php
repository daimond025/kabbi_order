<?php

namespace app\modules\promocode\components\services\promoAction;


use app\models\Worker;
use app\modules\promocode\components\exceptions\DbException;
use app\modules\promocode\components\repositories\PromoActionRepository;
use app\modules\promocode\components\repositories\WorkerRepository;
use app\modules\promocode\components\services\GenerationPromoCodeService;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoCode;
use app\modules\promocode\models\forms\WorkerRegistrationForm;
use yii\helpers\ArrayHelper;

class PromoActionService
{

    protected $generatorCodes;

    public function __construct(GenerationPromoCodeService $generator)
    {
        $this->generatorCodes = $generator;
    }

    public function setSuitablePromoActionForWorker(WorkerRegistrationForm $form)
    {
        $tenantId     = Worker::findOne(['worker_id' => $form->worker_id])->tenant_id;
        $citiesId     = ArrayHelper::getColumn(
            WorkerRepository::getCitiesHasWorker($form->worker_id), 'city_id'
        );
        $positionsId  = ArrayHelper::getColumn(
            WorkerRepository::getPositionsHasWorker($form->worker_id), 'position_id'
        );
        $carClassesId = ArrayHelper::getColumn(
            WorkerRepository::getCarClassHasWorker($form->worker_id), 'class_id'
        );

        /* @var $suitablePromoActions Promo[] */
        $suitablePromoActions = PromoActionRepository::getAllPromoActionsByCriteria(
            $tenantId, $citiesId, $positionsId, Promo::FIELD_TYPE_PROMO_DRIVERS, $carClassesId
        );

        /* @var $promoActionsAtWorker Promo[] */
        $promoActionsAtWorker = PromoActionRepository::getPromoActionsAtWorker($form->worker_id);

        $diffPromo = (new PromoDiff($suitablePromoActions, $promoActionsAtWorker))->getDiff();

        foreach ($diffPromo as $missingPromo) {

            $this->generatorCodes->createdOnePromoCodeForWorker($missingPromo, $form->worker_id);

        }

        return true;
    }
}