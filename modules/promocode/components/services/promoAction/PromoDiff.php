<?php

namespace app\modules\promocode\components\services\promoAction;


use app\modules\promocode\components\exceptions\ExceptionRuntime;
use app\modules\promocode\models\entities\Promo;

class PromoDiff
{

    protected $arrPromo1;
    protected $arrPromo2;

    /**
     * PromoDiff constructor.
     *
     * @param $arrPromo1 Promo[]
     * @param $arrPromo2 Promo[]
     */
    public function __construct(array &$arrPromo1, array &$arrPromo2)
    {
        $this->arrPromo1 =& $arrPromo1;
        $this->arrPromo2 =& $arrPromo2;
    }

    /**
     * @return Promo[]
     * @throws ExceptionRuntime
     */
    public function getDiff()
    {
        $arrPromoIdsAndCarClassIds1 = [];
        foreach ($this->arrPromo1 as $promo1) {
            $arrPromoIdsAndCarClassIds1[] = $promo1->promo_id . ':' . $promo1->car_class_id;
        }

        $arrPromoIdsAndCarClassIds2 = [];
        foreach ($this->arrPromo2 as $promo2) {
            $arrPromoIdsAndCarClassIds2[] = $promo2->promo_id . ':' . $promo2->car_class_id;
        }

        $diff = array_diff($arrPromoIdsAndCarClassIds1, $arrPromoIdsAndCarClassIds2);

        $arrPromo = [];
        foreach ($diff as $item) {
            if (preg_match('#(\d+):(\d+)#', $item, $matches)) {
                $arrPromo[] = $this->getPromoByPromoIdAndCarClassId($this->arrPromo1, $matches[1], $matches[2]);
            } elseif (preg_match('#(\d+):#', $item, $matches)) {
                $arrPromo[] = $this->getPromoByPromoIdAndCarClassId($this->arrPromo1, $matches[1]);
            } else {
                throw new ExceptionRuntime('Error diff promo.');
            }
        }

        return $arrPromo;
    }

    protected function &getPromoByPromoIdAndCarClassId(array &$arrPromo, $promoId, $carClassId = null)
    {
        /* @var $arrPromo Promo[] */
        foreach ($arrPromo as $promo) {
            if ($carClassId) {
                if ($promo->promo_id == $promoId && $promo->car_class_id = $carClassId) {
                    return $promo;
                }
            } else {
                if ($promo->promo_id == $promoId) {
                    return $promo;
                }
            }
        }
        throw new ExceptionRuntime('Error diff promo.');
    }

}