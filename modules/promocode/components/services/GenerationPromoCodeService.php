<?php

namespace app\modules\promocode\components\services;


use app\modules\promocode\components\exceptions\DbException;
use app\modules\promocode\components\exceptions\ExceptionRuntime;
use app\modules\promocode\components\extensions\codes\CodesGenerator;
use app\modules\promocode\components\extensions\codes\controllers\PromoController;
use app\modules\promocode\components\extensions\codes\settings\PromoSetting;
use app\modules\promocode\components\repositories\WorkerRepository;
use app\modules\promocode\models\entities\Promo;
use app\modules\promocode\models\entities\PromoCode;

class GenerationPromoCodeService
{

    public function createdOnePromoCodeForWorker(Promo $promoAction, $workerId)
    {
        $setting    = new PromoSetting($promoAction->count_symbols_in_code, $promoAction->symbols,
            $promoAction->tenant_id, 1);
        $controller = new PromoController();
        $generated  = new CodesGenerator($setting, $controller);

        $worker = ['worker_id' => $workerId, 'car_class_id' => $promoAction->car_class_id];

        $this->savePromoCode($promoAction, $generated->getCodes()[0], $worker);
        return true;
    }


    public function createdGenerateCodes(Promo $promoAction)
    {
        $setting    = new PromoSetting($promoAction->count_symbols_in_code, $promoAction->symbols,
            $promoAction->tenant_id, $promoAction->count_codes);
        $controller = new PromoController();
        $generated  = new CodesGenerator($setting, $controller);

        foreach ($generated->getCodes() as $code) {
            $this->savePromoCode($promoAction, $code);
        }
        return true;
    }

    public function createdDriverCodes(Promo $promoAction)
    {

        $workers = WorkerRepository::getWorkersForPromoCodes($promoAction->tenant_id, $promoAction->city,
            $promoAction->position_id, $promoAction->carClasses);

        $setting    = new PromoSetting($promoAction->count_symbols_in_code, $promoAction->symbols,
            $promoAction->tenant_id, count($workers));
        $controller = new PromoController();
        $generated  = new CodesGenerator($setting, $controller);

        $codes = $generated->getCodes();

        if (count($workers) !== count($codes)) {
            throw new ExceptionRuntime('The number of code and workers does not match');
        }

        foreach ($codes as $key => $code) {
            $this->savePromoCode($promoAction, $code, $workers[$key]);
        }

        return true;
    }


    protected function savePromoCode($promoAction, $code, $worker = null)
    {
        $promoCode = new PromoCode([
            'promo_id'     => $promoAction->promo_id,
            'code'         => $code,
            'worker_id'    => is_null($worker) ? null : $worker['worker_id'],
            'car_class_id' => is_null($worker)
                ? null
                : array_key_exists('car_class_id', $worker)
                    ? $worker['car_class_id']
                    : null,
        ]);

        if (!$promoCode->save(false)) {

            throw new DbException('Error save promo-code');
        }

        return true;
    }

}