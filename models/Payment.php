<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer                      $id
 * @property integer                      $tenant_id
 * @property integer                      $payment_number
 * @property string                       $description
 * @property integer                      $purchased_at
 * @property string                       $payment_sum
 * @property string                       $discount
 * @property integer                      $fixed_discount
 * @property integer                      $currency_id
 * @property integer                      $confirmed_at
 * @property integer                      $created_at
 * @property integer                      $updated_at
 * @property integer                      $created_by
 * @property integer                      $updated_by
 */
class Payment extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('payment', 'ID'),
            'tenant_id'      => Yii::t('payment', 'Tenant ID'),
            'payment_number' => Yii::t('payment', 'Payment Number'),
            'description'    => Yii::t('payment', 'Description'),
            'purchased_at'   => Yii::t('payment', 'Purchased At'),
            'purchasedAt'    => Yii::t('payment', 'Purchased At'),
            'payment_sum'    => Yii::t('payment', 'Payment Sum'),
            'discount'       => Yii::t('payment', 'Discount'),
            'fixed_discount' => Yii::t('payment', 'Fixed Discount'),
            'currency_id'    => Yii::t('payment', 'Currency ID'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_at'     => Yii::t('app', 'Updated At'),
            'created_by'     => Yii::t('app', 'Created By'),
            'updated_by'     => Yii::t('app', 'Updated By'),
        ];
    }
}
