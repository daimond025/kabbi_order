<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%republic}}".
 *
 * @property integer $republic_id
 * @property integer $country_id
 * @property string $name
 * @property string $shortname
 * @property double $timezone
 * @property string $name_en
 * @property string $name_az
 * @property string $name_de
 * @property string $name_sr
 * @property string $shortname_en
 * @property string $shortname_az
 * @property string $shortname_de
 * @property string $shortname_sr
 * @property string $name_ro
 * @property string $shortname_ro
 * @property string $time_zone
 * @property string $name_uz
 * @property string $shortname_uz
 * @property string $name_fi
 * @property string $shortname_fi
 * @property string $name_fa
 * @property string $shortname_fa
 *
 * @property Street[] $streets
 */
class Republic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%republic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'shortname'], 'required'],
            [['country_id'], 'integer'],
            [['timezone'], 'number'],
            [['name', 'name_en', 'name_az', 'name_de', 'name_sr', 'name_fi', 'name_fa'], 'string', 'max' => 100],
            [['shortname', 'shortname_en', 'shortname_az', 'shortname_de', 'shortname_sr', 'name_ro', 'shortname_ro'], 'string', 'max' => 50],
            [['time_zone', 'name_uz', 'shortname_uz', 'shortname_fi', 'shortname_fa'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'republic_id' => 'Republic ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
            'shortname' => 'Shortname',
            'timezone' => 'Timezone',
            'name_en' => 'Name En',
            'name_az' => 'Name Az',
            'name_de' => 'Name De',
            'name_sr' => 'Name Sr',
            'shortname_en' => 'Shortname En',
            'shortname_az' => 'Shortname Az',
            'shortname_de' => 'Shortname De',
            'shortname_sr' => 'Shortname Sr',
            'name_ro' => 'Name Ro',
            'shortname_ro' => 'Shortname Ro',
            'time_zone' => 'Time Zone',
            'name_uz' => 'Name Uz',
            'shortname_uz' => 'Shortname Uz',
            'name_fi' => 'Name Fi',
            'shortname_fi' => 'Shortname Fi',
            'name_fa' => 'Name Fa',
            'shortname_fa' => 'Shortname Fa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreets()
    {
        return $this->hasMany(Street::className(), ['republic_id' => 'republic_id']);
    }
}
