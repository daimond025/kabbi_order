<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%taxi_tariff}}".
 *
 * @property integer                $tariff_id
 * @property integer                $tenant_id
 * @property integer                $class_id
 * @property integer                $block
 * @property integer                $group_id
 * @property string                 $name
 * @property string                 $description
 * @property integer                $sort
 * @property integer                $enabled_site
 * @property integer                $enabled_app
 * @property integer                $enabled_operator
 * @property integer                $enabled_bordur
 * @property string                 $logo
 * @property integer                $position_id
 *
 * @property AdditionalOption[]     $additionalOptions
 * @property ClientBonusHasTariff[] $clientBonusHasTariffs
 * @property OptionActiveDate[]     $optionActiveDates
 * @property OptionTariff[]         $optionTariffs
 * @property PhoneLine[]            $phoneLines
 * @property TaxiTariffGroup        $group
 * @property Position               $position
 * @property CarClass               $class
 * @property Tenant                 $tenant
 * @property TaxiTariffHasCity[]    $taxiTariffHasCities
 */
class TaxiTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id'], 'required'],
            [
                [
                    'tenant_id',
                    'class_id',
                    'block',
                    'group_id',
                    'sort',
                    'enabled_site',
                    'enabled_app',
                    'enabled_operator',
                    'enabled_bordur',
                    'position_id',
                ],
                'integer',
            ],
            [['description'], 'string'],
            [['name', 'logo'], 'string', 'max' => 255],
            [
                ['group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TaxiTariffGroup::className(),
                'targetAttribute' => ['group_id' => 'group_id'],
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
            ],
            [
                ['class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['class_id' => 'class_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'        => 'Tariff ID',
            'tenant_id'        => 'Tenant ID',
            'class_id'         => 'Class ID',
            'block'            => 'Block',
            'group_id'         => 'Group ID',
            'name'             => 'Name',
            'description'      => 'Description',
            'sort'             => 'Sort',
            'enabled_site'     => 'Enabled Site',
            'enabled_app'      => 'Enabled App',
            'enabled_operator' => 'Enabled Operator',
            'enabled_bordur'   => 'Enabled Bordur',
            'logo'             => 'Logo',
            'position_id'      => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonusHasTariffs()
    {
        return $this->hasMany(ClientBonusHasTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionActiveDates()
    {
        return $this->hasMany(OptionActiveDate::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionTariffs()
    {
        return $this->hasMany(OptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TaxiTariffGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffHasCities()
    {
        return $this->hasMany(TaxiTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }
}
