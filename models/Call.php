<?php


namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%call}}".
 *
 * @property integer $id
 * @property string $uniqueid
 * @property integer $tenant_id
 * @property integer $order_id
 * @property integer $worker_id
 * @property integer $user_opertor_id
 * @property string $type
 * @property string $source
 * @property string $destination
 * @property integer $phone_line_id
 * @property string $channel
 * @property string $destinationchannel
 * @property string $destinationcontext
 * @property string $lastapplication
 * @property string $starttime
 * @property string $answertime
 * @property string $endtime
 * @property integer $duration
 * @property integer $billableseconds
 * @property string $disposition
 * @property string $amaflags
 * @property string $userfield
 * @property integer $create_time
 *
 * @property Worker $worker
 * @property PhoneLine $phoneLine
 * @property Tenant $tenant
 * @property User $userOpertor
 */
class Call extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'uniqueid',
                    'tenant_id',
                    'type',
                    'source',
                    'destination',
                    'channel',
                    'destinationchannel',
                    'lastapplication',
                    'duration',
                    'billableseconds',
                    'disposition',
                    'create_time',
                ],
                'required',
            ],
            [['tenant_id', 'order_id', 'phone_line_id', 'duration', 'billableseconds', 'create_time'], 'integer'],
            [['type'], 'string'],
            [['starttime', 'answertime', 'endtime'], 'safe'],
            [['uniqueid'], 'string', 'max' => 60],
            [['source', 'destination', 'lastapplication'], 'string', 'max' => 20],
            [['channel', 'destinationchannel', 'disposition', 'amaflags'], 'string', 'max' => 50],
            [['userfield'], 'string', 'max' => 30],
            [
                ['phone_line_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PhoneLine::className(),
                'targetAttribute' => ['phone_line_id' => 'line_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'uniqueid'           => Yii::t('employee', 'Uniqueid'),
            'tenant_id'          => Yii::t('employee', 'Tenant ID'),
            'order_id'           => Yii::t('employee', 'Order ID'),
            'type'               => Yii::t('employee', 'Type'),
            'source'             => Yii::t('employee', 'Source'),
            'destination'        => Yii::t('employee', 'Destination'),
            'phone_line_id'      => Yii::t('employee', 'Phone Line ID'),
            'channel'            => Yii::t('employee', 'Channel'),
            'destinationchannel' => Yii::t('employee', 'Destinationchannel'),
            'lastapplication'    => Yii::t('employee', 'Lastapplication'),
            'starttime'          => Yii::t('employee', 'Starttime'),
            'answertime'         => Yii::t('employee', 'Answertime'),
            'endtime'            => Yii::t('employee', 'Endtime'),
            'duration'           => Yii::t('employee', 'Duration'),
            'billableseconds'    => Yii::t('employee', 'Billableseconds'),
            'disposition'        => Yii::t('employee', 'Disposition'),
            'amaflags'           => Yii::t('employee', 'Amaflags'),
            'userfield'          => Yii::t('employee', 'Userfield'),
            'create_time'        => Yii::t('employee', 'Create Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLine()
    {
        return $this->hasOne(PhoneLine::className(), ['line_id' => 'phone_line_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}