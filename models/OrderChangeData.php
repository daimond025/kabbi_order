<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_order_change_data".
 *
 * @property integer $change_id
 * @property integer $tenant_id
 * @property integer $order_id
 * @property string  $change_field
 * @property string  $change_object_id
 * @property string  $change_object_type
 * @property string  $change_subject
 * @property string  $change_val
 * @property string  $change_time
 *
 * @property Order   $order
 * @property Tenant  $tenant
 */
class OrderChangeData extends ActiveRecord
{
    const SUBJECT_SYSTEM = 'system';
    const SUBJECT_WORKER = 'worker';
    const SUBJECT_DISPATCHER = 'disputcher';

    const OBJECT_TYPE_WORKER = 'worker';
    const OBJECT_TYPE_ORDER = 'order';
    const OBJECT_TYPE_EXCHANGE = 'exchange';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_change_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'order_id'], 'required'],
            [['tenant_id', 'order_id'], 'integer'],
            [['change_field', 'change_object_id', 'change_object_type'], 'string', 'max' => 45],
            [['change_subject'], 'string', 'max' => 100],
            [['change_val'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'change_id'          => 'Change ID',
            'tenant_id'          => 'Tenant ID',
            'order_id'           => 'Order ID',
            'change_field'       => 'Change Field',
            'change_object_id'   => 'Change Object ID',
            'change_object_type' => 'Change Object Type',
            'change_subject'     => 'Change Subject',
            'change_val'         => 'Change Val',
            'change_time'        => 'Change Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @param int    $tenantId
     * @param int    $orderId
     * @param string $subject
     * @param string $field
     * @param string $value
     * @param int    $objectId
     * @param string $objectType
     *
     * @throws \Exception
     */
    public static function saveChangeData($tenantId, $orderId, $subject, $field, $value, $objectId, $objectType)
    {
        $orderChange = new OrderChangeData();

        $orderChange->tenant_id          = $tenantId;
        $orderChange->order_id           = $orderId;
        $orderChange->change_subject     = (string)$subject;
        $orderChange->change_field       = (string)$field;
        $orderChange->change_val         = (string)$value;
        $orderChange->change_object_id   = (string)$objectId;
        $orderChange->change_object_type = (string)$objectType;
        $orderChange->change_time        = (int)time();

        if (!$orderChange->save()) {
            throw new \Exception(implode('; ', $orderChange->getFirstErrors()));
        }
    }
}