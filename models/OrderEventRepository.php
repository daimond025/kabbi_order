<?php


namespace app\models;


use Yii;

class OrderEventRepository
{
    public function get($key)
    {
        return $this->getDb()->executeCommand('GET', [$key]);
    }

    public function set($key, $value)
    {
        return $this->getDb()->executeCommand('SET', [$key, $value]);
    }

    public function delete($key)
    {
        return (bool) $this->getDb()->executeCommand('DEL', [$key]);
    }

    private function getDb()
    {
        return Yii::$app->redis_order_event;
    }
}