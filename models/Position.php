<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%position}}".
 *
 * @property integer             $position_id
 * @property string              $name
 * @property integer             $module_id
 * @property bool                $has_car
 * @property integer             $transport_type_id
 *
 */
class Position extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'module_id'], 'required'],
            [['module_id', 'transport_type_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['has_car'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id'       => Yii::t('employee', 'Position ID'),
            'name'              => Yii::t('employee', 'Name'),
            'module_id'         => Yii::t('employee', 'Module ID'),
            'has_car'           => Yii::t('employee', 'Link the car'),
            'transport_type_id' => Yii::t('employee', 'Transport type'),
        ];
    }
}
