<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property integer $currency_id
 * @property integer $type_id
 * @property string $name
 * @property string $code
 * @property string $symbol
 * @property integer $numeric_code
 * @property integer $minor_unit
 *
 * @property Account[] $accounts
 * @property CurrencyType $type
 * @property OneTimeService[] $oneTimeServices
 * @property Payment[] $payments
 * @property PaymentForAdditionalOption[] $paymentForAdditionalOptions
 * @property PaymentForOneTimeService[] $paymentForOneTimeServices
 * @property PaymentForTariff[] $paymentForTariffs
 * @property PriceHistory[] $priceHistories
 * @property Tariff[] $tariffs
 * @property TariffAdditionalOption[] $tariffAdditionalOptions
 * @property TenantTariffOrder[] $tenantTariffOrders
 * @property TenantTariffOrderHasProduct[] $tenantTariffOrderHasProducts
 * @property TenantTariffPrice[] $tenantTariffPrices
 * @property TenantTariffProduct[] $products
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id', 'numeric_code', 'minor_unit'], 'integer'],
            [['name', 'code', 'symbol'], 'string', 'max' => 255],
            [
                ['type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CurrencyType::className(),
                'targetAttribute' => ['type_id' => 'type_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id'  => 'Currency ID',
            'type_id'      => 'Type ID',
            'name'         => 'Name',
            'code'         => 'Code',
            'symbol'       => 'Symbol',
            'numeric_code' => 'Numeric Code',
            'minor_unit'   => 'Minor Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CurrencyType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOneTimeServices()
    {
        return $this->hasMany(OneTimeService::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForAdditionalOptions()
    {
        return $this->hasMany(PaymentForAdditionalOption::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForOneTimeServices()
    {
        return $this->hasMany(PaymentForOneTimeService::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForTariffs()
    {
        return $this->hasMany(PaymentForTariff::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceHistories()
    {
        return $this->hasMany(PriceHistory::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(Tariff::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffAdditionalOptions()
    {
        return $this->hasMany(TariffAdditionalOption::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffOrders()
    {
        return $this->hasMany(TenantTariffOrder::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffOrderHasProducts()
    {
        return $this->hasMany(TenantTariffOrderHasProduct::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffPrices()
    {
        return $this->hasMany(TenantTariffPrice::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(TenantTariffProduct::className(),
            ['id' => 'product_id'])->viaTable('tenant_tariff__price', ['currency_id' => 'currency_id']);
    }
}
