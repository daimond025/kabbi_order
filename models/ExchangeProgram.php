<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%exchange_program}}".
 *
 * @property integer              $exchange_program_id
 * @property string               $name
 * @property integer              $created_at
 * @property integer              $updated_at
 * @property integer              $sort
 * @property integer              $block
 *
 * @property ExchangeConnection[] $exchangeConnections
 */
class ExchangeProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exchange_program}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'sort'], 'integer'],
            [['block'], 'in', 'range' => ['0','1']],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'exchange_program_id' => Yii::t('app', 'Exchange Program ID'),
            'name'                => Yii::t('app', 'Name'),
            'created_at'          => Yii::t('app', 'Created At'),
            'updated_at'          => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchangeConnections()
    {
        return $this->hasMany(ExchangeConnection::className(), ['exchange_program_id' => 'exchange_program_id']);
    }
}
