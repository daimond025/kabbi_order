<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%order_status}}".
 *
 * @property integer $status_id
 * @property string $name
 * @property string $status_group
 * @property integer $dispatcher_sees
 *
 * @property ClientStatusEvent[] $clientStatusEvents
 * @property DefaultClientStatusEvent[] $defaultClientStatusEvents
 */
class OrderStatusRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dispatcher_sees'], 'integer'],
            [['name', 'status_group'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id'       => 'Status ID',
            'name'            => 'Name',
            'status_group'    => 'Status Group',
            'dispatcher_sees' => 'Dispatcher Sees',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientStatusEvents()
    {
        return $this->hasMany(ClientStatusEvent::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultClientStatusEvents()
    {
        return $this->hasMany(DefaultClientStatusEvent::className(), ['status_id' => 'status_id']);
    }
}
