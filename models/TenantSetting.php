<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tenant_setting}}".
 *
 * @property integer         $setting_id
 * @property integer         $tenant_id
 * @property string          $name
 * @property string          $value
 * @property string          $type
 * @property integer         $city_id
 *
 * @property City            $city
 * @property DefaultSettings $name0
 * @property Tenant          $tenant
 */
class TenantSetting extends \yii\db\ActiveRecord
{
    const TYPE_SETTING_GENERAL = 'general';
    const TYPE_SETTING_ORDERS = 'orders';
    const TYPE_SETTING_DRIVERS = 'drivers';
    const TYPE_SETTING_SYSTEM = 'system';

    const CURRENCY_TYPE = 'CURRENCY';

    const SETTING_SHOW_WORKER_PHONE = 'SHOW_WORKER_PHONE';
    const SETTING_SHOW_WORKER_ADDRESS = 'SHOW_WORKER_ADDRESS';
    const SETTING_SHOW_WORKER_CLIENT = 'SHOW_WORKER_CLIENT';
    const SETTING_FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES = 'FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES';
    const SETTING_PRE_ORDER = 'PRE_ORDER';
    const SETTING_PICK_UP = 'PICK_UP';
    const SETTING_ORDER_FORM_ADD_POINT_PHONE = 'ORDER_FORM_ADD_POINT_PHONE';
    const SETTING_ORDER_FORM_ADD_POINT_COMMENT = 'ORDER_FORM_ADD_POINT_COMMENT';
    const SETTING_REQUIRE_POINT_CONFIRMATION_CODE = 'REQUIRE_POINT_CONFIRMATION_CODE';
    const SETTING_ALLOW_RESERVE_URGENT_ORDERS = 'ALLOW_RESERVE_URGENT_ORDERS';
    const SETTING_MAX_COUNT_RESERVE_URGENT_ORDERS = 'MAX_COUNT_RESERVE_URGENT_ORDERS';

    const GEOCODER_TYPE = 'GEOCODER_TYPE';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'value'], 'required'],
            [['tenant_id', 'city_id'], 'integer'],
            [['value', 'type'], 'string'],
            [['name'], 'string', 'max' => 45],
            [
                ['tenant_id', 'name', 'city_id'],
                'unique',
                'targetAttribute' => ['tenant_id', 'name', 'city_id'],
                'message'         => 'The combination of Tenant ID, Name and City ID has already been taken.',
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['name'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => DefaultSettings::className(),
                'targetAttribute' => ['name' => 'name'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'value'      => 'Value',
            'type'       => 'Type',
            'city_id'    => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getName0()
    {
        return $this->hasOne(DefaultSettings::className(), ['name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public static function getSettingValue($tenant_id, $setting, $city_id = null, $position_id = null)
    {
        return self::find()
            ->where([
                'tenant_id' => $tenant_id,
                'name'      => $setting,
            ])
            ->andFilterWhere([
                'city_id'     => $city_id,
                'position_id' => $position_id,
            ])
            ->select('value')
            ->scalar();
    }

    /**
     * @param integer      $tenant_id
     * @param string       $setting
     * @param string       $type
     * @param null|integer $city_id
     * @param null|integer $position_id
     *
     * @return false|null|string
     */
    public static function getSettingValueByType(
        $tenant_id,
        $setting,
        $type,
        $city_id = null,
        $position_id = null
    ) {
        switch ($type) {
            case self::TYPE_SETTING_ORDERS:
            case self::TYPE_SETTING_DRIVERS:
                return self::getSettingValue($tenant_id, $setting, $city_id, $position_id);
            case self::TYPE_SETTING_GENERAL:
                return self::getSettingValue($tenant_id, $setting, $city_id);
            case self::TYPE_SETTING_SYSTEM:
                return self::getSettingValue($tenant_id, $setting);
        }

        throw new \RuntimeException('Error, unchanged type of settings: ' . $type);
    }
}
