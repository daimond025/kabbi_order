<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $user_id
 * @property string $tenant_id
 * @property string $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property integer $active_time
 * @property string $ha1
 * @property string $ha1b
 * @property integer $auth_exp
 * @property string $lang
 * @property string $last_session_id
 * @property string $md5_password
 * @property string $access_token
 *
 * @property Order[] $Orders
 * @property OrderHistory[] $OrderHistories
 * @property Support[] $Supports
 * @property SupportFeedback[] $SupportFeedbacks
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserHasCity[] $UserHasCities
 * @property \common\modules\city\models\City[] $cities
 * @property UserSetting[] $UserSettings
 * @property UserWorkingTime[] $UserWorkingTimes
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_name', 'position_id'], 'required', 'on' => 'insert'],
            [['email'], 'required', 'message' => t('app', 'Email cannot be blank'), 'on' => 'insert'],
            [['email'], 'string', 'max' => 45],
            [
                ['email'],
                'unique',
                'targetClass' => User::className(),
                'filter'      => ['not', ['user_id' => $this->user_id]],
            ],
            [['email'], 'email'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [['active'], 'integer'],
            [['city_list', 'block', 'isRightsChange', 'rights', 'cropParams'], 'safe'],
            [['address'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            ['lang', 'string', 'max' => 10],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'new_password'    => t('user', 'Password'),
            'password_repeat' => t('user', 'Repeat password'),
            'address'         => t('user', 'Address'),
            'birth'           => t('user', 'Birth'),
            'name'            => t('user', 'Name'),
            'last_name'       => t('user', 'Last name'),
            'second_name'     => t('user', 'Second name'),
            'create_time'     => t('user', 'Registration date'),
            'position_id'     => t('user', 'Position'),
            'position.name'   => t('user', 'Position'),
            'city'            => t('user', 'City'),
            'email'           => t('app', 'Username'),
            'phone'           => t('user', 'Mobile phone'),
            'active'          => t('user', 'Active user'),
            'photo'           => t('user', 'Photo'),
            'contact_name'    => t('user', 'Contact name'),
            'active_time'     => 'Online',
            'city_list'       => t('user', 'Working in cities'),
            'lang'            => t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks()
    {
        return $this->hasMany(SupportFeedback::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(\common\modules\city\models\City::className(),
            ['city_id' => 'city_id'])->viaTable('{{%user_has_city}}', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers()
    {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkingTimes()
    {
        return $this->hasMany(UserWorkingTime::className(), ['user_id' => 'user_id']);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function findIdentity($id)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getAuthKey()
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
}
