<?php
namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%geo_address}}".
 * @property integer $id
 * @property string $provider
 * @property string $place_id
 * @property string $address
 * @property integer $updated_at
 * @property integer $created_at
 */
class Address extends ActiveRecord
{

    public static $mounth  = 60 * 60 * 24 * 30 * 1000;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_address}}';
    }


    public function rules()
    {
        return [
            [['place_id', 'address'], 'required'],
            [['updated_at','created_at'], 'integer'],
        ];
    }
    /**
     * @param Address $address
     * */
    public static function needUpdate($address)
    {
        $update = isset($address->updated_at) ? $address->updated_at : 0;
        $time = time() - (int)$update;

        return $time >= self::$mounth ? true : false;

    }

    public function afterFind()
    {
        parent::afterFind();
        $this->address = unserialize($this->address);
    }

    public function beforeSave($insert)
    {
        $this->address = serialize($this->address);

        if($insert){
            $this->created_at = time();
            $this->updated_at = time();
        }else{
            $this->updated_at = time();
        }
        return parent::beforeSave($insert) ;
    }
}