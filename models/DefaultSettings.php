<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%default_settings}}".
 *
 * @property string          $name
 * @property string          $value
 * @property string          $type
 *
 * @property TenantSetting[] $tenantSettings
 */
class DefaultSettings extends \yii\db\ActiveRecord
{
    const GEOSERVICE_API_KEY = 'GEOSERVICE_API_KEY';
    const CLIENT_API_KEY = 'CLIENT_API_KEY';
    const SETTING_MAX_NUMBER_OF_ADDRESS_POINTS = 'MAX_NUMBER_OF_ADDRESS_POINTS';
    const SETTING_CURRENCY = 'CURRENCY';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value', 'type'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'  => 'Name',
            'value' => 'Value',
            'type'  => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantSettings()
    {
        return $this->hasMany(TenantSetting::className(), ['name' => 'name']);
    }

    public static function getSetting($name, $defaultValue = null)
    {
        $value = self::find()->select('value')->where(['name' => $name])->scalar();

        return $value === false ? $defaultValue : $value;
    }

    public static function getCachedSetting($name, $defaultValue = null)
    {
        $cache    = Yii::$app->cache;
        $cacheKey = self::class . '_' . $name;

        $value = $cache->get($cacheKey);
        if ($value === false) {
            $value = self::getSetting($name, $defaultValue);
            $cache->set($cacheKey, $value, 15 * 60);
        }

        return $value;
    }
}
