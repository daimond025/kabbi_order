<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer              $order_id
 * @property integer              $tenant_id
 * @property integer              $worker_id
 * @property integer              $car_id
 * @property integer              $city_id
 * @property integer              $tariff_id
 * @property integer              $user_create
 * @property integer              $status_id
 * @property integer              $user_modifed
 * @property integer              $company_id
 * @property integer              $parking_id
 * @property string               $address
 * @property string               $comment
 * @property string               $predv_price
 * @property string               $device
 * @property integer              $order_number
 * @property string               $payment
 * @property integer              $show_phone
 * @property integer              $create_time
 * @property integer              $status_time
 * @property integer              $time_to_client
 * @property string               $client_device_token
 * @property integer              $order_time
 * @property string               $predv_distance
 * @property integer              $predv_time
 * @property integer              $call_warning_id
 * @property string               $phone
 * @property integer              $client_id
 * @property integer              $bonus_payment
 * @property integer              $currency_id
 * @property integer              $time_offset
 * @property integer              $position_id
 * @property integer              $is_fix
 *
 * @property BonusFailLog[]       $bonusFailLogs
 * @property CardPayment[]        $cardPayments
 * @property CardPaymentLog[]     $cardPaymentLogs
 * @property ClientOrderFromApp[] $clientOrderFromApps
 * @property RawOrderCalc[]       $rawOrderCalcs
 * @property Transaction[]        $transactions
 * @property UdsGameOrder         $udsGameOrder
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'tenant_id',
                    'tariff_id',
                    'status_id',
                    'address',
                    'order_number',
                    'status_time',
                    'client_id',
                    'currency_id',
                    'position_id',
                ],
                'required',
            ],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'car_id',
                    'city_id',
                    'tariff_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'time_to_client',
                    'order_time',
                    'predv_time',
                    'call_warning_id',
                    'client_id',
                    'bonus_payment',
                    'currency_id',
                    'time_offset',
                    'position_id',
                    'is_fix',
                ],
                'integer',
            ],
            [['address', 'comment', 'device', 'payment', 'client_device_token'], 'string'],
            [['predv_price', 'predv_distance'], 'number'],
            [['phone'], 'string', 'max' => 255],
            [
                ['tenant_id', 'order_number'],
                'unique',
                'targetAttribute' => ['tenant_id', 'order_number'],
                'message'         => 'The combination of Tenant ID and Order Number has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'            => 'Order ID',
            'tenant_id'           => 'Tenant ID',
            'worker_id'           => 'Worker ID',
            'car_id'              => 'Car ID',
            'city_id'             => 'City ID',
            'tariff_id'           => 'Tariff ID',
            'user_create'         => 'User Create',
            'status_id'           => 'Status ID',
            'user_modifed'        => 'User Modifed',
            'company_id'          => 'Company ID',
            'parking_id'          => 'Parking ID',
            'address'             => 'Address',
            'comment'             => 'Comment',
            'predv_price'         => 'Predv Price',
            'device'              => 'Device',
            'order_number'        => 'Order Number',
            'payment'             => 'Payment',
            'show_phone'          => 'Show Phone',
            'create_time'         => 'Create Time',
            'status_time'         => 'Status Time',
            'time_to_client'      => 'Time To Client',
            'client_device_token' => 'Client Device Token',
            'order_time'          => 'Order Time',
            'predv_distance'      => 'Predv Distance',
            'predv_time'          => 'Predv Time',
            'call_warning_id'     => 'Call Warning ID',
            'phone'               => 'Phone',
            'client_id'           => 'Client ID',
            'bonus_payment'       => 'Bonus Payment',
            'currency_id'         => 'Currency ID',
            'time_offset'         => 'Time Offset',
            'position_id'         => 'Position ID',
            'is_fix'              => 'Is Fix',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker_company()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReview()
    {
        return $this->hasOne(ClientReview::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(Exchange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParking()
    {
        return $this->hasOne(Parking::className(), ['parking_id' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatusRecord::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModifed()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_modifed']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHasOptions()
    {
        return $this->hasMany(OrderHasOption::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%order_has_option}}',
            ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    public function getDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPayment()
    {
        return $this->hasOne(CardPayment::className(), ['order_id' => 'order_id']);
    }
}
