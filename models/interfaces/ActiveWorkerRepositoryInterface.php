<?php


namespace app\models\interfaces;


interface ActiveWorkerRepositoryInterface
{
    public function getOne($callsign);
}