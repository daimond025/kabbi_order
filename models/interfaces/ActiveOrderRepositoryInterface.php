<?php


namespace app\models\interfaces;


interface ActiveOrderRepositoryInterface
{
    /**
     * @return array
     */
    public function getList();

    public function publish($orderId);

    /**
     * @param int $orderId
     * @return array
     */
    public function getOne($orderId);

    /**
     * @param int $orderId
     * @return mixed
     */
    public function delete($orderId);

    /**
     * @param int $orderId
     * @param string $value
     * @return mixed
     */
    public function set($orderId, $value);
}