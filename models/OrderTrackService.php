<?php

namespace app\models;

use app\models\ar\mongodb\OrderTrack;

/**
 * Class OrderTrackService
 * @package app\models
 */
class OrderTrackService
{
    /**
     * Getting order track
     *
     * @param int $orderId
     *
     * @return array
     */
    public function getTrack($orderId)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        return empty($record['tracking']) ? [] : $record['tracking'];
    }
}