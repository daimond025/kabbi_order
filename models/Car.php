<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%car}}".
 *
 * @property integer $car_id
 * @property integer $tenant_id
 * @property integer $class_id
 * @property integer $city_id
 * @property string $name
 * @property string $gos_number
 * @property integer $color
 * @property string $year
 * @property string $photo
 * @property string $owner
 * @property integer $raiting
 * @property string $create_time
 * @property integer $active
 * @property string $license
 * @property string $license_scan
 * @property string $brand
 * @property string $model
 * @property integer $group_id
 *
 * @property WorkerGroup $group
 * @property CarClass $class
 * @property Tenant $tenant
 * @property CarColor $color0
 * @property CarHasOption[] $carHasOptions
 * @property CarOption[] $options
 * @property CarHasWorkerGroupClass[] $carHasWorkerGroupClasses
 * @property CarClass[] $classes
 * @property CarHasWorkerGroupTariff[] $carHasWorkerGroupTariffs
 * @property WorkerTariff[] $tariffs
 * @property DriverHasCarOld[] $driverHasCarOlds
 * @property DriverOld[] $drivers
 * @property TransportTypeFieldValue[] $transportTypeFieldValues
 * @property WorkerHasCar[] $workerHasCars
 * @property WorkerHasPosition[] $hasPositions
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'gos_number'], 'required'],
            [['tenant_id', 'class_id', 'city_id', 'color', 'raiting', 'active', 'group_id'], 'integer'],
            [['year', 'create_time'], 'safe'],
            [['owner'], 'string'],
            [['name', 'license'], 'string', 'max' => 45],
            [['gos_number'], 'string', 'max' => 10],
            [['photo', 'license_scan', 'brand', 'model'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkerGroup::className(), 'targetAttribute' => ['group_id' => 'group_id']],
            [['class_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarClass::className(), 'targetAttribute' => ['class_id' => 'class_id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenant::className(), 'targetAttribute' => ['tenant_id' => 'tenant_id']],
            [['color'], 'exist', 'skipOnError' => true, 'targetClass' => CarColor::className(), 'targetAttribute' => ['color' => 'color_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'Car ID',
            'tenant_id' => 'Tenant ID',
            'class_id' => 'Class ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'gos_number' => 'Gos Number',
            'color' => 'Color',
            'year' => 'Year',
            'photo' => 'Photo',
            'owner' => 'Owner',
            'raiting' => 'Raiting',
            'create_time' => 'Create Time',
            'active' => 'Active',
            'license' => 'License',
            'license_scan' => 'License Scan',
            'brand' => 'Brand',
            'model' => 'Model',
            'group_id' => 'Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor0()
    {
        return $this->hasOne(CarColor::className(), ['color_id' => 'color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%car_has_option}}', ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasWorkerGroupClasses()
    {
        return $this->hasMany(CarHasWorkerGroupClass::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return $this->hasMany(CarClass::className(), ['class_id' => 'class_id'])->viaTable('{{%car_has_worker_group_class}}', ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasWorkerGroupTariffs()
    {
        return $this->hasMany(CarHasWorkerGroupTariff::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['tariff_id' => 'tariff_id'])->viaTable('{{%car_has_worker_group_tariff}}', ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverHasCarOlds()
    {
        return $this->hasMany(DriverHasCarOld::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(DriverOld::className(), ['driver_id' => 'driver_id'])->viaTable('{{%driver_has_car_old}}', ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportTypeFieldValues()
    {
        return $this->hasMany(TransportTypeFieldValue::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['id' => 'has_position_id'])->viaTable('{{%worker_has_car}}', ['car_id' => 'car_id']);
    }
}
