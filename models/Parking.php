<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%parking}}".
 *
 * @property integer $parking_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property string $name
 * @property string $polygon
 * @property integer $sort
 * @property string $type
 * @property integer $is_active
 * @property integer $in_district
 * @property integer $out_district
 * @property string $in_distr_coefficient_type
 * @property string $out_distr_coefficient_type
 *
 * @property FixTariff[] $fixTariffs
 * @property FixTariff[] $fixTariffs0
 * @property Tenant $tenant
 * @property ParkingOrder[] $parkingOrders
 */
class Parking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'name', 'polygon', 'type'], 'required'],
            [['tenant_id', 'city_id', 'sort', 'is_active', 'in_district', 'out_district'], 'integer'],
            [['polygon'], 'string'],
            [['name', 'type', 'in_distr_coefficient_type', 'out_distr_coefficient_type'], 'string', 'max' => 45],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenant::className(), 'targetAttribute' => ['tenant_id' => 'tenant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parking_id' => 'Parking ID',
            'tenant_id' => 'Tenant ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'polygon' => 'Polygon',
            'sort' => 'Sort',
            'type' => 'Type',
            'is_active' => 'Is Active',
            'in_district' => 'In District',
            'out_district' => 'Out District',
            'in_distr_coefficient_type' => 'In Distr Coefficient Type',
            'out_distr_coefficient_type' => 'Out Distr Coefficient Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixTariffs()
    {
        return $this->hasMany(FixTariff::className(), ['from' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixTariffs0()
    {
        return $this->hasMany(FixTariff::className(), ['to' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkingOrders()
    {
        return $this->hasMany(ParkingOrder::className(), ['parking_id' => 'parking_id']);
    }
}
