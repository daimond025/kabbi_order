<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tenant_company}}".
 *
 * @property integer        $tenant_company_id
 * @property integer        $tenant_id
 * @property string         $name
 * @property string         $phone
 * @property integer        $sort
 * @property integer        $block
 * @property integer        $logo
 * @property integer        $use_logo_company
 * @property string         $stripe_account
 * @property string         $street
 * @property string         $city
 * @property string         $city_code
 * @property string         $tax_number
 * @property string         $fax
 * @property string         $site
 * @property integer        $user_contact
 * @property integer        $report_id
 *
 */
class TenantCompany extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_company}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_company_id' => 'Tenant Company ID',
            'tenant_id'         => 'Tenant ID',
            'name'              => t('tenant_company', 'Name'),
            'phone'             => t('tenant_company', 'Phone'),
            'sort'              => t('app', 'Sort'),
            'block'             => 'Block',
            'use_logo_company'  => t('tenant_company', 'Use the logo in the app'),
            'stripe_account'  => t('tenant_company', 'Stripe account'),

            // daimond
            'percent'             => t('tenant_company', 'Percent'),
            'street'             => t('tenant_company', 'Street'),
            'city'             => t('tenant_company', 'City'),
            'city_code'             => t('tenant_company', 'City_code'),
            'tax_number'             => t('tenant_company', 'StNr'),
            'contact_name'             => t('tenant_company', 'Contact_name'),
            'fax'             => t('tenant_company', 'Fax'),
            'site'             => t('tenant_company', 'Site'),
            'user_contact'             => t('tenant_company', 'User_contact'),
        ];
    }


}
