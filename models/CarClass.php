<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%car_class}}".
 *
 * @property integer $class_id
 * @property string $class
 * @property integer $type_id
 *
 * @property Car[] $cars
 * @property TransportType $type
 * @property CarHasWorkerGroupClass[] $carHasWorkerGroupClasses
 * @property Car[] $cars0
 * @property ClientBonus[] $clientBonuses
 * @property ClientCompanyHasTariff[] $clientCompanyHasTariffs
 * @property ClientCompany[] $companies
 * @property TaxiTariff[] $taxiTariffs
 * @property WorkerGroup[] $workerGroups
 * @property WorkerTariff[] $workerTariffs
 * @property WorkerTariff[] $workerTariffs0
 */
class CarClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'type_id'], 'required'],
            [['type_id'], 'integer'],
            [['class'], 'string', 'max' => 30],
            [
                ['type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TransportType::className(),
                'targetAttribute' => ['type_id' => 'type_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'Class ID',
            'class'    => 'Class',
            'type_id'  => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasWorkerGroupClasses()
    {
        return $this->hasMany(CarHasWorkerGroupClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars0()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%car_has_worker_group_class}}',
            ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasTariffs()
    {
        return $this->hasMany(ClientCompanyHasTariff::className(), ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(),
            ['company_id' => 'company_id'])->viaTable('{{%client_company_has_tariff}}', ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroups()
    {
        return $this->hasMany(WorkerGroup::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs0()
    {
        return $this->hasMany(WorkerTariff::className(), ['class_id' => 'class_id']);
    }
}
