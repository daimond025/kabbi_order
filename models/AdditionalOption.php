<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%additional_option}}".
 *
 * @property integer $id
 * @property integer $tariff_id
 * @property integer $additional_option_id
 * @property string $price
 * @property string $tariff_type
 *
 * @property CarOption $additionalOption
 * @property TaxiTariff $tariff
 */
class AdditionalOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'additional_option_id', 'tariff_type'], 'required'],
            [['tariff_id', 'additional_option_id'], 'integer'],
            [['price'], 'number'],
            [['tariff_type'], 'string'],
            [['additional_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarOption::className(), 'targetAttribute' => ['additional_option_id' => 'option_id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxiTariff::className(), 'targetAttribute' => ['tariff_id' => 'tariff_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'additional_option_id' => 'Additional Option ID',
            'price' => 'Price',
            'tariff_type' => 'Tariff Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'additional_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
