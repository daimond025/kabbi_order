<?php

namespace app\models;

use app\modules\v1\models\workers\client\ClientQuery;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%client}}".
 *
 * @property integer $client_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property string $photo
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $email
 * @property integer $black_list
 * @property integer $priority
 * @property string $create_time
 * @property integer $active
 * @property integer $success_order
 * @property integer $fail_worker_order
 * @property integer $fail_client_order
 * @property integer $fail_dispatcher_order
 * @property string $birth
 * @property integer $password
 * @property string $device
 * @property string $device_token
 * @property string $lang
 * @property string $description
 * @property string $auth_key
 * @property integer $active_time
 *
 * @property BonusFailLog[] $bonusFailLogs
 * @property CardPaymentLog[] $cardPaymentLogs
 * @property Tenant $tenant
 * @property ClientHasCompany[] $clientHasCompanies
 * @property ClientCompany[] $companies
 * @property ClientOrderFromApp $clientOrderFromApp
 * @property ClientPhone[] $clientPhones
 * @property ClientReview[] $clientReviews
 * @property ClientReviewRaiting[] $clientReviewRaitings
 * @property UdsGameClient $udsGameClient
 */
class Client extends \yii\db\ActiveRecord
{

    const TYPE_WINFON = 'WINFON';
    const TYPE_IOS = 'IOS';
    const TYPE_ANDROID = 'ANDROID';
    const TYPE_WEB = 'WEB';

    const CLIENT_TYPE_LIST = [
        self::TYPE_WINFON,
        self::TYPE_IOS,
        self::TYPE_ANDROID,
        self::TYPE_WEB,
    ];

    public $phone;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    $newValue = [];

                    foreach ($value as $item) {
                        $newValue[] = preg_replace("/[^0-9]/", '', $item);
                    }

                    return $newValue;
                },
            ],
            ['phone', 'uniquePhone'],
            [
                [
                    'tenant_id',
                    'black_list',
                    'priority',
                    'active',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'city_id',
                ],
                'integer',
            ],
            [['new_phone', 'birth_day', 'birth_month', 'birth_year', 'cropParams'], 'safe'],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 45],
            [['email'], 'email'],
            [['description'], 'string'],
        ];
    }

    public function uniquePhone()
    {
        $query = self::find();

        if ($this->isNewRecord) {
            $query->where(['tenant_id' => $this->tenant_id]);
        } else {
            $query->where('tenant_id = :tenant_id AND ' . self::tableName() . '.client_id != :client_id',
                [':tenant_id' => $this->tenant_id, ':client_id' => $this->client_id]);
        }

        $query->joinWith([
            'clientPhones' => function (ActiveQuery $sub_query) {
                $sub_query->where(['value' => $this->phone]);
            },
        ]);

        if ($query->exists()) {
            $this->addError('phone', t('validator', 'Client with the phone already exists'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'             => 'Client ID',
            'tenant_id'             => 'Tenant ID',
            'city_id'               => 'City ID',
            'photo'                 => 'Photo',
            'last_name'             => 'Last Name',
            'name'                  => 'Name',
            'second_name'           => 'Second Name',
            'email'                 => 'Email',
            'black_list'            => 'Black List',
            'priority'              => 'Priority',
            'create_time'           => 'Create Time',
            'active'                => 'Active',
            'success_order'         => 'Success Order',
            'fail_worker_order'     => 'Fail Worker Order',
            'fail_client_order'     => 'Fail Client Order',
            'fail_dispatcher_order' => 'Fail Dispatcher Order',
            'birth'                 => 'Birth',
            'password'              => 'Password',
            'device'                => 'Device',
            'device_token'          => 'Device Token',
            'lang'                  => 'Lang',
            'description'           => 'Description',
            'auth_key'              => 'Auth Key',
            'active_time'           => 'Active Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusFailLogs()
    {
        return $this->hasMany(BonusFailLog::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPaymentLogs()
    {
        return $this->hasMany(CardPaymentLog::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(),
            ['company_id' => 'company_id'])->viaTable('{{%client_has_company}}', ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientOrderFromApp()
    {
        return $this->hasOne(ClientOrderFromApp::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviews()
    {
        return $this->hasMany(ClientReview::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviewRaitings()
    {
        return $this->hasMany(ClientReviewRaiting::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUdsGameClient()
    {
        return $this->hasOne(UdsGameClient::className(), ['client_id' => 'client_id']);
    }

    public static function find()
    {
        return new ClientQuery(get_called_class());
    }
}
