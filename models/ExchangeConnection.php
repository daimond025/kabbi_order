<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "{{%exchange_connection}}".
 *
 * @property integer $exchange_connection_id
 * @property integer $exchange_program_id
 * @property integer $city_id
 * @property integer $position_id
 * @property string $block
 * @property string $clid
 * @property string $key_api
 * @property integer $rate
 * @property string $is_show_automate
 * @property integer $sort
 * @property string $name
 * @property integer $tenant_id
 *
 * @property City $city
 * @property ExchangeProgram $exchangeProgram
 * @property Position $position
 */
class ExchangeConnection extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exchange_connection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exchange_program_id', 'city_id', 'position_id', 'rate', 'clid', 'key_api', 'name'], 'required'],
            [['exchange_program_id', 'city_id', 'position_id', 'rate', 'sort'], 'integer'],
            [['block', 'is_show_automate'], 'in', 'range' => ['0', '1']],
            [['clid', 'key_api', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'exchange_connection_id' => Yii::t('exchange', 'Exchange Connection'),
            'position_id'            => Yii::t('exchange', 'Position'),
            'exchange_program_id'    => Yii::t('exchange', 'Program'),
            'city_id'                => Yii::t('exchange', 'City'),
            'clid'                   => Yii::t('exchange', 'Caller ID'),
            'key_api'                => Yii::t('exchange', 'Key API'),
            'rate'                   => Yii::t('exchange', 'Rate'),
            'is_show_automate'       => Yii::t('exchange', 'Automatic visibility of orders in the exchange'),
            'sort'                   => Yii::t('exchange', 'Sort'),
            'name'                   => Yii::t('exchange', 'Name'),
        ];
    }
}
