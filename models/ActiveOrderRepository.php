<?php


namespace app\models;


use app\models\interfaces\ActiveOrderRepositoryInterface;
use Yii;

class ActiveOrderRepository extends BaseRepository implements ActiveOrderRepositoryInterface
{
    /**
     * @return array
     */
    public function getList()
    {
        return $this->getDb()->executeCommand('hvals', [$this->tenantId]);
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getOne($orderId)
    {
        $order = $this->getDb()->executeCommand('hget', [$this->tenantId, $orderId]);

        return !empty($order) ? unserialize($order) : null;
    }

    /**
     * @param int $orderId
     * @return mixed
     */
    public function delete($orderId)
    {
        return $this->getDb()->executeCommand('hdel', [$this->tenantId, $orderId]);
    }

    /**
     * @param int $orderId
     * @param string $value
     * @return mixed
     */
    public function set($orderId, $value)
    {
        return $this->getDb()->executeCommand('hset',
            [$this->tenantId, $orderId, $value]);
    }

    public function publish($orderId)
    {
        return Yii::$app->redis_pub_sub->executeCommand('PUBLISH', ["order_" . $orderId, 'update_order_data']);
    }

    private function getDb()
    {
        return Yii::$app->redis_orders_active;
    }
}