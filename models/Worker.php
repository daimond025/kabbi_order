<?php

namespace app\models;

use app\modules\v1\exceptions\ExceptionFormatting;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker}}".
 *
 * @property integer $worker_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $callsign
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property string $device
 * @property string $device_token
 * @property string $lang
 * @property string $description
 * @property string $device_info
 * @property string $email
 * @property string $partnership
 * @property string $birthday
 * @property integer $block
 * @property integer $create_time
 * @property string $card_number
 *
 * @property Call[] $calls
 * @property City $city
 * @property Tenant $tenant
 * @property WorkerActiveAboniment[] $workerActiveAboniments
 * @property WorkerBlock[] $workerBlocks
 * @property WorkerHasDocument[] $workerHasDocuments
 * @property WorkerHasPosition[] $workerHasPositions
 * @property Position[] $positions
 * @property WorkerRefuseOrder[] $workerRefuseOrders
 * @property WorkerReviewRating[] $workerReviewRatings
 */
class Worker extends \yii\db\ActiveRecord
{
    const STATUS_FREE = 'FREE';
    const STATUS_BLOCKED = 'BLOCKED';
    const STATUS_ON_BREAK = 'ON_BREAK';
    const STATUS_ON_ORDER = 'ON_ORDER';
    const STATUS_ACCEPT_PREORDER = 'ACCEPT_PREORDER';
    const STATUS_REFUSE_PREORDER = 'REFUSE_PREORDER';

    const ACTION_PAUSE = 'pause';
    const ACTION_WORK = 'work';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'callsign', 'phone'], 'required'],
            [['tenant_id', 'city_id', 'callsign', 'block', 'create_time'], 'integer'],
            [['device', 'device_token', 'partnership'], 'string'],
            [['birthday'], 'safe'],
            [['password', 'photo', 'description', 'device_info'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name', 'email', 'card_number'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            [['lang'], 'string', 'max' => 10],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'city_id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenant::className(), 'targetAttribute' => ['tenant_id' => 'tenant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id' => 'Worker ID',
            'tenant_id' => 'Tenant ID',
            'city_id' => 'City ID',
            'callsign' => 'Callsign',
            'password' => 'Password',
            'last_name' => 'Last Name',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'device' => 'Device',
            'device_token' => 'Device Token',
            'lang' => 'Lang',
            'description' => 'Description',
            'device_info' => 'Device Info',
            'email' => 'Email',
            'partnership' => 'Partnership',
            'birthday' => 'Birthday',
            'block' => 'Block',
            'create_time' => 'Create Time',
            'card_number' => 'Card Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalls()
    {
        return $this->hasMany(Call::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerActiveAboniments()
    {
        return $this->hasMany(WorkerActiveAboniment::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerBlocks()
    {
        return $this->hasMany(WorkerBlock::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['position_id' => 'position_id'])->viaTable('{{%worker_has_position}}', ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerRefuseOrders()
    {
        return $this->hasMany(WorkerRefuseOrder::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerReviewRatings()
    {
        return $this->hasMany(WorkerReviewRating::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @param array $worker
     *
     * @return bool
     */
    public static function isWorkerBlocked(array $worker)
    {
        $status = self::getWorkerStatus($worker);

        return $status == self::STATUS_BLOCKED;
    }

    /**
     * @param array $worker
     *
     * @return bool
     */
    public static function isWorkerOnOrder(array $worker)
    {
        $status = self::getWorkerStatus($worker);

        return $status == self::STATUS_ON_ORDER;
    }

    /**
     * @param array $worker
     *
     * @return bool
     */
    public static function isWorkerOnBreak(array $worker)
    {
        $status = self::getWorkerStatus($worker);

        return $status == self::STATUS_ON_BREAK;
    }

    protected static function getWorkerStatus(array $worker)
    {
        $status = ArrayHelper::getValue($worker, 'worker.status');

        if (!$status) {
            $message = 'Error, no status worker.';

            throw new \RuntimeException(ExceptionFormatting::getFormattedString($message, $worker));
        }

        return $status;
    }
}
