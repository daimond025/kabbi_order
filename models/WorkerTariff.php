<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_worker_tariff".
 *
 * @property integer                   $tariff_id
 * @property integer                   $tenant_id
 * @property integer                   $class_id
 * @property string                    $name
 * @property string                    $type
 * @property integer                   $block
 * @property integer                   $days
 * @property string                    $subscription_limit_type
 * @property string                    $description
 * @property integer                   $position_id
 * @property string                    $period_type
 * @property string                    $period
 * @property string                    $cost
 * @property string                    $action_new_shift
 */
class WorkerTariff extends ActiveRecord
{
    const TYPE_ONCE = 'ONCE';
    const TYPE_SUBSCRIPTION = 'SUBSCRIPTION';

    const SUBSCRIPTION_LIMIT_TYPE_SHIFT = 'SHIFT_COUNT';
    const SUBSCRIPTION_LIMIT_TYPE_DAY = 'DAY_COUNT';

    const ACTION_NEW_SHIFT_CONTINUE_PERIOD = 'CONTINUE_PERIOD';
    const ACTION_NEW_SHIFT_NEW_PERIOD = 'NEW_PERIOD';

    const PERIOD_TYPE_INTERVAL = 'INTERVAL';
    const PERIOD_TYPE_HOURS = 'HOURS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'               => 'Tariff ID',
            'tenant_id'               => 'Tenant ID',
            'class_id'                => 'Class ID',
            'name'                    => 'Name',
            'type'                    => 'Type',
            'block'                   => 'Block',
            'days'                    => 'Days',
            'subscription_limit_type' => 'Subscription Limit Type',
            'description'             => 'Description',
            'position_id'             => 'Position ID',
            'period_type'             => 'Period Type',
            'period'                  => 'Period',
            'cost'                    => 'Cost',
        ];
    }
}
