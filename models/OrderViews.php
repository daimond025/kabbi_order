<?php


namespace app\models;


use yii\db\ActiveRecord;

class OrderViews extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_views}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tenant_id'], 'required'],
            [['new', 'works', 'warning', 'pre_order'], 'default', 'value' => 0],
            [['view_id', 'user_id', 'tenant_id', 'city_id', 'new', 'works', 'warning', 'pre_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'view_id'   => 'View ID',
            'user_id'   => 'User ID',
            'tenant_id' => 'Tenant ID',
            'city_id'   => 'City ID',
            'new'       => 'New',
            'works'     => 'Works',
            'warning'   => 'Warning',
            'pre_order' => 'Pre Order',
            'completed' => 'Completed',
            'rejected'  => 'Rejected',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
}