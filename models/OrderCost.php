<?php

namespace app\models;

use app\modules\v1\models\Order;
use Yii;

/**
 * This is the model class for table "{{%order_detail_cost}}".
 *
 * @property integer $detail_id
 * @property integer $order_id
 * @property string $cost
 * @property string $planting_cost
 * @property string $planting_km
 * @property string $distance_city
 * @property string $distance_out
 * @property string $cost_city
 * @property string $cost_out
 * @property string $time
 * @property string $time_city
 * @property string $time_out
 * @property string $wait_time
 * @property string $wait_time_cost
 * @property string $additionals_cost
 * @property integer $is_fix
 *
 * @property Order $order
 */
class OrderCost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_detail_cost}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'is_fix'], 'integer'],
            [
                [
                    'cost',
                    'planting_cost',
                    'planting_km',
                    'distance_city',
                    'distance_out',
                    'cost_city',
                    'cost_out',
                    'time',
                    'time_city',
                    'time_out',
                    'wait_time',
                    'wait_time_cost',
                    'additionals_cost',
                ],
                'string',
                'max' => 45,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'detail_id'        => 'Detail ID',
            'order_id'         => 'Order ID',
            'cost'             => 'Cost',
            'planting_cost'    => 'Planting Cost',
            'planting_km'      => 'Planting Km',
            'distance_city'    => 'Distance City',
            'distance_out'     => 'Distance Out',
            'cost_city'        => 'Cost City',
            'cost_out'         => 'Cost Out',
            'time'             => 'Time',
            'time_city'        => 'Time City',
            'time_out'         => 'Time Out',
            'wait_time'        => 'Wait Time',
            'wait_time_cost'   => 'Wait Time Cost',
            'additionals_cost' => 'Additionals Cost',
            'is_fix'           => 'Is Fix',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
