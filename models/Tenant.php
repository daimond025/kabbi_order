<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tenant}}".
 *
 * @property integer $tenant_id
 * @property string $company_name
 * @property string $full_company_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $contact_name
 * @property string $contact_second_name
 * @property string $contact_last_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $company_phone
 * @property string $domain
 * @property string $email
 * @property string $inn
 * @property string $create_time
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $archive
 * @property string $logo
 * @property string $director
 * @property string $director_position
 * @property string $status
 *
 * @property Account[] $accounts
 * @property AutocallFile[] $autocallFiles
 * @property Call[] $calls
 * @property Car[] $cars
 * @property CardPaymentLog[] $cardPaymentLogs
 * @property Client[] $clients
 * @property ClientBonus[] $clientBonuses
 * @property ClientCompany[] $clientCompanies
 * @property ClientStatusEvent[] $clientStatusEvents
 * @property DriverOld[] $driverOlds
 * @property OrderViews[] $orderViews
 * @property Parking[] $parkings
 * @property Payment[] $payments
 * @property PhoneLine[] $phoneLines
 * @property SmsLog[] $smsLogs
 * @property SmsTemplate[] $smsTemplates
 * @property Support[] $supports
 * @property TaxiTariff[] $taxiTariffs
 * @property TaxiTariffGroup[] $taxiTariffGroups
 * @property TenantAdditionalOption[] $tenantAdditionalOptions
 * @property TenantHasBonusSystem $tenantHasBonusSystem
 * @property TenantHasCarOption[] $tenantHasCarOptions
 * @property TenantHasCity[] $tenantHasCities
 * @property City[] $cities
 * @property TenantHasSms[] $tenantHasSms
 * @property TenantHelper[] $tenantHelpers
 * @property TenantNotificationLog[] $tenantNotificationLogs
 * @property TenantSetting[] $tenantSettings
 * @property TenantTariff[] $tenantTariffs
 * @property User[] $users
 * @property Worker[] $workers
 * @property WorkerGroup[] $workerGroups
 * @property WorkerTariff[] $workerTariffs
 * @property TenantTariffOrder[] $tenantTariffOrders
 */
class Tenant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_phone', 'email'], 'required'],
            [['create_time', 'archive'], 'safe'],
            [['status'], 'string'],
            [['company_name', 'full_company_name', 'legal_address', 'post_address', 'logo'], 'string', 'max' => 255],
            [['contact_name', 'contact_second_name', 'contact_last_name', 'contact_email', 'email', 'bookkeeper', 'ogrn', 'site', 'director_position'], 'string', 'max' => 45],
            [['contact_phone', 'company_phone', 'domain', 'inn'], 'string', 'max' => 15],
            [['kpp'], 'string', 'max' => 10],
            [['director'], 'string', 'max' => 50],
            [['domain'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_id' => 'Tenant ID',
            'company_name' => 'Company Name',
            'full_company_name' => 'Full Company Name',
            'legal_address' => 'Legal Address',
            'post_address' => 'Post Address',
            'contact_name' => 'Contact Name',
            'contact_second_name' => 'Contact Second Name',
            'contact_last_name' => 'Contact Last Name',
            'contact_phone' => 'Contact Phone',
            'contact_email' => 'Contact Email',
            'company_phone' => 'Company Phone',
            'domain' => 'Domain',
            'email' => 'Email',
            'inn' => 'Inn',
            'create_time' => 'Create Time',
            'bookkeeper' => 'Bookkeeper',
            'kpp' => 'Kpp',
            'ogrn' => 'Ogrn',
            'site' => 'Site',
            'archive' => 'Archive',
            'logo' => 'Logo',
            'director' => 'Director',
            'director_position' => 'Director Position',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutocallFiles()
    {
        return $this->hasMany(AutocallFile::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalls()
    {
        return $this->hasMany(Call::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPaymentLogs()
    {
        return $this->hasMany(CardPaymentLog::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientStatusEvents()
    {
        return $this->hasMany(ClientStatusEvent::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverOlds()
    {
        return $this->hasMany(DriverOld::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkings()
    {
        return $this->hasMany(Parking::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmsLogs()
    {
        return $this->hasMany(SmsLog::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmsTemplates()
    {
        return $this->hasMany(SmsTemplate::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffGroups()
    {
        return $this->hasMany(TaxiTariffGroup::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantAdditionalOptions()
    {
        return $this->hasMany(TenantAdditionalOption::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasBonusSystem()
    {
        return $this->hasOne(TenantHasBonusSystem::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('{{%tenant_has_city}}', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasSms()
    {
        return $this->hasMany(TenantHasSms::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHelpers()
    {
        return $this->hasMany(TenantHelper::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantNotificationLogs()
    {
        return $this->hasMany(TenantNotificationLog::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantSettings()
    {
        return $this->hasMany(TenantSetting::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffs()
    {
        return $this->hasMany(TenantTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroups()
    {
        return $this->hasMany(WorkerGroup::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffOrders()
    {
        return $this->hasMany(TenantTariffOrder::className(), ['tenant_id' => 'tenant_id']);
    }
}
