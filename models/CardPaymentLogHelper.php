<?php

namespace app\models;

use Yii;

class CardPaymentLogHelper
{
    /**
     * Added result of the payment gate operation to log
     * @param integer $tenantId
     * @param integer $orderId
     * @param integer $clientId
     * @param string $type
     * @param string $params
     * @param string $response
     * @param integer $result
     * @throws \yii\base\Exception
     */
    public static function log($tenantId, $orderId, $clientId, $type, $params,
        $response, $result)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $params = [
                'tenant_id' => $tenantId,
                'order_id'  => $orderId,
                'client_id' => $clientId,
                'type'      => $type,
                'params'    => json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'response'  => json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'result'    => $result,
            ];

            $model = new CardPaymentLog($params);

            if ($model->save()) {
                $transaction->commit();
                return;
            } else {
                $transaction->rollback();
                $message = implode("\n", $model->getFirstErrors());
            }
        } catch (\Exception $ex) {
            $transaction->rollback();
            $message = $ex->getMessage();
        }

        Yii::error(implode("\n", [
            "Error occurred saving payment information to log.",
            print_r($params, true),
            $message,
        ]), "bank-card");
    }
}
