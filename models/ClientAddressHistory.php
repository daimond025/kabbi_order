<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class ClientAddressHistory
 * @package app\modules\address\models
 *
 * @property int    $history_id
 * @property int    $client_id
 * @property int    $city_id
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $lat
 * @property string $lon
 * @property string $placeId
 * @property string $create_time
 *
 * @property array  $address_array
 */
class ClientAddressHistory extends ActiveRecord
{

    const COUNT_RECORDS = 20;

    public static function tableName()
    {
        return '{{%client_address_history}}';
    }

    public function rules()
    {
        return [
            [['client_id', 'city_id'], 'required'],
            [['city', 'street', 'house'], 'string', 'max' => 255],
            ['client_id', 'exist', 'targetClass' => Client::className()],
            ['city_id', 'exist', 'targetClass' => City::className()],
            [['lat', 'lon', 'placeId'], 'string', 'max' => 255],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->deleteExcessLastRecords();
        }
    }

    /**
     * Получить последние несколько адресов клиента
     *
     * @param $clientId
     * @param $cityId
     *
     * @return array|ActiveRecord[]
     */
    public static function findLast($clientId, $cityId)
    {
        return self::find()
            ->where([
                'client_id' => $clientId,
                'city_id'   => $cityId,
            ])
            ->limit(self::COUNT_RECORDS)
            ->orderBy(['create_time' => SORT_DESC])
            ->all();
    }

    public function updateTime()
    {
        $this->create_time = date('Y-m-d H:i:s');

        return $this->save(true, ['create_time']);
    }

    public function getLabel()
    {
        $array = [];

        if ($this->city) {
            $array[] = $this->city;
        }
        if ($this->street) {
            $array[] = $this->street;
        }
        if ($this->house) {
            $array[] = $this->house;
        }

        return implode(', ', $array);
    }

    protected function deleteExcessLastRecords()
    {
        $delete = self::find()
            ->where([
                'client_id' => $this->client_id,
                'city_id'   => $this->city_id,
            ])
            ->orderBy([
                'create_time' => SORT_DESC,
            ])
            ->offset(self::COUNT_RECORDS)
            ->all();
        foreach ($delete as $model) {
            /** @var $model $this */
            $model->delete();
        }
    }
}
