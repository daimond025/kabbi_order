<?php

namespace app\models;

use yii\db\ActiveRecord;

class TenantCityHasPosition extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%tenant_city_has_position}}';
    }

    public function rules()
    {
        return [];
    }
}
