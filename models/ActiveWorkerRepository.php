<?php


namespace app\models;


use app\models\interfaces\ActiveWorkerRepositoryInterface;
use Yii;

class ActiveWorkerRepository extends BaseRepository implements ActiveWorkerRepositoryInterface
{
    /**
     * @param int $callsign
     * @return mixed
     */
    public function getOne($callsign)
    {
        return unserialize($this->getDb()->executeCommand('hget', [$this->tenantId, $callsign]));
    }

    private function getDb()
    {
        return Yii::$app->redis_workers;
    }
}