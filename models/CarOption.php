<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%car_option}}".
 *
 * @property integer $option_id
 * @property string $name
 * @property integer $type_id
 *
 * @property AdditionalOption[] $additionalOptions
 * @property CarHasOption[] $carHasOptions
 * @property Car[] $cars
 * @property TransportType $type
 * @property TenantHasCarOption[] $tenantHasCarOptions
 * @property WorkerOptionDiscount[] $workerOptionDiscounts
 */
class CarOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type_id'], 'required'],
            [['type_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [
                ['type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TransportType::className(),
                'targetAttribute' => ['type_id' => 'type_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'name'      => 'Name',
            'type_id'   => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['additional_option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%car_has_option}}',
            ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOptionDiscounts()
    {
        return $this->hasMany(WorkerOptionDiscount::className(), ['car_option_id' => 'option_id']);
    }
}
