<?php

namespace app\models;

use yii\db\ActiveRecord;

class TenantHasCity extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tenant_has_city}}';
    }

    public function rules()
    {
        return [];
    }
}
