<?php


namespace app\behaviors;


use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Controller;
use yii\web\Response;

class StopJsonEncode extends Behavior
{
    public $actions = [];

    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    /**
     * @param ActionEvent $action the action to be executed.
     * @return boolean whether the action should continue to be executed.
     */
    public function beforeAction($action)
    {
        if (in_array($action->action->id, $this->actions)) {
            $this->stopJsonEncode();
        }

        return true;
    }

    protected function stopJsonEncode()
    {
        $response = Yii::$app->response;
        $headers = $response->headers;
        $response->format = Response::FORMAT_RAW;
        $headers->set('Content-Type', 'application/json; charset=UTF-8');
    }
}