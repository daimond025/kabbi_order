<?php

namespace app\components;

use app\models\CardPaymentLog;
use app\models\CardPaymentLogHelper;
use paymentGate\ProfileService;
use Yii;
use app\components\curl\exceptions\RestException;
use yii\base\Component;

/**
 * Model of bank card
 */
class Card extends Component
{
    public $url;
    public $path;
    public $profileService;

    public function __construct(ProfileService $profileService, array $config = [])
    {
        parent::__construct($config);

        $this->profileService = $profileService;
    }

    public function init()
    {
        parent::init();

        $this->url = rtrim($this->url, '/');
    }

    /**
     * Getting a list of bank cards
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $clientId
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getCards($tenantId, $cityId, $clientId)
    {
        $profile = $this->profileService->getProfile($tenantId, $cityId);


        $response = Yii::$app->restCurl->get(implode("/",
            [
                $this->url,
                $this->path,
                $profile,
                "client_{$clientId}",
            ]));

        return isset($response['result']) ? $response['result'] : [];
    }

    public function doAction($type, $tenantId, $clientId, $url, $postParams)
    {
        try {
            switch ($type) {
                case CardPaymentLog::TYPE_ADD_CARD:
                    $response = Yii::$app->restCurl->post($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_CHECK_CARD:
                    $response = Yii::$app->restCurl->put($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_REMOVE_CARD:
                    $response = Yii::$app->restCurl->delete($url, $postParams);
                    break;
                default:
                    throw new \yii\base\NotSupportedException("Type of action is not supported by log");
            }
            $result = CardPaymentLog::RESULT_OK;
        } catch (RestException $ex) {
            $response = [
                'message'  => $ex->getMessage(),
                'code'     => $ex->getCode(),
                'response' => $ex->getResponse(),
            ];
            $exception = $ex;
            $result = CardPaymentLog::RESULT_ERROR;
        } catch (\Exception $ex) {
            $response = [
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode(),
            ];
            $exception = $ex;
            $result = CardPaymentLog::RESULT_ERROR;
        }

        CardPaymentLogHelper::log($tenantId, null, $clientId, $type, $postParams,
            $response, $result);

        if ($result == CardPaymentLog::RESULT_OK) {
            return $response;
        } else {
            Yii::error(implode("\n",
                [
                    "An error has occurred in step '{$type}'",
                    "Url: {$url}",
                    print_r($postParams, true),
                    print_r($response, true),
                ]), 'bank-card');
            throw new \Exception();
        }
    }

    /**
     * Binding bank card
     *
     * @param integer $tenantId
     * @param integer $clientId
     *
     * @throws RestException
     * @return array
     */
    public function create($tenantId, $clientId)
    {
        $url = implode("/",
            [
                $this->url,
                $this->path,
                $tenantId,
                "client_{$clientId}",
            ]);

        // empty array is bugfix for common\components\curl\Curl
        $postParams = ['empty' => 'empty'];

        return $this->doAction(CardPaymentLog::TYPE_ADD_CARD, $tenantId,
            $clientId, $url, $postParams);
    }

    /**
     * Check whether the bank card is binded
     *
     * @param integer $tenantId
     * @param integer $clientId
     * @param string $orderId
     *
     * @throws RestException
     * @return array
     */
    public function check($tenantId, $clientId, $orderId)
    {
        $url = implode("/",
            [
                $this->url,
                $this->path,
                $tenantId,
                "client_{$clientId}",
            ]);

        return $this->doAction(CardPaymentLog::TYPE_CHECK_CARD, $tenantId,
            $clientId, $url, compact('orderId'));
    }

    /**
     * Delete bank card
     *
     * @param integer $tenantId
     * @param integer $clientId
     * @param string $pan
     *
     * @throws RestException
     * @return boolean
     */
    public function delete($tenantId, $clientId, $pan)
    {
        $url = implode("/",
            [
                $this->url,
                $this->path,
                $tenantId,
                "client_{$clientId}",
                $pan,
            ]);

        return $this->doAction(CardPaymentLog::TYPE_REMOVE_CARD, $tenantId, $clientId,
            $url, []);
    }

    /**
     * Check whether bank card is valid
     *
     * @param integer $tenantId
     * @param integer $cityId
     * @param integer $clientId
     * @param string  $pan
     *
     * @return boolean
     * @throws \yii\db\Exception
     */
    public function isValid($tenantId, $cityId, $clientId, $pan)
    {
        $cards = $this->getCards($tenantId, $cityId, $clientId);

        return in_array($pan, $cards);
    }
}
