<?php

namespace app\components\helpers;

use bonusSystem\BonusSystem;

class ApiClientHelper
{

    const GREATER_THAN_OR_EQUALS_TO = '>=';


    public static function isVersionAppForBonusSystem($clientVersion)
    {
        return version_compare(
            $clientVersion,
            BonusSystem::VERSION_APP_FOR_BONUS,
            self::GREATER_THAN_OR_EQUALS_TO
        );
    }
}
