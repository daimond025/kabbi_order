<?php

namespace app\components\serviceEngine;

use app\components\consul\ConsulService;
use app\components\consul\exceptions\ConsulServiceException;
use yii\base\Object;
use Yii;

/**
 * Class ServiceEngine
 * @package frontend\components\serviceEngine
 */
class ServiceEngine extends Object
{
    const SHIFT_END_REASON = 'operator_service';
    const SHIFT_END_EVENT_SENDER_TYPE = 'user';
    const CONSUL_SERVICE_NAME = 'service_engine';

    const SUCCESS_RESULT = 1;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }


    /**
     * @param $tenantId
     * @param $tenantLogin
     * @param $workerCallsign
     * @param $shiftId
     * @param $shiftLifeTime
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function createWorkerShiftHandler($tenantId, $tenantLogin, $workerCallsign, $shiftId, $shiftLifeTime)
    {
        $result = $this->sendRequest("watch_worker_shift", [
            'tenant_login'    => (string)$tenantLogin,
            'worker_callsign' => (string)$workerCallsign,
            'shift_life_time' => (string)$shiftLifeTime,
            'tenant_id'       => (string)$tenantId,
            'shift_id'        => (string)$shiftId,
        ]);

        return isset($result['result']) && $result['result'] == self::SUCCESS_RESULT;
    }

    /**
     * Send order to service_engine
     * @param $orderId
     * @param $tenantId
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function neworderAuto($orderId, $tenantId)
    {

        $params = [
            'order_id'  => (string)$orderId,
            'tenant_id' => (string)$tenantId,
        ];

        $response = $this->sendRequest('neworder_auto', $params);

        return isset($response['result']) && $response['result'] == self::SUCCESS_RESULT;
    }


    /**
     * Can offer order to worker
     * @param $tenantId
     * @param $orderId
     * @param $callsign
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function canOfferOrderToWorker($tenantId, $orderId, $callsign, $positionID, $order_city)
    {
        $params = [
            'tenant_id'       => $tenantId,
            'order_id'        => $orderId,
            'worker_callsign' => $callsign,
            'position_id' => $positionID,
            'order_city' => $order_city,
        ];

        $response = $this->sendRequest('can_offer_order_for_worker', $params);
        return isset($response['result']) && $response['result'] == self::SUCCESS_RESULT;
    }

    /**
     * Close worker shift
     * @param $tenantId
     * @param $tenantLogin
     * @param $workerCallsign
     * @param $userId
     * @param $shiftId
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function closeWorkerShift($tenantId, $tenantLogin, $workerCallsign, $userId, $shiftId)
    {
        $result = $this->sendRequest('close_worker_shift', [
            'tenant_id'                   => (string)$tenantId,
            'tenant_login'                => (string)$tenantLogin,
            'worker_callsign'             => (string)$workerCallsign,
            'shift_end_reason'            => (string)self::SHIFT_END_REASON,
            'shift_end_event_sender_type' => (string)self::SHIFT_END_EVENT_SENDER_TYPE,
            'shift_end_event_sender_id'   => (string)$userId,
            'shift_id'                    => (string)$shiftId,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `close_worker_shift` (tenant_id={$tenantId}, tenant_login={$tenantLogin}, worker_callsign={$workerCallsign}, shift_id={$shiftId}): . {$result['error']}.");
        }
        return isset($result['result']) && $result['result'] == self::SUCCESS_RESULT;
    }


    /**
     * Send request
     * @param $method
     * @param $params
     * @return bool|mixed
     * @throws \yii\base\InvalidConfigException
     */
    private function sendRequest($method, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }
        $url = $this->getUrl($method) . "?" . $params;
        if (!$url) {
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencode',
        ]);
        $result = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url
     * @param $method
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function getUrl($method)
    {
        try {
            /**
             * @var $service ConsulService;
             */
            $service = app()->get('consulService');
            return $service->createMethodUrl(self::CONSUL_SERVICE_NAME, $method);
        } catch (ConsulServiceException $ex) {
            Yii::error('Ошибка получения location из consul  для:' . self::CONSUL_SERVICE_NAME . ". Ошибка: " . $ex->getMessage());
            return false;
        }
    }
}
