<?php

namespace app\components;

use app\models\TenantSetting;


/**
 * Middleware for taxi-service-api route-analyzer methods
 *
 * @author Sergey K.
 */
class TaxiRouteAnalyzer extends \yii\base\Component
{
    /**
     * @var string
     */
    public $url;

    /**
     * Timeout of request to taxi-service-api
     * @var int
     */
    public $timeout = 15;

    /**
     * Route analyze. Return time, distance, cost and other data of route.
     *
     * @param string $tenantId
     * @param string $cityId
     * @param array $newAddressArray
     * @param string|array $additionalOption format: 1,2,3 | [1,2,3]
     * @param string $tariffId
     * @param string $orderDate format: d-m-Y H:i:s
     * @param integer $clientId
     *
     * @return array|mixed
     */
    public function analyzeRoute($tenantId, $cityId, $newAddressArray, $additionalOption, $tariffId, $orderDate, $clientId = null)
    {
        $newAddressArray = json_encode(array_values($newAddressArray));
        $newAddressArray = urlencode($newAddressArray);
        if (is_array($additionalOption)) {
            $additionalOption = implode(',', $additionalOption);
        }
        $params = [
            'tenant_id'     => $tenantId,
            'city_id'       => $cityId,
            'address_array' => $newAddressArray,
            'additional'    => $additionalOption,
            'tariff_id'     => $tariffId,
            'order_time'    => $orderDate,
            'lang'          => $this->getLangParam($tenantId, $cityId),
            'geocoder_type' => $this->getGeocoderType($tenantId, $cityId),
            'client_id'     => $clientId,
        ];



        $result = $this->sendPostRequest('analyze_route', 1, $params);

        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    private function getLangParam($tenantId, $cityId)
    {
        $lang = TenantSetting::getSettingValue($tenantId, 'LANGUAGE', $cityId);

        return $lang ?: 'ru';
    }

    private function getGeocoderType($tenantId, $cityId)
    {
        $geocoderType = TenantSetting::getSettingValue($tenantId, 'GEOCODER_TYPE', $cityId);

        return $geocoderType ?: 'ru';
    }

    /**
     * Method add coords for address if they'a missing
     *
     * @param array $address
     * @param int $tenantId
     * @param int $cityId
     *
     * @return array|mixed
     */
    public function fixCoords($address, $tenantId, $cityId)
    {
        $address = json_encode($address);
        $address = urlencode($address);
        $params = [
            'address'       => $address,
            'lang'          => $this->getLangParam($tenantId, $cityId),
            'geocoder_type' => $this->getGeocoderType($tenantId, $cityId),
        ];
        $result = $this->sendGetRequest('fix_coords', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * The method will be defined the point belongs to what parkings
     *
     * @param string $tenantId
     * @param string $cityId
     * @param string $lat
     * @param string $lon
     * @param bool $jsonFormat
     *
     * @return array|mixed
     */
    public function getParkingByCoords($tenantId, $cityId, $lat, $lon, $jsonFormat = false)
    {
        $jsonFormat = ($jsonFormat == true) ? 1 : 0;
        $params = [
            'tenant_id'   => $tenantId,
            'city_id'     => $cityId,
            'lat'         => $lat,
            'lon'         => $lon,
            'json_format' => $jsonFormat,
        ];
        $result = $this->sendGetRequest('get_parking_by_coords', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Method formatig parking to default type
     *
     * @param array $arParking
     * @param bool $reverse
     */
    public function formatParkings($arParking, $reverse = true)
    {
        $reverse = ($reverse == true) ? 1 : 0;
        $arParking = json_encode($arParking);
        $arParking = urlencode($arParking);
        $params = [
            'parkings' => $arParking,
            'reverse'  => $reverse,
        ];
        $result = $this->sendPostRequest('format_parkings', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Find at witch parking point is in
     *
     * @param string $lat
     * @param string $lon
     * @param array $arParking
     */
    public function findPointLocationInParking($lat, $lon, $arParking)
    {
        $arParking = json_encode($arParking);
        $arParking = urlencode($arParking);
        $params = [
            'lat'      => $lat,
            'lon'      => $lon,
            'parkings' => $arParking,
        ];
        $result = $this->sendPostRequest('find_point_location_in_parking', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Определение типа тарифа
     *
     * @param int $tariff
     * @param string $date 19.04.2016 11:35:00
     *
     * @return array ["15","22","23"]
     */
    public function addOptions($tariff, $date)
    {
        $params = [
            'tariff_id' => $tariff,
            'date'      => $date,
        ];
        $result = $this->sendPostRequest('add_options', 1, $params);

        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Send GET request to taxi-service-api
     *
     * @param string $method
     * @param int $version
     * @param array $params
     *
     * @return mixed
     */
    private function sendGetRequest($method, $version, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }

        if (strlen($params) > 0) {
            $url = $this->getUrl($method, $version) . "?" . $params;
        } else {
            $url = $this->getUrl($method, $version);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $result = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Send POST request  taxi-service-api
     *
     * @param string $method
     * @param int $version
     * @param array $params
     *
     * @return mixed
     */
    private function sendPostRequest($method, $version, $params)
    {
        $url = $this->getUrl($method, $version);
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $result = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url of taxi-service-api route-analyzer method
     *
     * @param string $method
     * @param int $version
     *
     * @return string
     */
    private function getUrl($method, $version)
    {
        return "{$this->url}v{$version}/route-analyzer/{$method}";
    }
}
