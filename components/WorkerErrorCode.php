<?php

namespace app\components;


class WorkerErrorCode
{
    const OK                              = 0;
    const INTERNAL_ERROR                  = 1;
    const INCORRECTLY_SIGNATURE           = 2;
    const UNKNOWN_REQUEST                 = 3;
    const BAD_PARAM                       = 4;
    const MISSING_INPUT_PARAMETER         = 5;
    const EMPTY_VALUE                     = 6;
    const EMPTY_DATA_IN_DATABASE          = 7;
    const BLACK_LIST                      = 8;
    const ACCESS_DENIED                   = 9;
    const WORKER_NOT_EXIST                = 10;
    const WORKER_ALLREADY_ON_SHIFT        = 40;
    const BAD_BALANCE                     = 41;
    const CAR_IS_BUSY                     = 42;
    const BAD_TARIFF                      = 44;
    const SHIFT_IS_CLOSED                 = 50;
    const PARKING_NOT_EXIST               = 60;
    const ORDER_IS_BUSY                   = 70;
    const WORKER_BLOCKED                  = 80;
    const WORKER_PRE_ORDER_BLOCKED        = 81;
    const OLD_APP_VERSION                 = 82;
    const WORKER_LIMIT                    = 83;
    const CITY_BLOCKED                    = 84;
    const WORKER_ALLREADY_BLOCKED         = 85;
    const FORBIDDEN_ACTION                = 100;
    const DENY_REFUSE_ORDER               = 110;
    const WORKER_ALREADY_HAS_ACTIVE_ORDER = 111;
    const ORDER_ASSIGN_LIMIT              = 112;

    const ORDER_ASSIGN_WORKER             = 125;
    const ORDER_SELECT_DRIVER             = 126;

    // повторение измнеение сатауса заказа
    const ORDER_REPEAT_STATUS            = 500;


    const ERROR_MESSAGES = [
        self::OK                              => 'OK',
        self::INTERNAL_ERROR                  => 'INTERNAL_ERROR',
        self::INCORRECTLY_SIGNATURE           => 'INCORRECTLY_SIGNATURE',
        self::UNKNOWN_REQUEST                 => 'UNKNOWN_REQUEST',
        self::BAD_PARAM                       => 'BAD_PARAM',
        self::MISSING_INPUT_PARAMETER         => 'MISSING_INPUT_PARAMETER',
        self::EMPTY_VALUE                     => 'EMPTY_VALUE',
        self::EMPTY_DATA_IN_DATABASE          => 'EMPTY_DATA_IN_DATABASE',
        self::BLACK_LIST                      => 'BLACK_LIST',
        self::ACCESS_DENIED                   => 'ACCESS_DENIED',
        self::WORKER_NOT_EXIST                => 'WORKER_NOT_EXIST',
        self::WORKER_ALLREADY_ON_SHIFT        => 'WORKER_ALLREADY_ON_SHIFT',
        self::BAD_BALANCE                     => 'BAD_BALANCE',
        self::CAR_IS_BUSY                     => 'CAR_IS_BUSY',
        self::BAD_TARIFF                      => 'BAD_TARIFF',
        self::SHIFT_IS_CLOSED                 => 'SHIFT_IS_CLOSED',
        self::PARKING_NOT_EXIST               => 'PARKING_NOT_EXIST',
        self::ORDER_IS_BUSY                   => 'ORDER_IS_BUSY',
        self::WORKER_BLOCKED                  => 'WORKER_BLOCKED',
        self::WORKER_PRE_ORDER_BLOCKED        => 'WORKER_PRE_ORDER_BLOCKED',
        self::OLD_APP_VERSION                 => 'OLD_APP_VERSION',
        self::WORKER_LIMIT                    => 'WORKER_LIMIT',
        self::CITY_BLOCKED                    => 'CITY_BLOCKED',
        self::WORKER_ALLREADY_BLOCKED         => 'WORKER_ALLREADY_BLOCKED',
        self::FORBIDDEN_ACTION                => 'FORBIDDEN_ACTION',
        self::DENY_REFUSE_ORDER               => 'DENY_REFUSE_ORDER',
        self::WORKER_ALREADY_HAS_ACTIVE_ORDER => 'WORKER_ALREADY_HAS_ACTIVE_ORDER',
        self::ORDER_ASSIGN_LIMIT              => 'ORDER_ASSIGN_LIMIT',
        self::ORDER_ASSIGN_WORKER              => 'WORKER_ASSIGN_TO_ORDER',
        self::ORDER_SELECT_DRIVER              => 'ORDER_SELECT_DRIVER',
        self::ORDER_REPEAT_STATUS              => 'ORDER_REPEAT_STATUS',
    ];

    public $errorCodeData = self::ERROR_MESSAGES;

    /**
     * Getting error data by code
     *
     * @param $errorCode
     *
     * @return array
     */
    public static function getErrorData($errorCode)
    {
        return [
            'code' => $errorCode,
            'info' => self::ERROR_MESSAGES[$errorCode],
        ];
    }
}