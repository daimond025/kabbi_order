<?php

namespace bonusSystem\models\ar;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%bonus_system}}".
 *
 * @property integer              $id
 * @property string               $name
 * @property integer              $created_at
 * @property integer              $updated_at
 *
 * @property ClientBonus[]        $clientBonuses
 * @property TenantHasBonusSystem $tenantHasBonusSystem
 */
class BonusSystem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonus_system}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['bonus_system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasBonusSystem()
    {
        return $this->hasOne(TenantHasBonusSystem::className(), ['bonus_system_id' => 'id']);
    }
}
