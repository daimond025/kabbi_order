<?php

namespace bonusSystem\models\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_bonus_gootax}}".
 *
 * @property integer     $id
 * @property integer     $bonus_id
 * @property string      $actual_date
 * @property string      $bonus_type
 * @property string      $bonus
 * @property string      $min_cost
 * @property string      $payment_method
 * @property string      $max_payment_type
 * @property string      $max_payment
 * @property string      $bonus_app_type
 * @property string      $bonus_app
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property ClientBonus $bonus0
 */
class ClientBonusGootax extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus_gootax}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_id', 'bonus'], 'required'],
            [['bonus_id', 'created_at', 'updated_at'], 'integer'],
            [['actual_date', 'bonus_type', 'payment_method', 'max_payment_type', 'bonus_app_type'], 'string'],
            [['bonus', 'min_cost', 'max_payment', 'bonus_app'], 'number'],
            [['bonus_id'], 'unique'],
            [
                ['bonus_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ClientBonus::className(),
                'targetAttribute' => ['bonus_id' => 'bonus_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'bonus_id'         => 'Bonus ID',
            'actual_date'      => 'Actual Date',
            'bonus_type'       => 'Bonus Type',
            'bonus'            => 'Bonus',
            'min_cost'         => 'Min Cost',
            'payment_method'   => 'Payment Method',
            'max_payment_type' => 'Max Payment Type',
            'max_payment'      => 'Max Payment',
            'bonus_app_type'   => 'Bonus App Type',
            'bonus_app'        => 'Bonus App',
            'created_at'       => 'Created At',
            'updated_at'       => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus0()
    {
        return $this->hasOne(ClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }
}
