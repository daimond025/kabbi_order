<?php

namespace bonusSystem\models\UDSGame;

/**
 * Class Operation
 * @package bonusSystem\models\UDSGame
 */
class Operation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $scoresDelta;

    /**
     * @var float
     */
    private $scoresNew;

    /**
     * @var float
     */
    private $cash;

    /**
     * @var float
     */
    private $total;


    /**
     * Operation constructor.
     *
     * @param int   $id
     * @param float $scoresDelta
     * @param float $scoresNew
     * @param float $cash
     * @param float $total
     */
    public function __construct($id, $scoresDelta, $scoresNew, $cash, $total)
    {
        $this->id          = $id;
        $this->scoresDelta = $scoresDelta;
        $this->scoresNew   = $scoresNew;
        $this->cash        = $cash;
        $this->total       = $total;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getScoresDelta()
    {
        return $this->scoresDelta;
    }

    /**
     * @return float
     */
    public function getScoresNew()
    {
        return $this->scoresNew;
    }

    /**
     * @return float
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

}