<?php

namespace bonusSystem\models\UDSGame;

/**
 * Class Company
 * @package bonusSystem\models\UDSGame
 */
class Company
{
    const CHARGE_SCORES = 'CHARGE_SCORES';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $promoCode;

    /**
     * @var string
     */
    private $baseDiscountPolicy;

    /**
     * @var MarketingSettings
     */
    private $marketingSettings;

    /**
     * Company constructor.
     *
     * @param int               $id
     * @param string            $name
     * @param string            $promoCode
     * @param string            $baseDiscountPolicy
     * @param MarketingSettings $marketingSettings
     */
    public function __construct(
        $id,
        $name,
        $promoCode,
        $baseDiscountPolicy,
        MarketingSettings $marketingSettings
    ) {
        $this->id                 = $id;
        $this->name               = $name;
        $this->promoCode          = $promoCode;
        $this->baseDiscountPolicy = $baseDiscountPolicy;
        $this->marketingSettings  = $marketingSettings;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    /**
     * @return string
     */
    public function getBaseDiscountPolicy()
    {
        return $this->baseDiscountPolicy;
    }

    /**
     * @return MarketingSettings
     */
    public function getMarketingSettings()
    {
        return $this->marketingSettings;
    }

}