<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class GetPaymentStrategyException
 * @package bonusSystem\exceptions
 */
class GetPaymentStrategyException extends Exception
{

}