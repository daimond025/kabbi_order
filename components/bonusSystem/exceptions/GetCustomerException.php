<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class GetCustomerException
 * @package bonusSystem\exceptions
 */
class GetCustomerException extends Exception
{

}