<?php

namespace app\components;

use yii\base\Object;

class OrderError extends Object
{
    const NO_RESULT                        = 0;
    const SUCCESS                          = 100;
    const ERROR_SAVING                     = 101;
    const INVALID_ORDER_TIME               = 102;
    const OLD_DATA                         = 103;
    const FORBIDDEN_CHANGE_ORDER_TIME      = 104;
    const INVALID_VALUE                    = 105;
    const FORBIDDEN_CHANGE_WORKER          = 106;
    const INTERNAL_ERROR                   = 107;
    const COMPLETE_ORDER_ERROR             = 108;
    const COMPANY_BLOCKED_ERROR            = 109;
    const NO_RELEVANT_EXCHANGE_CONNECTIONS = 110;
    const FORBIDDEN_CHANGE_STATUS_ID       = 111;

    const BAD_PARAM                       = 4;
    const MISSING_PARAMETER               = 5;
    const BLACK_LIST                      = 8;
    const BAD_PAY_TYPE                    = 11;
    const NO_MONEY                        = 12;
    const INVALID_PAN                     = 15;
    const INVALID_PROMO_CODE              = 40;
    const BONUS_SYSTEM_IS_NOT_ACTIVATED   = 41;
    const ORDER_TIME_INCORRECT            = 42;
    const EXPECTED_ARRAY                  = 43;
    const TARIFF_ERROR                    = 44;
    const WORKER_SHIFT_IS_CLOSED          = 45;
    const WORKER_BLOCKED                  = 46;
    const WORKER_ALREADY_HAS_ACTIVE_ORDER = 47;
    const NO_PASSENGER_PHONE              = 48;

    private $errorCode = self::NO_RESULT;
    private $errorMessage;

    /**
     * @return array
     */
    public function getMap()
    {
        return [
            self::NO_RESULT                   => 'The action is not performed',
            self::SUCCESS                     => 'The operation was successfully completed',
            self::ERROR_SAVING                => 'Error saving the order',
            self::INVALID_ORDER_TIME          => 'Order time is incorrect',
            self::OLD_DATA                    => 'Old data for updating',
            self::FORBIDDEN_CHANGE_ORDER_TIME => 'Forbidden to change the order time',
            self::INVALID_VALUE               => 'Invalid attribute value',
            self::FORBIDDEN_CHANGE_WORKER     => 'Forbidden to change the worker',
            self::INTERNAL_ERROR              => 'Internal error',
            self::COMPLETE_ORDER_ERROR        => 'Complete order error',
            self::COMPANY_BLOCKED_ERROR       => 'Company is blocked',
            self::FORBIDDEN_CHANGE_STATUS_ID  => 'Forbidden to change the order status id',

            self::TARIFF_ERROR                  => 'TARIFF_ERROR',
            self::EXPECTED_ARRAY                => 'EXPECTED_ARRAY',
            self::ORDER_TIME_INCORRECT          => 'ORDER_TIME_INCORRECT',
            self::BONUS_SYSTEM_IS_NOT_ACTIVATED => 'BONUS_SYSTEM_IS_NOT_ACTIVATED',
            self::INVALID_PROMO_CODE            => 'INVALID_PROMO_CODE',
            self::INVALID_PAN                   => 'INVALID_PAN',
            self::NO_MONEY                      => 'NO_MONEY',
            self::BAD_PAY_TYPE                  => 'BAD_PAY_TYPE',
            self::BLACK_LIST                    => 'BLACK_LIST',
            self::MISSING_PARAMETER             => 'MISSING_PARAMETER',
            self::BAD_PARAM                     => 'BAD_PARAM',
        ];
    }

    /**
     * @return string|null
     */
    public function getErrorMessage()
    {

        if (empty($this->errorMessage)) {
            $errorMap = $this->getMap();
            $this->errorMessage = isset($errorMap[$this->errorCode]) ? $errorMap[$this->errorCode] : null;
        }

        return $this->errorMessage;
    }

    /**
     * Getting error data by code
     * @return array
     */
    public function getData()
    {
        return [
            'code' => $this->errorCode,
            'info' => $this->getErrorMessage(),
        ];
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setErrorCode($code)
    {
        $this->errorCode = $code;
        $this->errorMessage = null;

        return $this;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setErrorMessage($message)
    {
        $this->errorMessage = $message;

        return $this;
    }
}