<?php

namespace app\components\exchange;

use app\components\exchange\exceptions\ExchangeServiceException;
use app\models\ExchangeConnection;
use app\models\OrderChangeData;
use GuzzleHttp\Client;
use yii\base\Object;
use app\components\exchange\exceptions\NoRelevantExchangeConnections;
use app\components\exchange\exceptions\NotSupportedExchangeProgram;
use app\models\ActiveOrderRepository;
use app\components\OrderError;
use app\modules\v1\models\OrderChangeRuleManager;
use app\models\OrderStatusRecord;


class ServiceExchange extends Object
{


    /* @var string */
    public $baseUrl;

    /* @var int */
    public $timeout = 15;

    /* @var int */
    public $connectionTimeout = 15;

    /* @var Client */
    private $httpClient;

    const UP_AND_UP_PROGRAM_ID = 1;

    /* @var OrderError */
    protected $response;


    /**
     * @inherited
     */
    public function init()
    {
        parent::init();

        $this->response = new OrderError();
        $this->httpClient = new Client([
            'base_uri'          => $this->baseUrl,
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
    }


    /**
     * Update order at exchange
     * @param $order
     * @param $eventSubject
     * @return bool
     * @throws \Exception
     * @throws NoRelevantExchangeConnections
     * @throws NotSupportedExchangeProgram
     */
    public function updateAtExchange($order, $eventSubject)
    {
        $exchangeConnectionId = $order['exchange_info']['exchange_connection_id'];
        $exchangeConnection = $this->getExchangeConnectionById($exchangeConnectionId);
        $orderId = $order['order_id'];
        if (!$exchangeConnection) {
            throw new NoRelevantExchangeConnections('No relevant exchange connections for order id=' . $order['order_id']);
        }
        $exchangeProgramId = $exchangeConnection->exchange_program_id;
        if (!in_array($exchangeProgramId, $this->getSupportedExchangeProgramIds(), false)) {
            throw new NotSupportedExchangeProgram('Exchange with id = ' . $exchangeProgramId . ' not supported by service');
        }
        switch ($exchangeProgramId) {
            case self::UP_AND_UP_PROGRAM_ID:
                $exchangeUrl = $this->baseUrl . 'upup/tenants/' . $order['tenant_login'] . '/orders/update/' . $orderId;
                $formParams = [
                    'clid'      => $exchangeConnection->clid,
                    'api_key'   => $exchangeConnection->key_api,
                    'show_auto' => $exchangeConnection->is_show_automate,
                    'rate'      => $exchangeConnection->rate,
                ];
                break;
            default:
                $exchangeUrl = "";
                $formParams = [];
                break;
        }
        $response = $this->httpClient->request('POST', $exchangeUrl, [
            'connection_timeout' => $this->connectionTimeout,
            'timeout'            => $this->timeout,
            'form_params'        => $formParams
        ]);

        if ($response->getStatusCode() === 200) {
            OrderChangeData::saveChangeData(
                $order['tenant_id'],
                $order['order_id'],
                $eventSubject,
                'action',
                'update',
                $order['exchange_info']['exchange_order_id'],
                OrderChangeData::OBJECT_TYPE_EXCHANGE
            );
            return true;
        }
        \Yii::error('status code = ' . $response->getStatusCode());
        \Yii::error($response->getBody()->getContents());
        return false;
    }


    /**
     * Delete order from exchange
     * @param $order
     * @param $eventSubject
     * @return bool
     * @throws \Exception
     * @throws NoRelevantExchangeConnections
     * @throws NotSupportedExchangeProgram
     */
    public function deleteFromExchange($order, $eventSubject)
    {

        $exchangeConnectionId = $order['exchange_info']['exchange_connection_id'];
        $exchangeConnection = $this->getExchangeConnectionById($exchangeConnectionId);
        $orderId = $order['order_id'];
        if (!$exchangeConnection) {
            throw new NoRelevantExchangeConnections('No relevant exchange connections for order id=' . $order['order_id']);
        }
        $exchangeProgramId = $exchangeConnection->exchange_program_id;
        if (!in_array($exchangeProgramId, $this->getSupportedExchangeProgramIds(), false)) {
            throw new NotSupportedExchangeProgram('Exchange with id = ' . $exchangeProgramId . ' not supported by service');
        }
        switch ($exchangeProgramId) {
            case self::UP_AND_UP_PROGRAM_ID:
                $exchangeUrl = $this->baseUrl . 'upup/tenants/' . $order['tenant_login'] . '/orders/delete/' . $orderId;
                $formParams = [
                    'clid'      => $exchangeConnection->clid,
                    'api_key'   => $exchangeConnection->key_api,
                    'show_auto' => $exchangeConnection->is_show_automate,
                    'rate'      => $exchangeConnection->rate,
                ];
                break;
            default:
                $exchangeUrl = "";
                $formParams = [];
                break;
        }
        $response = $this->httpClient->request('POST', $exchangeUrl, [
            'connection_timeout' => $this->connectionTimeout,
            'timeout'            => $this->timeout,
            'form_params'        => $formParams
        ]);

        if ($response->getStatusCode() === 200) {
            $this->deleteExchangeData($order);
            OrderChangeData::saveChangeData(
                $order['tenant_id'],
                $order['order_id'],
                $eventSubject,
                'action',
                'delete',
                $order['exchange_info']['exchange_order_id'],
                OrderChangeData::OBJECT_TYPE_EXCHANGE
            );
            return true;
        }
        \Yii::error('status code = ' . $response->getStatusCode());
        \Yii::error($response->getBody()->getContents());
        return false;
    }


    /**
     * Send to exchange
     * @param $tenantId
     * @param $orderId
     * @param $eventSubject
     * @return bool
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws NoRelevantExchangeConnections
     * @throws NotSupportedExchangeProgram
     * @throws ExchangeServiceException
     */
    public function sendToExchange($tenantId, $orderId, $eventSubject)
    {
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $tenantId]);
        $order = $activeOrderRepository->getOne($orderId);
        /**
         * @var $exchangeConnection ExchangeConnection
         */
        $exchangeConnection = $this->getRelevantExchangeConnection($order['tenant_id'], $order['city_id'], $order['position_id']);
        if (!$exchangeConnection) {
            throw new NoRelevantExchangeConnections('No relevant exchange connections for order id=' . $order['order_id']);
        }
        $exchangeProgramId = $exchangeConnection->exchange_program_id;
        if (!in_array($exchangeProgramId, $this->getSupportedExchangeProgramIds(), false)) {
            throw new NotSupportedExchangeProgram('Exchange with id = ' . $exchangeProgramId . ' not supported by service');
        }
        $status = OrderStatusRecord::findOne($order['status_id']);
        /**
         * @var $manager OrderChangeRuleManager
         */
        $manager = \Yii::createObject(OrderChangeRuleManager::class);
        if (!$manager->canSendToExchange($status, $eventSubject)) {
            throw new ExchangeServiceException('You can not send order to exchange at this status');
        }
        switch ($exchangeProgramId) {
            case self::UP_AND_UP_PROGRAM_ID:
                $exchangeUrl = $this->baseUrl . "upup/tenants/" . $order['tenant_login'] . "/orders/add";
                $formParams = [
                    'order_id'  => $order['order_id'],
                    'clid'      => $exchangeConnection->clid,
                    'api_key'   => $exchangeConnection->key_api,
                    'show_auto' => $exchangeConnection->is_show_automate,
                    'rate'      => $exchangeConnection->rate,
                ];
                break;
            default:
                $exchangeUrl = "";
                $formParams = [];
                break;
        }
        $response = $this->httpClient->request('POST', $exchangeUrl, [
            'connection_timeout' => $this->connectionTimeout,
            'timeout'            => $this->timeout,
            'form_params'        => $formParams
        ]);
        $exchangeOrderId = json_decode($response->getBody()->getContents(), true)['exchange_order_id'];
        if ($exchangeOrderId) {
            $exchangeParams = [
                'exchange_program_id'    => $exchangeProgramId,
                'exchange_connection_id' => $exchangeConnection->exchange_connection_id,
                'exchange_order_id'      => $exchangeOrderId,
                'updated_at'             => time(),
            ];
            $this->persistExchangeData($order, $exchangeParams);
            OrderChangeData::saveChangeData(
                $tenantId,
                $orderId,
                $eventSubject,
                'action',
                'add',
                $exchangeOrderId,
                OrderChangeData::OBJECT_TYPE_EXCHANGE
            );
            return true;
        }
        return false;
    }

    /**
     * Get order info from exchange
     * @param $order
     * @return bool|mixed
     * @throws NoRelevantExchangeConnections
     * @throws NotSupportedExchangeProgram
     */
    public function getOrderInfoFromExchange($order)
    {
        $orderId = $order['order_id'];
        /**
         * @var $exchangeConnection ExchangeConnection
         */
        $exchangeConnection = $this->getRelevantExchangeConnection($order['tenant_id'], $order['city_id'], $order['position_id']);
        if (!$exchangeConnection) {
            throw new NoRelevantExchangeConnections('No relevant exchange connections for order id=' . $orderId);
        }
        $exchangeProgramId = $exchangeConnection->exchange_program_id;
        if (!in_array($exchangeProgramId, $this->getSupportedExchangeProgramIds(), false)) {
            throw new NotSupportedExchangeProgram('Exchange with id = ' . $exchangeProgramId . ' not supported by service');
        }

        switch ($exchangeProgramId) {
            case self::UP_AND_UP_PROGRAM_ID:
                $exchangeUrl = $this->baseUrl . 'upup/tenants/' . $order['tenant_login'] . '/orders/info/' . $orderId;
                $formParams = [
                    'clid'      => $exchangeConnection->clid,
                    'api_key'   => $exchangeConnection->key_api,
                    'show_auto' => $exchangeConnection->is_show_automate,
                    'rate'      => $exchangeConnection->rate,
                ];
                break;
            default:
                $exchangeUrl = '';
                $formParams = [];
                break;
        }
        $response = $this->httpClient->request('POST', $exchangeUrl, [
            'connection_timeout' => $this->connectionTimeout,
            'timeout'            => $this->timeout,
            'form_params'        => $formParams
        ]);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
        \Yii::error('status code = ' . $response->getStatusCode());
        \Yii::error($response->getBody()->getContents());
        return false;

    }


    /**
     * Get relevant exchange connection
     * @param $tenantId
     * @param $cityId
     * @param $positionId
     * @return ExchangeConnection
     */
    private function getRelevantExchangeConnection($tenantId, $cityId, $positionId)
    {
        /* @var $connection ExchangeConnection */
        $connection = ExchangeConnection::find()
            ->where([
                'tenant_id'   => (int)$tenantId,
                'city_id'     => (int)$cityId,
                'position_id' => (int)$positionId,
                'block'       => '0'
            ])
            ->one();
        return $connection;
    }

    /**
     * Get exchange connection by id
     * @param $exchangeConnectionId
     * @return ExchangeConnection
     */
    private function getExchangeConnectionById($exchangeConnectionId)
    {
        return ExchangeConnection::findOne($exchangeConnectionId);
    }

    private function getSupportedExchangeProgramIds()
    {
        return [self::UP_AND_UP_PROGRAM_ID];
    }

    /**
     * Persist exchange data
     * @param array $order
     * @param array $params
     */
    private function persistExchangeData($order, $params)
    {
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $order['tenant_id']]);
        $order['exchange_info'] = $params;
        $activeOrderRepository->set($order['order_id'], serialize($order));
    }


    /**
     * Delete exchange data
     * @param array $order
     */
    private function deleteExchangeData($order)
    {
        $activeOrderRepository = new ActiveOrderRepository(['tenantId' => $order['tenant_id']]);
        unset($order['exchange_info']);
        $activeOrderRepository->set($order['order_id'], serialize($order));
    }



}