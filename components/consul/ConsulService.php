<?php

namespace app\components\consul;

use yii\base\Component;
use SensioLabs\Consul\ServiceFactory;
use app\components\consul\exceptions\ConsulServiceException;

class ConsulService extends Component
{
    /**
     * @var ServiceFactory
     */
    private $serviceFactory;

    /**
     * @var string
     */
    public $consulAgentHost;

    /**
     * @var integer
     */
    public $consulAgentPort;

    public function init()
    {
        $this->serviceFactory = new ServiceFactory(['base_uri' => 'http://' . $this->consulAgentHost . ':' . $this->consulAgentPort]);
        parent::init();
    }

    /**
     * @throws ConsulServiceException
     */
    public function getAgentInfo()
    {
        $agent = $this->serviceFactory->get('agent');
        if (!json_decode($agent->self()->getBody(true), true)) {
            throw new ConsulServiceException('Error of getting agent info');
        }
    }


    /**
     * Get 'catalog' service
     * @return mixed
     */
    protected function getCatalogService()
    {
        return $this->serviceFactory->get('catalog');
    }

    /**
     * Get 'health' service
     * @return mixed
     */
    protected function getHealthService()
    {
        return $this->serviceFactory->get('health');
    }

    /**
     * Lists the nodes in a given service
     * @param string $serviceName
     * @return array
     */
    public function getCatalogOfServicesByName($serviceName)
    {
        $catalog = $this->getCatalogService();
        return json_decode($catalog->service($serviceName)->getBody(true), true);
    }

    /**
     * Lists the healthy in a given service
     * @param string $serviceName
     * @return array
     */
    public function getHealthOfServicesByName($serviceName)
    {
        $health = $this->getHealthService();
        return json_decode($health->service($serviceName)->getBody(true), true);
    }

    /**
     * Get healthy services location
     * @param string $serviceName
     * @return array
     * @throws ConsulServiceException
     */
    public function getHealthyServicesLocation($serviceName)
    {
        $listOfHealthyServicesLocation = [];
        try {
            $listOfServices = $this->getHealthOfServicesByName($serviceName);
        } catch (\Exception $ex) {
            throw new ConsulServiceException('Error of getting healthy services with name:' . $serviceName, 0, $ex);
        }

        foreach ($listOfServices as $serviceInfo) {
            $checksCount     = count($serviceInfo['Checks']);
            $checksGoodCount = 0;
            foreach ($serviceInfo['Checks'] as $check) {
                if ($check["Status"] == "passing") {
                    $checksGoodCount++;
                }
            }
            if ($checksGoodCount == $checksCount) {
                $listOfHealthyServicesLocation[] = ['host' => $serviceInfo['Service']['Address'], 'port' => $serviceInfo['Service']['Port']];
            }
        }
        return $listOfHealthyServicesLocation;
    }

    /**
     * Get random healthy service location
     * @param $serviceName
     * @return array for ex.: ['host'=>'127.0.0.1','port'=>'8088']
     * @throws ConsulServiceException
     */
    public function getRandomHealthyServiceLocation($serviceName)
    {
        $serviceLocations = $this->getHealthyServicesLocation($serviceName);
        $location         = $serviceLocations[array_rand($serviceLocations)];
        if (!isset($location['host'])) {
            throw new ConsulServiceException('No healthy nodes for service:' . $serviceName);
        }
        return $location;
    }


    /**
     * Create method url
     * @param $serviceName
     * @param $method
     * @return string
     * @throws ConsulServiceException
     */
    public function createMethodUrl($serviceName, $method)
    {
        $location = $this->getRandomHealthyServiceLocation($serviceName);
        return "http://" . $location['host'] . ":" . $location["port"] . "/" . $method;
    }


}