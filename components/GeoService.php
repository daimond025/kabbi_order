<?php
namespace app\components;


use app\models\TenantSetting;

class GeoService extends \yii\base\Component
{
    /**
     * @var string
     */
    public $url;

    /**
     * Timeout of request to taxi-service-api
     * @var int
     */
    public $timeout = 15;

    private function getLangParam($tenantId, $cityId)
    {
        $lang = TenantSetting::getSettingValue($tenantId, 'LANGUAGE', $cityId);

        return $lang ?: 'ru';
    }

    /**
     * The method will be defined the point belongs to what parkings
     *
     * @param string $tenantId
     * @param string $cityId
     * @param string $lat
     * @param string $lon
     * @param bool $jsonFormat
     *
     * @return array|mixed
     */
    public function placeDeteils($tenantId, $cityId, $lang, $placeId)
    {
        $params = [
            'tenant_id'   => $tenantId,
            'city_id'     => $cityId,
            'lang'        => $lang,
            'placeId'     => $placeId,
        ];
        $result = $this->sendGetRequest('details', 1, $params);
        if (isset($result['results'])) {
            return $result["results"];
        }
    }

    /**
     * Send GET request to taxi-service-api
     *
     * @param string $method
     * @param int $version
     * @param array $params
     *
     * @return mixed
     */
    private function sendGetRequest($method, $version, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }

        if (strlen($params) > 0) {
            $url = $this->getUrl($method, $version) . "?" . $params;
        } else {
            $url = $this->getUrl($method, $version);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $result = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url of taxi-service-api route-analyzer method
     *
     * @param string $method
     * @param int $version
     *
     * @return string
     */
    private function getUrl($method, $version)
    {
        return "{$this->url}v{$version}/{$method}";
    }
}