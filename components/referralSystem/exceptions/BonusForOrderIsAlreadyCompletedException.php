<?php

namespace referralSystem\exceptions;

/**
 * Class BonusForOrderIsAlreadyCompletedException
 * @package referralSystem\exceptions
 */
class BonusForOrderIsAlreadyCompletedException extends \DomainException
{

}