<?php

namespace referralSystem\models;

/**
 * Class ReferralSystem
 * @package referralSystem\models
 */
class ReferralSystem
{
    const TYPE_ON_REGISTER = 'ON_REGISTER';
    const TYPE_ON_ORDER = 'ON_ORDER';

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $maxOrderCount;
    /**
     * @var float
     */
    private $minOrderPrice;
    /**
     * @var Referrer
     */
    private $referrer;
    /**
     * @var BonusCalculation
     */
    private $referrerCalculation;
    /**
     * @var Referral
     */
    private $referral;
    /**
     * @var BonusCalculation
     */
    private $referralCalculation;

    /**
     * ReferralSystem constructor.
     *
     * @param int              $id
     * @param string           $name
     * @param int              $maxOrderCount
     * @param float            $minOrderPrice
     * @param Referrer         $referrer
     * @param BonusCalculation $referrerCalculation
     * @param Referral         $referral
     * @param BonusCalculation $referralCalculation
     */
    public function __construct(
        $id,
        $name,
        $maxOrderCount,
        $minOrderPrice,
        Referrer $referrer,
        BonusCalculation $referrerCalculation,
        Referral $referral,
        BonusCalculation $referralCalculation
    ) {

        $this->id                  = $id;
        $this->name                = $name;
        $this->maxOrderCount       = $maxOrderCount;
        $this->minOrderPrice       = $minOrderPrice;
        $this->referrer            = $referrer;
        $this->referrerCalculation = $referrerCalculation;
        $this->referral            = $referral;
        $this->referralCalculation = $referralCalculation;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getMaxOrderCount()
    {
        return $this->maxOrderCount;
    }

    /**
     * @return float
     */
    public function getMinOrderPrice()
    {
        return $this->minOrderPrice;
    }

    /**
     * @return Referrer
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * @return BonusCalculation
     */
    public function getReferrerCalculation()
    {
        return $this->referrerCalculation;
    }

    /**
     * @return Referral
     */
    public function getReferral()
    {
        return $this->referral;
    }

    /**
     * @return BonusCalculation
     */
    public function getReferralCalculation()
    {
        return $this->referralCalculation;
    }

}