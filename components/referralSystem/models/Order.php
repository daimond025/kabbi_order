<?php

namespace referralSystem\models;

/**
 * Class Order
 * @package referralSystem\models
 */
class Order
{
    /**
     * @var int
     */
    private $orderId;
    /**
     * @var int
     */
    private $tenantId;
    /**
     * @var int
     */
    private $cityId;
    /**
     * @var int
     */
    private $clientId;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var int
     */
    private $currencyId;

    /**
     * Order constructor.
     *
     * @param int   $orderId
     * @param int   $tenantId
     * @param int   $cityId
     * @param int   $clientId
     * @param float $amount
     * @param int   $currencyId
     */
    public function __construct($orderId, $tenantId, $cityId, $clientId, $amount, $currencyId)
    {
        $this->orderId    = $orderId;
        $this->tenantId   = $tenantId;
        $this->cityId     = $cityId;
        $this->clientId   = $clientId;
        $this->amount     = $amount;
        $this->currencyId = $currencyId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }
}