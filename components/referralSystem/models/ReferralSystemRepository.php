<?php

namespace referralSystem\models;

use referralSystem\exceptions\BonusForOrderIsAlreadyCompletedException;
use referralSystem\exceptions\BonusForOrderNotFoundException;
use referralSystem\exceptions\BonusForOrderSaveException;
use referralSystem\exceptions\ReferralSystemNotFoundException;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;

/**
 * Class ReferralSystemRepository
 * @package referralSystem\models
 */
class ReferralSystemRepository
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * ReferralSystemRepository constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $tenantId
     * @param int $cityId
     * @param int $referralId
     *
     * @return array
     * @throws ReferralSystemNotFoundException
     */
    private function getReferralSystemData($tenantId, $cityId, $referralId)
    {
        $system = (new Query())
            ->select([
                'id'                   => 'r.referral_id',
                'r.name',
                'max_order_count'      => 'r.order_count',
                'r.min_price',
                'referrer_client_id'   => 'm.client_id',
                'r.referrer_bonus',
                'r.referrer_bonus_type',
                'referral_id'          => 's.subscriber_id',
                'referral_client_id'   => 's.client_id',
                'r.referral_bonus',
                'r.referral_bonus_type',
                'referral_order_count' => 's.order_count',
            ])
            ->from('{{%referral}} as r')
            ->innerJoin('{{%referral_system_members}} as m', 'r.referral_id = m.referral_id')
            ->innerJoin('{{%referral_system_subscribers}} as s', 'm.member_id = s.member_id')
            ->where([
                'r.tenant_id' => $tenantId,
                'r.city_id'   => $cityId,
                'r.type'      => ReferralSystem::TYPE_ON_ORDER,
                's.client_id' => $referralId,
                'r.active'    => 1,
            ])
            ->orderBy(['r.sort' => SORT_ASC])
            ->one();

        if (empty($system)) {
            throw new ReferralSystemNotFoundException(
                "Referral system not found: tenantId={$tenantId}, cityId={$cityId}, referralId={$referralId}");
        }

        return $system;
    }

    /**
     * @param int $tenantId
     * @param int $cityId
     * @param int $referralId
     *
     * @return ReferralSystem
     * @throws ReferralSystemNotFoundException
     */
    public function getReferralSystem($tenantId, $cityId, $referralId)
    {
        $data = $this->getReferralSystemData($tenantId, $cityId, $referralId);

        $referrer            = new Referrer((int)$data['referrer_client_id']);
        $referrerCalculation = new BonusCalculation((float)$data['referrer_bonus'], $data['referrer_bonus_type']);

        $referral            = new Referral((int)$data['referral_id'], (int)$data['referral_client_id'],
            $data['referral_order_count']);
        $referralCalculation = new BonusCalculation((float)$data['referral_bonus'], $data['referral_bonus_type']);

        return new ReferralSystem(
            (int)$data['id'],
            $data['name'],
            $data['max_order_count'] === null ? null : (int)$data['max_order_count'],
            (float)$data['min_price'],
            $referrer,
            $referrerCalculation,
            $referral,
            $referralCalculation
        );
    }

    /**
     * @param int $orderId
     *
     * @return array
     * @throws BonusForOrderNotFoundException
     */
    private function getBonusForOrderData($orderId)
    {
        $data = (new Query())
            ->from('{{%referral_bonus_for_order}}')
            ->select(['id', 'order_id', 'referrer_id', 'referrer_bonus', 'referral_id', 'referral_bonus', 'completed'])
            ->where(['order_id' => $orderId])
            ->one();

        if (empty($data)) {
            throw new BonusForOrderNotFoundException("Bonus for order not found: orderId={$orderId}");
        }

        return $data;
    }

    /**
     * @param Order $order
     *
     * @return BonusForOrder
     * @throws BonusForOrderNotFoundException
     */
    public function getBonusForOrder(Order $order)
    {
        $bonusData = $this->getBonusForOrderData($order->getOrderId());

        $referrerBonus = new Bonus($bonusData['referrer_id'],
            new BonusValue((float)$bonusData['referrer_bonus'], $order->getCurrencyId()));
        $referralBonus = new Bonus($bonusData['referral_id'],
            new BonusValue((float)$bonusData['referral_bonus'], $order->getCurrencyId()));

        return new BonusForOrder((int)$bonusData['id'], $order, $referrerBonus, $referralBonus,
            (int)$bonusData['completed'] === 1);
    }

    /**
     * @param BonusForOrder $bonusForOrder
     *
     * @throws Exception
     */
    private function insertBonusForOrder(BonusForOrder $bonusForOrder)
    {
        $this->db->createCommand()->insert('{{%referral_bonus_for_order}}', [
            'order_id'       => $bonusForOrder->getOrder()->getOrderId(),
            'referrer_id'    => $bonusForOrder->getReferrerBonus()->getClientId(),
            'referrer_bonus' => $bonusForOrder->getReferrerBonus()->getBonusValue()->getAmount(),
            'referral_id'    => $bonusForOrder->getReferralBonus()->getClientId(),
            'referral_bonus' => $bonusForOrder->getReferralBonus()->getBonusValue()->getAmount(),
            'completed'      => $bonusForOrder->isCompleted() ? 1 : 0,
            'created_at'     => time(),
            'updated_at'     => time(),
        ])->execute();
    }

    /**
     * @param BonusForOrder $bonusForOrder
     *
     * @throws Exception
     */
    private function updateBonusForOrder(BonusForOrder $bonusForOrder)
    {
        $this->db->createCommand()->update('{{%referral_bonus_for_order}}', [
            'order_id'       => $bonusForOrder->getOrder()->getOrderId(),
            'referrer_id'    => $bonusForOrder->getReferrerBonus()->getClientId(),
            'referrer_bonus' => $bonusForOrder->getReferrerBonus()->getBonusValue()->getAmount(),
            'referral_id'    => $bonusForOrder->getReferralBonus()->getClientId(),
            'referral_bonus' => $bonusForOrder->getReferralBonus()->getBonusValue()->getAmount(),
            'completed'      => $bonusForOrder->isCompleted() ? 1 : 0,
            'updated_at'     => time(),
        ], ['id' => $bonusForOrder->getId()])->execute();
    }

    /**
     * @param BonusForOrder $bonusForOrder
     *
     * @throws \referralSystem\exceptions\BonusForOrderSaveException
     */
    public function saveBonusForOrder(BonusForOrder $bonusForOrder)
    {
        try {
            if ($bonusForOrder->getId() === null) {
                $this->insertBonusForOrder($bonusForOrder);
            } else {
                $this->updateBonusForOrder($bonusForOrder);
            }
        } catch (\Exception $ex) {
            throw new BonusForOrderSaveException(
                "Bonus save error: id={$bonusForOrder->getId()}, orderId={$bonusForOrder->getOrder()->getOrderId()}", 0,
                $ex);
        }
    }

    /**
     * @param Order $order
     *
     * @throws BonusForOrderNotFoundException
     * @throws BonusForOrderIsAlreadyCompletedException
     * @throws Exception
     */
    public function deleteBonusForOrder(Order $order)
    {
        $bonus = $this->getBonusForOrder($order);

        if ($bonus->isCompleted()) {
            throw new BonusForOrderIsAlreadyCompletedException("Bonus for order is already completed: orderId={$order->getOrderId()}");
        }

        $this->db->createCommand()->delete('{{%referral_bonus_for_order}}',
            ['order_id' => $order->getOrderId()])->execute();
    }

    /**
     * @param Referral $referral
     *
     * @throws Exception
     */
    public function saveReferral(Referral $referral)
    {
        $this->db->createCommand()->update('{{%referral_system_subscribers}}', [
            'order_count' => $referral->getOrderCount(),
        ], [
            'subscriber_id' => $referral->getId(),
        ])->execute();
    }
}