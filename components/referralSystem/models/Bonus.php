<?php

namespace referralSystem\models;

/**
 * Class Bonus
 * @package referralSystem\models
 */
class Bonus
{
    /**
     * @var int
     */
    private $clientId;
    /**
     * @var BonusValue
     */
    private $bonusValue;

    /**
     * Bonus constructor.
     *
     * @param int        $clientId
     * @param BonusValue $bonusValue
     */
    public function __construct($clientId, BonusValue $bonusValue)
    {
        $this->clientId   = $clientId;
        $this->bonusValue = $bonusValue;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return BonusValue
     */
    public function getBonusValue()
    {
        return $this->bonusValue;
    }
}