<?php

namespace referralSystem\models;

/**
 * Class Referrer
 * @package referralSystem\models
 */
class Referrer
{
    /**
     * @var int
     */
    private $clientId;

    /**
     * Referrer constructor.
     *
     * @param int $clientId
     */
    public function __construct($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }
}