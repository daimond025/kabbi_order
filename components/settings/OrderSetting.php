<?php

namespace app\components\settings;

use app\components\repositories\OrderRepository;
use app\models\TenantSetting;
use app\modules\v1\exceptions\ExceptionFormatting;
use app\modules\v1\models\dto\SettingExistOrderDto;
use app\modules\v1\models\OrderRecord;

class OrderSetting
{

    const SECOND_IN_MINUTE = 60;

    protected $order;
    /** @var OrderRepository  */
    protected $orderRepository;

    protected $settingsActiveOrder;

    protected $preOrderTime;
    protected $pickUp;
    protected $currencyId;
    protected $orderFromAddPointPhone;
    protected $orderFromAddPointComment;
    protected $requirePointConfirmationCode;

    public function __construct(OrderRecord $order)
    {
        $this->order = $order;
        $this->orderRepository = \Yii::createObject(OrderRepository::class);
    }

    /**
     * @return \app\modules\v1\models\dto\SettingExistOrderDto
     * @throws \yii\db\Exception
     */
    public function getSettingsActiveOrder()
    {
        if (!$this->settingsActiveOrder instanceof SettingExistOrderDto) {
            $this->settingsActiveOrder = $this->orderRepository->getSettingsActiveOrder($this->order);
        }

        return $this->settingsActiveOrder;
    }

    public function getPreOrderTimeInSecond()
    {
        if (!$this->preOrderTime) {
            $preOrderTime = self::getSettingValue(
                TenantSetting::SETTING_PRE_ORDER,
                TenantSetting::TYPE_SETTING_ORDERS
            );

            $this->preOrderTime = empty($preOrderTime)
                ? OrderRecord::PRE_ODRER_TIME
                : $preOrderTime * self::SECOND_IN_MINUTE;
        }

        return $this->preOrderTime;
    }

    public function getPickUp()
    {
        if (!$this->pickUp) {
            $pickUp = self::getSettingValue(
                TenantSetting::SETTING_PICK_UP,
                TenantSetting::TYPE_SETTING_ORDERS
            );

            $this->pickUp = empty($pickUp) ? OrderRecord::PICK_UP_TIME : $pickUp;
        }

        return $this->pickUp;
    }

    public function getCurrencyId()
    {
        if (!$this->currencyId) {
            $currencyId = self::getSettingValue(
                TenantSetting::CURRENCY_TYPE,
                TenantSetting::TYPE_SETTING_GENERAL
            );

            if (!isset($currencyId)) {
                $message = 'Невозможно создать заказ.Отсутствует валюта.';
                $context = [
                    'tenant_id' => $this->order->tenant_id,
                    'city_id'   => $this->order->city_id
                ];

                throw new \RuntimeException(ExceptionFormatting::getFormattedString($message, $context));
            }

            $this->currencyId = $currencyId;
        }


        return $this->currencyId;
    }

    public function getOrderFromAddPointPhone()
    {
        if (!$this->orderFromAddPointPhone) {
            $orderFromAddPointPhone = self::getSettingValue(
                TenantSetting::SETTING_ORDER_FORM_ADD_POINT_PHONE,
                TenantSetting::TYPE_SETTING_ORDERS
            );

            if (!isset($orderFromAddPointPhone)) {
                $message = 'Невозможно создать заказ. Отсутствует информация для добавления телефона в точки адреса.';
                $context = [
                    'tenant_id'   => $this->order->tenant_id,
                    'city_id'     => $this->order->city_id,
                    'position_id' => $this->order->position_id,
                ];
                throw new \RuntimeException(ExceptionFormatting::getFormattedString($message, $context));
            }

            $this->orderFromAddPointPhone = $orderFromAddPointPhone;
        }

        return $this->orderFromAddPointPhone;
    }

    public function getOrderFromAddPointComment()
    {
        if (!$this->orderFromAddPointComment) {
            $orderFromAddPointComment = self::getSettingValue(
                TenantSetting::SETTING_ORDER_FORM_ADD_POINT_COMMENT,
                TenantSetting::TYPE_SETTING_ORDERS
            );

            if (!isset($orderFromAddPointComment)) {
                $message = 'Невозможно создать заказ. Отсутствует информация для добавления коментарий в точки адреса.';
                $context = [
                    'tenant_id'   => $this->order->tenant_id,
                    'city_id'     => $this->order->city_id,
                    'position_id' => $this->order->position_id,
                ];
                throw new \RuntimeException(ExceptionFormatting::getFormattedString($message, $context));
            }

            $this->orderFromAddPointComment = $orderFromAddPointComment;
        }


        return $this->orderFromAddPointComment;
    }

    public function getRequirePointConfirmationCode()
    {
        if (!$this->requirePointConfirmationCode) {
            $requirePointConfirmationCode = self::getSettingValue(
                TenantSetting::SETTING_REQUIRE_POINT_CONFIRMATION_CODE,
                TenantSetting::TYPE_SETTING_ORDERS
            );

            if (!isset($requirePointConfirmationCode)) {
                $message = 'Невозможно создать заказ. Отсутствует информация о создании кодов для каждой точки заказа.';
                $context = [
                    'tenant_id'   => $this->order->tenant_id,
                    'city_id'     => $this->order->city_id,
                    'position_id' => $this->order->position_id,
                ];
                throw new \RuntimeException(ExceptionFormatting::getFormattedString($message, $context));
            }

            $this->requirePointConfirmationCode = $requirePointConfirmationCode;
        }

        return $this->requirePointConfirmationCode;
    }

    protected function getSettingValue($settingName, $settingType)
    {
        return TenantSetting::getSettingValueByType(
            $this->order->tenant_id,
            $settingName,
            $settingType,
            $this->order->city_id,
            $this->order->position_id
        );
    }
}
