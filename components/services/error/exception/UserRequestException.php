<?php

namespace app\components\services\error\exception;

use Throwable;
use yii\base\Model;
use yii\web\HttpException;

class UserRequestException extends HttpException
{

    protected $errors;

    public function __construct($message = "", $code = 0, Throwable $previous = null, Model $model = null)
    {
        if ($model) {
            \Yii::error($model->getErrors());
            $this->errors = $model->getErrors();
            $message = json_encode($model->getErrors());
            $statusCode = 422;

        } else {
            \Yii::error($message);
            $statusCode = 400;
        }

        parent::__construct($statusCode, $message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
