<?php

namespace app\components\services\transaction;


use app\modules\v1\models\workers\balance\Transaction;

class TransactionService
{

    /* @var $transactionModel TransactionModel */
    protected $transactionModel;

    public function setTransactionModel(TransactionModel $model)
    {
        $this->transactionModel = $model;
    }

    public function createTransaction(TransactionModel $model)
    {
        $this->setTransactionModel($model);

        $responseCode = Transaction::createTransaction($this->prepareTransactionData());
        if ($responseCode == Transaction::SUCCESS_RESPONSE) {
            return true;
        } else {
            $error = "ERROR AT closeOrderTransaction. Error code: " . $responseCode;
            \Yii::error($error);
            throw new TransactionException($error);
        }
    }


    protected function prepareTransactionData()
    {
        return [
            'type_id'            => $this->transactionModel->type_id,
            'tenant_id'          => $this->transactionModel->tenant_id,
            'sender_owner_id'    => $this->transactionModel->sender_owner_id,
            'sender_acc_kind_id' => $this->transactionModel->sender_acc_kind_id,
            'currency_id'        => $this->transactionModel->currency_id,
            'sum'                => $this->transactionModel->sum,
            'comment'            => $this->transactionModel->comment,
            'user_created'       => $this->transactionModel->user_created,
            'payment_method'     => $this->transactionModel->payment_method,
        ];
    }


}