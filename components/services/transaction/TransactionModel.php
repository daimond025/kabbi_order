<?php

namespace app\components\services\transaction;


use app\models\DefaultSettings;
use app\models\TenantSetting;
use app\modules\promocode\models\entities\PromoClient;
use app\modules\v1\models\workers\balance\Transaction;
use bonusSystem\models\ar\Account;
use yii\base\Model;

class TransactionModel extends Model
{

    const COMMENT_BONUS = 'bonusActivationPromo;Receipt of bonuses for activation of the promotional code {code} for the promo campaign {promoName};%s;%d;%s';

    public $type_id;
    public $tenant_id;
    public $sender_owner_id;
    public $sender_acc_kind_id;
    public $currency_id;
    public $sum;
    public $comment;
    public $user_created;
    public $payment_method;
    public $acc_type_id;


    public function fillDataForPromoBonus(PromoClient $promoClient)
    {
        $promo = $promoClient->promoCode->promo;

        $this->type_id            = Transaction::DEPOSIT_TYPE;
        $this->tenant_id          = $promo->tenant_id;
        $this->sender_owner_id    = $promoClient->client_id;
        $this->sender_acc_kind_id = Account::ACCOUNT_KIND_ID_CLIENT_BONUS;
        $this->currency_id        = TenantSetting::getSettingValue(
            $promo->tenant_id, DefaultSettings::SETTING_CURRENCY, $promo->city
        );
        $this->sum                = $promo->client_bonuses;
        $this->comment            = sprintf(
            self::COMMENT_BONUS, $promo->name, $promo->promo_id, $promoClient->promoCode->code
        );
        $this->user_created       = null;
        $this->payment_method     = Transaction::BONUS_PAYMENT;
        $this->acc_type_id        = Account::ACCOUNT_TYPE_ID_PASSIVE;


        return $this;
    }


}