<?php

namespace healthCheck\checks;

use GuzzleHttp\Client;
use healthCheck\exceptions\HealthCheckException;

/**
 * Class HttpServiceCheck
 * @package healthCheck\checks
 */
class HttpServiceCheck extends BaseCheck
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * BasicServiceCheck constructor.
     *
     * @param string $name
     * @param string $url
     * @param int    $statusCode
     */
    public function __construct($name, $url, $statusCode = 200)
    {
        $this->url        = $url;
        $this->statusCode = $statusCode;

        parent::__construct($name);
    }

    public function run()
    {
        try {
            $client   = new Client();
            $response = $client->get($this->url);

            $actualStatusCode = (string)$response->getStatusCode();
            if ($actualStatusCode !== (string)$this->statusCode) {
                throw new HealthCheckException("Incorrect status code [{$actualStatusCode}]");
            }
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Check service exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}