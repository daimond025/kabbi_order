<?php

namespace paymentGate;

use paymentGate\exceptions\ProfileNotFoundException;
use yii\db\Connection;
use yii\db\Exception;

class ProfileService
{
    const SQL_PAYMENT_GATE_PROFILE = <<<SQL
SELECT p.profile
FROM tbl_paygate_profile p
   INNER JOIN tbl_tenant_city_has_paygate_profile c
    ON p.id = c.profile_id AND p.tenant_id = c.tenant_id AND c.city_id = :cityId
WHERE p.tenant_id = :tenantId
LIMIT 1
SQL;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $tenantId
     * @param int $cityId
     *
     * @return string
     *
     * @throws ProfileNotFoundException
     * @throws Exception
     */
    public function getProfile($tenantId, $cityId)
    {
        $profile = $this->connection->createCommand(self::SQL_PAYMENT_GATE_PROFILE, [
            'tenantId' => (int)$tenantId,
            'cityId'   => (int)$cityId,
        ])->queryScalar();


        if (empty($profile)) {
            throw new ProfileNotFoundException("Payment gate profile not found: tenantId=$tenantId, cityId=$cityId");
        }

        return (string)$profile;
    }
}