<?php

namespace paymentGate;

use app\components\curl\RestCurl;
use yii\log\Logger;

class PaymentGateService
{
    /**
     * @var RestCurl
     */
    private $restCurl;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $url;

    public function __construct(RestCurl $restCurl, Logger $logger, $url)
    {
        $this->restCurl = $restCurl;
        $this->logger = $logger;
        $this->url = $url;
    }

    /**
     * @param string $profile
     *
     * @return array
     */
    public function get($profile)
    {
        try {
            $response = $this->restCurl->get("{$this->url}v0/profiles/$profile");

            return isset($response['result']) ? $response['result'] : [];
        } catch (\Exception $e) {
            $this->logger->log(
                "Get payment gate profile error: profile=$profile, error={$e->getMessage()}",
                Logger::LEVEL_ERROR
            );
        }
    }
}