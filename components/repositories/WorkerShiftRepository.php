<?php

namespace app\components\repositories;

class WorkerShiftRepository
{
    public function getActiveWorker($callsign, $tenantId)
    {
        return unserialize(\Yii::$app->redis_workers->executeCommand('hget', [$tenantId, $callsign]));
    }
}
