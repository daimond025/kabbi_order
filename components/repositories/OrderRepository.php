<?php

namespace app\components\repositories;

use app\modules\v1\exceptions\ExceptionFormatting;
use app\modules\v1\models\dto\SettingExistOrderDto;
use app\modules\v1\models\OrderRecord;
use yii\db\ActiveQuery;
use yii\redis\Connection;

class OrderRepository
{
    const EXCEPTION_REDIS = 'EXCEPTION_REDIS';
    const EXCEPTION_MYSQL = 'EXCEPTION_MYSQL';

    /** @var Connection */
    protected $redisOrderActive;

    public function __construct()
    {
        $this->redisOrderActive = \Yii::$app->redis_orders_active;
    }

    public function saveOrder($tenantId, $orderId, $orderSerialized)
    {
        return $this->redisOrderActive->executeCommand('hset', [$tenantId, $orderId, $orderSerialized]);
    }

    public function deleteOrder(OrderRecord $order)
    {
        $this->redisOrderActive->executeCommand('hdel', [$order->tenant_id, $order->order_id]);

        if (!$order->delete()) {
            throw new \RuntimeException(self::EXCEPTION_MYSQL);
        }
    }

    /**
     * @param OrderRecord $order
     *
     * @return SettingExistOrderDto
     * @throws \yii\db\Exception
     */
    public function getSettingsActiveOrder(OrderRecord $order)
    {
        return $this->getSettingsActiveOrderByData($order->tenant_id, $order->order_id);
    }

    /**
     * @param $tenantId
     * @param $orderId
     *
     * @return SettingExistOrderDto
     * @throws \yii\db\Exception
     */
    public function getSettingsActiveOrderByData($tenantId, $orderId)
    {
        $activeOrder = $this->redisOrderActive
            ->executeCommand('hget', [$tenantId, $orderId]);

        if (!$activeOrder) {
            throw new \RuntimeException(ExceptionFormatting::getFormattedString(
                'Error, attempt to receive an active order: ',
                [$tenantId, $orderId]
            ));
        }

        $activeOrder = unserialize($activeOrder);
        $setting = $activeOrder['settings'];

        $orderSetting = new SettingExistOrderDto();
        $orderSetting->loadSettings($setting);

        return $orderSetting;
    }

    /**
     * @param $orderId
     * @param $tariffId
     *
     * @return array|null
     */
    public function getOrder($orderId, $tariffId)
    {
        return OrderRecord::find()
            ->where([
                'order_id' => $orderId,
            ])
            ->with([
                'status',
                'tariff',
                'tariff.class'              => function (ActiveQuery $query) {
                    $query->select(['class_id', 'class']);
                },
                'client'                    => function (ActiveQuery $query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang', 'email', 'send_to_email']);
                },
                'userCreated'               => function (ActiveQuery $query) {
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'options.additionalOptions' => function (ActiveQuery $query) use ($tariffId) {
                    $query->select(['id', 'additional_option_id', 'price'])->where([
                        'tariff_id'   => $tariffId,
                        'tariff_type' => "CURRENT",
                    ]);
                },
                'tenant'                    => function (ActiveQuery $query) {
                    $query->select(['domain', 'company_name', 'contact_phone', 'contact_email', 'site']);
                },
                'worker',
                'clientPassenger'           => function (ActiveQuery $query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang', 'email', 'send_to_email']);
                },
            ])
            ->asArray()
            ->one();
    }
}
