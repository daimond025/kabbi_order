<?php

namespace app\components\repositories;


use app\models\Position;
use yii\helpers\ArrayHelper;

class PositionRepository
{

    protected $positionsHasCar;

    public function getPositionsHasCarClass()
    {
        if (!$this->positionsHasCar) {
            $this->positionsHasCar = ArrayHelper::getColumn(Position::find()
                ->where(['not', ['transport_type_id' => null]])
                ->all(), 'transport_type_id');
        }

        return $this->positionsHasCar;
    }

}