<?php

namespace app\components\bootstrap;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Language
 * @package frontend\bootstrap
 */
class Language implements BootstrapInterface
{
    /**
     * @var array
     */
    public $supportedLanguages = [];

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $lang = Yii::$app->request->headers->get('lang');

        if ($lang && in_array($lang, $this->supportedLanguages)) {
            $app->language = $lang;
        } else {
            $app->language = 'en-US';
        }
    }

    /**
     * Getting preferred language
     * @return string
     */
    public function getPreferredLanguage()
    {
        return Yii::$app->request->getPreferredLanguage($this->supportedLanguages);
    }
}
