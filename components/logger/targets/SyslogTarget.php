<?php

namespace logger\targets;

/**
 * Class SyslogTarget
 * @package logger\targets
 */
class SyslogTarget implements LogTargetInterface
{
    /**
     * @var string
     */
    private $identity;

    /**
     * SyslogTarget constructor.
     *
     * @param $identity
     */
    public function __construct($identity)
    {
        $this->identity = $identity;
    }

    /**
     * @inheritdoc
     */
    public function export($content)
    {
        openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
        syslog(LOG_INFO, $content);
        closelog();
    }

}