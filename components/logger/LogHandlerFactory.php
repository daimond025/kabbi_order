<?php

namespace logger;

use logger\handlers\BasicHandler;
use logger\handlers\LogHandlerInterface;
use yii\web\Request;
use yii\web\Response;

/**
 * Class LogHandlerFactory
 * @package logger
 */
class LogHandlerFactory
{
    /**
     * Getting log handler
     *
     * @param string   $method
     * @param Request  $request
     * @param Response $response
     *
     * @return LogHandlerInterface
     */
    public function getHandler($method, $request, $response)
    {
        switch ($method) {
            case '/v1/order/create':
                return new BasicHandler([
                    'id' => ['name' => 'uuid'],
                ], [
                    'orderId' => 'order_id',
                ]);
            case '/v1/order/update-event':
            case '/v1/orders/update_order_data':
                return new BasicHandler([
                    'id'      => ['name' => 'uuid'],
                    'orderId' => ['name' => 'order_id'],
                    'service' => ['name' => 'sender_service'],
                ]);
            default:
                return new BasicHandler();
        }
    }
}