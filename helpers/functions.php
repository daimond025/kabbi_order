<?php

/**
 * functions.php file.
 * Global shorthand functions for commonly used Yii methods.
 */

/**
 * Returns the application instance.
 * @return \yii\web\Application
 */
function app()
{
    return Yii::$app;
}

/**
 * Dumps the given variable using CVarDumper::dumpAsString().
 *
 * @param mixed $var
 * @param int   $depth
 * @param bool  $highlight
 */
function dump($var, $depth = 10, $highlight = true)
{
    return \yii\helpers\VarDumper::dump($var, $depth, $highlight);
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var, $showDebugCallPath = false, $depth = 10, $highlight = true)
{
    if ($showDebugCallPath) {
        $db   = current(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1));
        $dump = [
            'file' => $db['file'],
            'line' => $db['line'],
            'dump' => $var,
        ];
    } else {
        $dump = $var;
    }

    dump($dump, $depth, $highlight);
    die();
}

/**
 * Returns user component.
 * @return \app\modules\v1\models\user\User
 */
function user()
{
    return app()->getUser()->identity;
}

/**
 * Returns post data
 * @param string|null $name
 * @param string|null $defaultValue
 * @return mixed
 */
function post($name = null, $defaultValue = null)
{
    return app()->request->post($name, $defaultValue);
}
/**
 * Returns get data
 * @param string|null $name
 * @param string|null $defaultValue
 * @return mixed
 */
function get($name = null, $defaultValue = null)
{
    return app()->request->get($name, $defaultValue);
}

/**
 * Yii translate
 * @param string $category the message category.
 * @param string $message the message to be translated.
 * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
 * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current
 * @return string the translated message.
 */
function t($category, $message, $params = [], $language = null)
{
    return Yii::t($category, $message, $params, $language);
}
