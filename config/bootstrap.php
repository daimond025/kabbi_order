<?php

$basePath = dirname(__DIR__);
\Yii::setAlias('logger', $basePath . '/components/logger');
\Yii::setAlias('healthCheck', $basePath . '/components/healthCheck');
\Yii::setAlias('paymentGate', $basePath . '/components/paymentGate');

\logger\ApiLogger::setupStartTimeForStatistic();