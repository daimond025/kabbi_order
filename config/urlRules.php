<?php

return [
    'GET version' => '/v1/api/version',
    'GET status'  => '/v1/api/status',

    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => ['v1/order', 'promocode/promo'],
        'extraPatterns' => [
            'GET active/<order_id:\d+>'      => 'active',
            'GET active'                     => 'active-list',
            'GET number/<order_number:\d+>'  => 'view-by-number',
            'POST number/<order_number:\d+>' => 'update-by-number',
            'GET count'                      => 'count',
            'GET <order_id:\d+>'             => 'view',
            'POST <order_id:\d+>'            => 'update',
            'GET <order_id:\d+>/track'       => 'track',
            'GET <order_id:\d+>/events'      => 'events',
            'GET <order_id:\d+>/cost'        => 'detail-cost',
            'GET <order_id:\d+>/fields'      => 'fields',
            'GET get-available-attributes'   => 'get-available-attributes',
            'POST update_order_data'         => 'update-event',
            'POST send_to_exchange'          => 'send-to-exchange-event',

            'POST activate_promo_code'                => 'activate-promo-code',
            'GET find_suitable_promo_code/<tenant_id:\d+>/<client_id:\d+>/<order_time:\d+>/<city_id:\d+>/<position_id:\d+>'
                                                      => 'find-suitable-promo-code',
            'GET promo_code_info/<code_id:\d+>'       => 'promo-code-info',
            'GET worker_registration/<worker_id:\d+>' => 'worker-registration',
            'POST generated_promo_codes'              => 'generated-promo-codes',
        ],
    ],
];