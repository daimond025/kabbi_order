<?php

$version  = require __DIR__ . '/version.php';
$identity = getenv('SYSLOG_IDENTITY') . '-' . $version;

\Yii::$container->set(\paymentGate\ProfileService::class, function ($container, $params, $config) {
    return new \paymentGate\ProfileService(Yii::$app->getDb());
});

\Yii::$container->set(
    \logger\targets\LogTargetInterface::class,
    \logger\targets\SyslogTarget::class,
    [$identity]
);

\Yii::$container->set(
    \logger\LogHandlerFactory::class
);

\Yii::$container->set(\paymentGate\PaymentGateService::class, function ($container, $params, $config) {
    $url = isset(\Yii::$app->params['paygate.url']) ? \Yii::$app->params['paygate.url'] : null;

    return new \paymentGate\PaymentGateService(\Yii::$app->restCurl, \Yii::getLogger(), $url);
});