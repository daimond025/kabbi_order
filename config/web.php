<?php

$params = require __DIR__ . '/params.php';

$db = require __DIR__ . '/db.php';

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$config = [
    'id'         => 'api_order',
    'basePath'   => dirname(__DIR__),
    'aliases'    => [
        '@bower'         => '@vendor/bower-asset',
        '@npm'           => '@vendor/npm-asset',
        'bonusSystem'    => '@app/components/bonusSystem',
        'referralSystem' => '@app/components/referralSystem',
    ],
    'bootstrap'  => ['log', 'bootstrapLanguage'],
    'components' => [
        // Yii2 components
        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'bootstrapLanguage' => [
            'class'              => \app\components\bootstrap\Language::class,
            'supportedLanguages' => array_keys($params['supportedLanguages']),
        ],

        'errorHandler' => [
            'class' => \app\components\services\error\ErrorHandler::class,
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'enableCaching'    => YII_ENV_PROD,
                    'cachingDuration'  => 3600,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'enableCaching'    => YII_ENV_PROD,
                    'cachingDuration'  => 3600,
                ],
            ],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'request' => [
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'response' => [
            'format'       => yii\web\Response::FORMAT_JSON,
            'charset'      => 'UTF-8',
            'on afterSend' => function () {
                /** @var $apiLogger \logger\ApiLogger */
                $apiLogger = \Yii::$app->apiLogger;
                $apiLogger->setRequest(\Yii::$app->request);
                $apiLogger->setResponse(\Yii::$app->response);
                $apiLogger->export();
            },
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/urlRules.php',
        ],

        'user'                     => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],


        // Databases
        'db'                       => $db['dbMain'],
        'mongodb'                  => $db['mongodbMain'],
        'redis_pub_sub'            => $db['redisMainPubSub'],
        'redis_workers'            => $db['redisMainWorkers'],
        'redis_orders_active'      => $db['redisMainOrdersActive'],
        'redis_order_event'        => $db['redisMainOrderEvent'],
        'redis_main_check_status'  => $db['redisMainCheckStatus'],
        'redis_cache_check_status' => $db['redisCacheCheckStatus'],
        'redis_worker_shift'       => $db['redisMainWorkerShift'],
        'redis_worker_orders'      => $db['redisMainWorkerOrders'],

        'apiLogger' => logger\ApiLogger::class,

        'gearman' => [
            'class' => 'app\components\gearman\Gearman',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],

        'restCurl' => 'app\components\curl\RestCurl',

        'routeAnalyzer' => [
            'class' => 'app\components\TaxiRouteAnalyzer',
            'url'   => getenv('API_SERVICE_URL'),
        ],
        'geoService' => [
            'class' => 'app\components\GeoService',
            'url'   => getenv('API_GEO_URL'),
        ],
        'card'          => [
            'class' => 'app\components\Card',
            'url'   => getenv('API_PAYGATE_URL'),
            'path'  => getenv('API_PAYGATE_PATH'),
        ],

        'consulService'   => [
            'class'           => 'app\components\consul\ConsulService',
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ],
        'serviceExchange' => [
            'class'             => 'app\components\exchange\ServiceExchange',
            'baseUrl'           => getenv('API_EXCHANGE_URL'),
            'connectionTimeout' => getenv('API_EXCHANGE_CONNECTION_TIMEOUT'),
            'timeout'           => getenv('API_EXCHANGE_TIMEOUT'),
        ]
    ],
    'modules'    => [
        'v1'        => [
            'class' => 'app\modules\v1\Module',
        ],
        'promocode' => [
            'class' => 'app\modules\promocode\Module',
        ],
    ],

    'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];
}

return $config;
