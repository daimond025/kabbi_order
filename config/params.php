<?php

return [
    'service.engine.url'   => getenv('SERVICE_ENGINE_URL'),
    'saveOrder.retryCount' => 5,

    'paygate.url' => getenv('API_PAYGATE_URL'),

    'version'            => require __DIR__ . '/version.php',
    'supportedLanguages' => require __DIR__ . '/supportedLanguages.php',
];
